import numpy as np
from sklearn.metrics import precision_recall_fscore_support

def accuracy_score_sets(y_true, y_predicted, normalize=True):
    count_correct = 0
    for predicted_class, true_class in zip(y_predicted, y_true):
        if predicted_class != 'None' and predicted_class is not None and true_class is not None:
            if eval(predicted_class)[0] in true_class:
                count_correct += 1
        elif (predicted_class is None or predicted_class == 'None') and true_class is None:
            count_correct += 1

    if not normalize:
        return count_correct

    return count_correct / len(y_true)


def surprisal(y_true, y_pred, applicants, agent_frequencies_per_applicant, agent_encoding=None, task='multi'):
    y_true = np.array(y_true).ravel()
    y_pred = np.array(y_pred).ravel()
    applicants = np.array(applicants).ravel()
    correct_indices = np.where((y_true == y_pred))[0]
    applicants_correct = np.take(applicants, correct_indices)
    y_pred_correct = np.take(y_pred, correct_indices)
    if y_pred_correct.shape[0] == 0:
        return 0
    matrix = np.column_stack((applicants_correct.astype(np.object), y_pred_correct))
    surprisals = np.apply_along_axis(lambda row: _map_to_surprisal(row[0], row[1], agent_frequencies_per_applicant, agent_encoding, task),
                                     1,
                                     matrix)
    return np.mean(surprisals)


def high_surprisal_counts(y_true, y_pred, applicants, agent_frequencies_per_applicant,
                          agent_encoding=None, task='multi', threshold=4):
    y_true = np.array(y_true).ravel()
    y_pred = np.array(y_pred).ravel()
    applicants = np.array(applicants).ravel()
    correct_indices = np.where((y_true == y_pred))[0]
    applicants_correct = np.take(applicants, correct_indices)
    y_pred_correct = np.take(y_pred, correct_indices)
    if y_pred_correct.shape[0] == 0:
        return 0
    matrix = np.column_stack((applicants_correct.astype(np.object), y_pred_correct))
    surprisals = np.apply_along_axis(lambda row: _map_to_surprisal(row[0], row[1], agent_frequencies_per_applicant, agent_encoding, task),
                                     1,
                                     matrix)
    return len(list(filter(lambda x: x > threshold, surprisals))) / len(y_true)


def _map_to_surprisal(applicant, class_pred, agent_frequencies_per_applicant, agent_encoding=None, task='multi'):
    distribution = agent_frequencies_per_applicant.get(applicant, {})
    if agent_encoding is not None and task == 'multi':
        class_pred = agent_encoding[class_pred]
    elif agent_encoding is None and task == 'multi':
        raise RuntimeError

    return _single_surprisal(class_pred, distribution)


def _single_surprisal(x, distribution):
    probability_function = dict(distribution)
    # there are 4000 agents
    probability_x = probability_function[x] if x in probability_function else 1 / 1.4E4
    probability_x = 1 / 1.4E4 if probability_x < 1 / 1.4E4 else probability_x
    return np.log2(1 / probability_x)


def precision_recall_support_labels(y_true, y_pred):
    print('|--- ClassificationMetrics: {} expected unique classes, {} predicted unique classes'.format(
        len(np.unique(y_true)), len(np.unique(y_pred))))
    precision, recall, fscore, support = precision_recall_fscore_support(y_true, y_pred)
    precision_orig = precision
    predicted_labels_unique = np.unique(y_pred)
    labels = np.unique(list(y_pred) + list(y_true))
    labels = [list(labels).index(l) for l in predicted_labels_unique]
    precision = precision.take(labels)
    fscore = fscore.take(labels)
    recall = recall.take(labels)
    return precision, recall, support, predicted_labels_unique, precision_orig

import numpy as np
from sklearn.metrics import precision_recall_fscore_support


def surprisal(x, distribution):
    probability_function = dict(distribution)
    # there are 4000 agents
    probability_x = probability_function[x] if x in probability_function else 1 / 1.4E4
    return np.log2(1 / probability_x)


def surprisal_evaluation(y_true, y_pred, applicants, agent_frequencies_per_applicant, classes=None):
    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    applicants = np.array(applicants)
    correct_indices = np.where((y_true - y_pred) == 0)[0]
    applicants_correct = np.take(applicants, correct_indices)
    y_pred_correct = np.take(y_pred, correct_indices)
    matrix = np.column_stack((applicants_correct.astype(np.object), y_pred_correct))
    surprisals = np.apply_along_axis(lambda row: surprisal(row[1], agent_frequencies_per_applicant[row[0]]), 1, matrix)
    return np.mean(surprisals)


c0_history = [1, 1, 1, 1, 2]
c1_history = [2, 2, 2, 1, 1]
c2_history = [1, 1, 2, 3, 4]
c3_history = [2, 2, 1, 3, 3]

frequencies = {'c0': [(1, 0.8), (2, 0.2)],
               'c1': [(1, 0.4), (2, 0.6)],
               'c2': [(1, 0.4), (2, 0.2), (3, 0.2), (4, 0.2)],
               'c3': [(1, 0.2), (2, 0.4), (3, 0.4)]
               }

c0_expected = [1, 1, 2]
c1_expected = [2, 2, 1]
c2_expected = [4]
c3_expected = [3, 1]

c0_predicted_baseline = [1, 1, 1]
c1_predicted_baseline = [2, 2, 2]
c2_predicted_baseline = [1]
c3_predicted_baseline = [3, 3]

c0_predicted_alternative = [1, 2, 2]  # one mistake more than baseline but one rare agent correct
c1_predicted_alternative = [3, 2, 1]  # same number of mistakes but one rare agent correct
c2_predicted_alternative = [1]        # same as baseline
c3_predicted_alternative = [2, 3]     # same number of mistakes but one rare agent correct

trues = c0_expected + \
        c1_expected + \
        c2_expected + \
        c3_expected
baseline_predictions = c0_predicted_baseline + \
                       c1_predicted_baseline + \
                       c2_predicted_baseline + \
                       c3_predicted_baseline
alternative_predictions = c0_predicted_alternative + \
                          c1_predicted_alternative + \
                          c2_predicted_alternative + \
                          c3_predicted_alternative
applicants = ['c0']*len(c0_expected) + \
             ['c1']*len(c1_expected) + \
             ['c2']*len(c2_expected) + \
             ['c3']*len(c3_expected)

baseline_results = precision_recall_fscore_support(trues, baseline_predictions)
alternative_results = precision_recall_fscore_support(trues, alternative_predictions)

print('--------')
print('precision', baseline_results[0], np.mean(baseline_results[0]))
print('fscore', baseline_results[2], np.mean(baseline_results[2]))
suprisals_baseline = surprisal_evaluation(trues, baseline_predictions, applicants, frequencies, classes=[1,2,3,4])
print('surprisal', suprisals_baseline)

print('--------')
print('precision', alternative_results[0], np.mean(alternative_results[0]))
print('fscore', alternative_results[2], np.mean(alternative_results[2]))
surprisals_alternative = surprisal_evaluation(trues, alternative_predictions, applicants, frequencies, classes=[1,2,3,4])
print('surprisal', surprisals_alternative)

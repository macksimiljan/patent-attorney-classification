from pac.utils.database.connection_point import instantiate_engine
from enum import Enum
import numpy as np
import pandas as pd

from pac.utils.io.misc_files import write_applicant_categories


class ApplicantCategory(Enum):
    less_applications_less_agents = 0
    less_applications_many_agents = 1
    many_applications_less_agents = 2
    many_applications_many_agents = 3


mean_applications = 127.8
mean_unique_agents = 6


def map_to_category(count_applications, count_unique_agents):
    if count_applications <= mean_applications and count_unique_agents <= mean_unique_agents:
        return ApplicantCategory.less_applications_less_agents.value
    elif count_applications <= mean_applications and count_unique_agents > mean_unique_agents:
        return ApplicantCategory.less_applications_many_agents.value
    elif count_applications > mean_applications and count_unique_agents <= mean_unique_agents:
        return ApplicantCategory.many_applications_less_agents.value
    elif count_applications > mean_applications and count_unique_agents > mean_unique_agents:
        return ApplicantCategory.many_applications_many_agents.value
    else:
        raise RuntimeError


def map_applicant_data(data):
    data['categories'] = data.apply(lambda row: map_to_category(row['count_applications'], row['count_unique_agents']),
                                    axis=1)
    data = data.drop(['count_applications', 'count_unique_agents'],
                     axis=1)
    return data


def sql():
    return """
        SELECT t0.*, t1.count_applications
        FROM 
        (SELECT t.grouping_applicant as applicant_grouping, COUNT(DISTINCT t.grouping_agent) as count_unique_agents
            FROM view_triplets t INNER JOIN view_patents p ON (t.patent_id = p.id )
            WHERE t.grouping_applicant IS NOT NULL
            AND t.grouping_agent IS NOT NULL
            AND p.ep_application_date >= DATE('2012-01-01')
            AND p.ep_application_date < DATE('2017-01-01')
            GROUP BY t.grouping_applicant
        ) AS t0, 
        (SELECT t.grouping_applicant as applicant_grouping, COUNT(*) as count_applications
            FROM view_triplets t INNER JOIN view_patents p ON (t.patent_id = p.id )
            WHERE t.grouping_applicant IS NOT NULL
            AND p.ep_application_date >= DATE('2012-01-01')
            AND p.ep_application_date < DATE('2017-01-01')
            GROUP BY t.grouping_applicant
        ) AS t1
        WHERE t0.applicant_grouping = t1.applicant_grouping
    """


engine = instantiate_engine()

applicant_data = pd.read_sql_query(sql(), engine)
applicant_data = map_applicant_data(applicant_data)

print(applicant_data['categories'].value_counts())

write_applicant_categories(applicant_data)

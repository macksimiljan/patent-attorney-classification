from scipy.sparse import csr_matrix
from scipy.sparse import hstack
from sklearn.preprocessing import StandardScaler

from pac.preprocessing.agent_probabilities import agent_probabilities_per_applicant
from pac.preprocessing.encoding.boolean_feature_encoder import BooleanFeatureEncoder
from pac.preprocessing.encoding.categorical_feature_encoder import *
from pac.preprocessing.encoding.classes_encoder import ClassesEncoder
from pac.preprocessing.mapping.date_mapping import DateMapping
from pac.utils.database.connection_point import instantiate_engine
from pac.utils.io.feature_classes_point import read_split_training_dataset
from pac.utils.io.feature_classes_point import read_test_dataset


class PreprocessingPipeline:

    def __init__(self, settings):
        self._settings = settings

        self._classes_grouping = None
        self._classes_binary = None

        self.X_train = None
        self.X_test = None

        self.features = None
        self.applicants_of_test = None
        self.agent_encoding = None
        self.agent_probabilities = None
        self.gap_sizes = None

        self.y_train_multi = None
        self.y_test_multi = None
        self.y_train_binary = None
        self.y_test_binary = None

    def perform(self, only_classes=False):
        if not only_classes:
            print('   |--- PreprocessingPipeline: preprocessing features ...')
            self._preprocess_features()
        print('   |--- PreprocessingPipeline: preprocessing classes ...')
        self._preprocess_classes()

    def _preprocess_features(self):
        if self._settings.preprocess_with_hashing:
            self._preprocess_with_hash()
            return

        self.X_train, self.X_test = self._read_training_test()
        self.agent_probabilities = agent_probabilities_per_applicant(self.X_train)

        print('   |--- PreprocessingPipeline: encoding patent status ...')
        self.X_train, self.X_test = NominalFeatureEncoder(Constants.Training.Features.status_category,
                                                          Constants.Test.Features.status_category,
                                                          prefix='patent_status').encode(self.X_train, self.X_test)
        print('   |--- PreprocessingPipeline: encoding company types ...')
        encoder = BooleanFeatureEncoder()
        pairs = [(Constants.Training.Features.is_technology_company, Constants.Test.Features.is_technology_company),
                 (Constants.Training.Features.is_service_provider, Constants.Test.Features.is_service_provider),
                 (Constants.Training.Features.is_too, Constants.Test.Features.is_tto),
                 (Constants.Training.Features.is_research_organisation, Constants.Test.Features.is_research_organisation)]
        for pair in pairs:
            self.X_train, self.X_test = encoder.encode(pair[0],
                                                       pair[1],
                                                       self.X_train,
                                                       self.X_test)

        print('   |--- PreprocessingPipeline: initializing encoders ...')
        applicant_encoder = ApplicantGroupingEncoder()
        cities_encoder = CityEncoder()
        countries_encoder = CountryEncoder()
        ipc_encoder = IpcClassEncoder()
        date_mapping = DateMapping()
        scaler_date = StandardScaler()
        X = self.X_train

        print('   |--- PreprocessingPipeline: encoding dates (training) ...')
        encoded_dates = date_mapping.map_training(X)
        encoded_dates = scaler_date.fit_transform(encoded_dates.values.reshape(-1, 1))
        print('   |--- PreprocessingPipeline: encoding applicant groupings (training) ...')
        applicant_groupings = X[Constants.Training.Features.grouping].values
        encoded_applicants = applicant_encoder.encode(applicant_groupings)
        print('   |--- PreprocessingPipeline: encoding cities (training) ...')
        applicant_cities = X[Constants.Training.Features.cities].values
        encoded_cities = cities_encoder.encode(applicant_cities)
        print('   |--- PreprocessingPipeline: encoding countries (training) ...')
        applicant_countries = X[Constants.Training.Features.countries].values
        encoded_countries = countries_encoder.encode(applicant_countries)
        print('   |--- PreprocessingPipeline: encoding ipc classes (training) ...')
        applicant_ipc = X[Constants.Training.Features.second_level_classes].values
        encoded_ipc_classes = ipc_encoder.encode(applicant_ipc)

        to_be_dropped = ['agent_grouping_list',
                         'agent_old_vs_new',
                         'applicant_lat_lon_pairs_list',
                         'patent_first_level_classes_list']
        to_be_dropped += [Constants.Training.Features.grouping,
                          Constants.Training.Features.cities,
                          Constants.Training.Features.date,
                          Constants.Training.Features.countries,
                          Constants.Training.Features.second_level_classes]
        X = X.drop(to_be_dropped, axis=1)
        columns = X.columns.tolist()\
                  + applicant_encoder.groupings\
                  + cities_encoder.cities\
                  + ['application_date']\
                  + countries_encoder.countries\
                  + ipc_encoder.ipc_classes
        X = hstack([csr_matrix(X),
                    encoded_applicants,
                    encoded_cities,
                    encoded_dates,
                    encoded_countries,
                    encoded_ipc_classes], format='csr')
        print('   |--- PreprocessingPipeline: X_train_sparse.shape = {}'.format(X.shape))
        self.X_train = X
        self.features = np.array(columns)

        X = self.X_test
        self.gap_sizes = self.X_test['missing_applicants_from_sequence_count'].values
        print('   |--- PreprocessingPipeline: encoding dates (test) ...')
        encoded_dates = date_mapping.map_test(X)
        encoded_dates = scaler_date.transform(encoded_dates.values.reshape(-1, 1))
        print('   |--- PreprocessingPipeline: encoding applicant groupings (test) ...')
        applicant_groupings = X[Constants.Test.Features.grouping].values
        encoded_applicants = applicant_encoder.encode(applicant_groupings)
        print('   |--- PreprocessingPipeline: encoding cities (test) ...')
        applicant_cities = X[Constants.Test.Features.cities].values
        encoded_cities = cities_encoder.encode(applicant_cities)
        print('   |--- PreprocessingPipeline: encoding countries (test) ...')
        applicant_countries = X[Constants.Test.Features.countries].values
        encoded_countries = countries_encoder.encode(applicant_countries)
        print('   |--- PreprocessingPipeline: encoding ipc classes (test) ...')
        applicant_ipc = X[Constants.Test.Features.second_level_classes].values
        encoded_ipc_classes = ipc_encoder.encode(applicant_ipc)

        to_be_dropped = ['agent_grouping',
                         'agent_old_vs_new',
                         'applicant_lat_lon_pairs',
                         'missing_applicants_from_sequence_count',
                         'patent_first_level_classes',
                         'patent_id',
                         'previous_agents']
        to_be_dropped += [Constants.Test.Features.grouping,
                          Constants.Test.Features.cities,
                          Constants.Test.Features.date,
                          Constants.Test.Features.countries,
                          Constants.Test.Features.second_level_classes]
        X = X.drop(to_be_dropped, axis=1)
        X = hstack([csr_matrix(X),
                    encoded_applicants,
                    encoded_cities,
                    encoded_dates,
                    encoded_countries,
                    encoded_ipc_classes], format='csr')
        print('   |--- PreprocessingPipeline: X_test_sparse.shape = {}'.format(X.shape))
        self.X_test = X

    def _preprocess_with_hash(self):
        print('   |--- PreprocessingPipeline: preprocessing with hash ...')
        self.X_train, self.X_test = self._read_training_test()
        self.agent_probabilities = agent_probabilities_per_applicant(self.X_train)

        print('   |--- PreprocessingPipeline: encoding patent status ...')
        self.X_train, self.X_test = NominalFeatureEncoder(Constants.Training.Features.status_category,
                                                          Constants.Test.Features.status_category,
                                                          prefix='patent_status').encode(self.X_train, self.X_test)

        print('   |--- PreprocessingPipeline: initializing encoders ...')
        applicant_encoder = ApplicantGroupingEncoderWithHash()
        cities_encoder = CityEncoderWithHash()
        countries_encoder = CountryEncoderWithHash()
        ipc_encoder = IpcClassEncoderWithHash()
        company_type_encoder = CompanyTypeEncoder()
        date_mapping = DateMapping()
        scaler_date = StandardScaler()
        X = self.X_train

        print('   |--- PreprocessingPipeline: encoding dates (training) ...')
        encoded_dates = date_mapping.map_training(X)
        encoded_dates = scaler_date.fit_transform(encoded_dates.values.reshape(-1, 1))
        print('   |--- PreprocessingPipeline: encoding applicant groupings (training) ...')
        applicant_groupings = X[Constants.Training.Features.grouping].values
        encoded_applicants = applicant_encoder.encode(applicant_groupings)
        print('   |--- PreprocessingPipeline: encoding company types ...')
        research_oganisations = X[Constants.Training.Features.is_research_organisation]
        tech_comps = X[Constants.Training.Features.is_technology_company]
        ttos = X[Constants.Training.Features.is_too]
        serivce_provides = X[Constants.Training.Features.is_service_provider]
        encoded_company_types = company_type_encoder.encode(research_oganisations, tech_comps, ttos, serivce_provides)
        print('   |--- PreprocessingPipeline: encoding cities (training) ...')
        applicant_cities = X[Constants.Training.Features.cities].values
        encoded_cities1, encoded_cities2 = cities_encoder.encode(applicant_cities)
        print('   |--- PreprocessingPipeline: encoding countries (training) ...')
        applicant_countries = X[Constants.Training.Features.countries].values
        encoded_countries1, encoded_countries2 = countries_encoder.encode(applicant_countries)
        print('   |--- PreprocessingPipeline: encoding ipc classes (training) ...')
        applicant_ipc = X[Constants.Training.Features.second_level_classes].values
        encoded_ipc_classes1, encoded_ipc_classes2 = ipc_encoder.encode(applicant_ipc)

        to_be_dropped = ['agent_grouping_list',
                         'agent_old_vs_new',
                         'applicant_lat_lon_pairs_list',
                         'applicant_cooccurrences',
                         'patent_first_level_classes_list']
        to_be_dropped += [Constants.Training.Features.is_research_organisation,
                          Constants.Training.Features.is_technology_company,
                          Constants.Training.Features.is_too,
                          Constants.Training.Features.is_service_provider,
                          Constants.Training.Features.grouping,
                          Constants.Training.Features.cities,
                          Constants.Training.Features.date,
                          Constants.Training.Features.countries,
                          Constants.Training.Features.second_level_classes]
        X = X.drop(to_be_dropped, axis=1)
        columns = X.columns.tolist()\
                  + applicant_encoder.applicant_groupings\
                  + company_type_encoder.company_types\
                  + cities_encoder.cities\
                  + ['application_date']\
                  + countries_encoder.countries\
                  + ipc_encoder.ipc_classes
        X = hstack([csr_matrix(X),
                    encoded_applicants,
                    encoded_company_types,
                    encoded_cities1,
                    encoded_cities2,
                    encoded_dates,
                    encoded_countries1,
                    encoded_countries2,
                    encoded_ipc_classes1,
                    encoded_ipc_classes2], format='csr')
        print('   |--- PreprocessingPipeline: X_train_sparse.shape = {}'.format(X.shape))
        self.X_train = X
        self.features = np.array(columns)

        X = self.X_test
        self.gap_sizes = self.X_test['missing_applicants_from_sequence_count'].values
        print('   |--- PreprocessingPipeline: encoding dates (test) ...')
        encoded_dates = date_mapping.map_test(X)
        encoded_dates = scaler_date.transform(encoded_dates.values.reshape(-1, 1))
        print('   |--- PreprocessingPipeline: encoding applicant groupings (test) ...')
        applicant_groupings = X[Constants.Test.Features.grouping].values
        encoded_applicants = applicant_encoder.encode(applicant_groupings)
        print('   |--- PreprocessingPipeline: encoding company types ...')
        research_oganisations = X[Constants.Test.Features.is_research_organisation]
        tech_comps = X[Constants.Test.Features.is_technology_company]
        ttos = X[Constants.Test.Features.is_tto]
        serivce_provides = X[Constants.Test.Features.is_service_provider]
        encoded_company_types = company_type_encoder.encode(research_oganisations, tech_comps, ttos, serivce_provides)
        print('   |--- PreprocessingPipeline: encoding cities (test) ...')
        applicant_cities = X[Constants.Test.Features.cities].values
        encoded_cities1, encoded_cities2 = cities_encoder.encode(applicant_cities)
        print('   |--- PreprocessingPipeline: encoding countries (test) ...')
        applicant_countries = X[Constants.Test.Features.countries].values
        encoded_countries1, encoded_countries2 = countries_encoder.encode(applicant_countries)
        print('   |--- PreprocessingPipeline: encoding ipc classes (test) ...')
        applicant_ipc = X[Constants.Test.Features.second_level_classes].values
        encoded_ipc_classes1, encoded_ipc_classes2 = ipc_encoder.encode(applicant_ipc)

        to_be_dropped = ['agent_grouping',
                         'agent_old_vs_new',
                         'applicant_lat_lon_pairs',
                         'missing_applicants_from_sequence_count',
                         'patent_first_level_classes',
                         'patent_id',
                         'previous_agents']
        to_be_dropped += [Constants.Test.Features.grouping,
                          Constants.Test.Features.is_research_organisation,
                          Constants.Test.Features.is_technology_company,
                          Constants.Test.Features.is_tto,
                          Constants.Test.Features.is_service_provider,
                          Constants.Test.Features.cities,
                          Constants.Test.Features.date,
                          Constants.Test.Features.countries,
                          Constants.Test.Features.second_level_classes]
        X = X.drop(to_be_dropped, axis=1)
        X = hstack([csr_matrix(X),
                    encoded_applicants,
                    encoded_company_types,
                    encoded_cities1,
                    encoded_cities2,
                    encoded_dates,
                    encoded_countries1,
                    encoded_countries2,
                    encoded_ipc_classes1,
                    encoded_ipc_classes2], format='csr')
        print('   |--- PreprocessingPipeline: X_test_sparse.shape = {}'.format(X.shape))
        self.X_test = X

    def _preprocess_classes(self):
        # encode multi classes
        training, test = self._classes_grouping['training'], self._classes_grouping['test']
        encoder = ClassesEncoder()
        self.y_train_multi, self.y_test_multi = encoder.encode(training, test)
        print('   |--- PreprocessingPipeline: y_train_multi.shape = {}, y_test_multi.shape = {}'.format(
            self.y_train_multi.shape,
            self.y_test_multi.shape))
        self.agent_encoding = encoder.internal_encoder().classes_

        # encode binary classes
        training, test = self._classes_binary['training'], self._classes_binary['test']
        self.y_train_binary, self.y_test_binary = ClassesEncoder().encode(training, test)
        print('   |--- PreprocessingPipeline: y_train_binary.shape = {}, y_test_binary.shape = {}'.format(
            self.y_train_binary.shape,
            self.y_test_binary.shape))

    def _read_training_test(self):
        print('   |--- PreprocessingPipeline: reading raw data')
        X_train = read_split_training_dataset(self._settings.training_interval[0], self._settings.training_interval[1])
        X_test = read_test_dataset(self._settings.test_year, gap_size=self._settings.gap_size)

        X_train.dropna(inplace=True, subset=['applicant_grouping'])
        X_test.dropna(inplace=True, subset=['applicant_grouping'])
        if self._settings.filter_active:
            allowed_applicants = []
            if self._settings.filter_by_germany:
                # applicants with headquarter in Germany
                allowed_applicants = pd.read_sql_query("""SELECT grouping_applicant
                                                          FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id),
                                                                                                  locations l, cities c
                                                          WHERE t.grouping_agent IS NOT NULL
                                                          AND t.grouping_applicant IS NOT NULL
                                                          AND p.ep_application_date >= DATE('2012-01-01')
                                                          AND l.company_id = t.applicant_id
                                                          AND l.category_location_id = 1
                                                          AND l.city_id = c.id
                                                          AND c.id IN
                                                              (
                                                              SELECT id
                                                              FROM cities
                                                              WHERE country = 'Germany'
                                                              );""", instantiate_engine()).values.reshape(-1)
            elif self._settings.filter_by_united_states:
                allowed_applicants = pd.read_sql_query("""SELECT grouping_applicant
                                                                          FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id),
                                                                                                                  locations l, cities c
                                                                          WHERE t.grouping_agent IS NOT NULL
                                                                          AND t.grouping_applicant IS NOT NULL
                                                                          AND p.ep_application_date >= DATE('2012-01-01')
                                                                          AND l.company_id = t.applicant_id
                                                                          AND l.category_location_id = 1
                                                                          AND l.city_id = c.id
                                                                          AND c.id IN
                                                                              (
                                                                              SELECT id
                                                                              FROM cities
                                                                              WHERE country = 'United States'
                                                                              );""",
                                                       instantiate_engine()).values.reshape(-1)
            elif self._settings.filter_by_united_kingdom:
                # applicants with headquarter in United Kingdom
                allowed_applicants = pd.read_sql_query("""SELECT grouping_applicant
                                                          FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id),
                                                                                                  locations l, cities c
                                                          WHERE t.grouping_agent IS NOT NULL
                                                          AND t.grouping_applicant IS NOT NULL
                                                          AND p.ep_application_date >= DATE('2012-01-01')
                                                          AND l.company_id = t.applicant_id
                                                          AND l.category_location_id = 1
                                                          AND l.city_id = c.id
                                                          AND c.id IN
                                                              (
                                                              SELECT id
                                                              FROM cities
                                                              WHERE country = 'United Kingdom'
                                                              );""", instantiate_engine()).values.reshape(-1)
            elif self._settings.filter_by_procter:
                allowed_applicants = ['g536']
            elif self._settings.filter_by_idf_innov:
                allowed_applicants = ['g7570']
            elif self._settings.filter_by_fraunhofer:
                allowed_applicants = ['g392']
            elif self._settings.filter_by_arithmetic:
                allowed_applicants = ["g3481", "g1617", "g5397", "g1322", "g675", "g3224", "g1415", "g5900",
                                             "g965", "g1657", "g1515", "g1642", "g1615", "g1397", "g4140", "g1639",
                                             "g2549", "g1327", "g517", "g1085", "g3853", "g2878", "g1441", "g1582",
                                             "g1783", "g1405", "g1695", "g980", "g3557", "g3103", "g1823", "g1094",
                                             "g1544", "g1558", "g1342", "g1648", "g1413", "g1836", "g4010", "g646",
                                             "g1806", "g7009", "g1408", "g1248", "g3825", "g2007", "g512", "g3655",
                                             "g6202", "g1369", "g823", "g7780", "g3442", "g3840", "g2723", "g1467",
                                             "g1637", "g3826", "g1435", "g1731", "g1490", "g1523", "g1799", "g1450",
                                             "g3502", "g3862", "g3646", "g1143", "g655", "g1101", "g1457", "g166",
                                             "g6761", "g1524", "g1613", "g611", "g790", "g1046", "g1538", "g1367",
                                             "g1246", "g1512", "g2870", "g1383", "g624", "g673", "g1785", "g1608",
                                             "g1428", "g1722", "g2996", "g1509", "g1379", "g6370", "g1042", "g3108",
                                             "g1629", "g1607", "g3822", "g1592", "g835", "g5734", "g6371", "g828",
                                             "g1739", "g1144", "g679", "g1526", "g1334", "g1763", "g6178", "g1666",
                                             "g1106", "g3188", "g3232", "g6184", "g710", "g1720", "g1668", "g3645",
                                             "g3617", "g397"]
            elif self._settings.filter_by_median:
                allowed_applicants = ["g3428", "g5600", "g3590", "g3864", "g1176", "g1264", "g394", "g486",
                                         "g893", "g5427", "g1049", "g6587", "g5878", "g2477", "g8629", "g2969",
                                         "g5307", "g8827", "g8881", "g7758", "g3819", "g1027", "g7809", "g4262",
                                         "g3361", "g3554", "g7855", "g3570", "g3888", "g6445", "g3883", "g534",
                                         "g793", "g358", "g8425", "g2429", "g3615", "g375", "g3801", "g541",
                                         "g2168", "g6229", "g2213", "g5418", "g3686", "g3805", "g3745", "g1183",
                                         "g1234", "g3775", "g3751", "g8468", "g3813", "g730", "g3946", "g2464",
                                         "g6441", "g6561", "g357", "g8411", "g5463", "g6653", "g3812", "g3973",
                                         "g5310", "g4184", "g3972", "g373", "g22", "g2578", "g678", "g363", "g8348",
                                         "g2710", "g3289", "g698", "g1199", "g5426", "g377", "g2890", "g5198",
                                         "g3545", "g2582", "g7721", "g3670", "g8021", "g5211", "g6255", "g3698",
                                         "g8020", "g1678"]
            elif not self._settings.filter_by_more_than_one_agent:
                raise RuntimeError

            if self._settings.filter_by_more_than_one_agent:
                more_than_one_agent = pd.read_sql_query("""SELECT t.grouping_applicant
                                                            FROM view_triplets t INNER JOIN epo_patents p ON t.patent_id = p.id
                                                            WHERE p.ep_application_date >= DATE('2012-01-01')
                                                            AND p.ep_application_date < DATE('2018-01-01') 
                                                            AND t.grouping_agent IS NOT NULL
                                                            AND t.grouping_applicant IS NOT NULL
                                                            EXCEPT
                                                            SELECT t0.grouping_applicant
                                                            FROM (
                                                                SELECT t.grouping_applicant, COUNT(DISTINCT t.grouping_agent) as no_unique_agents
                                                                FROM view_triplets t INNER JOIN epo_patents p ON t.patent_id = p.id
                                                                WHERE p.ep_application_date >= DATE('2012-01-01')
                                                                AND p.ep_application_date < DATE('2018-01-01') 
                                                                AND t.grouping_agent IS NOT NULL
                                                                AND t.grouping_applicant IS NOT NULL
                                                                GROUP BY t.grouping_applicant
                                                                HAVING COUNT(DISTINCT t.grouping_agent) = 1
                                                                ORDER BY COUNT(DISTINCT t.grouping_agent)
                                                            ) AS t0
                                                            ;""", instantiate_engine()).values.reshape(-1)
                if len(allowed_applicants) > 0:
                    allowed_applicants = list(set(allowed_applicants).intersection(more_than_one_agent))
                else:
                    allowed_applicants = more_than_one_agent

            X_train = X_train[X_train.applicant_grouping.isin(allowed_applicants)]
            X_test = X_test[X_test.applicant_grouping.isin(allowed_applicants)]
            X_train = X_train.reset_index(drop=True)
            X_test = X_test.reset_index(drop=True)

        if self._settings.under_sampling:
            ones = X_train.loc[X_train[Constants.Training.Classes.old_vs_new] == 1]
            zeros = X_train.loc[X_train[Constants.Training.Classes.old_vs_new] == 0]
            no_zeros = min(zeros.shape[0], 3 * ones.shape[0])
            zeros = zeros.sample(n=no_zeros)
            X_train = pd.concat([ones, zeros], ignore_index=True)
            X_train = X_train.sample(frac=1)
            X_train.reset_index(drop=True, inplace=True)

        self.applicants_of_test = X_test.applicant_grouping.values

        self._classes_grouping = {'training': X_train[Constants.Training.Classes.groupings],
                                  'test': X_test[Constants.Test.Classes.grouping]}
        self._classes_binary = {'training': X_train[Constants.Training.Classes.old_vs_new],
                                'test': X_test[Constants.Test.Classes.old_vs_new]}

        return X_train, X_test


class Settings:

    def __init__(self):
        self.training_features = None
        self.test_features = None

        self.training_interval = [2012, 2016]
        self.test_year = 2017

        self.gap_size = None

        self.preprocess_with_hashing = False

        self.filter_active = False
        self.filter_by_germany = False
        self.filter_by_united_states = False
        self.filter_by_united_kingdom = False
        self.filter_by_procter = False
        self.filter_by_idf_innov = False
        self.filter_by_fraunhofer = False
        self.filter_by_arithmetic = False
        self.filter_by_median = False
        self.filter_by_more_than_one_agent = False
        self.under_sampling = False

    def set_training_features(self, val):
        self.training_features = val
        return self

    def set_test_features(self, val):
        self.test_features = val
        return self

    def set_training_interval(self, val):
        self.training_interval = val
        return self

    def set_test_year(self, val):
        self.test_year = val
        return self

    def set_gap_size(self, val):
        self.gap_size = val
        return self

    def set_preprocess_with_hash(self, val):
        self.preprocess_with_hashing = val
        return self

    def set_filter(self, val):
        if val == 'germany':
            self.filter_by_germany = True
            self.filter_active = True
        elif val == 'united states':
            self.filter_by_united_states = True
            self.filter_active = True
        elif val == 'united_kingdom':
            self.filter_by_united_kingdom = True
            self.filter_active = True
        elif val == 'procter':
            self.filter_by_procter = True
            self.filter_active = True
        elif val == 'idf innov':
            self.filter_by_idf_innov = True
            self.filter_active = True
        elif val == 'fraunhofer':
            self.filter_by_fraunhofer = True
            self.filter_active = True
        elif val == 'arithmetic':
            self.filter_by_arithmetic = True
            self.filter_active = True
        elif val == 'median':
            self.filter_by_median = True
            self.filter_active = True
        elif val == 'more_than_1_agent':
            self.filter_by_more_than_one_agent = True
            self.filter_active = True
        elif not val:
            self.filter_active = False
        else:
            raise RuntimeError

        return self

    def set_under_sampling(self, val):
        self.under_sampling = val
        return self

from collections import Counter

from pac import Constants


def _to_probability_function(agent_list):
    c = Counter(agent_list)
    agent_count = len(agent_list)
    prob_function = list(map(lambda pair: (pair[0], pair[1] / agent_count), c.items()))
    new_agent_prob = len(c) / agent_count
    old_agent_prob = 1 - new_agent_prob
    prob_function.extend([(0, old_agent_prob), (1, new_agent_prob)])
    return dict(prob_function)


def agent_probabilities_per_applicant(training_dataset):
    applicants = training_dataset[Constants.Training.Features.grouping].copy()
    agent_grouping_list = training_dataset[Constants.Training.Classes.groupings].copy()
    # agent_grouping_list = agent_grouping_list.apply(_to_probability_function)
    probabilities = {}
    for applicant, agent in zip(applicants, agent_grouping_list):
        agents_per_applicant = probabilities.get(applicant, [])
        agents_per_applicant.append(agent)
        probabilities[applicant] = agents_per_applicant
    probabilities = list(map(lambda e: (e[0], _to_probability_function(e[1])), probabilities.items()))
    return probabilities

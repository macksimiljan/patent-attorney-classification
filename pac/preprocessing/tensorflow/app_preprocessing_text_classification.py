import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
from sklearn.preprocessing import LabelEncoder
from tensorflow import keras
from sklearn.metrics import accuracy_score
from pac.evaluation.classification_metrics import precision_recall_support_labels
from pac.utils.database.connection_point import instantiate_engine
from pac.utils.io.feature_classes_point import read_split_training_dataset
from pac.utils.io.feature_classes_point import read_test_dataset
from pac.utils.io.final_train_test import read_gap_sizes
from sklearn.utils import class_weight

from imblearn.over_sampling import SMOTE
from collections import Counter

from random import shuffle


default_colors = sns.diverging_palette(220, 20, n=5)
sns.set_palette(default_colors)
sns.set_style("ticks")


max_text_length = 23
number_of_epochs = 30
batch_size = 512
task = 'multi'
maximum_gap_size = 'a'
class_evidence = 1
training_years = (2016, None)
test_year = 2017


def build_texts_and_labels_and_vocabulary(dataset, task='multi'):
    if task != 'multi' and task != 'binary':
        raise RuntimeError

    texts = []
    labels = []
    vocabulary, i = {}, 2
    for row in dataset.itertuples():
        labels.append(row[1]) if task == 'multi' else labels.append(row[2])
        words = []
        for city_list in row[3]:
            if city_list[0] is None:
                continue
            words.extend(city_list)
        words.extend(row[4])
        for country_list in row[5]:
            if country_list[0] is None:
                continue
            words.extend(country_list)
        if row[7][0]:
            words.append('research organisation')
        if row[8][0]:
            words.append('service provider')
        if row[9][0]:
            words.append('technology company')
        if row[10][0]:
            words.append('tto')
        words.append(row[12][0:4])
        if row[14][0] is not None:
            words.extend(row[14])
        if row[15][0] is not None:
            words.append(row[15])
        texts.append(words)

        for word in words:
            if word not in vocabulary:
                vocabulary[word] = i
                i += 1
    return texts, labels, vocabulary


def map_texts_and_extract_labels(dataset, vocabulary, task='multi'):
    if task != 'multi' and task != 'binary':
        raise RuntimeError

    texts = []
    labels = []
    for row in dataset.itertuples():
        labels.append(row[1]) if task == 'multi' else labels.append(row[2])
        words = []
        for city_list in row[3]:
            if city_list[0] is None:
                continue
            words.extend(city_list)
        words.append(row[5])
        for country_list in row[4]:
            if country_list[0] is None:
                continue
            words.extend(country_list)
        if row[6][0]:
            words.append('research organisation')
        if row[7][0]:
            words.append('service provider')
        if row[8][0]:
            words.append('technology company')
        if row[9][0]:
            words.append('tto')
        if row[15][0] is not None:
            words.extend(row[15])
        if row[16][0] is not None:
            words.append(row[16])

        words_mapped_to_integer = []
        for word in words:
            if word in vocabulary:
                words_mapped_to_integer.append(vocabulary[word])
            else:
                words_mapped_to_integer.append(vocabulary['<UNKNOWN>'])
        texts.append(words_mapped_to_integer)
    return texts, labels


def map_words_to_integers(texts, vocabulary):
    mapped_texts = []
    for words in texts:
        mapped_texts.append(list(map(
            lambda w: vocabulary[w] if w in vocabulary else vocabulary['UNKNOWN'],
            words)))
    return mapped_texts


dataset = read_split_training_dataset(training_years[0], training_years[1])
# dataset = dataset.sample(frac=1).reset_index(drop=True)
# allowed_applicants = []
# dataset = dataset[dataset.applicant_grouping.isin(allowed_applicants)]
# dataset = dataset.reset_index(drop=True)

texts_train, labels_train, vocabulary = build_texts_and_labels_and_vocabulary(dataset, task=task)
vocabulary['<UNKNOWN>'] = 1
vocabulary['<PAD>'] = 0
print('vocabulary size:', len(vocabulary))

# threshold = sum(labels_train)
# no0, no1 = 0, 0
# texts_train_resampled = []
# labels_train_resampled = []
# for i, label in enumerate(labels_train):
#     if label == 0 and no0 < 2*threshold:
#         texts_train_resampled.append(texts_train[i])
#         labels_train_resampled.append(label)
#         no0 += 1
#     elif label == 1 and no1 < threshold:
#         texts_train_resampled.append(texts_train[i])
#         labels_train_resampled.append(label)
#         no1 += 1
#     if no0 >= 2*threshold and no1 >= threshold:
#         break
# texts_train = texts_train_resampled
# labels_train = labels_train_resampled

dataset = read_test_dataset(test_year)
# gap_sizes = read_gap_sizes(training_years, test_year)
# mask = (dataset['missing_applicants_from_sequence_count'] <= maximum_gap_size)
# dataset = dataset[mask]
# dataset = dataset[dataset.applicant_grouping.isin(allowed_applicants)]
# dataset = dataset.reset_index(drop=True)
x_test, labels_test = map_texts_and_extract_labels(dataset, vocabulary, task=task)
dataset = None

# allowed_agents = list(map(
#         lambda x: x[0],
#         list(filter(
#             lambda x: x[1] >= class_evidence,
#             list(Counter(labels_train).items())))))
# texts_train_mod = []
# labels_train_mod = []
# texts_test_mod = []
# labels_test_mod = []
# for t, l in zip(texts_train, labels_train):
#     if l in allowed_agents:
#         texts_train_mod.append(t)
#         labels_train_mod.append(l)
# for t, l in zip(x_test, labels_test):
#     if l in allowed_agents:
#         texts_test_mod.append(t)
#         labels_test_mod.append(l)
# texts_train = texts_train_mod
# labels_train = labels_train_mod
# x_test = texts_test_mod
# labels_test = labels_test_mod

x_train = map_words_to_integers(texts_train, vocabulary)
label_encoder = LabelEncoder()
label_encoder.fit(labels_train + labels_test)
number_of_classes = len(label_encoder.classes_)
y_train = label_encoder.transform(labels_train)
y_test = label_encoder.transform(labels_test)
y_train = keras.utils.to_categorical(y_train, num_classes=number_of_classes)
y_test = keras.utils.to_categorical(y_test, num_classes=number_of_classes)
print('# labels:', number_of_classes)
print('# training:', y_train.shape[0])
print('# test:', y_test.shape[0])


train_text_sizes = list(map(lambda x: len(x), x_train))
print('maximum text size:', np.max(train_text_sizes),
      'avg text size:', np.mean(train_text_sizes),
      'maximum text length set to', max_text_length)
x_train = keras.preprocessing.sequence.pad_sequences(x_train,
                                                     value=vocabulary['<PAD>'],
                                                     padding='post',
                                                     maxlen=max_text_length)
x_test = keras.preprocessing.sequence.pad_sequences(x_test,
                                                     value=vocabulary['<PAD>'],
                                                     padding='post',
                                                     maxlen=max_text_length)


model = keras.Sequential()
model.add(keras.layers.Embedding(len(vocabulary), 128))
model.add(keras.layers.GlobalAveragePooling1D())
model.add(keras.layers.Dense(64, activation=tf.tanh))
model.add(keras.layers.Dense(number_of_classes, activation='softmax'))


model.summary()

loss_function = 'categorical_crossentropy'
model.compile(loss=loss_function,
              optimizer='adam',
              metrics=['accuracy'])

# class_weights = [0.5, 1000]
history = model.fit(x_train,
                    y_train,
                    epochs=number_of_epochs,
                    batch_size=batch_size,
                    validation_data=(x_test, y_test),
                    verbose=2)

probs_matrix = model.predict(x_test)
y_true = np.argmax(y_test, axis=1)


def most_likely_agents(probs_matrix, threshold=3):
    result = []
    highest_probs_list = []
    for probs in probs_matrix:
        probs = list(probs)
        highest_probs = list(sorted(list(enumerate(probs)), key=lambda p: -p[1]))[0:threshold]
        highest_probs_list.append(highest_probs)
        classes = list(map(
            lambda p: p[0],
            highest_probs))
        result.append(classes)
    print('\n{}\n'.format(highest_probs_list[0 : 20]))
    return result, highest_probs_list


def threshold_eval(top_agents_matrix, y_true):
    print('\n{}\n'.format(y_true[0 : 20]))
    correct = 0
    for top_agents, y in zip(top_agents_matrix, y_true):
        if y in top_agents:
            correct += 1
    return correct / y_true.shape[0]


def probs_of_correct_and_false_classes(probs_matrix, y_true, y_pred):
    max_probs = probs_matrix.max(axis=1)
    true_probs = []
    false_probs = []
    for y1, y2, prob in zip(y_true, y_pred, max_probs):
        true_probs.append(prob) if y1 == y2 else false_probs.append(prob)

    plt.boxplot([true_probs, false_probs], whis='range', labels=['correct', 'false'])
    plt.tight_layout()
    plt.title('Posterior Probabilities')
    plt.savefig('box_posterior.eps')
    plt.show()
    return [np.mean(true_probs), np.std(true_probs), np.median(true_probs)], \
           [np.mean(false_probs), np.std(false_probs), np.median(false_probs)]


kappa = 3
result, kappa_predictions = most_likely_agents(probs_matrix, threshold=kappa)
accuracy_kappa = threshold_eval(result, y_true)
print('accuracy kappa={}: {:8.4f}'.format(kappa, accuracy_kappa))
y_pred = np.argmax(probs_matrix, axis=1)
precision, recall, _, labels, _ = precision_recall_support_labels(y_true, y_pred)
print('precision:', np.mean(precision), np.std(precision))
print('accuracy:', accuracy_score(y_true, y_pred))
print('average probs true/ false: ', probs_of_correct_and_false_classes(probs_matrix, y_true, y_pred))

h = {}
with open('labels_mapping_new2old.csv', 'w') as f:
    for old, new in zip(labels_test, y_true):
        h[new] = old
    for old, new in zip(labels_train, np.argmax(y_train, axis=1)):
        h[new] = old
    for key in h.keys():
        f.write("{},{}\n".format(key, h[key]))
with open('precisions.csv', 'w') as f:
    for l, p in zip(labels, precision):
        f.write('{},{},{}\n'.format(l, h[l], p))
with open('kappa_predictions.csv', 'w') as f:
    for kappa_prediction, actual_agent in zip(kappa_predictions, y_true):
        pred0 = kappa_prediction[0]
        pred1 = kappa_prediction[1]
        pred2 = kappa_prediction[2]
        f.write('({},{},{},{}), ({},{},{},{}), ({},{},{},{})\n'.format(
            pred0[0], h[pred0[0]], pred0[1], actual_agent == pred0[0],
            pred1[0], h[pred1[0]], pred1[1], actual_agent == pred1[0],
            pred2[0], h[pred2[0]], pred2[1], actual_agent == pred2[0]
        ))

acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(1, len(acc) + 1)

plt.figure(figsize=(5,4))
plt.plot(epochs, loss, label='Training loss', linestyle='--')
plt.plot(epochs, val_loss, label='Validation loss')
plt.xlabel('Epochs')
# plt.xticks([1, 5, 10, 15, 20, 25, 30, 35, 40, 45])
plt.ylabel('Loss')
plt.yticks([0, 2, 4, 6])
plt.ylim(ymax=6.5)
plt.legend()
plt.tight_layout()
plt.savefig('loss.eps')
plt.show()

plt.figure(figsize=(5,4))
plt.plot(epochs, acc, label='Training Accuracy', linestyle='--')
plt.plot(epochs, val_acc, label='Validation Accuracy')
plt.xlabel('Epochs')
# plt.xticks()
plt.ylabel('Accuracy')
plt.yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
plt.ylim(ymax=1.1)
plt.legend()
plt.tight_layout()
plt.savefig('accuracy.eps')
plt.show()

plt.figure(figsize=(6,4))
plt.scatter(precision, recall, alpha=0.6)
mean_p = np.mean(precision)
mean_r = np.mean(recall)
plt.plot([mean_p, mean_p], [-0.1, 1.1], linestyle='--', c=sns.color_palette()[-2], label='Average')
plt.plot([-0.1, 1.1], [mean_r, mean_r], linestyle='--', c=sns.color_palette()[-2])
plt.xlabel('Precision')
plt.xlim(xmin=-0.05, xmax=1.05)
plt.ylim(ymin=-0.05, ymax=1.05)
plt.ylabel('Recall')
plt.legend(loc='upper left')
plt.tight_layout()
plt.savefig('precision_recall.eps')
plt.show()

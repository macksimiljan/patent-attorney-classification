import re

import numpy as np
import pandas as pd
import pyprind
import tensorflow as tf

from collections import Counter

from pac.utils.io.final_train_test import read_X_train_y_train_X_test_y_test
from pac.utils.io.final_train_test import read_features_agent_encoding_applicants_agent_probabilities

training_interval = (2012, 2016)
test_year = 2017
task = 'multi'

X_train, y_train, X_test, y_test = read_X_train_y_train_X_test_y_test(training_interval, test_year, test_class=task)
data, _, _, _ = read_features_agent_encoding_applicants_agent_probabilities(training_interval, test_year)
print('labels (train):', Counter(y_train).most_common(10))
print('labels (test): ', Counter(y_test).most_common(10))
print(np.unique(y_test.tolist() + y_train.tolist()).shape[0])
print('y_train.shape', y_train.shape, 'y_test.shape', y_test.shape)
print()

agent_mapping = list(np.unique(y_train))

simple_encoding = False


################################
# To CSV
################################
def to_simple_csv(X, y, suffix):
    file_content = ''
    for row_index in range(X.shape[0]):
        f = ''
        row = X[row_index, :]
        date = None
        for col_index, datum in zip(row.indices, row.data):
            original_feature_name = data[col_index]
            feature_name = original_feature_name\
                .replace('country_', '')\
                .replace('city_', '')\
                .replace('applicant_is_', '')\
                .replace('_list', '')\
                .replace('patent_status_', '')
            feature_name = feature_name.replace(' ', '_')

            if feature_name == 'application_date':
                date = datum
                continue
            f += '{} '.format(feature_name)
        file_content += f.strip() + "," + str(date) + ',' + str(agent_mapping.index(y[row_index])) + "\n"

        pbar.update()

    with open('../../../data/tf_data_{}.csv'.format(suffix), 'w') as file:
        file.write(file_content)


def to_csv(X, y, suffix):
    file_content = ''
    for row_index in range(X.shape[0]):
        row = X[row_index, :]
        date = 0
        ipc_classes = []
        cities = []
        countries = []
        status = ''
        applicant_type = ''
        applicant = ''
        label = y[row_index]

        for col_index, datum in zip(row.indices, row.data):
            original_feature_name = data[col_index]
            feature_name = original_feature_name \
                .replace('country_', '') \
                .replace('city_', '') \
                .replace('applicant_is_', '') \
                .replace('_list', '') \
                .replace('patent_status_', '')
            feature_name = feature_name.replace(' ', '_')

            if feature_name == 'application_date':
                date = datum
            elif original_feature_name.startswith('country_'):
                countries.append(feature_name)
            elif original_feature_name.startswith('city_'):
                cities.append(feature_name)
            elif original_feature_name.startswith('applicant_is_'):
                applicant_type = feature_name
            elif original_feature_name.startswith('patent_status_'):
                status = feature_name
            elif original_feature_name.startswith('ipc_'):
                ipc_classes.append(feature_name)
            elif re.match('[ag][0-9]+', feature_name):
                applicant = feature_name

        file_content += '{},{},{},{},{},{},{},{},{},{},{},{},{},{}'.format(
            date,
            ipc_classes[0] if len(ipc_classes) > 0 else '',
            ipc_classes[1] if len(ipc_classes) > 1 else '',
            ipc_classes[2] if len(ipc_classes) > 2 else '',
            ipc_classes[3] if len(ipc_classes) > 3 else '',
            cities[0] if len(cities) > 0 else '',
            cities[1] if len(cities) > 1 else '',
            cities[2] if len(cities) > 2 else '',
            cities[3] if len(cities) > 3 else '',
            countries[0] if len(countries) > 0 else '',
            status,
            applicant_type,
            applicant,
            label
        ) + "\n"

        pbar.update()

    with open('../../../data/tf_data_{}.csv'.format(suffix), 'w') as file:
        file.write(file_content)


pbar = pyprind.ProgBar(X_train.shape[0], title='Process X_train')
if simple_encoding:
    to_simple_csv(X_train, y_train, 'train')
else:
    to_csv(X_train, y_train, 'train')

pbar = pyprind.ProgBar(X_test.shape[0], title='Process X_test')
if simple_encoding:
    to_simple_csv(X_test, y_test, 'test')
else:
    to_csv(X_test, y_test, 'test')


################################
# CSV to TFRecord
################################

def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _float_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))

if simple_encoding:
    csv = pd.read_csv('../../../data/tf_data.csv', header=None).values
    with tf.python_io.TFRecordWriter('../../../data/train.tfrecords') as writer:
        for data in csv:
            description = data[0]
            application_date = data[1]
            label = data[2]
            example = tf.train.Example(
                features=tf.train.Features(
                    feature={
                        'description': _bytes_feature(description.encode()),
                        'application_date': _float_feature(application_date),
                        'label': _int64_feature(label)
                    }))
            writer.write(example.SerializeToString())


class FeatureList:

    def __init__(self):
        self._values = set()

    def add(self, items):
        self._values.update(items)

    def count(self):
        return len(self._values)

    def reset(self):
        self._values = set()

    def values(self):
        return sorted(list(self._values))


preprocessed_features = FeatureList()

replaced_features = FeatureList()

import os
from time import time

from numpy import savez
from scipy.sparse import save_npz

from pac.preprocessing.preprocessing_pipeline import PreprocessingPipeline
from pac.preprocessing.preprocessing_pipeline import Settings
from pac.utils.io import DATA_PATH

gap_size = None
training_interval = [2012, 2017]
test_year = 2018
filter = 'more_than_1_agent'
under_sampling = False
feature_hashing = True

print('|--- AppPreprocessing: training: {}, test: {}, gap size: {}, filter: {}'.format(
    training_interval,
    test_year,
    gap_size,
    filter
))
settings = Settings()\
    .set_training_interval(training_interval)\
    .set_test_year(test_year)\
    .set_filter(filter)\
    .set_under_sampling(under_sampling)\
    .set_preprocess_with_hash(feature_hashing)

start_time = time()
preprocessing = PreprocessingPipeline(settings)
preprocessing.perform()


path = os.path.join(DATA_PATH,
                    '{}_{}#{}'.format(
                        training_interval[0],
                        training_interval[1],
                        test_year
                    ))
print('|--- AppPreprocessing: writing {}/ {} to {}'.format(
    training_interval,
    test_year,
    path))
savez(os.path.join(path, 'ys_features'),
      y_train_multi=preprocessing.y_train_multi,
      y_test_multi=preprocessing.y_test_multi,
      y_train_binary=preprocessing.y_train_binary,
      y_test_binary=preprocessing.y_test_binary,
      features=preprocessing.features,
      applicants_of_test=preprocessing.applicants_of_test,
      agent_encoding=preprocessing.agent_encoding,
      agent_probabilities=preprocessing.agent_probabilities,
      gap_sizes=preprocessing.gap_sizes)
save_npz(os.path.join(path, 'X_train'), preprocessing.X_train)
save_npz(os.path.join(path, 'X_test'), preprocessing.X_test)

print('|--- AppPreprocessing: done in {}s'.format(round(time() - start_time, 2)))




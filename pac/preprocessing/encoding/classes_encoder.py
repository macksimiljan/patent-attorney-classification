import pandas as pd
from sklearn.preprocessing import LabelEncoder


class ClassesEncoder:

    def __init__(self):
       self._encoder = LabelEncoder()

    def internal_encoder(self):
        return self._encoder

    def encode(self, training, test):
        classes = pd.concat([training, test], ignore_index=True)

        self._encoder.fit(classes)
        encoded_classes_training = self._encoder.transform(training)
        encoded_classes_test = self._encoder.transform(test)

        return encoded_classes_training, encoded_classes_test

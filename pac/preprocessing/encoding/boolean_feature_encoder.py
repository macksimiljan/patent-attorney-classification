class BooleanFeatureEncoder:

    def encode(self, feature_training, feature_test, training, test):
        if feature_training not in training.columns:
            print('#####\nFeature {} not in training.\n#####'.format(feature_training))
            return training, test

        training[feature_training] = self._encode_boolean(training[feature_training])
        test[feature_test] = self._encode_boolean(test[feature_test])
        return training, test

    def _encode_boolean(self, booleans):
        return booleans.apply(lambda x: 1 if x else 0)

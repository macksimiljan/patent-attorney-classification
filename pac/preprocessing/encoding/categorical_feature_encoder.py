import numpy as np
from scipy.sparse import lil_matrix
from sklearn.feature_extraction import FeatureHasher
from sklearn.preprocessing import StandardScaler

from pac.preprocessing import preprocessed_features
from pac.utils.database.connection_point import instantiate_engine
from pac.utils.io.feature_classes_point import *


class NominalFeatureEncoder:

    def __init__(self, feature_training, feature_test, prefix=''):
        self._feature_training = feature_training
        self._feature_test = feature_test
        self._prefix = prefix

    def encode(self, training, test):
        if self._feature_training not in training.columns:
            print('#####\nFeature {} not in training.\n#####'.format(self._feature_training))
            return training, test

        combined = pd.concat([training[self._feature_training], test[self._feature_test]],
                             ignore_index=True)
        encoded_combined = pd.get_dummies(combined, prefix=self._prefix)
        encoded_training = encoded_combined.loc[0:training.shape[0]-1, :]
        encoded_test = encoded_combined.loc[training.shape[0]:, :]
        encoded_test.reset_index(drop=True, inplace=True)
        encoded_combined = None

        encoded_training.columns = encoded_test.columns

        preprocessed_features.add(encoded_test.columns)

        training = training.drop(self._feature_training, axis=1)
        test = test.drop(self._feature_test, axis=1)
        training = pd.concat([training, encoded_training], axis=1)
        test = pd.concat([test, encoded_test], axis=1)
        return training, test


class OrdinalFeatureEncoding:

    def __init__(self, feature_training, feature_test):
        self._feature_training = feature_training
        self._feature_test = feature_test

    def encode(self, training, test):
        if self._feature_training not in training.columns:
            print('#####\nFeature {} not in training.\n#####'.format(self._feature_training))
            return training, test

        combined = pd.concat([training[self._feature_training], test[self._feature_test]],
                             ignore_index=True)
        combined = np.unique(combined[combined.notna()].values)
        numerical_values = np.arange(0, combined.shape[0])
        mapping = dict(zip(combined.tolist(), numerical_values.tolist()))

        training[self._feature_training] = training[self._feature_training].map(mapping)
        test[self._feature_test] = test[self._feature_test].map(mapping)

        return training, test


class CompanyTypeEncoder:

    def __init__(self):
        self.company_types = ['is_research_organisation', 'is_technology_company', 'is_tto', 'is_service_provider']

    def encode(self, research_organisations, tech_comps, ttos, service_providers):
        init = lil_matrix((len(research_organisations), 4))
        for row_index, row in enumerate(zip(research_organisations, tech_comps, ttos, service_providers)):
            for i in range(4):
                if True in row[i]:
                    init[row_index, i] = 1
        return init


class ApplicantGroupingEncoder:

    def __init__(self):
        self._set_applicant_groupings()

    def _set_applicant_groupings(self):
        engine = instantiate_engine()
        self.groupings = pd.read_sql_query("SELECT DISTINCT artifical_grouping "
                                            "FROM view_grouping_applicants "
                                            "ORDER BY artifical_grouping ", engine).iloc[:, 0]
        self.groupings = list(self.groupings.values)

    def encode(self, applicant_groupings):
        init = lil_matrix((len(applicant_groupings), len(self.groupings)))
        for row_index, grouping in enumerate(applicant_groupings):
            if type(grouping) is str:
                column_index = self.groupings.index(grouping)
                init[row_index, column_index] = 1
        return init


class ApplicantGroupingEncoderWithHash:

    def __init__(self):
        self.hasher = FeatureHasher(n_features=19, input_type='string')
        self.applicant_groupings = ['applicant_grouping'] * 19

    def encode(self, applicant_groupings):
        return self.hasher.fit_transform(applicant_groupings)


class CityEncoder:

    def __init__(self):
        self._set_cities()

    def _set_cities(self):
        engine = instantiate_engine()
        self.cities = pd.read_sql_query("SELECT DISTINCT 'city_' || c.full_name "
                                         "FROM cities c "
                                         "ORDER BY 'city_' || c.full_name", engine).iloc[:, 0]
        self.cities = list(self.cities.values)

    def encode(self, applicant_cities):
        init = lil_matrix((len(applicant_cities), len(self.cities)))
        for row_index, city_list in enumerate(applicant_cities):
            if city_list is not None and type(city_list) is not float and len(city_list) > 0:
                for city in city_list:
                    column_index = self.cities.index('city_' + city)
                    init[row_index, column_index] = 1
        return init


class CityEncoderWithHash:

    def __init__(self):
        self.hasher = FeatureHasher(n_features=10, input_type='string')
        self.cities = ['city'] * 20

    def encode(self, applicant_cities):
        first_cities = []
        second_cities = []
        for row_index, city_list in enumerate(applicant_cities):
            if city_list is not None and type(city_list) is not float and len(city_list) > 0:
                temp = []
                for c in city_list:
                    temp.extend(c)
                city_list = temp
                city_list = list(filter(lambda x: x is not None, city_list))
                if len(city_list) > 0:
                    first_cities.append(city_list[0])
                    if len(city_list) > 1:
                        second_cities.append(city_list[1])
                    else:
                        second_cities.append('')
                else:
                    first_cities.append('')
                    second_cities.append('')
            else:
                first_cities.append('')
                second_cities.append('')
        first_cities = self.hasher.fit_transform(first_cities)
        second_cities = self.hasher.fit_transform(second_cities)
        return first_cities, second_cities


class CountryEncoder:

    def __init__(self):
        self._set_countries()

    def _set_countries(self):
        engine = instantiate_engine()
        self.countries = pd.read_sql_query("SELECT DISTINCT 'country_' || country "
                                           "FROM cities "
                                           "WHERE country IS NOT NULL ORDER BY 'country_' || country", engine).iloc[:, 0]
        self.countries = list(self.countries)

    def encode(self, applicant_countries):
        init = lil_matrix((len(applicant_countries), len(self.countries)))
        for row_index, country_list in enumerate(applicant_countries):
            if country_list is not None and type(country_list) is not float and len(country_list) > 0:
                for country in country_list:
                    column_index = self.countries.index('country_' + country)
                    init[row_index, column_index] = 1
        return init


class CountryEncoderWithHash:

    def __init__(self):
        self.hasher = FeatureHasher(n_features=8, input_type='string')
        self.countries = ['country'] * 16

    def encode(self, applicant_countries):
        first_countries = []
        second_countries = []
        for row_index, country_list in enumerate(applicant_countries):
            if country_list is not None and type(country_list) is not float and len(country_list) > 0:
                temp = []
                for c in country_list:
                    temp.extend(c)
                country_list = temp
                country_list = list(filter(lambda x: x is not None, country_list))
                if len(country_list) > 0:
                    first_countries.append(country_list[0])
                    if len(country_list) > 1:
                        second_countries.append(country_list[1])
                    else:
                        second_countries.append('')
                else:
                    first_countries.append('')
                    second_countries.append('')
            else:
                first_countries.append('')
                second_countries.append('')
        first_countries = self.hasher.fit_transform(first_countries)
        second_countries = self.hasher.fit_transform(second_countries)
        return first_countries, second_countries


class IpcClassEncoder:

    def __init__(self):
        self._set_ipc_classes()

    def _set_ipc_classes(self):
        engine = instantiate_engine()
        self.ipc_classes = pd.read_sql_query("SELECT DISTINCT lower('ipc_' || SUBSTRING(name, 1, 3)) as second_level "
                                             "FROM ipc_classes "
                                             "WHERE LENGTH(SUBSTRING(name, 1, 3)) = 3 "
                                             "ORDER BY lower('ipc_' || SUBSTRING(name, 1, 3));",
                                             engine).iloc[:, 0]
        self.ipc_classes = list(self.ipc_classes)

    def encode(self, applicant_ipc_classes):
        init = lil_matrix((len(applicant_ipc_classes), len(self.ipc_classes)))
        for row_index, ipc_class_list in enumerate(applicant_ipc_classes):
            if ipc_class_list is not None and type(ipc_class_list) is not float and len(ipc_class_list) > 0:
                for ipc_class in ipc_class_list:
                    if ipc_class is not None:
                        column_index = self.ipc_classes.index('ipc_' + ipc_class.lower())
                        init[row_index, column_index] = 1
        return init


class IpcClassEncoderWithHash:

    def __init__(self):
        self.hasher = FeatureHasher(n_features=18, input_type='string')
        self.ipc_classes = ['ipc']*36

    def encode(self, applicant_ipc_classes):
        first_classes = []
        second_classes = []
        for row_index, ipc_class_list in enumerate(applicant_ipc_classes):
            if ipc_class_list is not None and type(ipc_class_list) is not float and len(ipc_class_list) > 0:
                if ipc_class_list[0] is not None:
                    first_classes.append(ipc_class_list[0])
                else:
                    first_classes.append('')
                if len(ipc_class_list) > 1:
                    second_classes.append(ipc_class_list[1])
                else:
                    second_classes.append('')
            else:
                first_classes.append('')
                second_classes.append('')
        first_classes = self.hasher.fit_transform(first_classes)
        second_classes = self.hasher.fit_transform(second_classes)
        return first_classes, second_classes

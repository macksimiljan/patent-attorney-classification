import numpy as np
import pandas as pd

from pac import Constants
from pac.preprocessing import preprocessed_features
from pac.preprocessing import replaced_features


class DateMapping:

    def __init__(self,
                 feature_training=Constants.Training.Features.date,
                 feature_test=Constants.Test.Features.date):
        self._feature_training = feature_training
        self._feature_test = feature_test
        self._feature_preprocessed = 'application_date'
        preprocessed_features.add([self._feature_preprocessed])
        replaced_features.add([self._feature_training, self._feature_test])

    def map(self, training, test):
        training[self._feature_preprocessed] = self.map_dates(training[self._feature_training])

        test[self._feature_preprocessed] = self.map_dates(test[self._feature_test], unit='ms')

        return training, test

    def map_training(self, training):
        return self.map_dates(training[self._feature_training])

    def map_test(self, test):
        return self.map_dates(test[self._feature_test], unit='ms')

    def map_dates(self, dates, unit=None):
        result = pd.to_datetime(dates, unit=unit).astype(np.int64) / 1e9
        return result.astype(int)

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.metrics import confusion_matrix


def show_heat_map(test, predicted_agents, dir_path):
    df = pd.DataFrame(data={'true': test['agent_id'], 'predicted': predicted_agents })
    sample = df.sample(n=100)
    confusion_mc = confusion_matrix(sample['true'], sample['predicted'])
    df_cm = pd.DataFrame(confusion_mc)
    plt.figure(figsize=(5.5,4))
    sns.heatmap(df_cm, annot=False, xticklabels=False, yticklabels=False)
    plt.ylabel('True Agent')
    plt.xlabel('Predicted Agent')
    path = '{}/{}'.format(dir_path, 'figure1.eps')
    plt.savefig(path, bbox_inches='tight')
    plt.show()
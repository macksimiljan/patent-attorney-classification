def query_sequence_count_per_interval():
    return """
        SELECT min, max, SUBSTRING(CAST(min AS TEXT), 1, 4) || ' - ' || SUBSTRING(CAST(max AS TEXT), 1, 4) AS interval, COUNT(*) AS abs_freq
        FROM (
            SELECT applicant_id, MIN(ep_application_date) AS min, MAX(ep_application_date) AS max
            FROM (
                SELECT v.applicant_id, v.patent_id, date_part('year', ep_application_date) AS ep_application_date, v.agent_id
                FROM view_pairing_patent_applicant_agent v JOIN view_patents p ON v.patent_id = p.id
                WHERE agent_id IS NOT NULL
                ) AS t0
            GROUP BY applicant_id
            ) AS t1
        GROUP BY min, max
        ORDER BY min, max
    """


def query_correlation_number_applications_interval_length():
    return """
        SELECT number_applications, interval_length, COUNT(*) AS size
        FROM (
            SELECT applicant_id, COUNT(DISTINCT patent_id) AS number_applications, MAX(ep_application_date) - MIN(ep_application_date) + 1 AS interval_length
            FROM (
                SELECT v.applicant_id, v.patent_id, date_part('year', ep_application_date) AS ep_application_date, v.agent_id
                FROM view_pairing_patent_applicant_agent v JOIN view_patents p ON v.patent_id = p.id
                WHERE agent_id IS NOT NULL
                AND applicant_id IS NOT NULL
                ) AS t0
            GROUP BY applicant_id
            ) AS t1
        GROUP BY number_applications, interval_length
    """


def query_agent_classes():
    return """
        SELECT g.agent AS agent_id,
                g.artifical_grouping AS agent_grouping,
                a.cities AS agent_cities,
                a.countries AS agent_countries,
                a.lat_lon_pairs AS agent_lat_lon_pairs,
                FLOOR(LN(a.employee_count+1)) AS agent_category_employee_count,
                a.second_level_classes AS agent_second_level_classes,
                a.first_level_classes AS agent_first_level_classes
        FROM view_grouping_agents g
                JOIN view_agent_complete a ON (g.agent = a.id)
    """


def query_training_dataset(year_start, year_end=None):
    year_end = year_start + 1 if year_end is None else year_end + 1
    return """
        SELECT applicant_grouping,
            JSON_AGG(applicant_is_tto) AS applicant_is_tto_list,
            JSON_AGG(applicant_is_research_organisation) AS applicant_is_research_organisation_list,
            JSON_AGG(applicant_is_service_provider) AS applicant_is_service_provider_list,
            JSON_AGG(applicant_is_technology_company) AS applicant_is_technology_company_list,
            JSON_AGG(applicant_cities) AS applicant_cities_list,
            JSON_AGG(applicant_countries) AS applicant_countries_list,
            JSON_AGG(applicant_lat_lon_pairs) AS applicant_lat_lon_pairs_list,
            JSON_AGG(patent_ep_application_date) AS patent_ep_application_date_list,
            JSON_AGG(patent_status_category) AS patent_status_category_list,
            JSON_AGG(patent_second_level_classes) AS patent_second_level_classes_list,
            JSON_AGG(patent_first_level_classes) AS patent_first_level_classes_list,
            JSON_AGG(t1.cooccurrences) AS applicant_cooccurrences,
            JSON_AGG(agent_grouping) AS agent_grouping_list
        FROM (
            SELECT
                    p.id AS patent_id,
                    p.ep_application_date AS patent_ep_application_date,
                    p.status_category AS patent_status_category,
                    p.second_level_classes AS patent_second_level_classes,
                    p.first_level_classes AS patent_first_level_classes,
                    t.grouping_applicant AS applicant_grouping,
                    JSON_AGG(DISTINCT appl.is_tto) AS applicant_is_tto,
                    JSON_AGG(DISTINCT appl.is_research_organisation) AS applicant_is_research_organisation,
                    JSON_AGG(DISTINCT appl.is_service_provider) AS applicant_is_service_provider,
                    JSON_AGG(DISTINCT appl.is_technology_company) AS applicant_is_technology_company,
                    JSON_AGG(DISTINCT appl.cities) AS applicant_cities,
                    JSON_AGG(DISTINCT appl.countries) AS applicant_countries,
                    JSON_AGG(DISTINCT appl.lat_lon_pairs) AS applicant_lat_lon_pairs,
                    t.grouping_agent AS agent_grouping
            FROM view_triplets t
                    JOIN view_patents_complete p ON t.patent_id = p.id
                    JOIN view_applicants appl ON t.applicant_id = appl.id
                    JOIN view_agent_complete agent ON t.agent_id = agent.id
            WHERE p.ep_application_date >= DATE('{}-01-01')
                AND p.ep_application_date < DATE('{}-01-01')
                AND t.grouping_agent IS NOT NULL
                AND t.grouping_applicant IS NOT NULL
            GROUP BY p.id, p.ep_application_date, p.status_category, p.second_level_classes, p.first_level_classes,
                      t.grouping_applicant, t.grouping_agent
            ORDER BY applicant_grouping, patent_ep_application_date
        ) AS t,
        (
            SELECT t.patent_id, JSON_AGG(DISTINCT t.grouping_applicant) AS cooccurrences
            FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
            WHERE t.grouping_agent IS NOT NULL
            AND t.grouping_applicant IS NOT NULL
            AND p.ep_application_date >= DATE('2012-01-01')
            GROUP BY t.patent_id
        ) AS t1
        WHERE t1.patent_id = t.patent_id
        GROUP BY applicant_grouping
    """.format(
        year_start,
        year_end
    )


def query_test_dataset(year):
    return """
        SELECT
            t_out.*,
            (ROW_NUMBER() OVER (PARTITION BY applicant_grouping ORDER BY patent_ep_application_date)) - 1 AS missing_applicants_from_sequence_count,
            temp.previous_agents
        FROM (
            SELECT 
                t.grouping_applicant AS applicant_grouping,            
                JSON_AGG(appl.is_tto) AS applicant_is_tto,
                JSON_AGG(appl.is_research_organisation) AS applicant_is_research_organisation,
                JSON_AGG(appl.is_service_provider) AS applicant_is_service_provider,
                JSON_AGG(appl.is_technology_company) AS applicant_is_technology_company,
                JSON_AGG(appl.cities) AS applicant_cities,
                JSON_AGG(appl.countries) AS applicant_countries,
                JSON_AGG(appl.lat_lon_pairs) AS applicant_lat_lon_pairs,
                p.id AS patent_id,
                p.ep_application_date AS patent_ep_application_date,
                p.status_category AS patent_status_category,
                p.second_level_classes AS patent_second_level_classes,
                p.first_level_classes AS patent_first_level_classes,		
                t.grouping_agent AS agent_grouping
            FROM view_triplets t
                    JOIN view_patents_complete p ON t.patent_id = p.id
                    LEFT OUTER JOIN view_applicants appl ON t.applicant_id = appl.id
                    LEFT OUTER JOIN view_agent_complete agent ON t.agent_id = agent.id
            WHERE p.ep_application_date >= DATE('{}-01-01')
                AND p.ep_application_date < DATE('{}-01-01')
                AND t.grouping_agent IS NOT NULL
                AND t.grouping_applicant IS NOT NULL
            GROUP BY t.grouping_applicant,
                      p.id, p.ep_application_date, p.status_category, p.second_level_classes, p.first_level_classes,
                      t.grouping_agent
        ) AS t_out,
        (
            SELECT t.grouping_applicant, JSON_AGG(DISTINCT t.grouping_agent) AS previous_agents
            FROM view_triplets t, view_patents p
            WHERE t.patent_id = p.id
            AND p.ep_application_date >= DATE('2012-01-01')
            AND p.ep_application_date < DATE('{}-01-01')
            AND t.grouping_agent IS NOT NULL
            AND t.grouping_applicant IS NOT NULL
            GROUP BY t.grouping_applicant
        ) AS temp
        WHERE temp.grouping_applicant = applicant_grouping
    """.format(
        year,
        year + 1,
        year
    )


def query_gap_sizes_for_2017():
    return """
        SELECT missing_applicants_from_sequence_count, COUNT(*) AS no
        FROM (
        SELECT t.grouping_applicant AS applicant_grouping,
                    (ROW_NUMBER() OVER (PARTITION BY t.grouping_applicant ORDER BY p.ep_application_date)) - 1 AS missing_applicants_from_sequence_count,
                    appl.is_tto AS applicant_is_tto,
                    appl.is_research_organisation AS applicant_is_research_organisation,
                    appl.is_service_provider AS applicant_is_service_provider,
                    appl.is_technology_company AS applicant_is_technology_company,
                    CASE WHEN appl.cities = '{{NULL}}' THEN NULL ELSE appl.cities END AS applicant_cities,
                    CASE WHEN appl.countries = '{{NULL}}' THEN NULL ELSE appl.countries END AS applicant_countries,
                    CASE WHEN appl.lat_lon_pairs = '{{NULL}}' THEN NULL ELSE appl.lat_lon_pairs END AS applicant_lat_lon_pairs,
                    p.id AS patent_id,
                    p.ep_application_date AS patent_ep_application_date,
                    p.status_category AS patent_status_category,
                    p.second_level_classes AS patent_second_level_classes,
                    p.first_level_classes AS patent_first_level_classes,		
                    t.grouping_agent AS agent_grouping,
                    agent.countries AS agent_countries,
                    FLOOR(LN(agent.employee_count+1)) AS agent_category_employee_count
            FROM view_triplets t
                    JOIN view_patents_complete p ON t.patent_id = p.id
                    LEFT OUTER JOIN view_applicants appl ON t.applicant_id = appl.id
                    LEFT OUTER JOIN view_agent_complete agent ON t.agent_id = agent.id
            WHERE p.ep_application_date >= DATE('2017-01-01')
                AND p.ep_application_date < DATE('2018-01-01')
                AND t.agent_id IS NOT NULL
        ) AS t0
        GROUP BY missing_applicants_from_sequence_count
        ORDER BY missing_applicants_from_sequence_count
    """

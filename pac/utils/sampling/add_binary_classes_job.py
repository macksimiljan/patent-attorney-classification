from time import time

import pac.utils.io.feature_classes_point as pac_io
from pac import Constants


class AddBinaryClassesJob:

    def perform(self, training_interval, test_year=None):
        year_start, year_end = training_interval
        print('AddBinaryClassesJob.perform({}, {})'.format(training_interval, test_year))
        start_time = time()
        self._perform_on_training(year_start, year_end)
        print('  |--- done training in {}s'.format(round(time() - start_time, 4)))

        if test_year is None:
            return

        start_time = time()
        self._perform_on_test(test_year)
        print('  |--- done test in {}s'.format(round(time() - start_time, 4)))

    def _perform_on_training(self, year_start, year_end=None):
        dataset = pac_io.read_training_dataset(year_start, year_end)
        dataset[Constants.Training.Classes.old_vs_new] = dataset.apply(lambda row: self._map_to_binary_classes_training(row), axis=1)
        pac_io.write_as_training_dataset(dataset, year_start, year_end)

    def _perform_on_test(self, year):
        dataset = pac_io.read_test_dataset(year)
        dataset[Constants.Test.Classes.old_vs_new] = dataset.apply(lambda row: self._map_to_binary_classes_test(row), axis=1)
        pac_io.write_as_test_dataset(dataset, year)

    def _map_to_binary_classes_training(self, row):
        # 1/ true meaning 'new agent'
        # 0/ false meaning 'known agent'
        # e.g. [a1, a1, a4, a1] --> [1, 0, 1, 0]
        agents = row[Constants.Training.Classes.groupings]
        if len(agents) == 1:
            return [1]

        binaries = []
        old_agents = []
        for agent in agents:
            if agent in old_agents:
                binaries.append(0)
            else:
                binaries.append(1)
                old_agents.append(agent)
        return binaries

    def _map_to_binary_classes_test(self, row):
        previous_agents = row[Constants.Test.Features.previous_agents]
        current_agent = row[Constants.Test.Classes.grouping]
        if current_agent in previous_agents:
            return 0
        else:
            return 1

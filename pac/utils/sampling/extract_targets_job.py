import os

import pandas as pd

from pac.utils.sampling.queries import *
from pac.utils.io import DATA_PATH
from pac.utils.io import TEST_FILE_NAME


class ExtractTargetsJob:

    def __init__(self, engine,):
        self.engine = engine

    def perform(self):
        print('ExtractTargetsJob.perform()')
        result = pd.read_sql_query(query_agent_classes(), self.engine)
        result.to_json(
            path_or_buf=os.path.join(DATA_PATH, TEST_FILE_NAME),
            orient='records',
            lines=True)

from time import time

import pandas as pd

from pac.utils.sampling.queries import *
from pac.utils.io.feature_classes_point import write_as_test_dataset
from pac.utils.io.feature_classes_point import write_as_training_dataset


class ExtractSequencesJob:

    def __init__(self, engine):
        self.engine = engine

    def perform(self, training_interval, test_year=None):
        year_start, year_end = training_interval
        print('ExtractSequencesJob.perform({}, {})'.format(training_interval, test_year))
        start_time = time()
        training = pd.read_sql_query(query_training_dataset(year_start, year_end), self.engine)
        write_as_training_dataset(training, year_start, year_end)
        print('  |--- done training in {}s'.format(round(time() - start_time, 4)))

        if test_year is None:
            return

        start_time = time()
        test = pd.read_sql_query(query_test_dataset(test_year), self.engine)
        write_as_test_dataset(test, test_year)
        print('  |--- done test in {}s'.format(round(time() - start_time, 4)))

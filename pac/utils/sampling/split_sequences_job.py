from time import time

import pandas as pd

import pac.utils.io.feature_classes_point as pac_io
from pac import Constants


class SplitSequencesJob:

    def __init__(self):
        self.target_columns = None
        self.row_accumulator = None

    def perform(self, training_interval):
        year_start, year_end = training_interval
        print('SplitSequencesJob.perform({}, {})'.format(year_start, year_end))
        start_time = time()
        dataset = pac_io.read_training_dataset(year_start, year_end)
        split_dataset = self.split_dataset_from(dataset)
        pac_io.write_as_split_training_dataset(split_dataset, year_start, year_end)
        print('  |--- done in {}s'.format(round(time() - start_time, 4)))

    def split_dataset_from(self, dataset):
        # see https://gist.github.com/jlln/338b4b0b55bd6984f883
        self.target_columns = list(dataset)
        self.target_columns.remove(Constants.Training.Features.grouping)
        self.row_accumulator = []
        dataset.apply(self.split_list_to_rows, axis=1)
        new_df = pd.DataFrame(self.row_accumulator)
        return new_df

    def split_list_to_rows(self, row):
        tmp_dict = {}
        no_rows = len(row[self.target_columns[0]])

        for column in self.target_columns:
            tmp_dict[column] = row[column]

        for i in range(no_rows):
            new_row = row.to_dict()
            for column in tmp_dict:
                tc_list = tmp_dict[column]
                new_row[column] = tc_list[i]
            self.row_accumulator.append(new_row)

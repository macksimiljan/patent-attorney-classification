from pac.utils.sampling.add_binary_classes_job import AddBinaryClassesJob
from pac.utils.sampling.extract_sequences_job import ExtractSequencesJob
from pac.utils.sampling.split_sequences_job import SplitSequencesJob
from pac.utils.database.connection_point import instantiate_engine

engine = instantiate_engine()

# get the data from the database
job = ExtractSequencesJob(engine)
job.perform((2012, 2016), 2017)
job.perform((2012, 2017), 2018)
job.perform((2016, None), None)
job.perform((2016, 2017), None)


# enrich the data with old-vs-new-agent classes
job = AddBinaryClassesJob()
job.perform((2012, 2016), 2017)
job.perform((2012, 2017), 2018)
job.perform((2016, None), None)
job.perform((2016, 2017), None)

# split the sequences for non-sequential use
job = SplitSequencesJob()
job.perform((2012, 2016))
job.perform((2012, 2017))
job.perform((2016, None))
job.perform((2016, 2017))

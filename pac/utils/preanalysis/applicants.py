from datetime import datetime

import pandas as pd

from pac.utils.database.connection_point import fetch_multiple_rows
from pac.utils.database.connection_point import fetch_single_row
from pac.utils.database.connection_point import instantiate_engine
from pac.utils.plotter.histograms import map_to_histogram_data
from pac.utils.plotter.histograms import show_histogram
from pac.utils.plotter.histograms import show_rank_histogram
from pac.preanalysis.helpers import map_null_value
from pac.preanalysis.queries.applicants_queries import *


def run_applicants_preanalysis(connection, text_file, figures_path):
    print('TODO: Eval for company type')

    with open(text_file, 'a') as f:
        f.write('===== Applicants {} =====\n'.format(datetime.now()))
        f.write('#Applicants: {}\n'.format(applicants_count(connection)))
        f.write('#Applicants out of business: {}\n'.format(out_of_business_applicants_count(connection)))
        f.write('#Applicants as initiative: {}'.format(applicants_as_initiative_count(connection)))
        f.write('{:20} | {:20} | {:20}\n'.format('Category #Employees', 'abs freq', 'rel freq'))
        for row in applicants_per_number_of_employees(connection):
            f.write('{:20} | {:20} | {:20}\n'.format(map_null_value(row['category_id']), row['abs_freq'], round(row['rel_freq'], 4)))

    x_values, y_values = map_to_histogram_data(
        patents_per_applicant(connection),
        'rank', 'abs_freq', ignore_null_value=True)
    path = '{}/{}'.format(figures_path, 'applicants_rank_patents.eps')
    show_rank_histogram(x_values, y_values, 'Rank Applicant', '#Patents', figure_path=path)

    engine = instantiate_engine()
    result = pd.read_sql_query(query_applicants_per_patent(), engine)
    path = '{}/{}'.format(figures_path, 'applicants_rank_applicants.eps')
    show_histogram(result['company_count'], result['patent_count'], '#Applicants per Patent', '#Patents',
                   y_logarithmic=True, figure_path=path)

    x_values, y_values = map_to_histogram_data(
        applicants_per_quality_score(connection),
        'quality_score', 'abs_freq', ignore_null_value=True)
    path = '{}/{}'.format(figures_path, 'applicants_quality_score_applicants.eps')
    show_histogram(y_values, y_values, 'Quality Score', '#Applicants', y_logarithmic=True, figure_path=path)

    result = pd.read_sql_query(query_cooccurrences_applicants(), engine)
    path = '{}/{}'.format(figures_path, 'applicants_cooccurrences.eps')
    show_rank_histogram(result['rank'], result['patents_count'], 'Rank Cooccurrence', '#Patents', figure_path=path)

    result = pd.read_sql_query(query_agents_per_applicant(), engine)
    path = '{}/{}'.format(figures_path, 'applicants_agents_per_applicant.eps')
    show_rank_histogram(result['rank'], result['abs_freq'], 'Rank Applicant', '#Agents', figure_path=path)

    result = pd.read_sql_query(query_applicants_per_agent(), engine)
    path = '{}/{}'.format(figures_path, 'applicants_applicants_per_agent.eps')
    show_rank_histogram(result['rank'], result['abs_freq'], 'Rank Agent', '#Applicants', figure_path=path)


def applicants_count(connection):
    result = fetch_single_row(connection, query_applicants_count())
    return result[0]


def patents_per_applicant(connection):
    result = fetch_multiple_rows(connection, query_patents_per_applicant())
    return result


def out_of_business_applicants_count(connection):
    result = fetch_single_row(connection, query_out_of_business_applicants_count())
    return result[0]


def applicants_per_number_of_employees(connection):
    result = fetch_multiple_rows(connection, query_applicants_per_number_of_employees())
    return result


def applicants_as_initiative_count(connection):
    result = fetch_single_row(connection, query_initiatives_count())
    return result[0]


def applicants_per_quality_score(connection):
    result = fetch_multiple_rows(connection, query_applicants_per_quality_score())
    return result

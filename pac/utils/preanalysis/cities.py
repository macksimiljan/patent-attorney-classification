from datetime import datetime

from pac.utils.database.connection_point import fetch_multiple_rows
from pac.utils.plotter.scatters import show_scatter_plot
from pac.utils.plotter.histograms import map_to_multiple_histogram_data
from pac.utils.plotter.histograms import show_multiple_histograms
from pac.preanalysis.queries.cities_queries import *

from pac.utils.database.connection_point import instantiate_engine

import pandas as pd


def run_cities_preanalysis(connection, text_file, figures_path):

    with open(text_file, 'a') as f:
        f.write('===== Cities {} =====\n'.format(datetime.now()))
        f.write('"Athens" is ambigous, 3 cities with this full name, desambiguated by area levels\n')
        f.write('{:15} | {:15} | {:20} | {:20}\n'.format('City', 'Country', 'abs freq (applicants)', 'rel freq (applicants)'))
        for row in applicants_per_top_ten_cities(connection):
            f.write('{:15} | {:15} | {:20} | {:20}\n'.format(row['city_name'], row['city_country'], row['abs_freq'], round(row['rel_freq'], 4)))
        f.write('{:15} | {:15} | {:20} | {:20}\n'.format('City', 'Country', 'abs freq (agents)', 'rel freq (agents)'))
        for row in agents_per_top_ten_cities(connection):
            f.write('{:15} | {:15} | {:20} | {:20}\n'.format(row['city_name'], row['city_country'], row['abs_freq'], round(row['rel_freq'], 4)))

    result_sets = [applicants_per_count_cities(connection), agents_per_count_cities(connection)]
    x_values, y_values_array = map_to_multiple_histogram_data(result_sets, 'city_count', 'company_count')
    path = '{}/{}'.format(figures_path, 'cities_city_actor.eps')
    show_multiple_histograms(x_values, y_values_array, '#Cities Per Actor', '#Actors',
                             ['Applicants', 'Agents'], figure_path=path)

    engine = instantiate_engine()

    result_agent = pd.read_sql_query(query_agents_per_country(0.03), engine)
    result_applicant = pd.read_sql_query(query_applicants_per_country(0.03), engine)
    result_merged = result_agent.merge(result_applicant, how='outer', on='city_country', sort=True,
                                       suffixes=['_agent', '_applicant'])
    path = '{}/{}'.format(figures_path, 'cities_country_actor.eps')
    show_multiple_histograms(result_merged['city_country'], [result_merged['rel_freq_agent'], result_merged['rel_freq_applicant']], '', 'Proportion Actors',
                             ['Applicants', 'Agents'], figure_path=path, show_x_values=True)
    show_scatter_plot(result_merged['rel_freq_applicant'].fillna(0), result_merged['rel_freq_agent'].fillna(0),
                      "Proportion Applicants' Countries", "Proportion Agents' Countries",
                      dot_labels=result_merged['city_country'], dot_label_position=(0.005, -0.005), figure_path=path)


def applicants_per_top_ten_cities(connection):
    result = fetch_multiple_rows(connection, query_applicants_per_top_ten_cities())
    return result


def applicants_per_count_cities(connection):
    result = fetch_multiple_rows(connection, query_applicants_per_count_cities())
    return result


def agents_per_top_ten_cities(connection):
    result = fetch_multiple_rows(connection, query_agents_per_top_ten_cities())
    return result


def agents_per_count_cities(connection):
    result = fetch_multiple_rows(connection, query_agents_per_count_cities())
    return result

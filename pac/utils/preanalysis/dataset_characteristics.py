from pac.utils.database.connection_point import instantiate_engine
import pandas as pd
import numpy as np

from collections import Counter


engine = instantiate_engine()

# # patent applications ##################
# q = """SELECT COUNT(DISTINCT t.patent_id)
#         FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
#         WHERE t.grouping_agent IS NOT NULL
#         AND t.grouping_applicant IS NOT NULL
#         AND p.ep_application_date >= DATE('2012-01-01')
#         AND t.grouping_applicant = 'g1764'"""
# values = pd.read_sql_query(q, engine).values
# number_applications = values[0,0]
# print("# applications:", number_applications)
#
# # grouping size #########################
# q = """SELECT COUNT(DISTINCT company_id)
#         FROM grouping_relations
#         WHERE grouping_id = 1764"""
# values = pd.read_sql_query(q, engine).values
# print('grouping size:', values[0,0])
#
# # co-occurrences ########################
# q = """SELECT t.patent_id, COUNT(DISTINCT t.grouping_applicant)
#         FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
#         WHERE t.patent_id IN (
#             SELECT t.patent_id
#             FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
#             WHERE t.grouping_agent IS NOT NULL
#             AND t.grouping_applicant IS NOT NULL
#             AND p.ep_application_date >= DATE('2012-01-01')
#             AND t.grouping_applicant = 'g1764'
#             )
#         GROUP BY t.patent_id
#         HAVING COUNT(DISTINCT t.grouping_applicant) > 1"""
# values = pd.read_sql_query(q, engine).values
# print("# co-occurrences:", values.shape[0])
# print("% co-occurrences:", values.shape[0]/number_applications)
#
#
# # agents #############################
# q = """SELECT t.patent_id, t.grouping_agent
#         FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
#         WHERE t.grouping_agent IS NOT NULL
#         AND t.grouping_applicant IS NOT NULL
#         AND p.ep_application_date >= DATE('2012-01-01')
#         AND t.grouping_applicant = 'g1764'"""
# values = pd.read_sql_query(q, engine).values
# agents = values[:, 1]
# print("# agents:", np.unique(agents).shape[0])
#
# # relative importance ################
# counter = Counter(agents)
# second_to_first = counter.most_common(2)[1][1] / counter.most_common(2)[0][1]
# third_to_first = counter.most_common(3)[2][1] / counter.most_common(3)[0][1]
# print(counter)
# print('rel. importance 2nd/1st:', second_to_first)
# print('rel. importance 3rd/1st:', third_to_first)
#
# ############################
# ############################
#
# q = """SELECT DISTINCT t.grouping_applicant, t.patent_id, t.grouping_agent
#         FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
#         WHERE t.grouping_agent IS NOT NULL
#         AND t.grouping_applicant IS NOT NULL
#         AND p.ep_application_date >= DATE('2012-01-01')
#         AND t.grouping_applicant IN
#             (
#             SELECT grouping_applicant
#             FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id),
#                                                     locations l, cities c
#             WHERE t.grouping_agent IS NOT NULL
#             AND t.grouping_applicant IS NOT NULL
#             AND p.ep_application_date >= DATE('2012-01-01')
#             AND l.company_id = t.applicant_id
#             AND l.category_location_id = 1
#             AND l.city_id = c.id
#             AND c.id IN
#                 (
#                 SELECT id
#                 FROM cities
#                 WHERE country = 'Germany'
#                 )
#         )
# """
# df = pd.read_sql_query(q, engine)
#
# # examples ######################
# print('# examples:', df.values.shape[0])
# print('# applicants:', np.unique(df.values[:, 0]).size)
#
# # applications ##################
# number_applications = np.unique(df.values[:, 1]).size
# grouped = {}
# for tuple in df.itertuples():
#     applicant = tuple[1]
#     patent = tuple[2]
#     if applicant in grouped:
#         grouped[applicant].add(patent)
#     else:
#         grouped[applicant] = set()
#         grouped[applicant].add(patent)
# grouped = list(map(lambda pair: len(pair[1]), grouped.items()))
# print('# applications:', np.mean(grouped), np.std(grouped), np.median(grouped))
#
# # agents ###########################
# grouped = {}
# for tuple in df.itertuples():
#     applicant = tuple[1]
#     agent = tuple[3]
#     if applicant in grouped:
#         grouped[applicant].add(agent)
#     else:
#         grouped[applicant] = set()
#         grouped[applicant].add(agent)
# grouped_agg = list(map(lambda pair: len(pair[1]), grouped.items()))
# trivial = list(filter(lambda x: x == 1, grouped_agg))
# print('# agents:', np.mean(grouped_agg), np.std(grouped_agg), np.median(grouped_agg))
# print('% trivial:', len(trivial)/len(grouped_agg))
#
# # relative importance ################
# grouped = {}
# for tuple in df.itertuples():
#     applicant = tuple[1]
#     agent = tuple[3]
#     if applicant in grouped:
#         grouped[applicant].append(agent)
#     else:
#         grouped[applicant] = []
#         grouped[applicant].append(agent)
# seconds_to_firsts = []
# thirds_to_firsts = []
# counter = Counter()
# for pair in grouped.items():
#     agents = pair[1]
#     counter.clear()
#     counter.update(agents)
#     if len(counter) > 1:
#         second_to_first = counter.most_common(2)[1][1] / counter.most_common(2)[0][1]
#         seconds_to_firsts.append(second_to_first)
#         if len(counter) > 2:
#             third_to_first = counter.most_common(3)[2][1] / counter.most_common(3)[0][1]
#             thirds_to_firsts.append(third_to_first)
# print('rel. importance 2nd/1st:', np.mean(seconds_to_firsts), np.std(seconds_to_firsts), np.median(seconds_to_firsts))
# print('rel. importance 3rd/1st:', np.mean(thirds_to_firsts), np.std(thirds_to_firsts), np.median(thirds_to_firsts))
#
# # grouping size ######################
# grouping_applicants = np.unique(df.values[:, 0])
# grouping_ids = []
# art_grouping_ids = []
# for g in grouping_applicants:
#     grouping_ids.append(eval(g[1:])) if g[0] == 'g' else art_grouping_ids.append(1)
# q = """
#     SELECT grouping_id, COUNT(DISTINCT company_id)
#     FROM grouping_relations
#     WHERE grouping_id IN ({})
#     GROUP BY grouping_id
# """.format(str(grouping_ids)[1:-1])
# df_sub = pd.read_sql_query(q, engine)
# temp = df_sub.values[:, 1].tolist()
# temp.extend(art_grouping_ids)
# print('grouping size:', np.mean(temp), np.std(temp), np.median(temp))
#
# # co-occurrences ###################
# q = """SELECT t.patent_id, COUNT(DISTINCT t.grouping_applicant)
#         FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
#         WHERE t.patent_id IN (
#             SELECT t.patent_id
#             FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
#             WHERE t.grouping_agent IS NOT NULL
#             AND t.grouping_applicant IS NOT NULL
#             AND p.ep_application_date >= DATE('2012-01-01')
#             AND t.grouping_applicant IN ({})
#             )
#         GROUP BY t.patent_id
#         HAVING COUNT(DISTINCT t.grouping_applicant) > 1
# """.format(str(grouping_applicants.tolist())[1:-1])
# df_sub = pd.read_sql_query(q, engine)
# print('# co-occurrences:', df_sub.shape[0])
# print('% co-occurrences:', df_sub.shape[0]/number_applications)
#
# ############################
# ############################
#
# (
# SELECT t.grouping_applicant
# FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
# WHERE t.grouping_applicant IS NOT NULL
# AND t.grouping_applicant IS NOT NULL
# AND p.ep_application_date >= DATE('2012-01-01')
# GROUP BY t.grouping_applicant
# HAVING COUNT(DISTINCT t.grouping_agent) > 0 # 4
# AND COUNT(DISTINCT t.grouping_agent) < 4 # 8
# )
# INTERSECT
# (
# SELECT t.grouping_applicant
# FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
# WHERE t.grouping_applicant IS NOT NULL
# AND t.grouping_applicant IS NOT NULL
# AND p.ep_application_date >= DATE('2012-01-01')
# GROUP BY t.grouping_applicant
# HAVING COUNT(DISTINCT t.patent_id) > 8 # 51
# AND COUNT(DISTINCT t.patent_id) < 12 # 159
# )
# INTERSECT
# (
# SELECT DISTINCT 'g' || grouping_id
# FROM grouping_relations g INNER JOIN view_triplets t ON (g.company_id = t.applicant_id) INNER JOIN epo_patents p ON (t.patent_id = p.id)
# WHERE t.grouping_applicant IS NOT NULL
# AND t.grouping_applicant IS NOT NULL
# AND p.ep_application_date >= DATE('2012-01-01')
# AND t.grouping_applicant LIKE 'g%'
# GROUP BY 'g' || grouping_id
# HAVING COUNT(DISTINCT company_id) < 3 # 4
# )
# median_average_applicants = ["g3428", "g5600", "g3590", "g3864", "g1176", "g1264", "g394", "g486", "g893", "g5427", "g1049", "g6587", "g5878", "g2477", "g8629", "g2969", "g5307", "g8827", "g8881", "g7758", "g3819", "g1027", "g7809", "g4262", "g3361", "g3554", "g7855", "g3570", "g3888", "g6445", "g3883", "g534", "g793", "g358", "g8425", "g2429", "g3615", "g375", "g3801", "g541", "g2168", "g6229", "g2213", "g5418", "g3686", "g3805", "g3745", "g1183", "g1234", "g3775", "g3751", "g8468", "g3813", "g730", "g3946", "g2464", "g6441", "g6561", "g357", "g8411", "g5463", "g6653", "g3812", "g3973", "g5310", "g4184", "g3972", "g373", "g22", "g2578", "g678", "g363", "g8348", "g2710", "g3289", "g698", "g1199", "g5426", "g377", "g2890", "g5198", "g3545", "g2582", "g7721", "g3670", "g8021", "g5211", "g6255", "g3698", "g8020", "g1678"]
# arithm_average_applicants = ["g3481", "g1617", "g5397", "g1322", "g675", "g3224", "g1415", "g5900", "g965", "g1657", "g1515", "g1642", "g1615", "g1397", "g4140", "g1639", "g2549", "g1327", "g517", "g1085", "g3853", "g2878", "g1441", "g1582", "g1783", "g1405", "g1695", "g980", "g3557", "g3103", "g1823", "g1094", "g1544", "g1558", "g1342", "g1648", "g1413", "g1836", "g4010", "g646", "g1806", "g7009", "g1408", "g1248", "g3825", "g2007", "g512", "g3655", "g6202", "g1369", "g823", "g7780", "g3442", "g3840", "g2723", "g1467", "g1637", "g3826", "g1435", "g1731", "g1490", "g1523", "g1799", "g1450", "g3502", "g3862", "g3646", "g1143", "g655", "g1101", "g1457", "g166", "g6761", "g1524", "g1613", "g611", "g790", "g1046", "g1538", "g1367", "g1246", "g1512", "g2870", "g1383", "g624", "g673", "g1785", "g1608", "g1428", "g1722", "g2996", "g1509", "g1379", "g6370", "g1042", "g3108", "g1629", "g1607", "g3822", "g1592", "g835", "g5734", "g6371", "g828", "g1739", "g1144", "g679", "g1526", "g1334", "g1763", "g6178", "g1666", "g1106", "g3188", "g3232", "g6184", "g710", "g1720", "g1668", "g3645", "g3617", "g397"]
#
# q = """SELECT DISTINCT t.grouping_applicant, t.patent_id, t.grouping_agent
#         FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
#         WHERE t.grouping_agent IS NOT NULL
#         AND t.grouping_applicant IS NOT NULL
#         AND p.ep_application_date >= DATE('2012-01-01')
#         AND t.grouping_applicant IN
#             (
#               {}
#             )
# """.format(str(arithm_average_applicants)[1:-1])
# df = pd.read_sql_query(q, engine)
#
# # examples ######################
# print('# examples:', df.values.shape[0])
# print('# applicants:', np.unique(df.values[:, 0]).size)
#
# # applications ##################
# number_applications = np.unique(df.values[:, 1]).size
# grouped = {}
# for tuple in df.itertuples():
#     applicant = tuple[1]
#     patent = tuple[2]
#     if applicant in grouped:
#         grouped[applicant].add(patent)
#     else:
#         grouped[applicant] = set()
#         grouped[applicant].add(patent)
# grouped = list(map(lambda pair: len(pair[1]), grouped.items()))
# print('# applications:', np.mean(grouped), np.std(grouped), np.median(grouped))
#
# # agents ###########################
# grouped = {}
# for tuple in df.itertuples():
#     applicant = tuple[1]
#     agent = tuple[3]
#     if applicant in grouped:
#         grouped[applicant].add(agent)
#     else:
#         grouped[applicant] = set()
#         grouped[applicant].add(agent)
# grouped_agg = list(map(lambda pair: len(pair[1]), grouped.items()))
# trivial = list(filter(lambda x: x == 1, grouped_agg))
# print('# agents:', np.mean(grouped_agg), np.std(grouped_agg), np.median(grouped_agg))
# print('% trivial:', len(trivial)/len(grouped_agg))
#
# # relative importance ################
# grouped = {}
# for tuple in df.itertuples():
#     applicant = tuple[1]
#     agent = tuple[3]
#     if applicant in grouped:
#         grouped[applicant].append(agent)
#     else:
#         grouped[applicant] = []
#         grouped[applicant].append(agent)
# seconds_to_firsts = []
# thirds_to_firsts = []
# counter = Counter()
# for pair in grouped.items():
#     agents = pair[1]
#     counter.clear()
#     counter.update(agents)
#     if len(counter) > 1:
#         second_to_first = counter.most_common(2)[1][1] / counter.most_common(2)[0][1]
#         seconds_to_firsts.append(second_to_first)
#         if len(counter) > 2:
#             third_to_first = counter.most_common(3)[2][1] / counter.most_common(3)[0][1]
#             thirds_to_firsts.append(third_to_first)
# print('rel. importance 2nd/1st:', np.mean(seconds_to_firsts), np.std(seconds_to_firsts), np.median(seconds_to_firsts))
# print('rel. importance 3rd/1st:', np.mean(thirds_to_firsts), np.std(thirds_to_firsts), np.median(thirds_to_firsts))
#
# # grouping size ######################
# grouping_applicants = np.unique(df.values[:, 0])
# grouping_ids = []
# art_grouping_ids = []
# for g in grouping_applicants:
#     grouping_ids.append(eval(g[1:])) if g[0] == 'g' else art_grouping_ids.append(1)
# q = """
#     SELECT grouping_id, COUNT(DISTINCT company_id)
#     FROM grouping_relations
#     WHERE grouping_id IN ({})
#     GROUP BY grouping_id
# """.format(str(grouping_ids)[1:-1])
# df_sub = pd.read_sql_query(q, engine)
# temp = df_sub.values[:, 1].tolist()
# temp.extend(art_grouping_ids)
# print('grouping size:', np.mean(temp), np.std(temp), np.median(temp))
#
# # co-occurrences ###################
# q = """SELECT t.patent_id, COUNT(DISTINCT t.grouping_applicant)
#         FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
#         WHERE t.patent_id IN (
#             SELECT t.patent_id
#             FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
#             WHERE t.grouping_agent IS NOT NULL
#             AND t.grouping_applicant IS NOT NULL
#             AND p.ep_application_date >= DATE('2012-01-01')
#             AND t.grouping_applicant IN ({})
#             )
#         GROUP BY t.patent_id
#         HAVING COUNT(DISTINCT t.grouping_applicant) > 1
# """.format(str(grouping_applicants.tolist())[1:-1])
# df_sub = pd.read_sql_query(q, engine)
# print('# co-occurrences:', df_sub.shape[0])
# print('% co-occurrences:', df_sub.shape[0]/number_applications)
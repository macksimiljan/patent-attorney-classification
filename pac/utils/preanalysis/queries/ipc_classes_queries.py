def query_type_frequency_ipc_class(level=1):
    last_char = {
        1: 1,
        2: 3,
    }[level]
    return """
        SELECT SUBSTRING(name, 1, {}) AS ipc_class, COUNT(*) AS abs_freq, COUNT(*)*1.0 / (SELECT COUNT(*) FROM ipc_classes)  AS rel_freq
        FROM ipc_classes
        WHERE LENGTH(SUBSTRING(name, 1, {})) = {}
        GROUP BY SUBSTRING(name, 1, {})
        ORDER BY ipc_class
    """.format(last_char, last_char, last_char, last_char)


# for patents

def query_patent_token_frequency_ipc_class(level=1):
    level_attribute = {
        1: 'first_level_class',
        2: 'second_level_class'
    }[level]
    return """
        SELECT {} AS ipc_class, COUNT(*) AS abs_freq, COUNT(*)*1.0 / (SELECT COUNT(*) FROM view_patents_ipc_classes)  AS rel_freq
        FROM view_patents_ipc_classes
        GROUP BY {}
        ORDER BY {}
    """.format(level_attribute, level_attribute, level_attribute)


def query_patents_without_ipc_class():
    return """
        SELECT COUNT(*)
        FROM (
            SELECT p.id
            FROM epo_patents p
            EXCEPT
            SELECT pc.epo_patent_id
            FROM epo_patents_ipc_classes pc) AS t
    """


def query_distribution_number_patents_over_number_ipc_classes():
    return """
        SELECT t.number AS number_ipc_classes, COUNT(*) AS number_patents
        FROM (
            SELECT epo_patent_id, COUNT(*) AS number
            FROM epo_patents_ipc_classes
            GROUP BY epo_patent_id
            ) AS t
        GROUP BY t.number
        ORDER BY number_ipc_classes           
    """


def query_distribution_number_patents_over_number_ipc_classes_short():
    return """
        SELECT t.number AS number_ipc_classes, COUNT(*) AS number_patents
        FROM (
            SELECT epo_patent_id, COUNT(*) AS number
            FROM (
                SELECT DISTINCT pc.epo_patent_id, SUBSTRING(c.name, 1, 4)
                FROM epo_patents_ipc_classes pc, ipc_classes c
                WHERE pc.ipc_class_id = c.id
                ) AS t0
            GROUP BY epo_patent_id
            ) AS t
        GROUP BY t.number
        ORDER BY number_ipc_classes 
    """

# for agents

def query_agent_token_frequency_ipc_class(level=1):
    level_attribute = {
        1: 'first_level_class',
        2: 'second_level_class'
    }[level]
    return """
            SELECT {} AS ipc_class, COUNT(*) AS abs_freq, COUNT(*)*1.0 / (SELECT COUNT(*) FROM view_agents_ipc_classes)  AS rel_freq
            FROM view_agents_ipc_classes
            GROUP BY {}
            ORDER BY {}
        """.format(level_attribute, level_attribute, level_attribute)


def query_agents_without_ipc_class():
    return """
        SELECT COUNT(*)
        FROM (
            SELECT cp.company_id
            FROM companies_as_agents_epo_patents cp
            EXCEPT
            SELECT cc.company_id
            FROM companies_ipc_classes cc) AS t
    """


def query_distribution_number_agents_over_number_ipc_classes():
    return """
        SELECT t.number AS number_ipc_classes, COUNT(*) AS number_agents
        FROM (
            SELECT company_id, COUNT(*) AS number
            FROM companies_ipc_classes
            GROUP BY company_id
            ) AS t
        GROUP BY t.number
        ORDER BY number_ipc_classes           
    """


def query_distribution_number_agents_over_number_ipc_classes_short():
    return """
        SELECT t.number AS number_ipc_classes, COUNT(*) AS number_agents
        FROM (
            SELECT company_id, COUNT(*) AS number
            FROM (
                SELECT DISTINCT cc.company_id, SUBSTRING(c.name, 1, 4)
                FROM companies_ipc_classes cc, ipc_classes c
                WHERE cc.ipc_class_id = c.id
                ) AS t0
            GROUP BY company_id
            ) AS t
        GROUP BY t.number
        ORDER BY number_ipc_classes 
    """

def query_count_all():
    return """
        SELECT COUNT(*)
        FROM epo_patents
    """


def query_frequency_status_category():
    return """
        SELECT t.status_category, t.number AS abs_freq, t.number * 1.0 / (SELECT COUNT(*) FROM epo_patents) AS rel_freq
        FROM (
            SELECT status_category, COUNT(*) AS number
            FROM epo_patents
            GROUP BY status_category
            ORDER BY number DESC
        ) AS t
    """


def query_frequency_ep_application_date():
    return """
        SELECT to_char(t.ep_application_date_trunc, 'YYYY-MM') AS ep_application_date, t.number AS abs_freq, t.number * 1.0 / (SELECT COUNT(*) FROM epo_patents) AS rel_freq
        FROM (
            SELECT date_trunc('quarter', ep_application_date) AS ep_application_date_trunc, COUNT(*) AS number
            FROM epo_patents
            GROUP BY ep_application_date_trunc
            ORDER BY ep_application_date_trunc
        ) AS t
    """


def query_patents_without_applicants_count():
    return """
        SELECT COUNT(DISTINCT patent_id)
        FROM view_pairing_patent_applicant_agent
        WHERE applicant_id IS NULL
    """


def query_patents_without_agents_count():
    return """
        SELECT COUNT(DISTINCT patent_id)
        FROM view_pairing_patent_applicant_agent
        WHERE agent_id IS NULL
    """


def query_patents_per_month():
    return """
        SELECT date_trunc('month', ep_application_date) AS ep_application_date_month, COUNT(*) AS no
        FROM epo_patents
        GROUP BY date_trunc('month', ep_application_date)
        ORDER BY date_trunc('month', ep_application_date)
    """

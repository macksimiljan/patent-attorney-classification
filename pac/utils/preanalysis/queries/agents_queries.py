def query_agents_count():
    return """
        SELECT COUNT(*)
        FROM view_agents
    """


def query_agents_that_are_applicants():
    return """
        SELECT COUNT(*)
        FROM (
            SELECT id
            FROM view_agents
            INTERSECT
            SELECT id
            FROM view_applicants
            ) AS t
    """


def query_patents_per_agent():
    return """
        SELECT company_id, COUNT(*) AS abs_freq, ROW_NUMBER() OVER (ORDER BY COUNT(*) DESC) AS rank
        FROM companies_as_agents_epo_patents
        GROUP BY company_id
    """


def query_agents_per_patent():
    return """
        SELECT *
        FROM 
            (SELECT 0 AS company_count, COUNT(DISTINCT patent_id) AS patent_count
            FROM view_pairing_patent_applicant_agent
            WHERE agent_id IS NULL
            UNION
            SELECT company_count, COUNT(*) AS patent_count
            FROM (
                SELECT epo_patent_id, COUNT(*) AS company_count
                FROM companies_as_agents_epo_patents
                GROUP BY epo_patent_id) AS t0
            GROUP BY company_count) AS t1
        ORDER BY company_count
    """


def query_out_of_business_agents_count():
    return """
        SELECT COUNT(*)
        FROM view_agents a
        WHERE a."end" IS NOT NULL 
        AND LENGTH(a."end") > 0
    """


def query_agents_per_number_of_employees():
    return """
        SELECT a.category_number_of_employee_id AS category_id,
                COUNT(*) AS abs_freq,
                COUNT(*)*1.0 / (SELECT COUNT(*) FROM view_agents) AS rel_freq
        FROM view_agents a
        GROUP BY a.category_number_of_employee_id
        ORDER BY a.category_number_of_employee_id
    """


def query_initiatives_count():
    return """
        SELECT COUNT(*)
        FROM view_agents a
        WHERE a.is_initiative = true
    """


def query_agents_per_quality_score():
    return """
        SELECT a.quality_score / 10 AS quality_score,
            COUNT(*) AS abs_freq,
            COUNT(*)*1.0 / (SELECT COUNT(*) FROM view_agents) AS rel_freq
        FROM view_agents a
        GROUP BY a.quality_score / 10
        ORDER BY a.quality_score / 10
    """
CREATE OR REPLACE VIEW view_agents
AS
SELECT c.id,
		c.slug,
		c.is_tto,
		c.is_research_organisation,
		c.is_service_provider,
    c.is_technology_company,
		c.quality_score,
		ARRAY_AGG(DISTINCT cities.full_name) as cities,
		ARRAY_AGG(DISTINCT cities.country)  as countries,
		ARRAY_AGG('(' || cities.latitude || ', ' || cities.longitude || ')') as lat_lon_pairs,
		COUNT(*) as no_locations_agent
FROM companies c LEFT OUTER JOIN locations l ON (c.id = l.company_id) LEFT OUTER JOIN cities ON (l.city_id = cities.id)
WHERE c.id IN (SELECT company_id FROM companies_as_agents_epo_patents)
GROUP BY c.id,
		c.slug,
		c.is_tto,
		c.is_research_organisation,
		c.is_service_provider,
    c.is_technology_company,
		c.quality_score;

-----

CREATE OR REPLACE VIEW view_agent_complete
AS
SELECT c.id,
		c.slug,
		c.is_tto,
		c.is_research_organisation,
		c.is_service_provider,
    c.is_technology_company,
		c.quality_score,
		ARRAY_AGG(DISTINCT cities.full_name) as cities,
		ARRAY_AGG(DISTINCT cities.country)  as countries,
		ARRAY_AGG(DISTINCT '(' || cities.latitude || ', ' || cities.longitude || ')') as lat_lon_pairs,
		COUNT(DISTINCT l.id) as locations_count,
		ARRAY_AGG(DISTINCT e.person_id) as employees,
		COUNT(DISTINCT e.person_id) as employee_count,
		ARRAY_AGG(DISTINCT ipc_classes.name )AS second_level_classes,
		ARRAY_AGG(DISTINCT SUBSTRING(ipc_classes.name, 1, 1)) AS first_level_classes
FROM companies c
		LEFT OUTER JOIN locations l ON (c.id = l.company_id)
		LEFT OUTER JOIN cities ON (l.city_id = cities.id)
		LEFT OUTER JOIN key_employments e ON (e.company_id = c.id)
		LEFT OUTER JOIN companies_ipc_classes ON (companies_ipc_classes.company_id = c.id)
		LEFT OUTER JOIN ipc_classes ON (companies_ipc_classes.ipc_class_id = ipc_classes.id)
WHERE c.id IN (SELECT company_id FROM companies_as_agents_epo_patents)
GROUP BY c.id,
		c.slug,
		c.is_tto,
		c.is_research_organisation,
		c.is_service_provider,
    c.is_technology_company,
		c.quality_score;

-----

CREATE OR REPLACE VIEW view_applicants
AS
SELECT c.id,
		c.slug,
		c.is_tto,
		c.is_research_organisation,
		c.is_service_provider,
    c.is_technology_company,
		c.quality_score,
		ARRAY_AGG(DISTINCT cities.full_name) as cities,
		ARRAY_AGG(DISTINCT cities.country)  as countries,
		ARRAY_AGG('(' || cities.latitude || ', ' || cities.longitude || ')') as lat_lon_pairs,
		COUNT(*) as no_locations_applicant
FROM companies c LEFT OUTER JOIN locations l ON (c.id = l.company_id) LEFT OUTER JOIN cities ON (l.city_id = cities.id)
WHERE c.id IN (SELECT company_id FROM companies_as_applicants_epo_patents)
GROUP BY c.id,
		c.slug,
		c.is_tto,
		c.is_research_organisation,
		c.is_service_provider,
    	c.is_technology_company,
		c.quality_score;

-----

CREATE OR REPLACE VIEW view_patents
AS
SELECT p.id, p.patent_id, p.status_category, p.ep_application_date
FROM epo_patents p;

-----

CREATE OR REPLACE VIEW view_patents_complete
AS
SELECT p.id,
		p.patent_id,
		p.ep_application_date,
		p.status_category,
		ARRAY_AGG(DISTINCT epo_patents_ipc_classes.ipc_class_id) as ipc_classes_id,
		ARRAY_AGG(DISTINCT SUBSTRING(ipc_classes.name, 1, 3)) AS second_level_classes,
    ARRAY_AGG(DISTINCT SUBSTRING(name, 1, 1)) AS first_level_classes,
		COUNT(*) as ipc_classes_count
FROM epo_patents p
		LEFT OUTER JOIN epo_patents_ipc_classes ON (p.id = epo_patents_ipc_classes.epo_patent_id)
		LEFT OUTER JOIN ipc_classes ON (epo_patents_ipc_classes.ipc_class_id = ipc_classes.id)
GROUP BY p.id,
		p.patent_id,
		p.ep_application_date,
		p.status_category;

---

CREATE OR REPLACE VIEW view_agents_ipc_classes
AS
SELECT company_id AS agent_id, ipc_class_id, name AS second_level_class, SUBSTRING(name, 1, 1) AS first_level_class
FROM companies_ipc_classes JOIN ipc_classes ON companies_ipc_classes.ipc_class_id = ipc_classes.id;

-----

CREATE OR REPLACE VIEW view_patents_ipc_classes
AS
SELECT epo_patents_ipc_classes.epo_patent_id AS patent_id,
		epo_patents_ipc_classes.ipc_class_id,
		ipc_classes.name,
		SUBSTRING(ipc_classes.name, 1, 3) AS second_level_class,
    SUBSTRING(name, 1, 1) AS first_level_class
FROM epo_patents_ipc_classes JOIN ipc_classes ON epo_patents_ipc_classes.ipc_class_id = ipc_classes.id;

-----

CREATE OR REPLACE VIEW view_applicants_locations
AS
SELECT
		l.company_id AS applicant_id,
		CASE WHEN LENGTH(l."start") = 0 THEN NULL ELSE l."start" END,
		CASE WHEN LENGTH(l."end") = 0 THEN NULL ELSE l."end" END,
		c.id AS city_id,
		c.full_name AS city_name,
		c.country AS city_country,
		c.area_level1, c.area_level2, c.area_level3
FROM locations l JOIN cities c ON l.city_id = c.id
WHERE company_id IN (SELECT id FROM view_applicants);

-----

CREATE OR REPLACE VIEW view_agents_locations
AS
SELECT
		l.company_id AS agent_id,
		CASE WHEN LENGTH(l."start") = 0 THEN NULL ELSE l."start" END,
		CASE WHEN LENGTH(l."end") = 0 THEN NULL ELSE l."end" END,
		c.id AS city_id,
		c.full_name AS city_name,
		c.country AS city_country,
		c.area_level1, c.area_level2, c.area_level3
FROM locations l JOIN cities c ON l.city_id = c.id
WHERE company_id IN (SELECT id FROM view_agents);

-----

CREATE OR REPLACE VIEW view_pairing_patent_applicant_agent
AS
SELECT patents_applicants.patent_id, patents_applicants.applicant_id, agents_patents.agent_id
FROM
	(
		SELECT epo_patents.id AS patent_id, companies_as_applicants_epo_patents.company_id AS applicant_id
		FROM epo_patents
			LEFT OUTER JOIN
			companies_as_applicants_epo_patents ON epo_patents.id = companies_as_applicants_epo_patents.epo_patent_id
	) AS patents_applicants
	LEFT OUTER JOIN
	(
		SELECT companies_as_agents_epo_patents.company_id AS agent_id, companies_as_agents_epo_patents.epo_patent_id AS patent_id
		FROM companies_as_agents_epo_patents
	) AS agents_patents
	ON patents_applicants.patent_id = agents_patents.patent_id;

-----

CREATE OR REPLACE VIEW view_grouping_applicants
AS
SELECT DISTINCT ap.company_id AS applicant, CASE WHEN gr.grouping_id IS NULL THEN 'a' || ap.company_id ELSE 'g' || gr.grouping_id END AS artifical_grouping
FROM companies_as_applicants_epo_patents ap
		LEFT OUTER JOIN grouping_relations gr ON (ap.company_id = gr.company_id);

-----

CREATE OR REPLACE VIEW view_grouping_agents
AS
SELECT DISTINCT ap.company_id AS agent, CASE WHEN gr.grouping_id IS NULL THEN 'a' || ap.company_id ELSE 'g' || gr.grouping_id END AS artifical_grouping
FROM companies_as_agents_epo_patents ap
		LEFT OUTER JOIN grouping_relations gr ON (ap.company_id = gr.company_id);

-----

CREATE OR REPLACE VIEW view_triplets
AS
SELECT p.*, g_appl.artifical_grouping AS grouping_applicant, g_agent.artifical_grouping AS grouping_agent
FROM view_pairing_patent_applicant_agent p
		LEFT OUTER JOIN view_grouping_applicants g_appl ON p.applicant_id = g_appl.applicant
		LEFT OUTER JOIN view_grouping_agents g_agent ON p.agent_id = g_agent.agent;

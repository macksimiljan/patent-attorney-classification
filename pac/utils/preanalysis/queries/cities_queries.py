def query_applicants_per_top_ten_cities():
    return """
        SELECT l.city_name, l.city_country, l.area_level1, l.area_level2, l.area_level3,
            COUNT(*) AS abs_freq,
            COUNT(*)*1.0 / (SELECT COUNT(*) FROM view_applicants_locations) AS rel_freq
        FROM view_applicants_locations l
        GROUP BY l.city_name, l.city_country, l.area_level1, l.area_level2, l.area_level3
        ORDER BY COUNT(*) DESC
        LIMIT(10)
    """


def query_applicants_per_country(minimum_rel_freq):
    return """
        SELECT l.city_country, COUNT(*) AS abs_freq, COUNT(*)*1.0 / (SELECT COUNT(*) FROM view_applicants_locations) AS rel_freq
        FROM view_applicants_locations l
        GROUP BY l.city_country
        HAVING COUNT(*)*1.0 / (SELECT COUNT(*) FROM view_applicants_locations) >= {}
        ORDER BY l.city_country
    """.format(minimum_rel_freq)


def query_applicants_per_count_cities():
    return """
        SELECT city_count, COUNT(*) AS company_count
        FROM (
            SELECT a.id AS applicant_id, 0 AS city_count
            FROM view_applicants a LEFT OUTER JOIN  view_applicants_locations l ON a.id = l.applicant_id
            WHERE l.city_name IS NULL
            UNION
            SELECT a.id AS applicant_id, COUNT(*) AS city_count
            FROM view_applicants a LEFT OUTER JOIN  view_applicants_locations l ON a.id = l.applicant_id
            WHERE l.city_name IS NOT NULL
            GROUP BY a.id
            ) AS t
        GROUP BY city_count
        ORDER BY city_count
    """


def query_agents_per_top_ten_cities():
    return """
        SELECT l.city_name, l.city_country, l.area_level1, l.area_level2, l.area_level3,
            COUNT(*) AS abs_freq,
            COUNT(*)*1.0 / (SELECT COUNT(*) FROM view_agents_locations) AS rel_freq
        FROM view_agents_locations l
        GROUP BY l.city_name, l.city_country, l.area_level1, l.area_level2, l.area_level3
        ORDER BY COUNT(*) DESC
        LIMIT(10)
    """


def query_agents_per_country(minimum_rel_freq):
    return """
        SELECT l.city_country, COUNT(*) AS abs_freq, COUNT(*)*1.0 / (SELECT COUNT(*) FROM view_agents_locations) AS rel_freq
        FROM view_agents_locations l
        GROUP BY l.city_country
        HAVING COUNT(*)*1.0 / (SELECT COUNT(*) FROM view_agents_locations) >= {}
        ORDER BY l.city_country
    """.format(minimum_rel_freq)


def query_agents_per_count_cities():
    return """
        SELECT city_count, COUNT(*) AS company_count
        FROM (
            SELECT a.id AS agent_id, 0 AS city_count
            FROM view_agents a LEFT OUTER JOIN  view_agents_locations l ON a.id = l.agent_id
            WHERE l.city_name IS NULL
            UNION
            SELECT a.id AS agent_id, COUNT(*) AS city_count
            FROM view_agents a LEFT OUTER JOIN  view_agents_locations l ON a.id = l.agent_id
            WHERE l.city_name IS NOT NULL
            GROUP BY a.id
            ) AS t
        GROUP BY city_count
        ORDER BY city_count
    """
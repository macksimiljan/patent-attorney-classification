def query_patent_applicant_agent_combination_per_quarter():
    return """
        SELECT quarter, COUNT(*) AS number_combinations
        FROM (
            SELECT v.*, 
                p.ep_application_date,
                date_trunc('quarter', p.ep_application_date) AS quarter
            FROM view_pairing_patent_applicant_agent v JOIN epo_patents p ON v.patent_id = p.id
            ) AS t
        GROUP BY quarter
        ORDER BY quarter
    """
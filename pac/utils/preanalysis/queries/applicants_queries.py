def query_applicants_count():
    return """
        SELECT COUNT(*)
        FROM view_applicants
    """


def query_patents_per_applicant():
    return """
        SELECT company_id, COUNT(*) AS abs_freq, ROW_NUMBER() OVER (ORDER BY COUNT(*) DESC) AS rank
        FROM companies_as_applicants_epo_patents
        GROUP BY company_id
    """


def query_applicants_per_patent():
    return """
        SELECT *
        FROM 
            (SELECT 0 AS company_count, COUNT(DISTINCT patent_id) AS patent_count
            FROM view_pairing_patent_applicant_agent
            WHERE applicant_id IS NULL
            UNION
            SELECT company_count, COUNT(*) AS patent_count
            FROM (
                SELECT epo_patent_id, COUNT(*) AS company_count
                FROM companies_as_applicants_epo_patents
                GROUP BY epo_patent_id) AS t0
            GROUP BY company_count) AS t1
        ORDER BY company_count
    """


def query_out_of_business_applicants_count():
    return """
        SELECT COUNT(*)
        FROM view_applicants a
        WHERE a."end" IS NOT NULL 
        AND LENGTH(a."end") > 0
    """


def query_applicants_per_number_of_employees():
    return """
        SELECT a.category_number_of_employee_id AS category_id,
                COUNT(*) AS abs_freq,
                COUNT(*)*1.0 / (SELECT COUNT(*) FROM view_applicants) AS rel_freq
        FROM view_applicants a
        GROUP BY a.category_number_of_employee_id
        ORDER BY a.category_number_of_employee_id
    """


def query_initiatives_count():
    return """
        SELECT COUNT(*)
        FROM view_applicants a
        WHERE a.is_initiative = true
    """


def query_applicants_per_quality_score():
    return """
        SELECT a.quality_score / 10 AS quality_score,
            COUNT(*) AS abs_freq,
            COUNT(*)*1.0 / (SELECT COUNT(*) FROM view_applicants) AS rel_freq
        FROM view_applicants a
        GROUP BY a.quality_score / 10
        ORDER BY a.quality_score / 10
    """


def query_cooccurrences_applicants():
    return """
        SELECT applicants, applicants_size, COUNT(*) patents_count, ROW_NUMBER () OVER (ORDER BY COUNT(*) DESC) AS rank
        FROM (
            SELECT patent_id, ARRAY_AGG(applicant_id) AS applicants, COUNT(*) AS applicants_size
            FROM (SELECT DISTINCT patent_id, applicant_id
            FROM view_pairing_patent_applicant_agent) AS t
            GROUP BY patent_id
            HAVING COUNT(*) > 1
        ) AS t1
        GROUP BY applicants, applicants_size
    """


def query_agents_per_applicant():
    return """
        SELECT applicant_id, COUNT(*) AS abs_freq, ROW_NUMBER () OVER (ORDER BY COUNT(*) DESC) AS rank
        FROM (
            SELECT DISTINCT applicant_id, agent_id
            FROM view_pairing_patent_applicant_agent
            WHERE applicant_id IS NOT NULL AND agent_id IS NOT NULL) AS t
        GROUP BY applicant_id
    """


def query_applicants_per_agent():
    return """
        SELECT agent_id, COUNT(*) AS abs_freq, ROW_NUMBER () OVER (ORDER BY COUNT(*) DESC) AS rank
        FROM (
            SELECT DISTINCT applicant_id, agent_id
            FROM view_pairing_patent_applicant_agent
            WHERE applicant_id IS NOT NULL AND agent_id IS NOT NULL) AS t
        GROUP BY agent_id
    """
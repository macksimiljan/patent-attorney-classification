def non_unique_agents_per_applicant():
    return """
        SELECT t.grouping_applicant as applicant_grouping, COUNT(*) as agent_groupings_size
        FROM view_triplets t INNER JOIN view_patents p ON (t.patent_id = p.id )
        WHERE t.grouping_applicant IS NOT NULL
        AND t.grouping_agent IS NOT NULL
        AND p.ep_application_date >= DATE('2012-01-01')
        AND p.ep_application_date < DATE('2018-01-01')
        GROUP BY t.grouping_applicant
        ORDER BY agent_groupings_size DESC
    """


def unique_agents_per_applicant():
    return """
        SELECT t.grouping_applicant as applicant_grouping, COUNT(DISTINCT t.grouping_agent) as agent_groupings_size
        FROM view_triplets t INNER JOIN view_patents p ON (t.patent_id = p.id )
        WHERE t.grouping_applicant IS NOT NULL
        AND t.grouping_agent IS NOT NULL
        AND p.ep_application_date >= DATE('2012-01-01')
        AND p.ep_application_date < DATE('2018-01-01')
        GROUP BY t.grouping_applicant
        ORDER BY agent_groupings_size DESC 
    """


def agent_sequences_per_applicant():
    return """
        SELECT tab.grouping_applicant as applicant_grouping, JSON_AGG(tab.grouping_agent) as agent_groupings
        FROM (
            SELECT t.grouping_applicant, t.grouping_agent
            FROM view_triplets t INNER JOIN view_patents p ON (t.patent_id = p.id )
            WHERE t.grouping_applicant IS NOT NULL
            AND t.grouping_agent IS NOT NULL
            AND p.ep_application_date >= DATE('2012-01-01')
            AND p.ep_application_date < DATE('2018-01-01')
            ORDER BY t.grouping_applicant, p.ep_application_date
            ) AS tab
        GROUP BY tab.grouping_applicant
    """

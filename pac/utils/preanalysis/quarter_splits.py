from datetime import datetime

import pandas as pd

from pac.utils.database.connection_point import instantiate_engine
from pac.utils.plotter.histograms import show_histogram
from pac.preanalysis.queries.quarter_splits_queries import *


def run_quarter_splits_preanalysis(text_file, figures_path):
    engine = instantiate_engine()
    result = pd.read_sql_query(query_patent_applicant_agent_combination_per_quarter(), engine)

    with open(text_file, 'a') as f:
        f.write('===== Quarter Splits {} =====\n'.format(datetime.now()))
        f.write('mean: {}'.format(result.iloc[0:-1].mean(axis=0)))
        f.write('std: {}'.format(result.iloc[0:-1].std(axis=0)))

    path = '{}/{}'.format(figures_path, 'quarter_splits_new_triples.eps')
    show_histogram(result.iloc[0:-1]['quarter'], result.iloc[0:-1]['number_combinations'],
                   'Quarter', '#New Triples', figure_path=path, date_on_x_axis=True)


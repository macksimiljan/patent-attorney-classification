from collections import Counter

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.stats import pearsonr
from sklearn.preprocessing import StandardScaler

import pac.utils.preanalysis.queries.customer_agent_relations_queries as q
from pac.utils.database.connection_point import instantiate_engine

engine = instantiate_engine()
result = pd.read_sql_query(q.non_unique_agents_per_applicant(), engine)
values_non_unique = result['agent_groupings_size'].values
mean_non_unique = np.mean(values_non_unique)
std = np.std(values_non_unique)
median_non_unique = np.median(values_non_unique)
print(mean_non_unique, median_non_unique)
values_non_unique_counted = Counter(values_non_unique).items()
values_non_unique_counted = sorted(values_non_unique_counted, key=lambda x: x[0])
x_values = list(map(lambda x: x[0], values_non_unique_counted))
y_values = list(map(lambda x: x[1], values_non_unique_counted))
plt.plot(x_values, y_values)
plt.plot([mean_non_unique, mean_non_unique], [1100, -35], '--', label='mean_non_unique')
plt.plot([median_non_unique, median_non_unique], [1100, -35], ':', label='median_non_unique')
plt.xlabel('# Non-Unique Agents per Applicant')
plt.ylabel('#')
plt.xscale('log')
plt.legend(loc='best')
plt.savefig('agent_groupings_size.eps')
plt.show()

result_unique = pd.read_sql_query(q.unique_agents_per_applicant(), engine)
values_unique = result_unique['agent_groupings_size'].values
mean_unique = np.mean(values_unique)
median_unique = np.median(values_unique)
print(mean_unique, median_unique)
values_unique_counted = Counter(values_unique).items()
values_unique_counted = sorted(values_unique_counted, key=lambda x: x[0])
x_values = list(map(lambda x: x[0], values_unique_counted))
y_values = list(map(lambda x: x[1], values_unique_counted))
plt.plot(x_values, y_values)
plt.plot([mean_unique, mean_unique], [2200, -35], '--', label='mean_unique')
plt.plot([median_unique, median_unique], [2200, -35], ':', label='median_unique')
plt.xlabel('# Unique Agents per Applicant')
plt.ylabel('#')
plt.xscale('log')
plt.legend(loc='best')
plt.savefig('agent_groupings_size_unique.eps')
plt.show()


results_merged = pd.merge(result, result_unique,
                          how='inner',
                          on='applicant_grouping',
                          suffixes=('_non_unique', '_unique'))
non_unique = results_merged['agent_groupings_size_non_unique']

unique = results_merged['agent_groupings_size_unique']

lb, rb, lt, rt = 0, 0, 0, 0
for nu, u in zip(non_unique, unique):
    if nu <= mean_non_unique and u <= mean_unique:
        lb += 1
    elif nu <= mean_non_unique and u > mean_unique:
        lt += 1
    elif nu > mean_non_unique and u <= mean_unique:
        rb += 1
    else:
        rt += 1

print('lb', round(lb/unique.values.shape[0], 3), 'rb', round(rb/unique.values.shape[0], 3),
      'lt', round(lt/unique.values.shape[0], 3), 'rt', round(rt/unique.values.shape[0], 3))

colors = []
for nu, u in zip(non_unique, unique):
    if nu <= mean_non_unique and u <= mean_unique:
        colors.append(sns.color_palette("Paired")[2])
    elif nu <= mean_non_unique and u > mean_unique:
        colors.append(sns.color_palette("Paired")[0])
    elif nu > mean_non_unique and u <= mean_unique:
        colors.append(sns.color_palette("Paired")[3])
    else:
        colors.append(sns.color_palette("Paired")[1])

plt.figure(figsize=(6,4))
plt.scatter(non_unique, unique, c=colors, s=25)
plt.xlabel('# Patent-Anmeldungen')
plt.ylabel('# Anwälte')
plt.xscale('log')
plt.yscale('log')
plt.ylim(ymin=0.5, ymax=200)
plt.savefig('correlation.eps')
plt.show()

non_unique = StandardScaler().fit_transform(non_unique.values.reshape(-1, 1))
unique = StandardScaler().fit_transform(unique.values.reshape(-1, 1))
print(pearsonr(non_unique, unique))

rel_frequencies_of_most_frequent_agent = []
rel_frequencies_of_second_frequent_agent = []
rel_frequencies_of_third_frequent_agent = []
rel_frequencies_of_fourth_frequent_agent = []
example_multinominals = []
baseline_correct = {'yes': 0, 'no': 0}
agent_frequencies = Counter()
result = pd.read_sql_query(q.agent_sequences_per_applicant(), engine)
german_applicants = pd.read_sql_query("""SELECT DISTINCT a.artifical_grouping
                                        FROM view_grouping_applicants a, locations l, cities c
                                        WHERE a.applicant = l.company_id
                                        AND l.city_id = c.id
                                        AND l.category_location_id = 1
                                        AND c.country = 'Germany';""", instantiate_engine()).values.reshape(-1)
counter = Counter()
german_agents = []
procter_agents = []
all_agents = []
for row in result.itertuples():
    counter.clear()
    agents = row[2]
    applicant = row[1]
    counter.update(agents)
    agent_frequencies.update(agents)
    highest_frequency = counter.most_common(1)[0][1]
    second_frequency = counter.most_common(2)[1][1] if len(counter.most_common(2)) >= 2 else -1
    third_frequencies = counter.most_common(3)[2][1] if len(counter.most_common(3)) >= 3 else -1
    fourth_frequencies = counter.most_common(4)[3][1] if len(counter.most_common(4)) >= 4 else -1
    total = len(agents)
    rel_frequencies_of_most_frequent_agent.append(highest_frequency / total)
    rel_frequencies_of_second_frequent_agent.append(second_frequency / total)
    rel_frequencies_of_third_frequent_agent.append(third_frequencies / total)
    rel_frequencies_of_fourth_frequent_agent.append(fourth_frequencies / total)
    baseline_correct['yes'] += highest_frequency
    baseline_correct['no'] += (total - highest_frequency)

    if applicant == 'g536':
        procter_agents.extend(row[2])
    if applicant in german_applicants:
        german_agents.extend(row[2])
    if applicant is not None and len(applicant) > 0:
        all_agents.extend(row[2])



def mapping(val):
    if val >= 0.999:
        val = 1.0
    elif val >= 0.899:
        val = 0.9
    elif val >= 0.799:
        val = 0.8
    elif val >= 0.699:
        val = 0.7
    elif val >= 0.599:
        val = 0.6
    elif val >= 0.499:
        val = 0.5
    elif val >= 0.399:
        val = 0.4
    elif val >= 0.299:
        val = 0.3
    elif val >= 0.199:
        val = 0.2
    elif val >= 0.099:
        val = 0.1
    elif val >= 0.0:
        val = 0.0
    else:
        val = None

    return '>= {}'.format(val) if val is not None else 'not def.'


rel_frequencies_of_most_frequent_agent = list(map(lambda x: mapping(x), rel_frequencies_of_most_frequent_agent))
rel_frequencies_of_second_frequent_agent = list(map(lambda x: mapping(x), rel_frequencies_of_second_frequent_agent))
rel_frequencies_of_third_frequent_agent = list(map(lambda x: mapping(x), rel_frequencies_of_third_frequent_agent))
rel_frequencies_of_fourth_frequent_agent = list(map(lambda x: mapping(x), rel_frequencies_of_fourth_frequent_agent))


def plot_frequencies(distribution, x_label, file_name):
    counter = Counter(distribution)
    total = sum(counter.values())
    data = list(map(lambda pair: (pair[0], pair[1]/total), counter.items()))
    data = sorted(data, key=lambda x: (False, x[0]) if x[0] == 'not def.' else (True, x[0]), reverse=True)
    x_values = list(map(lambda p: p[0], data))
    y_values = list(map(lambda p: p[1], data))
    y_values_cumulative = np.cumsum(y_values)
    print(data)
    print(y_values_cumulative)

    plt.bar(range(1, len(x_values) + 1),
            y_values,
            align='center')
    plt.step(range(1, len(x_values) + 1),
             y_values_cumulative,
             where='mid',
             label='cumulative sum',
             color=sns.color_palette()[1])
    plt.xticks(range(1, len(x_values) + 1), x_values)
    plt.legend(loc='best')

    plt.ylabel('Rel. Frequency over all Applicant Groupings')
    plt.xlabel(x_label)
    plt.savefig(file_name)
    plt.show()


plot_frequencies(rel_frequencies_of_most_frequent_agent,
                 'Served Applications by the Most Frequent Agent Grouping', 'freq_most_frequent_agent.eps')
plot_frequencies(rel_frequencies_of_second_frequent_agent,
                 'Served Applications by the 2nd Most Frequent Agent Grouping', 'freq_second_frequent_agent.eps')
plot_frequencies(rel_frequencies_of_third_frequent_agent,
                 'Served Applications by the 3rd Most Frequent Agent Grouping', 'freq_third_frequent_agent.eps')
plot_frequencies(rel_frequencies_of_fourth_frequent_agent,
                 'Served Applications by the 4th Most Frequent Agent Grouping', 'freq_fourth_frequent_agent.eps')

agents = set(all_agents)
agents = sorted(list(agents))

for i, agents_to_draw in enumerate([procter_agents, german_agents, all_agents]):
    counter = Counter(agents_to_draw)
    counts = list(counter.items())
    # for agent in agents:
    #     if agent not in counter:
    #         counts.append((agent, 0))
    counts = sorted(counts, key=lambda x: -x[1])
    x_values = list(map(lambda x: x[0], counts))
    y_values = list(map(lambda x: x[1], counts))
    label = ["Procter & Gamble Group", "Deutsche Patent-Anmelder", "Alle Patent-Anmelder"][i]
    plt.figure(figsize=(6,4))
    plt.bar(range(1, len(x_values) + 1),
            y_values,
            align='center',
            label=label)
    plt.yscale('log')
    # plt.ylim(ymax=4500, ymin=0.8)
    plt.xlim(xmin=0.5)
    plt.ylabel('# Patent-Anmeldungen')
    plt.xlabel('Rang Anwalt')
    plt.savefig('agent_distribution_{}.eps'.format(label))
    plt.show()


print(baseline_correct)

# -----------------------------------------------------
total = sum(agent_frequencies.values())
mean = np.mean(list(agent_frequencies.values()))
median = np.median(list(agent_frequencies.values()))
values = Counter(agent_frequencies.values()).items()
values = sorted(values, key=lambda x: x[0])
x_values = list(map(lambda x: x[0], values))
y_values = list(map(lambda x: x[1], values))
print(agent_frequencies[0], agent_frequencies[1], total, mean, median)
plt.plot(x_values, y_values)
plt.plot([mean, mean],
         [-10, 210],
         label='mean',
         linestyle='--')
plt.plot([median, median],
         [-10, 210],
         label='median',
         linestyle=':')
plt.xscale('log')
plt.xlabel('Abs. Frequency of Agent Grouping')
plt.ylabel('#')
plt.legend(loc='best')
plt.savefig('agent_probabilities.eps')
plt.show()


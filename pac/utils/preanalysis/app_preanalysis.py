import os

import yaml

from pac.utils.database.connection_point import open_connection
from pac.preanalysis.agents import run_agents_preanalysis
from pac.preanalysis.applicants import run_applicants_preanalysis
from pac.preanalysis.cities import run_cities_preanalysis
from pac.preanalysis.ipc_classes import run_ipc_classes_preanalysis
from pac.preanalysis.patents import run_patents_preanalysis
from pac.preanalysis.quarter_splits import run_quarter_splits_preanalysis

connection = open_connection()
path = os.path.abspath(__file__)
dir_path = os.path.dirname(path)
with open(dir_path+'/../../file_paths_config.yml') as f:
    path_config = yaml.load(f)
text_file = path_config['preanalysis']['text']
figure_path = path_config['preanalysis']['figures']

run_patents_preanalysis(connection, text_file, figure_path)
run_ipc_classes_preanalysis(connection, text_file, figure_path)
run_applicants_preanalysis(connection, text_file, figure_path)
run_cities_preanalysis(connection, text_file, figure_path)
run_agents_preanalysis(connection, text_file, figure_path)
run_quarter_splits_preanalysis(text_file, figure_path)

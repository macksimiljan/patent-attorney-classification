from datetime import datetime

import pandas as pd

from pac.utils.database.connection_point import fetch_multiple_rows
from pac.utils.database.connection_point import fetch_single_row
from pac.utils.database.connection_point import instantiate_engine
from pac.utils.plotter.histograms import map_to_histogram_data
from pac.utils.plotter.histograms import show_histogram
from pac.utils.plotter.scatters import show_scatter_plot
from pac.preanalysis.queries.ipc_classes_queries import *


def run_ipc_classes_preanalysis(connection, text_file, figures_path):
    engine = instantiate_engine()

    with open(text_file, 'a') as f:
        f.write('===== IPC Classes {} =====\n'.format(datetime.now()))

    # IPC type frequency
    result_set = type_frequency_ipc_class(connection, level=1)
    x_values, y_values = map_to_histogram_data(result_set, 'ipc_class', 'rel_freq')
    path = '{}/{}'.format(figures_path, 'ipc_classes_1st_level_types.eps')
    show_histogram(x_values, y_values, 'IPC 1st Level', '%Types', figure_path=path)

    # IPC token frequency, level 1
    result_agent = pd.read_sql_query(query_agent_token_frequency_ipc_class(1), engine)
    result_patent = pd.read_sql_query(query_patent_token_frequency_ipc_class(1), engine)
    result_merged = result_agent.merge(result_patent, how='outer', on='ipc_class', sort=True,
                                       suffixes=['_agent', '_patent'])
    path = '{}/{}'.format(figures_path, 'ipc_classes_token_frequency_1st_level.eps')
    show_scatter_plot(result_merged['rel_freq_agent'], result_merged['rel_freq_patent'], 'Prop. Agents', 'Prop. Patent Applications',
                      dot_labels=result_agent['ipc_class'], figure_path=path)

    # IPC token frequency, level 2
    result_agent = pd.read_sql_query(query_agent_token_frequency_ipc_class(2), engine)
    result_patent = pd.read_sql_query(query_patent_token_frequency_ipc_class(2), engine)
    result_merged = result_agent.merge(result_patent, how='outer', on='ipc_class', sort=True,
                                       suffixes=['_agent', '_patent'])
    path = '{}/{}'.format(figures_path, 'ipc_classes_token_frequency_2nd_level.eps')
    show_scatter_plot(result_merged['rel_freq_agent'], result_merged['rel_freq_patent'], '%Agent', '%Patent',
                      dot_labels=result_agent['ipc_class'], figure_path=path)

    patents_without_ipc_class = number_patents_without_ipc_class(connection)
    distribution = distribution_number_patents_over_number_ipc_classes(connection)
    number_ipc_classes, number_patents = map_to_histogram_data(distribution, 'number_ipc_classes',
                                                               'number_patents', null_value=patents_without_ipc_class)
    path = '{}/{}'.format(figures_path, 'ipc_classes_ipc_class_patent.eps')
    show_histogram(number_ipc_classes, number_patents, '#IPC Classes per Patent', '#Patents',
                   y_logarithmic=True, figure_path=path, maximum={'x': 30, 'y': 10**6})

    distribution = distribution_number_patents_over_number_ipc_classes_short(connection)
    number_ipc_classes, number_patents = map_to_histogram_data(distribution, 'number_ipc_classes',
                                                               'number_patents', null_value=patents_without_ipc_class)
    path = '{}/{}'.format(figures_path, 'ipc_classes_class_short_patents.eps')
    show_histogram(number_ipc_classes, number_patents, '#IPC Classes 2nd Level per Patent', '%Patents',
                   y_logarithmic=True, figure_path=path, maximum={'x': 20, 'y': 10**6}, x_ticks=[0,5,10,15,20])

    agents_without_ipc_class = number_patents_without_ipc_class(connection)
    distribution = distribution_number_agents_over_number_ipc_classes(connection)
    number_ipc_classes, number_agents = map_to_histogram_data(distribution, 'number_ipc_classes',
                                                               'number_agents', null_value=agents_without_ipc_class)
    path = '{}/{}'.format(figures_path, 'ipc_classes_ipc_class_agent.eps')
    show_histogram(number_ipc_classes, number_agents, '#IPC Classes 2nd Level per Agent', '%Agents',
                   y_logarithmic=True, figure_path=path, maximum={'x': 17, 'y': 10**6}, x_ticks=[0,5,10,15,20])

    distribution = distribution_number_agents_over_number_ipc_classes_short(connection)
    number_ipc_classes, number_agents = map_to_histogram_data(distribution, 'number_ipc_classes',
                                                               'number_agents', null_value=agents_without_ipc_class)
    path = '{}/{}'.format(figures_path, 'ipc_classes_ipc_class_short_agent.eps')
    show_histogram(number_ipc_classes, number_agents, 'number_ipc_classes_short', 'number_agents',
                   y_logarithmic=True, figure_path=path, maximum={'x': 30, 'y': 10**6})


def type_frequency_ipc_class(connection, level=1):
    result = fetch_multiple_rows(connection, query_type_frequency_ipc_class(level))
    return result


def number_patents_without_ipc_class(connection):
    result = fetch_single_row(connection, query_patents_without_ipc_class())
    return result[0]


def distribution_number_patents_over_number_ipc_classes(connection):
    result = fetch_multiple_rows(connection, query_distribution_number_patents_over_number_ipc_classes())
    return result


def distribution_number_patents_over_number_ipc_classes_short(connection):
    result = fetch_multiple_rows(connection, query_distribution_number_patents_over_number_ipc_classes_short())
    return result


def number_agents_without_ipc_class(connection):
    result = fetch_single_row(connection, query_agents_without_ipc_class())
    return result[0]


def distribution_number_agents_over_number_ipc_classes(connection):
    result = fetch_multiple_rows(connection, query_distribution_number_agents_over_number_ipc_classes())
    return result


def distribution_number_agents_over_number_ipc_classes_short(connection):
    result = fetch_multiple_rows(connection, query_distribution_number_agents_over_number_ipc_classes_short())
    return result

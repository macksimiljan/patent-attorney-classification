from datetime import datetime

from pac.utils.database.connection_point import fetch_multiple_rows
from pac.utils.database.connection_point import fetch_single_row
from pac.utils.database.connection_point import instantiate_engine
from pac.utils.plotter.histograms import map_to_histogram_data
from pac.utils.plotter.histograms import show_histogram
from pac.utils.plotter.histograms import show_rank_histogram
from pac.preanalysis.helpers import map_null_value
from pac.preanalysis.queries.agents_queries import *

import pandas as pd


def run_agents_preanalysis(connection, text_file, figures_path):
    print('TODO: Eval for company type')

    with open(text_file, 'a') as f:
        f.write('===== Agents {} =====\n'.format(datetime.now()))
        f.write('#Agents: {}\n'.format(agents_count(connection)))
        f.write('Overlap between agents and applicants: {}\n'.format(agents_that_are_applications(connection)))
        f.write('#Applicants out of business: {}\n'.format(out_of_business_agents_count(connection)))
        f.write('#Applicants as initiative: {}\n'.format(agents_as_initiative_count(connection)))
        f.write('{:20} | {:20} | {:20}\n'.format('Category #Employees', 'abs freq', 'rel freq'))
        for row in agents_per_number_of_employees(connection):
            f.write('{:20} | {:20} | {:20}\n'.format(map_null_value(row['category_id']), row['abs_freq'], round(row['rel_freq'], 4)))

    x_values, y_values = map_to_histogram_data(
        patents_per_agent(connection), 'rank', 'abs_freq', ignore_null_value=True)
    path = '{}/{}'.format(figures_path, 'agents_rank_patents.eps')
    show_rank_histogram(x_values, y_values, 'Rank Agent', '#Patents', figure_path=path)

    engine = instantiate_engine()
    result = pd.read_sql_query(query_agents_per_patent(), engine)
    path = '{}/{}'.format(figures_path, 'agents_rank_agents.eps')
    show_histogram(result['company_count'], result['patent_count'], '#Agents per Patent', '#Patents', y_logarithmic=True, figure_path=path)

    x_values, y_values = map_to_histogram_data(
        agents_per_quality_score(connection), 'quality_score', 'abs_freq', ignore_null_value=True)
    path = '{}/{}'.format(figures_path, 'agents_quality_score_agents.eps')
    show_histogram(y_values, y_values, 'Quality Score', '#Agents', y_logarithmic=True, figure_path=path)


def agents_count(connection):
    result = fetch_single_row(connection, query_agents_count())
    return result[0]


def agents_that_are_applications(connection):
    result = fetch_single_row(connection, query_agents_that_are_applicants())
    return result[0]


def patents_per_agent(connection):
    result = fetch_multiple_rows(connection, query_patents_per_agent())
    return result


def out_of_business_agents_count(connection):
    result = fetch_single_row(connection, query_out_of_business_agents_count())
    return result[0]


def agents_per_number_of_employees(connection):
    result = fetch_multiple_rows(connection, query_agents_per_number_of_employees())
    return result


def agents_as_initiative_count(connection):
    result = fetch_single_row(connection, query_initiatives_count())
    return result[0]


def agents_per_quality_score(connection):
    result = fetch_multiple_rows(connection, query_agents_per_quality_score())
    return result

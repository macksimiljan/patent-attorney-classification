from pac.utils.database.connection_point import instantiate_engine
from pac.utils.io.feature_classes_point import read_split_training_dataset
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from io import StringIO
from mpl_toolkits.mplot3d import Axes3D
from sklearn.feature_extraction import FeatureHasher

from scipy.stats import pearsonr
from sklearn.preprocessing import StandardScaler
from collections import Counter

default_colors = sns.diverging_palette(220, 20, n=5)
sns.set_palette(default_colors)
sns.set_style("ticks")

engine = instantiate_engine()
# df = pd.read_sql_query("""SELECT grouping_agent, COUNT(DISTINCT t.patent_id), ROW_NUMBER() OVER(ORDER BY COUNT(DISTINCT t.patent_id) DESC)
#                           FROM view_triplets t INNER JOIN epo_patents p on p.id = t.patent_id
#                           WHERE p.ep_application_date >= DATE('2012-01-01')
#                           AND p.ep_application_date < DATE('2018-01-01')
#                           AND grouping_agent IS NOT NULL
#                           GROUP BY grouping_agent;""", engine).values
# x_values = df[:, 2]
# y_values = df[:, 1]
#
# mean = np.mean(y_values)
# median = np.median(y_values)
# plt.figure(figsize=(6,4))
# plt.bar(x_values, y_values)
# plt.plot([0, 3000], [mean, mean], ls='-', label    ='Mittel: {:>4.1f}'.format(mean), color=sns.color_palette("hls", 8)[0])
# plt.plot([0, 3000], [median, median], ls='-', label='Median: {:>4.0f}'.format(median), color=sns.color_palette("hls", 8)[2])
# plt.yscale('log')
# plt.xscale('log')
# plt.xlabel('Rang Anwalt')
# plt.ylabel('# Anmeldungen')
# plt.legend(loc='best')
# plt.savefig('freq_multi.eps')
# plt.show()
#
# cum_sum = 0
# total = y_values.sum()
# for x, y in zip(x_values, y_values):
#     cum_sum += y
#     print(x, round(cum_sum / total * 100 , 1))
#
#
#
# df = read_split_training_dataset(2012, 2017)
# no_ones = df['agent_old_vs_new'].values.sum()
# no_zeros = df.shape[0] - no_ones
# plt.figure(figsize=(3,4))
# plt.bar([0.5, 1.5], [no_zeros, no_ones], tick_label=['alt', 'neu'])
# plt.yscale('log')
# plt.ylabel('# Anmeldungen')
# plt.savefig('freq_binary.eps')
# plt.show()
#
# # 1-Anmelder-Datensatz
# s = StringIO("""  Baseline  Tree_3  LogRegr_L2_2
# Precision    0.0949  0.334   0.1889
# Accuracy     0.0949  0.154058   0.2201
# Surprisal    3.17  3.55  3.96""")
# df = pd.read_csv(s, index_col=0, delimiter=' ', skipinitialspace=True)
#
#
# ax = df.plot(kind='bar', rot=0, width=0.75, figsize=(6,4))
# for p in ax.patches:
#     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.05 ))
# plt.yscale('log')
# plt.savefig('ergebnis_satt_multi.eps')
# plt.show()
#
# s = StringIO("""  Baseline  Tree_3  LogRegr_L2_2
# Precision    0.816  0.945   0.945
# Accuracy     0.816  0.839   0.839
# Surprisal    0.40  0.53  0.53""")
# df = pd.read_csv(s, index_col=0, delimiter=' ', skipinitialspace=True)
#
#
# ax = df.plot(kind='bar', rot=0, width=0.75, figsize=(6,4))
# # for p in ax.patches:
# #     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.01 ))
# plt.ylim(ymax=1.18)
# plt.savefig('ergebnis_procter_multi.eps')
# plt.show()
#
# s = StringIO("""  Baseline  Tree_3  LogRegr_L2_2
# Precision    0.995 0.995   0.995
# Accuracy     0.995  0.995   0.995
# Surprisal    0.14  0.14  0.14""")
# df = pd.read_csv(s, index_col=0, delimiter=' ', skipinitialspace=True)
#
# ax = df.plot(kind='bar', rot=0, width=0.75, figsize=(6,4))
# # for p in ax.patches:
# #     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.01 ))
# plt.ylim(ymax=1.18)
# plt.savefig('ergebnis_procter_binary.eps')
# plt.show()
#
#
# s = StringIO("""  Baseline  Tree_5  LogRegr  DNN
# Precision    0.260 0.420   0.347  0.212
# Accuracy     0.229  0.268   0.368  0.205
# Surprisal    1.57  2.67  2.64  1.84""")
# df = pd.read_csv(s, index_col=0, delimiter=' ', skipinitialspace=True)
#
# ax = df.plot(kind='bar', rot=0, width=0.78, figsize=(6,4))
# for p in ax.patches:
#     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.01 ))
# plt.savefig('ergebnis_germany_multi.eps')
# plt.show()
#
# s = StringIO("""  Baseline  Tree_5  LogRegr DNN
# Precision    0.670 0.564   0.973  0.640
# Accuracy     0.942  0.944   0.947  0.945
# Surprisal    0.12  0.32  0.33  0.15""")
# df = pd.read_csv(s, index_col=0, delimiter=' ', skipinitialspace=True)
#
# ax = df.plot(kind='bar', rot=0, width=0.75, figsize=(6,4))
# # for p in ax.patches:
# #     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.01 ))
# plt.savefig('ergebnis_germany_binary.eps')
# plt.show()
#
#
# s = StringIO("""  Baseline  Tree_10  LogRegr
# Precision    0.345 0.296   0.246
# Accuracy     0.323 0.278   0.295
# Surprisal    1.45  2.26  1.85""")
# df = pd.read_csv(s, index_col=0, delimiter=' ', skipinitialspace=True)
#
# ax = df.plot(kind='bar', rot=0, width=0.75, figsize=(6,4))
# # for p in ax.patches:
# #     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.01 ))
# plt.savefig('ergebnis_uk_multi.eps')
# plt.show()
#
# s = StringIO("""  Baseline  Tree_5  LogRegr
# Precision    0.545 0.968   0.466
# Accuracy     0.907  0.935   0.933
# Surprisal    0.24  0.27  0.27""")
# df = pd.read_csv(s, index_col=0, delimiter=' ', skipinitialspace=True)
#
# ax = df.plot(kind='bar', rot=0, width=0.75, figsize=(6,4))
# # for p in ax.patches:
# #     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.01 ))
# plt.savefig('ergebnis_uk_binary.eps')
# plt.show()
#
# s = StringIO("""  Baseline  Tree_35
# Precision    0.6133 0.447
# Accuracy     0.4481 0.1753
# Surprisal    0.84 1.51""")
# df = pd.read_csv(s, index_col=0, delimiter=' ', skipinitialspace=True)
#
# ax = df.plot(kind='bar', rot=0, width=0.75, figsize=(6,4))
# # for p in ax.patches:
# #     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.01 ))
# plt.savefig('ergebnis_complete_multi.eps')
# plt.show()
#
# s = StringIO("""  Baseline  Tree_10  LogRegr
# Precision    0.475 0.603   0.777
# Accuracy     0.968 0.968 0.968
# Surprisal    0.07  0.19  0.19""")
# df = pd.read_csv(s, index_col=0, delimiter=' ', skipinitialspace=True)
#
# ax = df.plot(kind='bar', rot=0, width=0.75, figsize=(6,4))
# # for p in ax.patches:
# #     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.01 ))
# plt.savefig('ergebnis_complete_binary.eps')
# plt.show()
#
# x_values = np.arange(0.01, 1, 0.01)
# y_values = [np.log2(1/x) for x in x_values]
# plt.figure(figsize=(6,4))
# plt.plot(x_values, y_values)
# plt.xlabel('P(a|c)')
# plt.ylabel('Surprisal')
# plt.savefig('surprisal.eps')
# plt.show()
#
#
# df = pd.read_sql_query("""SELECT DISTINCT l.agent_id, c.longitude, c.latitude, c.full_name
#                             FROM view_agents_locations l, view_triplets t, epo_patents p, cities c
#                             WHERE p.ep_application_date >= DATE('2012-01-01')
#                             AND t.grouping_agent IS NOT NULL
#                             AND t.grouping_applicant IS NOT NULL
#                             AND l.agent_id = t.agent_id
#                             AND t.patent_id = p.id
#                             AND c.country = 'Germany'
#                             AND c.longitude IS NOT NULL
#                             AND c.latitude IS NOT NULL
#                             AND l.city_id = c.id""", engine)
# x_values = df['longitude'].values
# y_values = df['latitude'].values
# plt.scatter(x_values,
#             y_values,
#             alpha=0.3)
# plt.scatter([11.56667, 13.388889, 8.683333, 6.9527],
#             [48.13333, 52.516667, 50.1166667, 50.9364],
#             color='red',
#             alpha=0.8)
# plt.show()
#
# ##############################################################
# ##############################################################
#
# s = StringIO(""", 1st Level IPC Class, Relative Frequency
# , A, 0.139493069447358
# , B, 0.150600273617214
# , C, 0.142687240159679
# , D, 0.0115445189389798
# , E, 0.0197988352062028
# , F, 0.0910442072044653
# , G, 0.19903288429774
# , H, 0.245798971128361
# """)
#
# df = pd.read_csv(s, index_col=0, delimiter=', ', skipinitialspace=True)
# print(df.columns)
# print(df.index.values)
# colors = sns.color_palette("Set2", n_colors=8)
# ax = df.plot.bar(x='1st Level IPC Class', y='Relative Frequency', rot=0, figsize=(6,4), color=colors)
# ax.legend_.remove()
# plt.tight_layout()
# plt.ylabel('Rel. Frequency')
# plt.savefig('ipc_1st_patents.eps')
# plt.show()
#
# s = StringIO(""", 2nd Level IPC Class, Relative Frequency
# , A61, 0.7232226224
# , A01, 0.0830332133
# , A47, 0.0586901427
# , A*, 0.1350540216
# , B60, 0.1893709328
# , B01, 0.1069414317
# , B29, 0.0869305857
# , B*, 0.6167570499
# , C07, 0.2412222543
# , C08, 0.2101470164
# , C12, 0.1469587777
# , C*, 0.4016719516
# , D06, 0.3610023493
# , D01, 0.1918559123
# , D21, 0.1816758027
# , D*, 0.2654659358
# , E21, 0.337240277
# , E04, 0.1800745871
# , E05, 0.1646244006
# , E*, 0.3180607352
# , F16, 0.2587313574
# , F01, 0.1782140834
# , F02, 0.1423447234
# , F*, 0.4207098358
# , G06, 0.3959039973
# , G01, 0.2729809798
# , G02, 0.1012837577
# , G*, 0.2298312653
# , H04, 0.5321397347
# , H01, 0.2672672673
# , H02, 0.104116775
# , H*, 0.0964762231
# """)
# old_colors = colors
# colors = []
# for c in old_colors:
#     for i in range(4):
#         colors.append(c)
# df = pd.read_csv(s, index_col=0, delimiter=', ', skipinitialspace=True)
# ax = df.plot.bar(x='2nd Level IPC Class', y='Relative Frequency', rot=0, figsize=(10,4), color=colors)
# ax.legend_.remove()
# plt.ylabel('Rel. Frequency within 1st Level Category')
# plt.savefig('ipc_2nd_patents.eps', bbox_inches='tight')
# plt.show()
#
# s = StringIO(""", 1st Level IPC Class, Relative Frequency Applications, Relative Frequency Agents
# , A, 0.1394930694, 0.1741312741
# , B, 0.1506002736, 0.194980695
# , C, 0.1426872402, 0.1293436293
# , D, 0.0115445189, 0.0258687259
# , E, 0.0197988352, 0.0853281853
# , F, 0.0910442072, 0.1328185328
# , G, 0.1990328843, 0.1447876448
# , H, 0.2457989711, 0.1127413127
# """)
# df = pd.read_csv(s, index_col=0, delimiter=", ", skipinitialspace=True)
# ax = df.plot.scatter(x='Relative Frequency Applications', y='Relative Frequency Agents', s=80, figsize=(6,4))
# for i, txt in enumerate(df['1st Level IPC Class']):
#     ax.annotate(txt, (df.iloc[i, 1] - 0.001, df.iloc[i, 2] + 0.01))
# plt.savefig('ipc_applications_agents.eps', bbox_inches='tight')
# plt.show()
# print(pearsonr(df.iloc[:,1], df.iloc[:, 2]))
#
#
# applicant = [0.9128654224, 0.0561590667, 0.0202981186, 0.0034571553, 0.0020701641, 0.0006306212, 0.004416225, 0.000071, 0.00001877, 0.00000938, 0.00000188, 0.00000188]
# agent = [0.9053167370, 0.0827802886, 0.0095794357, 0.0013250552, 0.0009722076, 0.0000206453, 0.0000056305, 0, 0, 0, 0, 0]
# index = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
# df = pd.DataFrame({'Applicant': applicant, 'Agent': agent}, index=index)
# df.plot.bar(rot=0, figsize=(6,4))
# plt.ylabel('Relative Frequency of Applications')
# plt.yscale('log')
# plt.xlabel('Number per Application')
# plt.savefig('multiple_entities_per_application.eps', bbox_inches='tight')
# plt.show()
#
# values = pd.read_sql("""SELECT t.grouping_agent, COUNT(DISTINCT t.patent_id)
#                         FROM view_triplets t INNER JOIN epo_patents p ON t.patent_id = p.id
#                         WHERE t.grouping_agent IS NOT NULL
#                         AND t.grouping_applicant IS NOT NULL
#                         AND p.ep_application_date >= DATE('2012-01-01')
#                         GROUP BY t.grouping_agent
#                         ORDER BY COUNT(DISTINCT t.patent_id) DESC;""", engine).values
# x_values = range(1, values.shape[0] + 1)
# y_values = values[:, 1]
# mean = np.mean(y_values)
# std = np.std(y_values)
# median = np.median(y_values)
# print(mean, std, median)
# grouped_values = {}
# for y in y_values:
#     key = np.floor(np.log10(y))
#     if key in grouped_values:
#         grouped_values[key] += 1
#     else:
#         grouped_values[key] = 1
# print(grouped_values)
# plt.figure(figsize=(4, 4))
# labels = ["$10^0\leq$", "$10^1\leq$", "$10^2\leq$", "$10^3\leq$", "$10^4\leq$"]
# plt.bar(list(grouped_values.keys()), list(grouped_values.values()), align='center', tick_label=labels)
# plt.yscale('log')
# plt.ylim(ymax=5000, ymin=1)
# plt.ylabel('# Agents')
# plt.xlabel('# Applications per Agent')
# plt.savefig('applications_per_agent.eps', bbox_inches='tight')
# plt.show()
#
# values = pd.read_sql("""SELECT t.grouping_applicant, COUNT(DISTINCT t.patent_id)
#                         FROM view_triplets t INNER JOIN epo_patents p ON t.patent_id = p.id
#                         WHERE t.grouping_agent IS NOT NULL
#                         AND t.grouping_applicant IS NOT NULL
#                         AND p.ep_application_date >= DATE('2012-01-01')
#                         GROUP BY t.grouping_applicant
#                         ORDER BY COUNT(DISTINCT t.patent_id) DESC;""", engine).values
# x_values = range(1, values.shape[0] + 1)
# y_values = values[:, 1]
# mean = np.mean(y_values)
# std = np.std(y_values)
# median = np.median(y_values)
# print(mean, std, median)
# grouped_values = {}
# for y in y_values:
#     key = np.floor(np.log10(y))
#     if key in grouped_values:
#         grouped_values[key] += 1
#     else:
#         grouped_values[key] = 1
# print(grouped_values)
# plt.figure(figsize=(4,4))
# labels = ["$10^0 \leq$", "$10^1\leq$", "$10^2\leq$", "$10^3\leq$", "$10^4\leq$"]
# plt.bar(list(grouped_values.keys()), list(grouped_values.values()), align='center', tick_label=labels)
# plt.yscale('log')
# plt.ylim(ymax=5000, ymin=1)
# plt.ylabel('# Applicants')
# plt.xlabel('# Applications per Applicant')
# plt.savefig('applications_per_applicant.eps', bbox_inches='tight')
# plt.show()
#
# values = pd.read_sql("""SELECT grouping_applicant, no_agents, size_grouping
#                         FROM (
#                         SELECT t.grouping_applicant, COUNT(DISTINCT t.grouping_agent) as no_agents
#                         FROM view_triplets t INNER JOIN epo_patents p ON t.patent_id = p.id
#                         WHERE t.grouping_agent IS NOT NULL
#                         AND t.grouping_applicant IS NOT NULL
#                         AND p.ep_application_date >= DATE('2012-01-01')
#                         AND t.grouping_applicant LIKE 'g%%'
#                         GROUP BY t.grouping_applicant) AS t0 INNER JOIN (
#                         SELECT 'g' || grouping_id as artifical_grouping, COUNT(DISTINCT company_id) as size_grouping
#                         FROM grouping_relations
#                         GROUP BY grouping_id
#                         ) AS t1 ON t0.grouping_applicant = t1.artifical_grouping
#                         UNION
#                         SELECT grouping_applicant, no_agents, 1
#                         FROM (
#                         SELECT t.grouping_applicant, COUNT(DISTINCT t.grouping_agent) as no_agents
#                         FROM view_triplets t INNER JOIN epo_patents p ON t.patent_id = p.id
#                         WHERE t.grouping_agent IS NOT NULL
#                         AND t.grouping_applicant IS NOT NULL
#                         AND p.ep_application_date >= DATE('2012-01-01')
#                         AND t.grouping_applicant LIKE 'a%%'
#                         GROUP BY t.grouping_applicant) AS t2;""", engine).values
#
# x_pearson = StandardScaler().fit_transform(values[:, 2].reshape(-1, 1))
# y_pearson = StandardScaler().fit_transform(values[:, 1].reshape(-1, 1))
# print(pearsonr(x_pearson, y_pearson))
# print(np.mean(values[:, 2]), np.std(values[:, 2]), np.median(values[:, 2]))
# plt.figure(figsize=(6,4))
# plt.scatter(values[:, 2], values[:, 1], s=30, alpha=0.5)
# plt.ylabel('# Agents per Applicant Grouping')
# plt.xlabel('Applicant Grouping Size')
# plt.xscale('log')
# plt.yscale('log')
# plt.ylim(ymax=210, ymin=0.8)
# plt.xlim(xmax=210, xmin=0.8)
# plt.tight_layout()
# plt.savefig('agents_per_grouping_size.eps')
# plt.show()
#
#
# values = pd.read_sql("""SELECT *
#                         FROM (
#                         SELECT t.grouping_applicant, COUNT(DISTINCT t.grouping_agent) as no_agents
#                         FROM view_triplets t INNER JOIN epo_patents p ON t.patent_id = p.id
#                         WHERE t.grouping_agent IS NOT NULL
#                         AND t.grouping_applicant IS NOT NULL
#                         AND p.ep_application_date >= DATE('2012-01-01')
#                         GROUP BY t.grouping_applicant
#                         ) AS t0 INNER JOIN (
#                         SELECT t.grouping_applicant, COUNT(DISTINCT t.patent_id) as no_patents
#                         FROM view_triplets t INNER JOIN epo_patents p ON t.patent_id = p.id
#                         WHERE t.grouping_agent IS NOT NULL
#                         AND t.grouping_applicant IS NOT NULL
#                         AND p.ep_application_date >= DATE('2012-01-01')
#                         GROUP BY t.grouping_applicant
#                         ) AS t1 ON t0.grouping_applicant = t1.grouping_applicant""", engine).values
#
# x_pearson = StandardScaler().fit_transform(values[:, 3].reshape(-1, 1))
# y_pearson = StandardScaler().fit_transform(values[:, 1].reshape(-1, 1))
# print(pearsonr(x_pearson, y_pearson))
# print(np.mean(values[:, 3]), np.std(values[:, 3]), np.median(values[:, 3]))
# plt.figure(figsize=(6,4))
# plt.scatter(values[:, 3], values[:, 1], s=30, alpha=0.5)
# plt.ylabel('# Agents per Applicant Grouping')
# plt.xlabel('# Patent Applications')
# plt.xscale('log')
# plt.yscale('log')
# plt.ylim(ymax=210, ymin=0.8)
# plt.xlim(xmax=30000, xmin=0.8)
# plt.tight_layout()
# plt.savefig('agents_per_patent_number.eps')
# plt.show()
#
# ikea = [30/38, 30/38, 4/38, 3/38, 1/38]
# palantir = [54/110, 36/110, 12/110, 6/110, 3/110]
# index = ['1st', '2nd', '3rd', '4th', '5th']
# df = pd.DataFrame({'IKEA': ikea, 'Palantir': palantir}, index=index)
# df.plot.bar(rot=0, figsize=(6,4))
# plt.ylim(ymax=1)
# plt.ylabel('Proportion of Patent Applications')
# plt.xlabel('$i$-th Most Frequent Agent Grouping')
# plt.savefig('selected_distribution_of_agents.eps', bbox_inches='tight')
# plt.show()
#
# ##############
#
# satt = [390, 369, 301, 255, 172, 150, 149, 146, 124, 105, 100, 98, 95, 80, 80, 80, 71, 58, 58, 50, 35, 30, 29, 29, 26, 25, 25, 23, 23, 23, 21, 19, 17, 17, 17, 16, 15, 15, 15, 15, 15, 14, 14, 14, 14, 11, 11, 10, 10, 10, 9, 9, 9, 9, 8, 8, 7, 7, 7, 7, 6, 6, 6, 6, 6, 5, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# c = 0
# for d in satt:
#     if d <= 3:
#         c += 1
# print(c/len(satt))
# # satt = list(map(lambda x: x/3236, satt))
# print(np.std(satt))
# print(np.mean(satt))
# print(sum(satt))
#
# plt.bar(range(1, len(satt)+1), satt)
# plt.yscale('log')
# plt.show()
#
#
#
# ###############
#
# df = pd.read_sql_query("""SELECT t0.grouping_applicant, JSON_AGG(t0.grouping_agent) AS agents, COUNT(DISTINCT t0.patent_id) AS no_patents, COUNT(DISTINCT t0.grouping_agent) as no_agents
#                             FROM (
#                                 SELECT DISTINCT t.grouping_applicant, t.grouping_agent, t.patent_id
#                                 FROM view_triplets t INNER JOIN epo_patents p ON t.patent_id = p.id
#                                 WHERE t.grouping_agent IS NOT NULL
#                                 AND t.grouping_applicant IS NOT NULL
#                                 AND p.ep_application_date >= DATE('2012-01-01')
#                                 ORDER BY t.patent_id
#                                 ) AS t0
#                             GROUP BY t0.grouping_applicant;""", engine)
# rel_frequencies_of_most_frequent_agent = []
# rel_frequencies_of_second_frequent_agent = []
# rel_frequencies_of_third_frequent_agent = []
# rel_frequencies_of_fourth_frequent_agent = []
# proportions_second_of_first = []
# proportions_third_of_second = []
# proportions_third_of_first = []
# proportions_fourth_of_first = []
# stds_per_agentsize = {}
# l = []
# counter = Counter()
# for row in df.itertuples():
#     counter.clear()
#     agents = row[2]
#     applicant = row[1]
#     number_unique_agents = row[4]
#     counter.update(agents)
#     highest_frequency = counter.most_common(1)[0][1]
#     second_frequency = counter.most_common(2)[1][1] if len(counter.most_common(2)) >= 2 else -1
#     third_frequencies = counter.most_common(3)[2][1] if len(counter.most_common(3)) >= 3 else -1
#     fourth_frequencies = counter.most_common(4)[3][1] if len(counter.most_common(4)) >= 4 else -1
#     total = row[3]
#     rel_frequencies_of_most_frequent_agent.append(highest_frequency / total)
#     rel_frequencies_of_second_frequent_agent.append(second_frequency / total)
#     rel_frequencies_of_third_frequent_agent.append(third_frequencies / total)
#     rel_frequencies_of_fourth_frequent_agent.append(fourth_frequencies / total)
#
#     pair = []
#     if second_frequency >= 0:
#         proportions_second_of_first.append(second_frequency/highest_frequency)
#         pair.append(second_frequency / highest_frequency)
#         if third_frequencies >= 0:
#             proportions_third_of_second.append(third_frequencies/second_frequency)
#             proportions_third_of_first.append(third_frequencies/highest_frequency)
#             pair.append(third_frequencies / second_frequency)
#             if fourth_frequencies >= 0:
#                 proportions_fourth_of_first.append(fourth_frequencies/highest_frequency)
#         else:
#             pair.append(None)
#
#     if len(pair) > 0:
#         l.append(pair)
#
#     rel_frequencies = list(map(lambda x: x/total, counter.values()))
#     mean = np.mean(rel_frequencies)
#     std = np.std(rel_frequencies)
#     if number_unique_agents in stds_per_agentsize:
#         stds_per_agentsize[number_unique_agents].append(std)
#     else:
#         stds_per_agentsize[number_unique_agents] = [std]
#
# print(np.mean(proportions_second_of_first), np.std(proportions_second_of_first), np.median(proportions_second_of_first))
#
# proportions_second_of_first = sorted(proportions_second_of_first, reverse=True)
# proportions_third_of_second = sorted(proportions_third_of_second, reverse=True)
# proportions_third_of_first = sorted(proportions_third_of_first, reverse=True)
# proportions_fourth_of_first = sorted(proportions_fourth_of_first, reverse=True)
#
#
# plt.figure(figsize=(6,4))
# plt.plot(range(1, len(proportions_third_of_first) + 1), proportions_third_of_first, label='3rd / 1st', color=sns.color_palette()[3])
# plt.plot(range(1, len(proportions_third_of_second) + 1), proportions_third_of_second, label='3rd / 2nd', color=sns.color_palette()[1])
# plt.plot(range(1, len(proportions_second_of_first) + 1), proportions_second_of_first, label='2nd / 1st', color=sns.color_palette()[0])
# plt.plot(range(1, len(proportions_fourth_of_first) + 1), proportions_fourth_of_first, label='4th / 1st', color=sns.color_palette()[4])
#
# plt.legend(loc='best')
# plt.ylabel('Applications Proportion of $i$th to $j$th Agent')
# plt.tick_params(
#     axis='x',          # changes apply to the x-axis
#     which='both',      # both major and minor ticks are affected
#     bottom=False,      # ticks along the bottom edge are off
#     top=False,         # ticks along the top edge are off
#     labelbottom=False) # labels along the bottom edge are off
# plt.tight_layout()
# plt.savefig('applications_proportion.eps')
# plt.show()
#
# l0 = sorted(l, key=lambda pair: pair[0], reverse=True)
# l1 = list(map(lambda pair: pair[0], l0))
# l2 = list(map(lambda pair: pair[1], l0))
# plt.plot(range(1, len(l1) + 1), l1, linestyle='None', marker='o')
# plt.plot(range(1, len(l2) + 1), l2, linestyle='None', marker='o')
# plt.show()
#
#
#
# data = []
# keys = [1,2,3,4,5,6,7,8,9,10,25,50,100,150]
# for key, value in stds_per_agentsize.items():
#     if key in keys:
#         data.append(value)
#
# plt.figure(figsize=(6, 4))
# plt.boxplot(data, whis='range', labels=keys)
# plt.xlabel('# Unique Agents')
# plt.ylabel('Standard Deviation of Agent Proportions')
# plt.tight_layout()
# plt.savefig('deviation_agent_proportion.eps')
# plt.show()
# print()
#
# def mapping(val):
#     if val >= 0.999:
#         val = 1.0
#     elif val >= 0.899:
#         val = 0.9
#     elif val >= 0.799:
#         val = 0.8
#     elif val >= 0.699:
#         val = 0.7
#     elif val >= 0.599:
#         val = 0.6
#     elif val >= 0.499:
#         val = 0.5
#     elif val >= 0.399:
#         val = 0.4
#     elif val >= 0.299:
#         val = 0.3
#     elif val >= 0.199:
#         val = 0.2
#     elif val >= 0.099:
#         val = 0.1
#     elif val >= 0.0:
#         val = 0.0
#     else:
#         val = None
#
#     if val == 1.0:
#         return '1.0'
#     else:
#         return '$\geq {}$'.format(val) if val is not None else 'not def.'
#
#
# rel_frequencies_of_most_frequent_agent = list(map(lambda x: mapping(x), rel_frequencies_of_most_frequent_agent))
# rel_frequencies_of_second_frequent_agent = list(map(lambda x: mapping(x), rel_frequencies_of_second_frequent_agent))
# rel_frequencies_of_third_frequent_agent = list(map(lambda x: mapping(x), rel_frequencies_of_third_frequent_agent))
# rel_frequencies_of_fourth_frequent_agent = list(map(lambda x: mapping(x), rel_frequencies_of_fourth_frequent_agent))
# rel_frequencies_of_fourth_frequent_agent.append('$\\geq 0.9$')
# print(rel_frequencies_of_fourth_frequent_agent)
#
#
# def plot_frequencies(distribution, x_label, file_name):
#     counter = Counter(distribution)
#     total = sum(counter.values())
#     data = list(map(lambda pair: (pair[0], round(pair[1]/total, 5)), counter.items()))
#     data = sorted(data, key=lambda x: (False, x[0]) if x[0] == 'not def.' else (True, x[0]), reverse=True)
#     x_values = list(map(lambda p: p[0], data))
#     y_values = list(map(lambda p: p[1], data))
#     y_values_cumulative = np.cumsum(y_values)
#     print(data)
#     print(y_values_cumulative)
#
#     plt.figure(figsize=(5, 4))
#     plt.bar(range(1, len(x_values) + 1),
#             y_values,
#             align='center')
#     plt.step(range(1, len(x_values) + 1),
#              y_values_cumulative,
#              where='mid',
#              label='cumulative sum',
#              color=sns.color_palette()[3])
#     plt.xticks(range(1, len(x_values) + 1), x_values, rotation=45)
#     plt.legend(loc='best')
#
#     plt.ylabel('Proportion of Applicant Groupings')
#     plt.xlabel(x_label)
#     plt.tight_layout()
#     plt.savefig(file_name)
#     plt.show()
#
#
# plot_frequencies(rel_frequencies_of_most_frequent_agent,
#                  'Proportion of Applications Served by the Most Frequent Agent', 'freq_most_frequent_agent.eps')
# plot_frequencies(rel_frequencies_of_second_frequent_agent,
#                  'Proportion of Applications Served by the 2nd Most Frequent Agent', 'freq_second_frequent_agent.eps')
# plot_frequencies(rel_frequencies_of_third_frequent_agent,
#                  'Proportion of Applications Served by the 3rd Most Frequent Agent', 'freq_third_frequent_agent.eps')
# plot_frequencies(rel_frequencies_of_fourth_frequent_agent,
#                  'Proportion of Applications Served by the 4th Most Frequent Agent', 'freq_fourth_frequent_agent.eps')
#
# #####################
#
#
# def sigmoid(z):
#     return 1.0 / (1.0 + np.exp(-z))
#
#
# z = np.arange(-7, 7, 0.1)
# plt.figure(figsize=(6,4))
# plt.plot([-7, 7], [0.5, 0.5], linestyle='--', color=sns.color_palette()[2])
# plt.plot([0, 0], [0, 1], linestyle='--', color=sns.color_palette()[2])
# plt.plot(z, sigmoid(z))
# plt.xlabel('$a$')
# plt.yticks([0, 0.5, 1])
# plt.ylabel('$\sigma(a)$')
# plt.tight_layout()
# plt.savefig('sigmoid_function.eps')
# plt.show()
#
#
# def cost_1(z):
#     return - np.log(sigmoid(z))
#
#
# def cost_0(z):
#     return - np.log(1 - sigmoid(z))
#
#
# z = np.arange(-10, 10, 0.1)
# c1 = [cost_1(x) for x in z]
# plt.figure(figsize=(6,4))
# plt.plot(sigmoid(z), c1, label='$J$ if $t_i$ = 1')
# c0 = [cost_0(x) for x in z]
# plt.plot(sigmoid(z), c0, label='$J$ if $t_i$ = 0')
# plt.xlabel('$\sigma(z)$')
# plt.ylabel('Costs $J$')
# plt.legend(loc='best')
# plt.tight_layout()
# plt.savefig('exampple_log_reg_costs.eps')
# plt.show()
#
# ############
#
s = StringIO(""", Longitude, Latitude, agent
, 11.414763, 53.629593, 1
, 13.58, 52.68, 1
, 13.22, 52.36, 1
, 7.16, 51.10, 2
, 6.80, 50.78, 2
, 11.576124, 48.137154, 2
, 2.57, 49.04, 3
, 2.11, 48.68, 3
, 12.496366, 41.902782, 4
, 14.305573, 40.853294, 4
""")
df = pd.read_csv(s, index_col=0, delimiter=", ", skipinitialspace=True)
plt.figure(figsize=(6,4))
plt.scatter(df.values[0:3, 0], df.values[0:3, 1], marker='o', s=100)
plt.scatter(df.values[3:6, 0], df.values[3:6, 1], marker='*', s=200)
plt.scatter(df.values[6:8, 0], df.values[6:8, 1], marker='D', s=100, c=default_colors[-2])
plt.scatter(df.values[8:10, 0], df.values[8:10, 1], marker='s', s=100, c=default_colors[-1])
plt.xlabel('Longitude')
plt.ylabel('Latitude')
plt.tight_layout()
plt.savefig('working_example_lon_lat.eps')
plt.show()
#
# ######################
#
# np.random.seed(1)
# X_xor = np.random.randn(200, 2)
# y_xor = np.logical_xor(X_xor[:, 0] > 0,
#                        X_xor[:, 1] > 0)
# y_xor = np.where(y_xor, 1, -1)
#
# plt.figure(figsize=(6,4))
# plt.scatter(X_xor[y_xor == 1, 0],
#             X_xor[y_xor == 1, 1],
#             c=sns.color_palette()[-1], marker='x',
#             label='Class 1')
# plt.scatter(X_xor[y_xor == -1, 0],
#             X_xor[y_xor == -1, 1],
#             c=sns.color_palette()[0],
#             marker='s',
#             label='Class 2')
#
# plt.xlim([-3, 3])
# plt.ylim([-3, 3])
# plt.legend(loc='best')
# plt.ylabel('x$_2$')
# plt.xlabel('x$_1$')
# plt.tight_layout()
# plt.savefig('xor.eps')
# plt.show()
#
#
# fig = plt.figure(figsize=(8,4))
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(X_xor[y_xor == 1, 0],
#             X_xor[y_xor == 1, 1],
#             [-5]*X_xor[y_xor == 1, 0].shape[0],
#             c=sns.color_palette()[-1], marker='x',
#             label='Class 1')
# ax.scatter(X_xor[y_xor == -1, 0],
#             X_xor[y_xor == -1, 1],
#             [5]*X_xor[y_xor == 1, 0].shape[0],
#             c=sns.color_palette()[0],
#             marker='s',
#             label='Class 2')
# plt.savefig('xor_3d.eps')
# plt.show()
#
#
# y_values = np.array(list(map(lambda x: 1 if x <0 else -1, X_xor[:, 0])))
# plt.figure(figsize=(6,4))
# plt.scatter(X_xor[y_values == 1, 0],
#             X_xor[y_values == 1, 1],
#             c=sns.color_palette()[-1], marker='x',
#             label='Class 1')
# plt.scatter(X_xor[y_values == -1, 0],
#             X_xor[y_values == -1, 1],
#             c=sns.color_palette()[0],
#             marker='s',
#             label='Class 2')
#
# plt.xlim([-3, 3])
# plt.ylim([-3, 3])
# plt.legend(loc='best')
# plt.ylabel('x$_2$')
# plt.xlabel('x$_1$')
# plt.tight_layout()
# plt.savefig('non_xor.eps')
# plt.show()
#
# #####################
#
# values = pd.read_sql_query("""SELECT t.grouping_applicant, COUNT(t.patent_id)
#                                 FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
#                                 WHERE t.grouping_agent IS NOT NULL
#                                 AND t.grouping_applicant IS NOT NULL
#                                 AND p.ep_application_date >= DATE('2017-01-01')
#                                 AND p.ep_application_date < DATE('2018-01-01')
#                                 GROUP BY t.grouping_applicant""", engine).values
# maximum_gap_sizes = list(
#     map(
#         lambda x: x - 1, values[:, 1]
#     ))
# grouped_gap_sizes = {5: 0, 10: 0, 50: 0, 100: 0, 500: 0, 5000: 0}
# for gap_size in maximum_gap_sizes:
#     if gap_size < 5:
#         grouped_gap_sizes[5] += 1
#     elif gap_size < 10:
#         grouped_gap_sizes[10] += 1
#     elif gap_size < 50:
#         grouped_gap_sizes[50] += 1
#     elif gap_size < 100:
#         grouped_gap_sizes[100] += 1
#     elif gap_size < 500:
#         grouped_gap_sizes[500] += 1
#     else:
#         grouped_gap_sizes[5000] += 1
# grouped_gap_sizes = sorted(list(grouped_gap_sizes.items()), key=lambda tuple: tuple[0])
# heights = list(map(lambda tuple: tuple[1]/3264, grouped_gap_sizes))
# x_ticks = ['$< 5$', '$< 10$', '$< 50$', '$< 100$', '$< 500$', '$\geq 500$']
# print(grouped_gap_sizes)
# print(np.mean(maximum_gap_sizes), np.std(maximum_gap_sizes), np.median(maximum_gap_sizes))
# plt.figure(figsize=(5,4))
# plt.bar(range(1, len(grouped_gap_sizes) + 1), heights, tick_label=x_ticks)
# plt.xlabel('Maximum Gap Size per Applicant')
# plt.ylabel('Proportion of Applicants')
# plt.ylim(ymax=0.8, ymin=0.0)
# plt.tight_layout()
# plt.savefig('gap_size_validation.eps')
# plt.show()
#
#
# values = pd.read_sql_query("""SELECT t.grouping_applicant, COUNT(t.patent_id)
#                                 FROM view_triplets t INNER JOIN epo_patents p ON (t.patent_id = p.id)
#                                 WHERE t.grouping_agent IS NOT NULL
#                                 AND t.grouping_applicant IS NOT NULL
#                                 AND p.ep_application_date >= DATE('2018-01-01')
#                                 AND p.ep_application_date < DATE('2019-01-01')
#                                 GROUP BY t.grouping_applicant""", engine).values
# maximum_gap_sizes = list(
#     map(
#         lambda x: x - 1, values[:, 1]
#     ))
# grouped_gap_sizes = {5: 0, 10: 0, 50: 0, 100: 0, 500: 0, 1000: 0, 5000: 0}
# for gap_size in maximum_gap_sizes:
#     if gap_size < 5:
#         grouped_gap_sizes[5] += 1
#     elif gap_size < 10:
#         grouped_gap_sizes[10] += 1
#     elif gap_size < 50:
#         grouped_gap_sizes[50] += 1
#     elif gap_size < 100:
#         grouped_gap_sizes[100] += 1
#     elif gap_size < 500:
#         grouped_gap_sizes[500] += 1
#     elif gap_size < 1000:
#         grouped_gap_sizes[1000] += 1
#     elif gap_size < 5000:
#         grouped_gap_sizes[5000] += 1
# grouped_gap_sizes = sorted(list(grouped_gap_sizes.items()), key=lambda tuple: tuple[0])
# heights = list(map(lambda tuple: tuple[1]/1594, grouped_gap_sizes))
# x_ticks = ['$< 5$', '$< 10$', '$< 50$', '$< 100$', '$< 500$']
# print(grouped_gap_sizes)
# print(np.mean(maximum_gap_sizes), np.std(maximum_gap_sizes), np.median(maximum_gap_sizes))
# plt.figure(figsize=(5,4))
# plt.bar(range(1, 5  + 1), heights[0:5], tick_label=x_ticks)
# plt.xlabel('Maximum Gap Size per Applicant')
# plt.ylabel('Proportion of Applicants')
# plt.ylim(ymax=0.8, ymin=0.0)
# plt.tight_layout()
# plt.savefig('gap_size_test.eps')
# plt.show()
#
# ################
#
# def surprisal(x):
#     return - np.log2(x)
#
# x_values = np.arange((1/13000), 1, 0.001)
# plt.figure(figsize=(6,4))
# plt.plot(x_values, [surprisal(x) for x in x_values])
# plt.xlabel('P(x)')
# plt.ylabel('Surprisal')
# plt.tight_layout()
# plt.savefig('surprisal.eps')
# plt.show()
#
# ###########################
#
# categories = ["g5713", "g2131", "g3737", "g7992", "g2716", "g2956", "g3947", "g2942", "g8163", "g2979", "g8164", "g2500", "g1729", "a5317", "g8548", "g5187", "g1587", "g1745", "g529", "g2411", "a6087", "g4074", "g1639", "g4248", "g2581", "g1151", "g394", "g6444", "g2920", "a4085", "g105", "g3996", "a5222", "g428", "g1321", "g2432", "g5761", "g3757", "g2561", "g6292", "g551", "g568", "g5529", "g1053", "g1531", "g1003", "g7902", "g2757", "a6133", "g4262", "g5810", "g838", "g8334", "g3062", "g6218", "g3504", "g3884", "a5742", "g471", "g7706", "g952", "g4031", "g2753", "g3760", "g1373", "g7134", "a2807", "a2643", "g7725", "g1360", "g3482", "g1306", "g970", "g85", "g751", "g1317", "g2723", "g3787", "g8226", "g6386", "g3966", "g6395", "a2614", "g2997", "g6486", "g3512", "g3041", "g478", "a2599", "g7955", "g821", "g3186", "g6209", "a5795", "g7193", "a5708", "g6446", "g507", "a5639", "g4149", "g504", "g4052", "g5682", "g7987", "g3019", "g5731", "g674", "g3149", "g8391", "g3113", "g5333", "g8059", "a5506", "g755", "g5656", "g3521", "g8466", "g3681", "g1332", "g5663", "g401", "a2882", "g1744", "g1097", "g3668", "g2912", "g832", "g8675", "g5825", "g1661", "a6679", "g5453", "g2884", "g5675", "a6002", "g3418", "g1655", "g3082", "g1790", "a5889", "a6331", "g3457", "g3048", "g2758", "g6082", "g4972", "g1236", "g6233", "g2578", "g3430", "g2743", "g3485", "g6482", "g6251", "g7273", "g8241", "g5450", "g6371", "g5688", "a5665", "g6334", "g3551", "a3703", "g1316", "g6411", "g3531", "g5426", "g2983", "g3865", "g7970", "g3758", "g848", "g8212", "g874", "g2105", "a6816", "g8515", "g883", "g5641", "a5849", "g5363", "g7704", "g6167", "g6323", "g538", "g6231", "g2957", "g935", "a3027", "g1678", "a3254", "g3575", "g2293", "g1169", "g8668", "g5397", "g7823", "g4266", "a5831", "g8529", "g1076", "g7288", "a2876", "g3234", "a5374", "g6397", "g5524", "g5373", "g3571", "g1642", "g2214", "g2166", "g3200", "a5854", "a5590", "a6642", "g8756", "g5612", "g474", "a4902", "g1994", "g5669", "g5920", "g5941", "g7985", "g4154", "a6342", "g3496", "a3204", "a6086", "g2886", "a2349", "g2878", "g1070", "g3047", "g2873", "a6462", "g8193", "g7906", "g4268", "g4122", "g476", "g4023", "g3814", "g4124", "g8731", "g6252", "g2667", "g8050", "g3687", "g1508", "g1648", "a2985", "a1952", "g4171", "a2434", "g783", "g4003", "g2718", "g6316", "g1141", "g6213", "g1703", "g1065", "g1216", "g3435", "g6688", "g8269", "g5497", "g534", "g3632", "a2964", "a2390", "g1852", "g2203", "g172", "g1305", "g3401", "g2072", "g1241", "g1523", "g375", "g2555", "g2171", "a5750", "g1769", "g8543", "g3801", "g2688", "g2404", "g6594", "g1281", "g1134", "g1143", "g579", "g19", "g3439", "g8561", "g7707", "g4086", "g6511", "g1487", "g7788", "g638", "g3581", "g2553", "g2443", "g1454", "g8255", "g45", "g3580", "g1012", "g47", "g6458", "g3775", "g347", "a3117", "a2426", "g772", "g599", "g1660", "g5639", "a2719", "g3043", "g4929", "g576", "g481", "g4881", "g4172", "g989", "g2917", "g7898", "g1425", "g4127", "a4915", "g3972", "a6589", "g6592", "g3488", "a5743", "g3808", "g8644", "g8665", "g5444", "g3599", "g5367", "g3158", "g6420", "g344", "g8825", "a4391", "g462", "g6273", "g5414", "g1620", "g855", "g7768", "g8634", "g1837", "g2724", "g5405", "g1997", "g7317", "g3773", "a5722", "g2328", "g3182", "g1130", "g1048", "g3140", "a2960", "g8021", "g1135", "g3617", "g3567", "g1402", "g2483", "g5727", "g2427", "g752", "g7284", "g2566", "g8639", "g878", "g1079", "g9", "g6177", "g7674", "g6424", "a6538", "g2521", "a6325", "g6104", "a5879", "g4048", "g7919", "g3511", "g200", "a2039", "a6815", "g7005", "g3688", "g7885", "g7875", "g5703", "g8109", "g8105", "g1179", "a28", "g3011", "a3266", "g8674", "a5541", "a2295", "g3930", "g8202", "g6183", "g7828", "g7837", "g7461", "g2531", "g5799", "g3710", "g5332", "g969", "g5339", "a3067", "a2626", "g3969", "g2589", "g5913", "g1618", "g3103", "g628", "a5033", "g8081", "g6430", "g3010", "g3514", "g2551", "g3518", "g1325", "g1464", "a4800", "g8017", "a2441", "g7890", "g8274", "g887", "g1876", "g5376", "g2292", "g6078", "a6743", "g3878", "g2424", "g8235", "g1285", "a6329", "g7475", "g5370", "g7910", "g683", "g737", "a6009", "g7577", "a3392", "g25", "g6286", "g3826", "g3654", "g1331", "a2644", "g867", "g4212", "g6406", "g1263", "g1643", "g1116", "g3152", "a5815", "g771", "g3161", "a4271", "g841", "g6599", "g3163", "g5778", "a13614", "g8733", "g997", "a3005", "g8221", "g723", "g3776", "g5744", "g8043", "a4197", "g1088", "g850", "g5636", "g8201", "g1610", "g1424", "a2684", "g8264", "g2864", "g4132", "g1743", "g5886", "g993", "g1410", "g8157", "g1378", "g6573", "g7931", "a2486", "g4070", "g7525", "g6396", "g4177", "g8468", "a3698", "a5318", "g730", "g1988", "a5334", "a6272", "g6012", "g2201", "g2909", "g682", "g4892", "g3671", "g545", "g7991", "g767", "g1208", "a4731", "g3658", "a5521", "g727", "g2616", "g1030", "g3312", "g1525", "g6389", "g1066", "g3800", "g5301", "g3076", "a5767", "g5694", "g7263", "g1310", "g743", "g414", "g6178", "g5773", "g3882", "g417", "g4060", "g8817", "g5812", "a6001", "g521", "a2410", "a5616", "g1350", "g7948", "g3735", "g2376", "g1032", "g1658", "a5585", "g1528", "g2384", "g3823", "g1657", "a3017", "g769", "g1671", "g1466", "g5823", "g380", "g3180", "g3730", "g7916", "g5484", "g6329", "g2545", "g6423", "a6384", "g3132", "g514", "g4953", "g6359", "g2959", "g327", "g2954", "g1256", "g1326", "a5683", "g886", "g3999", "a3226", "g3853", "g4064", "g1723", "g7983", "g6155", "g3792", "g4130", "a6874", "g3494", "g587", "g570", "g441", "g2377", "g8755", "g6301", "g657", "a3011", "g2990", "g4049", "g7057", "g1041", "g1558", "g477", "g453", "g1087", "g2938", "g6239", "g8218", "g1806", "g915", "g8395", "g2014", "g180", "a4088", "g6071", "g6318", "g502", "g3110", "g5740", "g1467", "g8531", "a6080", "g3727", "g860", "g6380", "g3396", "g2876", "g8770", "a6841", "g3502", "a3888", "g447", "g5653", "g919", "g7886", "g2760", "a5599", "a2612", "g422", "g4025", "g4285", "a5717", "g3559", "g4159", "g1792", "g5467", "g2041", "g6197", "g8347", "g4167", "g8877", "g329", "g8047", "g3507", "g3634", "g156", "g395", "a2596", "g3471", "g4258", "g4078", "g1082", "a3346", "g5683", "g1404", "g4240", "g2996", "g6211", "g357", "a3896", "g3176", "g612", "g1623", "g4075", "g4221", "g3490", "g5789", "g6073", "g934", "g22", "a2690", "g4193", "a2533", "g1111", "g1050", "g1117", "g1644", "a3692", "g216", "g802", "g835", "a410", "g6095", "g5365", "a6268", "a2292", "g5317", "a2421", "g7661", "g4059", "g1303", "g8622", "g8409", "g5480", "g7186", "g8278", "g3201", "g8368", "g2094", "g2536", "g3956", "g3848", "g8546", "g1343", "a4982", "g1200", "g4104", "g3552", "a4899", "g5406", "g2745", "g1128", "g3864", "a3876", "g3627", "g2871", "g8156", "g1576", "g1212", "g2549", "g1567", "g630", "g7338", "g1164", "a3870", "g8265", "g5451", "g3133", "g8015", "g2519", "g7782", "a2815", "a6737", "a4395", "g601", "a6519", "g1781", "g8108", "g2719", "g1958", "g1389", "g621", "g443", "g3631", "a6739", "g2564", "g7268", "a5531", "g5364", "g3112", "g703", "g4275", "g6084", "a6420", "g2215", "g929", "g1462", "g3217", "g6356", "g4101", "g808", "a6918", "a2540", "g744", "a5615", "g2055", "g3068", "g59", "g715", "g2919", "g826", "g3054", "a5962", "g2113", "g6349", "g6188", "g3547", "a2460", "g8079", "g3197", "g556", "g6077", "g7872", "g3499", "g1284", "g6288", "g4081", "g513", "g5848", "g3515", "g4267", "g5478", "g4196", "g8104", "g2950", "g3095", "g6450", "g912", "g1025", "a2399", "g597", "g1457", "g1838", "g7266", "a6879", "a3775", "g1172", "g5381", "g3708", "g3147", "a4406", "g3745", "g7473", "a4221", "g1183", "a5244", "g2927", "g8029", "g6442", "g6326", "g3935", "g361", "g1443", "a5562", "g3563", "g1504", "g1785", "g1727", "g5843", "a3079", "g1300", "g5415", "a5641", "g497", "g3088", "g6328", "g1594", "g3004", "g5584", "a2555", "g6572", "g4184", "a4435", "g7830", "g6291", "g3964", "a4379", "a6476", "g1093", "g3750", "g99", "g5749", "g2397", "g620", "g2903", "a3506", "a6800", "g950", "g7676", "g4096", "a3015", "a2994", "a3187", "g6186", "g5207", "g711", "g5507", "g5431", "g2890", "g2550", "g3852", "g2198", "g1590", "g3855", "g3799", "g5326", "g3616", "g3670", "g3747", "a6721", "g8217", "a6521", "g1153", "g2599", "g2195", "g8219", "g6547", "g7853", "g1095", "g6421", "g6215", "a6399", "g3590", "a2769", "g955", "g609", "g6576", "a1682", "g1479", "g455", "g44", "g3780", "g4957", "g583", "a5523", "g2612", "g7321", "g7868", "a2430", "g8420", "g3549", "g6368", "g1230", "g4923", "g8521", "g523", "g6341", "g524", "a5617", "g5366", "g2204", "g8088", "g7758", "g6217", "g1596", "g3724", "g5469", "g516", "g8121", "g3803", "g5709", "g7319", "g811", "g8068", "a6478", "g7472", "g5637", "g3085", "g8229", "g2001", "g3159", "g1779", "g5586", "g4116", "g842", "g6692", "g8207", "g1188", "g1924", "g2124", "a4023", "g3306", "g7982", "g6185", "g5786", "g1569", "g7851", "g3692", "g1780", "a5566", "g7909", "g6172", "g748", "g7450", "g3111", "g3102", "g1124", "g3055", "g2168", "g6229", "a6741", "g982", "g8739", "g3805", "a5760", "a5452", "g2071", "g8544", "g1044", "g8254", "g5818", "g994", "a5734", "g6600", "g8005", "g8374", "g7550", "g8055", "g8680", "g528", "g8728", "g968", "g8033", "g5938", "g3218", "a4538", "g3691", "g1340", "g4211", "a6191", "g7211", "g2205", "g8717", "g3851", "g1067", "g6693", "a5412", "g5441", "g3604", "g8683", "g1051", "g3983", "a5538", "g1675", "g1580", "a2432", "g1772", "g5312", "g6311", "a2991", "a7041", "g8559", "g7271", "g824", "g8348", "g861", "a4456", "g2674", "g1526", "g1334", "g5072", "g2593", "g1634", "g2960", "g3794", "g5748", "g718", "g8215", "a3160", "a6233", "g353", "g6184", "g1218", "g1539", "a6038", "g5616", "g4015", "a6885", "a3803", "g54", "g7417", "a2766", "g3537", "g2351", "a2767", "g2110", "g8737", "g7264", "g1258", "g695", "g868", "g6591", "g313", "g3842", "a5794", "g6376", "g893", "g3695", "g3656", "g1478", "g1240", "g2854", "g5495", "g1710", "g6342", "g7021", "g1184", "g6993", "g5878", "g2722", "g617", "g3945", "g2969", "g3629", "g6443", "g974", "g3783", "g6065", "a6245", "a6622", "g2448", "g4061", "g7811", "g1516", "g5350", "g8183", "g5677", "a4430", "g7956", "g2433", "g1094", "g2543", "a5745", "g1279", "g2585", "g2036", "g3564", "g1551", "g3620", "g5753", "g3554", "g1307", "g5906", "g4213", "g6981", "g1530", "g5866", "g1646", "g5733", "a5516", "g2006", "g1754", "g2576", "g5580", "a3354", "g8476", "g5454", "g2577", "g2385", "g1000", "g1588", "g1761", "g3472", "g2542", "g2051", "g606", "g6270", "g5874", "g5521", "g3434", "a3861", "a3606", "g2496", "a6144", "g5462", "a2420", "g892", "g3685", "g6569", "g2192", "g2530", "g8031", "g3216", "g6431", "g1204", "g631", "g1368", "g3694", "g8150", "g3613", "g7455", "g3169", "g3639", "a2423", "a5963", "g2045", "g1013", "a3880", "g8098", "a5560", "g3931", "g6355", "g4129", "a2663", "g6391", "g7959", "g3441", "g6507", "g419", "g3237", "g4192", "g5855", "g1098", "g371", "g1609", "g7323", "g408", "g1996", "g6309", "g733", "a2781", "g8084", "g342", "g1042", "g6521", "g3898", "g8160", "g479", "g6327", "g3924", "g797", "g5403", "a5305", "g1338", "g6433", "g3849", "g5109", "g6201", "g8441", "g1793", "g5784", "g2730", "g1339", "g6416", "g5798", "g6544", "g756", "g1586", "a3926", "g1185", "a5479", "g1358", "g5615", "a2479", "g7763", "g1550", "g1168", "g5506", "g8539", "g8039", "g5487", "g8276", "g5862", "g5940", "g7858", "g6314", "g3179", "g3867", "a5729", "g678", "g3437", "g1009", "g891", "g6690", "g3591", "g6980", "g7940", "a6011", "g3014", "g8520", "g3289", "g8101", "g1274", "g7769", "g2761", "g6602", "g1014", "a3495", "g4117", "g8070", "g3492", "g2406", "g8191", "g1144", "g924", "a6469", "g5385", "g1080", "g384", "g3643", "g8421", "g3875", "g7849", "a5805", "g4000", "g1333", "g8510", "a2494", "g6562", "a4411", "g7925", "g3859", "g2", "g1822", "a5713", "g2598", "g3759", "g3971", "g8097", "g8480", "g610", "a6108", "g8030", "g830", "g4014", "g6434", "g3444", "a5603", "g876", "g6403", "g6809", "g702", "g1356", "a2378", "g5702", "g5662", "g7790", "g6564", "g2683", "a6240", "g4214", "a4959", "g3185", "g2900", "g4006", "g1364", "a2893", "g2026", "a5635", "a5732", "g8238", "g1060", "g4089", "g1573", "a3303", "g6996", "g8282", "g2552", "g352", "g4259", "g506", "g1762", "a5810", "g1472", "a4401", "g3648", "g8741", "g1327", "g4168", "a5642", "g764", "g5921", "g517", "g1110", "g6523", "g6474", "a6411", "g3963", "g5730", "g4011", "g1647", "g1456", "g4237", "g5624", "g1063", "g5735", "g6192", "g2888", "g427", "g2887", "g1337", "g8393", "g6563", "a4289", "g393", "a5786", "g1233", "a2750", "g1146", "g6195", "a6596", "g5698", "g5770", "a5712", "g5939", "g1591", "g452", "g2410", "g799", "g632", "g7897", "g2896", "g3519", "g2403", "g1394", "g5822", "g5416", "g1247", "g554", "g1665", "g6284", "g4984", "a2311", "a5262", "g2610", "g1829", "a6227", "g3796", "a5804", "g1568", "g1115", "g3763", "g983", "g1841", "a5273", "g951", "g701", "a4440", "g4175", "g7864", "g7953", "g8789", "g2497", "g614", "g5648", "g5481", "g7855", "g1408", "g1540", "g5779", "g8356", "g7663", "a5658", "g897", "g3035", "g2675", "g4280", "g1244", "g3890", "g3832", "g2567", "g1071", "g2154", "g598", "g6081", "g3222", "g7895", "a5529", "g5739", "g976", "g577", "g571", "g470", "g6401", "g1465", "g7289", "g6290", "g6475", "a2482", "g670", "a3873", "a3227", "g6106", "g5787", "g41", "g3663", "a5893", "a5637", "g1799", "g8432", "a2526", "g6596", "g3566", "g3904", "a2435", "g849", "g499", "a5298", "g694", "a6480", "a6888", "g1562", "g2004", "g5421", "g2721", "a6520", "g5437", "a6638", "g4216", "g857", "g8195", "g1173", "g6361", "g4863", "g4099", "g5491", "a4729", "g7318", "g713", "g809", "a6761", "g5660", "g449", "g2573", "g8242", "g1210", "g3675", "g7975", "g3398", "g3207", "a5993", "g6298", "g5936", "g5303", "a5307", "g3416", "g6447", "g8551", "g4142", "g3354", "g6267", "g8155", "g3498", "g1324", "g2870", "g423", "g1534", "g1685", "a6831", "g8530", "g7937", "g7456", "g6407", "g6551", "g3447", "g2646", "g4136", "a4765", "g3939", "g921", "g6982", "g6441", "g2731", "g3841", "g1102", "g1788", "g5919", "g8540", "g4927", "g2962", "g941", "g2478", "g8071", "g310", "g1607", "g376", "g7708", "a3875", "g8623", "g8552", "g2511", "g2176", "g8009", "g2090", "a3014", "g7998", "a6145", "g2554", "g4980", "g5620", "g1062", "g1543", "g6546", "g1612", "g4264", "g3889", "a4808", "g5736", "g1419", "g1599", "g3384", "g8236", "g708", "g3063", "g6593", "a2594", "g1308", "g1734", "g2897", "g3015", "a2289", "g7449", "g2498", "a4087", "g6895", "g1409", "g698", "g3009", "a5766", "g1353", "a5842", "g716", "g845", "g3716", "a447", "g8290", "g5684", "g5413", "g1483", "a2586", "g663", "g4896", "g7943", "a4813", "g5806", "a5692", "g1007", "a2518", "g7923", "g1433", "g2685", "g2964", "g7322", "a3712", "g5325", "a5393", "g5500", "g5745", "a5792", "g1232", "g986", "g2020", "g6352", "a6022", "g1720", "g1382", "g4041", "g6553", "a5589", "g1668", "a6436", "g3847", "g1226", "g3645", "g6220", "g3820", "g1621", "g8002", "g10", "g3402", "g3233", "g3428", "g4256", "g8185", "g1521", "g6182", "g785", "a2335", "a4787", "g519", "g3224", "a3497", "g5900", "g5434", "g7059", "g5619", "g3811", "g355", "g1024", "g3461", "a3710", "g8876", "g906", "g1782", "g6295", "g5781", "g4073", "a2315", "g1302", "a3709", "g365", "g559", "g1397", "g852", "g4188", "g1154", "g6350", "a3754", "g4928", "g3060", "g7892", "g1264", "g956", "g434", "g1693", "a2297", "g7980", "a5746", "a3731", "g3550", "g8273", "g267", "a4973", "g4103", "g3167", "g3793", "g196", "a2530", "a4143", "g1277", "g8545", "g1049", "g1039", "g450", "a5391", "g3860", "g55", "g5522", "g7705", "g6076", "g626", "g6367", "g3534", "a5627", "a5714", "g5307", "g8139", "a5636", "g4272", "g6079", "g604", "g7454", "a2775", "g4115", "g1633", "g1195", "g3016", "g8761", "g5759", "a6269", "g1278", "g2428", "g4110", "g615", "a5392", "g1783", "g3417", "g5774", "g4162", "g1405", "g995", "g7827", "g402", "g3239", "g1084", "g649", "g5498", "g8086", "g837", "a3081", "g3957", "g705", "g8724", "g8856", "g6303", "g7900", "g389", "g2100", "g7938", "g2184", "a3778", "g5630", "g537", "g7993", "g8541", "g3601", "g472", "a2770", "g6524", "g2133", "g5846", "g3455", "g3704", "a6228", "g5802", "g8023", "g5917", "g8685", "g3655", "g2759", "a5999", "a5462", "g6583", "g1118", "a2636", "a4460", "g5083", "g1982", "g6472", "g7887", "g8106", "g2924", "a2635", "g7862", "g3828", "g3640", "g1849", "g5611", "g2968", "g6348", "g8317", "g944", "g7813", "g2429", "g2533", "g244", "g2913", "g4050", "g1728", "g7500", "a6192", "a5644", "g658", "g5776", "a3472", "g805", "g4862", "g8796", "g831", "a6006", "g2182", "g4276", "g7954", "g121", "g6526", "a4780", "a2956", "g3744", "g3033", "a5697", "g2734", "g8525", "g6455", "g4169", "g4289", "g2928", "g8752", "g5797", "g2546", "g1767", "g8412", "g2732", "g4072", "a5677", "g7952", "g562", "g1101", "g3725", "g7995", "a2762", "g6238", "a6830", "g763", "g2921", "g7966", "g8324", "g2073", "g8026", "a4579", "g1158", "g1209", "a6773", "g6113", "g7951", "g5362", "g3836", "g71", "g3779", "g3219", "g8094", "a6074", "g6992", "g8176", "g4872", "g7296", "a5633", "g1246", "g3138", "g5489", "g6385", "g6559", "g2762", "g3560", "g8375", "g3994", "g6299", "g8024", "g3397", "g7672", "g5811", "g4985", "a2324", "g3697", "g4273", "g2618", "g1702", "a3567", "g3029", "g602", "g6216", "g3459", "g8360", "a2961", "g2108", "a6355", "g8411", "g4261", "g645", "g5743", "g6653", "g5808", "g1699", "g5561", "g1138", "g2918", "g8501", "g3973", "g773", "g4151", "a5527", "g5650", "g5717", "a5187", "g3954", "g1292", "a6873", "g1592", "g2632", "g8245", "g1571", "a5536", "g8542", "g1800", "g7908", "g5668", "g1298", "a3844", "g5728", "g3077", "g8759", "g1773", "g8258", "g7929", "g6258", "g7958", "a5678", "a3248", "a5561", "a2674", "a5638", "g8387", "g8158", "g1735", "g5465", "g6525", "g26", "g8682", "a3929", "g4939", "g1259", "a5961", "g2032", "g7848", "g3139", "g4899", "g1047", "g1611", "g3000", "g1089", "g4202", "a5034", "a5757", "a6013", "a3141", "g1352", "g953", "g2416", "g3877", "g834", "g1705", "a2943", "g608", "g4158", "g6232", "a4049", "g448", "g1075", "g706", "g3755", "g5826", "g1105", "g8765", "g4971", "g712", "g6435", "g3693", "g3784", "g8323", "g5198", "g4040", "g3545", "a6007", "a4708", "g5763", "g7942", "a5919", "g1086", "g1473", "g1038", "a3578", "g6414", "g2523", "g896", "g406", "g7255", "g1355", "g3131", "g6205", "g8681", "g7776", "g7889", "g3578", "a5600", "g4119", "g6571", "a4755", "g4902", "a6774", "g1740", "g7963", "g5796", "g2955", "g1260", "g1059", "g7856", "g7971", "g4282", "g5760", "g717", "g4083", "g7280", "g3833", "a2359", "a3268", "g5439", "g8111", "g5657", "g5686", "g1595", "g8554", "g176", "g8256", "g2868", "g8694", "g7974", "a4812", "g1176", "g1604", "g78", "g6107", "g2158", "g5479", "g2216", "g3162", "g6204", "g1005", "g6590", "g7881", "g5315", "g5800", "g8063", "g665", "a2579", "g1286", "a2366", "a2371", "g1293", "g5475", "g1481", "g4279", "g337", "g2208", "g5922", "g8228", "g1724", "g8364", "g999", "g3199", "a3232", "g1323", "g2541", "g555", "g8275", "g2088", "a5654", "g2823", "g6986", "a2707", "g1498", "g2737", "g1441", "g178", "g3380", "a5171", "g8051", "g6260", "g7918", "g4111", "g729", "g3584", "g7965", "g1262", "g6481", "a5578", "g495", "g6554", "g1803", "g8034", "g8385", "g1544", "g5199", "g6560", "g1299", "g1194", "a3764", "g1624", "a3725", "g2494", "g8535", "g1222", "g7986", "g8240", "g2111", "g2963", "g6495", "g8175", "g7726", "g8162", "g3469", "g5435", "g8214", "g4239", "g7882", "a2944", "a5457", "g839", "g3073", "g339", "g3156", "g6695", "g7825", "a3969", "g336", "g3888", "g512", "g6990", "g488", "g460", "g1125", "a3167", "g1074", "a6037", "g565", "g650", "g1096", "g1650", "g697", "g424", "g6351", "g4120", "a6118", "g8220", "g3624", "g1553", "g1181", "g1635", "g928", "a2786", "a3242", "g2481", "g5707", "g7453", "a6696", "g3922", "g3541", "g1637", "g6322", "g3696", "g8222", "g6074", "a6532", "g661", "a2515", "g8416", "a3815", "a5668", "g6214", "g1349", "g3948", "g7997", "g3689", "g2180", "g5447", "g1500", "a2437", "g3357", "g348", "a6781", "g1420", "g4045", "a6239", "g6633", "a6283", "a6502", "g8507", "g3664", "a6395", "g1533", "a2567", "g6574", "g1238", "g6477", "g1201", "g3887", "g699", "g5708", "g1156", "g6761", "g954", "g3782", "g611", "g5409", "g8809", "g1297", "g2682", "g2613", "g3774", "a5811", "g8253", "g1367", "g4888", "g7451", "a5885", "g6381", "g3468", "g3741", "g3938", "g923", "g5509", "g7784", "g786", "g1737", "g6320", "g2645", "g1033", "g1092", "g3057", "g3751", "g1512", "g819", "g5181", "g8392", "a6852", "g634", "a5476", "g3114", "g7891", "g5213", "g2067", "g194", "a5819", "a2760", "g7844", "g8630", "g3965", "g1608", "g7261", "g8625", "g6561", "g8358", "g5380", "g2681", "g7297", "g7877", "g328", "g1354", "g4946", "g5488", "g8558", "g8080", "g8019", "g5527", "g5443", "a5579", "g6552", "a5568", "g760", "g1575", "a2976", "g1052", "g5741", "g8659", "g6510", "g738", "g2029", "g7812", "g4287", "g140", "g927", "g2509", "g1270", "g1491", "g2383", "g2440", "g2939", "g3660", "g5183", "g6372", "a4438", "g1434", "g5579", "g1174", "g2680", "g381", "g354", "g5323", "g979", "g3831", "a5816", "g4137", "g3187", "g3722", "g2120", "g3701", "g4071", "g2739", "g5742", "g8058", "g409", "g884", "g552", "a4963", "a5710", "g1756", "g4008", "g7946", "g636", "a5695", "a3959", "g6589", "g5494", "g5375", "g7254", "g8341", "g8705", "g399", "g3445", "g8414", "g8011", "g985", "g2582", "g1386", "g8067", "g1106", "g3606", "g1344", "g7932", "g5935", "a3009", "g269", "g1983", "g2211", "g2902", "g2116", "g3756", "g201", "g8188", "g1202", "g3558", "g742", "g2866", "g8396", "g1090", "g3605", "a6004", "g7869", "g1411", "g3598", "g3400", "g182", "g8845", "g5369", "g1619", "g1437", "a2878", "g1322", "a4051", "a5597", "g564", "g439", "g5331", "g693", "a5823", "g8322", "g3854", "g5384", "g5486", "g1797", "g5377", "g7760", "g6995", "g1387", "g4190", "g662", "g8196", "a6766", "g5510", "g8633", "g379", "a5268", "g975", "g6340", "g6221", "g1197", "a3871", "g3661", "g4030", "g791", "g6313", "g1843", "g873", "a5977", "g475", "g4252", "g7281", "g5172", "g2746", "g3050", "g778", "g920", "g1175", "g2137", "g548", "g1251", "g5490", "g1659", "a695", "g8042", "g4194", "g1182", "a4226", "g1510", "a4720", "g3817", "g3164", "g757", "g8514", "g6225", "g5492", "g536", "g333", "g5201", "g6200", "a2664", "g6499", "g5583", "g8646", "a6882", "g1341", "g6283", "g1597", "g60", "g6287", "g5912", "g8096", "a5050", "g5819", "g863", "g3927", "g5720", "g1253", "g981", "g1162", "a5718", "g2756", "g1535", "g1721", "g8557", "g1574", "g2694", "g1798", "a2765", "g6307", "g1674", "g3810", "a5079", "g7009", "g3802", "g8748", "g801", "g7829", "g4084", "g2518", "a6357", "a4892", "g836", "g1064", "a6313", "g5782", "g8046", "g593", "g2752", "g6179", "g7835", "g4286", "g3626", "g1265", "g8369", "g4205", "g5106", "g4139", "g1653", "g6069", "g635", "a3234", "a6455", "a5969", "g8428", "a2456", "g3615", "g5762", "g7806", "g7866", "g3404", "g2678", "g5723", "a2649", "g8448", "g1664", "a3089", "a2356", "g7283", "a5622", "g8743", "g1726", "g3308", "g2741", "a5587", "g2405", "a449", "a6000", "g8734", "g7654", "a5606", "a4532", "g2418", "g1556", "g1291", "a6343", "g6007", "g1493", "g3952", "g5455", "g2720", "g5667", "a12616", "g7854", "g8252", "a5396", "g8126", "g8145", "a5753", "g4135", "g2213", "g1605", "g6080", "g6575", "a2589", "g3934", "g6476", "g6219", "g6280", "g8260", "g589", "g5090", "a2899", "g4134", "g5666", "g1742", "g4054", "g4204", "a3265", "g2970", "g7671", "g3925", "g346", "g1234", "g4165", "g960", "a5634", "g5844", "g1825", "g6373", "a6640", "g4201", "g3622", "g4087", "g3438", "g5523", "g3576", "g8449", "g5582", "g4022", "g5701", "g5791", "a3246", "g5679", "g3486", "a5528", "g1520", "g1428", "g1722", "g3284", "g5192", "g1449", "a6049", "g1223", "g3623", "g1845", "g1694", "g1716", "g6333", "g4076", "g1205", "a2336", "g6400", "a4805", "g3812", "g5603", "g8631", "g4912", "g8161", "g795", "g1999", "g3134", "g6548", "g592", "g1186", "g3075", "g6294", "g851", "a4816", "g5456", "g971", "g7664", "g6174", "g964", "g403", "g2989", "g3081", "g7884", "g6319", "a4677", "g2686", "g7822", "g5804", "g8533", "g8174", "a3197", "g3791", "g2562", "g6699", "a5818", "g1684", "g531", "g2710", "a5796", "g2684", "g3475", "g4183", "g415", "g5876", "g4033", "g668", "g707", "g5803", "a5895", "g2587", "g6317", "g3540", "g2207", "a3280", "g485", "a6005", "g6555", "g8249", "g8076", "g1006", "g3652", "g1820", "g3523", "g2717", "g817", "g881", "g1056", "g762", "g437", "a4390", "g7290", "g1711", "g3565", "g1636", "a3123", "g8777", "g5228", "a2439", "g3136", "g1666", "g840", "g436", "g5382", "g3079", "g6607", "g5647", "g1546", "a6531", "g7276", "g5534", "g710", "g6266", "g889", "g5902", "g6378", "g5091", "g1057", "g4100", "a5704", "g4253", "g4182", "g1775", "g8532", "g3926", "g8224", "g7723", "g5807", "g8120", "g3391", "g1998", "g2076", "g8853", "g3786", "g7847", "a5564", "g3879", "g5712", "a2774", "g5407", "g8549", "g4203", "g1390", "a5604", "g1008", "g2891", "g5693", "g3830", "g7789", "a5808", "g487", "g2401", "g5665", "g1515", "g1589", "g2398", "g3548", "g3589", "a2407", "g2289", "g5852", "g3880", "g6388", "g898", "g7893", "g8487", "g2381", "g2163", "g359", "g6565", "g7019", "g4102", "g3155", "g3834", "g4037", "g6366", "g5785", "g1989", "g1459", "g4935", "g1145", "a2955", "g5205", "g7814", "a4434", "a2967", "g3034", "g8027", "a4462", "g3986", "g1752", "g8090", "g7939", "g5827", "g324", "g1830", "g360", "g998", "g421", "g1497", "g3662", "g4244", "g1527", "g7767", "a4400", "g6009", "g530", "g8881", "g8547", "g7474", "g8148", "g5189", "g6608", "g7298", "g3677", "g3569", "g7907", "g7703", "g6451", "g5322", "a5664", "g2008", "g1254", "g8095", "g3375", "g6224", "g5623", "g7141", "g1035", "g3921", "g1413", "a2375", "g2590", "g7840", "g561", "g3539", "g5725", "g8054", "g5449", "g2526", "a4707", "g4926", "a3163", "a1872", "g6265", "g1514", "a3026", "g539", "a5224", "g6364", "g7859", "g8815", "g1518", "a3745", "g4082", "g8153", "g8270", "g3407", "g5638", "g5649", "g6360", "g3587", "g6445", "g4941", "g1369", "g3857", "g8797", "g5517", "g5659", "g1718", "g5718", "g1670", "g3883", "g8359", "g2408", "g358", "g8693", "g5626", "g667", "a6620", "g2487", "g5726", "g7979", "g4039", "g1346", "g8052", "g8151", "g5349", "g4281", "g5614", "g653", "g3707", "g2074", "g2763", "g5468", "g4005", "g3970", "g8325", "g1461", "a2776", "g2200", "a3135", "g3679", "g388", "g5440", "g8237", "g525", "g2185", "g3651", "g1482", "g2000", "g3056", "a2333", "g1548", "g5436", "g8377", "g7962", "g1313", "g4263", "g5418", "g7132", "g390", "g1844", "g1400", "g1613", "g6413", "a4449", "g3184", "g5676", "g4265", "g3728", "g6473", "g4098", "g7941", "a6814", "g7691", "g709", "g6277", "g3764", "a5567", "g6181", "a6244", "g1401", "g8842", "g4962", "g8093", "g2687", "a5550", "g7200", "a5591", "g6345", "a6415", "g1746", "g8873", "a6713", "g155", "g4009", "g1021", "g1383", "g5794", "g5631", "a5466", "g2357", "g3625", "g6091", "g6404", "g3406", "g5420", "g3332", "g364", "g2220", "g7295", "g1406", "g2738", "a2798", "g3611", "g8216", "g8400", "g880", "g858", "g1365", "g1850", "g4020", "g3553", "g3104", "g412", "g1287", "g3680", "g5692", "g3608", "g1440", "g8636", "a3971", "g6603", "g1249", "a2291", "g6236", "g3706", "g7259", "g567", "g3408", "a3025", "a6917", "g5195", "g7976", "g7915", "g1055", "a5477", "g7870", "g5352", "g5771", "g977", "g1987", "g7904", "g5691", "a3492", "a2904", "g8669", "g961", "a5803", "a3194", "g1314", "a3772", "a6445", "g1100", "g4223", "g1451", "g1091", "g3937", "g5412", "g1439", "a5966", "g2893", "g903", "g5508", "g1739", "g3713", "a2655", "g6496", "g3456", "g5775", "a4775", "a5429", "a2799", "g679", "g372", "g6577", "g2574", "a5806", "g3984", "a5790", "g911", "g3394", "g2445", "g4277", "g3150", "g4271", "a4730", "g8697", "g7901", "a4743", "g7724", "a2763", "g7857", "g7008", "g8041", "g8354", "g1252", "a7063", "g1751", "g5747", "g2754", "a2428", "g2958", "g902", "g776", "g6324", "g7973", "g963", "g3761", "g3653", "g7836", "g8556", "g4219", "g3863", "g2150", "g627", "g2451", "g6212", "g2729", "g7996", "g432", "g1617", "g3574", "g2228", "g4200", "g3484", "g2219", "g3967", "g8461", "g3932", "g4043", "g425", "g2484", "g1320", "g4965", "g8721", "g4245", "a5661", "g8716", "g1565", "g965", "g3676", "a7024", "g4920", "a4990", "g3816", "g43", "g8018", "g3012", "g1542", "g302", "g1764", "g1043", "g3572", "g6198", "g1468", "g8704", "g6111", "g1028", "a6016", "g8326", "g6275", "g3950", "g859", "a6341", "g3074", "g7354", "g4260", "a5964", "g6384", "g5227", "g945", "g5918", "g8198", "g2402", "g6315", "g3410", "g2447", "g3006", "g6587", "g3785", "g3238", "g4181", "g1683", "g827", "g4189", "g4013", "g3003", "g2700", "g1672", "g1371", "g7772", "g3815", "g8049", "g8306", "a6235", "g8555", "g1541", "a6859", "g7018", "g563", "g1347", "g6382", "g1572", "g4208", "a5571", "g926", "a5741", "g4970", "g6991", "g3772", "a5970", "g1374", "g3647", "g8879", "g3868", "g6566", "a2409", "g6264", "g4180", "a3186", "g1564", "g1328", "g3870", "g4010", "g1626", "g7477", "g482", "g3998", "g3381", "g7936", "g7783", "g2607", "g4254", "g8038", "g8112", "g2217", "a4392", "g3570", "g1248", "g8517", "g3798", "g1547", "g6556", "g1469", "g467", "g3460", "g8022", "g1690", "g4147", "g4001", "g335", "g1578", "g1765", "g922", "a5671", "g4278", "a5640", "a4732", "g793", "g8085", "a3781", "a6594", "g3790", "g2431", "g3638", "g8425", "g4983", "g8074", "g7911", "g6627", "g1585", "g7791", "g5618", "g7879", "a5397", "g4138", "g6347", "g3600", "a5971", "g1131", "a5608", "g1120", "a4465", "g4150", "g3732", "g5602", "g731", "g6994", "g862", "g185", "a2613", "g3997", "g2140", "g5695", "g489", "a2823", "g3582", "g691", "a3002", "g3053", "g6436", "g7267", "g1190", "g4255", "g2337", "g8280", "g3636", "g7196", "g1399", "g6365", "g4026", "g3767", "g6070", "g3869", "g1275", "g7777", "a3483", "g1517", "g2153", "g1993", "g331", "g7874", "g5622", "g2985", "g1529", "g2595", "g1627", "a2777", "a2392", "g1304", "a6778", "g4274", "g7270", "g790", "a5910", "g7860", "g5809", "g5933", "g6452", "a3267", "g3107", "g5652", "g1396", "g7142", "g5428", "a6871", "g3602", "g3789", "g4290", "g8316", "g8418", "g2583", "a6345", "a2411", "g3923", "g6281", "a6471", "g2715", "a3292", "g8210", "g1784", "g4170", "g1161", "g7316", "a2353", "g7876", "a1636", "g1561", "g2114", "g6330", "a6187", "a5662", "g4992", "g8257", "a4529", "a3083", "g569", "g917", "g5750", "g673", "g6029", "g351", "g1509", "g7195", "g6363", "g1157", "g1725", "g6256", "g3702", "g1759", "g7279", "g6253", "g7471", "g2342", "g546", "g3495", "a5877", "g4128", "g2914", "g5024", "g8824", "g3092", "g8747", "a5800", "g3979", "g1458", "a6461", "g5519", "a5699", "a2600", "g3635", "g8271", "g3363", "g1137", "a3302", "g4227", "a7045", "g3714", "g3042", "g8706", "g366", "g8509", "g1384", "g8657", "a6705", "a3054", "a5455", "a2901", "g7491", "a2041", "g5530", "a6406", "g8069", "g1206", "g3196", "g8122", "g1656", "a6489", "g1199", "g2191", "g2704", "a4943", "g6235", "g1107", "g1552", "a6536", "g6983", "g42", "a5825", "g5841", "g939", "g8107", "a3244", "g2291", "g5001", "g2164", "a5774", "g8527", "a4856", "g5792", "g3886", "g7921", "g7721", "a6382", "g8007", "g1840", "g7914", "g5429", "g2575", "g6246", "g774", "a2531", "g2379", "g3698", "g704", "g7896", "g2889", "a2012", "g689", "g2747", "g1189", "g4877", "g8284", "g8661", "a5407", "a5973", "g3982", "g4199", "g3137", "g40", "g913", "a5653", "a2890", "a5797", "g461", "g1707", "g1415", "g8738", "g8263", "g6068", "g3738", "g3236", "g4948", "a3130", "g8632", "g6336", "g5942", "g8689", "g3259", "g1147", "g2085", "g6438", "g1477", "g334", "a3110", "g3153", "g8248", "g2104", "a6708", "g391", "g8286", "g5525", "g6383", "g1770", "g385", "g2160", "a2308", "g1099", "a6465", "a5400", "g4062", "g3106", "g5389", "a4380", "g5330", "g3746", "g2592", "g5777", "g700", "a2350", "g356", "g4079", "g170", "g5634", "g6019", "g8234", "g2221", "g1855", "g3240", "g1474", "g6306", "g8064", "g8827", "g794", "g829", "g4883", "a3028", "g6250", "g1579", "g814", "g5756", "g8684", "g1159", "g5173", "g2677", "g8321", "a4741", "g6087", "g362", "g816", "g4229", "g5425", "g3510", "g696", "g4058", "g3682", "g2522", "g8722", "g1215", "g3933", "g1549", "g3819", "g4968", "g5585", "g914", "g8662", "a4848", "g5438", "g7809", "g1836", "g483", "g2139", "g2539", "g7842", "g3067", "g3753", "g3568", "g6691", "g7961", "g1835", "g2196", "g2915", "g1700", "a2716", "g544", "g3478", "g191", "a3872", "g2015", "g2417", "g3525", "g7355", "g5817", "g1805", "g6105", "a4061", "a6892", "g7787", "g1430", "a2584", "g2508", "g4933", "g8667", "g1476", "g6293", "g8787", "g3949", "g1150", "g1187", "g3840", "g1463", "g438", "g3497", "g1802", "g550", "g2513", "g4269", "g8045", "g7899", "g1220", "g1122", "g1507", "g664", "g7135", "g291", "g779", "g2046", "g8250", "g6489", "g1731", "g3806", "g1814", "g4092", "g1269", "g6206", "a2330", "g1272", "g647", "g8075", "g1426", "g2697", "g451", "a6934", "a4699", "g6086", "g8538", "g7947", "g2425", "g8424", "g6343", "a6970", "g724", "g918", "g2407", "g8247", "g1403", "g7928", "g6694", "g4027", "g7845", "g8036", "g8199", "g5459", "g3936", "g4105", "g800", "g5587", "g2965", "a3782", "g1442", "a5756", "g3778", "g8244", "g1301", "g6418", "a2552", "a5986", "g6332", "g1502", "g1524", "g1717", "g1651", "g56", "a5725", "g1501", "g6018", "a4772", "g629", "g3683", "g7990", "a3129", "g5719", "g739", "g1421", "g4077", "g7759", "g3754", "g6484", "a2299", "g2095", "g3516", "g3277", "g1296", "g3928", "g1652", "g2689", "a5243", "g1139", "g2906", "a5701", "g7863", "g6300", "g3532", "a3484", "g8208", "g5891", "g2273", "g8008", "g1375", "g2713", "g174", "g5476", "g3630", "g4238", "g6297", "a6798", "a2704", "g426", "g5685", "g7202", "g1362", "g3978", "g1351", "g1069", "a2547", "g2502", "g1791", "g7006", "g8378", "g4220", "g7905", "g547", "g5310", "g3740", "g3940", "g392", "g349", "g7673", "g789", "g1712", "g1584", "g6189", "g2733", "a6526", "g6578", "a5573", "g722", "g3583", "g5820", "g2290", "g6122", "g4108", "g8227", "g7984", "g7286", "g3577", "g5851", "g442", "g6508", "g383", "g908", "a2816", "g4144", "g1073", "a13595", "g1407", "g53", "a5960", "a5983", "g4017", "g7968", "g3061", "g5617", "g3586", "a5041", "g3876", "g8486", "g5640", "g7865", "g1192", "a5588", "a590", "g7960", "g3995", "g3807", "g4973", "g2347", "g110", "g8061", "g4112", "g916", "a5691", "g1625", "g1891", "a2222", "g8053", "a6812", "g7714", "g518", "a6274", "g3881", "g4029", "g5410", "g7293", "g901", "g1227", "g2925", "g4018", "g560", "g936", "g648", "g397", "a5406", "g5445", "g1004", "a5613", "g1126", "g3788", "g871", "g7989", "g1496", "g1519", "a2687", "g581", "g4035", "g1329", "g735", "g5417", "g5501", "a5730", "g3650", "g1395", "g1290", "g6302", "g5849", "g770", "a6222", "a5048", "a4988", "g1112", "a5387", "g5850", "g984", "a2485", "a2442", "g4251", "g820", "g7258", "g803", "g1706", "g2038", "g6075", "g6549", "g4191", "g1697", "g486", "g8860", "g1505", "g1163", "g5386", "g7861", "g2882", "g1485", "a5709", "g3585", "a2571", "g6108", "g1422", "g1715", "g3477", "g8004", "g2003", "g8812", "g1085", "g1294", "g7880", "g1363", "a2950", "g5915", "a5807", "g5903", "a2558", "g905", "a3188", "g1582", "g6021", "g2162", "g4133", "g8223", "g895", "g2470", "g1602", "g721", "g386", "a2842", "g2949", "g2755", "g7978", "g8066", "g5711", "g572", "g2679", "g8057", "g833", "g532", "g865", "g962", "a2013", "g1438", "g1423", "g2952", "g1113", "g8677", "g6595", "a6492", "g1342", "g1488", "g8065", "g1203", "g3361", "g2673", "g3941", "g3642", "g3258", "g3285", "g4004", "g7981", "g1750", "g6187", "g7452", "g6092", "g3307", "g8857", "a2563", "g8285", "g1391", "g2007", "g1054", "g780", "g369", "g4068", "g1250", "g501", "g7780", "g8349", "g459", "a2717", "g1140", "g6390", "g4257", "a5270", "g5655", "g5847", "g3715", "g2532", "a3125", "g4126", "g1045", "g5502", "g618", "g681", "g1167", "g2894", "g2480", "g1448", "g3980", "g7843", "g1686", "g1730", "g8154", "g6963", "g2501", "g2625", "g1654", "a6855", "g1778", "g5793", "a6012", "g781", "g3069", "g6089", "g5877", "g1109", "g5764", "g1166", "g4889", "g8376", "g1180", "g343", "g3097", "g4131", "g875", "a2514", "g6497", "a6875", "g4860", "g4866", "g6487", "a6241", "g7924", "g1152", "g7448", "a4671", "g3845", "a6878", "g2099", "g8272", "g3729", "g473", "a6215", "g1061", "g5724", "g8149", "g959", "g4047", "g543", "g3690", "g5751", "g8772", "g3462", "g8037", "g1046", "g4065", "g2699", "g7920", "g3491", "g8628", "a4989", "g8099", "a2317", "g1570", "g1267", "g1696", "g7315", "a2508", "g3769", "g8701", "g8078", "g4163", "g7831", "g2395", "g2690", "g5635", "g4063", "g5493", "a2354", "a6658", "g7470", "g6175", "g3129", "a4951", "g1990", "a2875", "g5581", "g1361", "g3916", "g3487", "g498", "a3249", "a2462", "g8526", "g6370", "a6659", "g4226", "g3777", "g2867", "g654", "g5628", "g1555", "g404", "g3083", "g4090", "g7871", "g7207", "g3028", "a6230", "a5873", "g1119", "g3105", "a6649", "g768", "g788", "g7839", "g1083", "g1673", "g2206", "g1606", "g535", "g6010", "g882", "a5834", "g2676", "g5780", "g7781", "g2904", "g1315", "g3556", "g3464", "g4178", "g363", "g5477", "g3178", "g5734", "g5483", "g7841", "g6090", "a3300", "g1149", "g1266", "a4067", "g6276", "g4146", "a6053", "g1827", "g526", "g457", "g5700", "g52", "a2978", "g4250", "g2179", "g8103", "g3644", "g2386", "a2977", "g5557", "g7277", "g4288", "a4747", "a6864", "g8006", "g3726", "g6353", "g3588", "g4937", "g6440", "g7765", "g594", "g1537", "a6448", "g3513", "g1453", "g5461", "g3115", "g8820", "g1289", "g1455", "g4044", "g3008", "g1503", "g8117", "g7935", "a6385", "g5629", "g7095", "g1760", "g3168", "g6255", "a2303", "g8060", "g3612", "g3446", "a6237", "g8001", "g600", "a5968", "g6398", "g2155", "g6110", "g3839", "a6817", "a5643", "a5809", "g3555", "a6194", "a2444", "a5659", "g7201", "g2399", "a2755", "a5535", "g3649", "a4054", "g996", "a2497", "g798", "g622", "g3846", "g1170", "g198", "g4051", "g2586", "g8010", "g6230", "g1480", "g5681", "g3157", "g2396", "g2748", "g466", "g430", "a5626", "g7838", "g943", "g1992", "g5690", "g8152", "g4140", "g728", "g7779", "g4235", "g3903", "g445", "g1377", "g6522", "g3538", "g6094", "g3603", "g5892", "g8775", "g5200", "g2540", "g5661", "g1560", "g8754", "g8647", "g2926", "g8670", "g2265", "a3704", "a6919", "g8025", "g6109", "a2593", "g4291", "g6072", "a2541", "g368", "g6344", "g1040", "g1123", "g8671", "a4834", "g3247", "g4930", "g1581", "g3955", "a6840", "g1475", "g5696", "g1022", "g1808", "g3920", "g2991", "g2002", "g3235", "g1719", "g8177", "g2547", "g2998", "a6248", "g7282", "g637", "g350", "g5816", "g3013", "g894", "g4166", "a3013", "g580", "g2966", "a4455", "g4125", "g3557", "g5430", "a2580", "g3987", "g3262", "g1376", "g5699", "a5530", "a2483", "g480", "a4862", "g469", "g846", "g4217", "g7972", "a5974", "g6262", "g822", "g720", "g1237", "g872", "g7020", "a5480", "a3649", "a6362", "g642", "g8673", "g48", "g5689", "a3991", "g5104", "g578", "g1583", "g8500", "g633", "g3824", "a6671", "g3165", "g8624", "g6479", "g8327", "g4034", "a1930", "g444", "g6223", "g1372", "g3723", "g6377", "g3797", "g494", "g3431", "a4268", "g7922", "g3985", "g8467", "g8299", "a2880", "g3562", "a3031", "g4174", "a5820", "g8506", "g4283", "g6180", "g8259", "g1817", "g1824", "g3673", "g6362", "a2506", "g8307", "g5853", "g8044", "g4185", "g8660", "g1682", "a5565", "g3804", "g2569", "g1193", "g6234", "a2455", "g3637", "g6279", "g3752", "a3008", "g7299", "g5361", "g8637", "g3286", "g2728", "g2584", "a3256", "a5596", "a5583", "g655", "g2430", "g6261", "g1687", "g5805", "g5916", "g1311", "a3078", "a6052", "a3305", "g8225", "g3768", "a3217", "g6278", "g491", "g987", "a2757", "a5292", "g1538", "g8062", "g8178", "g1068", "g8239", "g8422", "g651", "g1471", "a2768", "g4870", "g3762", "g3765", "a2293", "g6375", "g7826", "g1335", "g5737", "g4938", "g1766", "g4907", "a5223", "g3748", "g2520", "g1758", "g3265", "g5824", "g6208", "g1995", "g374", "g624", "g540", "g6500", "g3866", "a2603", "g8460", "g1810", "g8534", "g468", "g1379", "g6605", "a5554", "a6143", "a6443", "g1796", "g7927", "g1747", "g4118", "g1034", "g8553", "g8266", "g1318", "g8355", "g4016", "a6425", "g7762", "g4979", "g1629", "a5798", "g3533", "g4113", "g4222", "a5721", "g616", "g6604", "g330", "g3944", "g3871", "g8146", "g1981", "g1191", "g2911", "g8102", "g619", "g6269", "g5687", "g8511", "g6597", "g4085", "g7690", "g7314", "g2048", "g8512", "g4198", "g585", "g8764", "g692", "a6435", "g1129", "a2504", "g1738", "a2536", "g3364", "g6689", "g338", "g8181", "g1432", "g5757", "g6274", "g656", "a2974", "a3455", "a6015", "g1763", "g2446", "g967", "g1681", "g7260", "g6011", "g6387", "g2135", "a5738", "g3489", "g4981", "g3465", "g4114", "g3232", "g6017", "g188", "g2725", "g5324", "g1688", "g1416", "g7878", "g8413", "g8119", "a2402", "a6219", "g1357", "g1701", "g8114", "g8186", "g4012", "g2152", "g4153", "a5657", "a4826", "g7766", "g4036", "g2905", "g3448", "a5716", "a2709", "g8180", "g1492", "g3093", "g7017", "g5446", "a5532", "g2227", "g5674", "g2477", "g6358", "g3795", "g340", "g2588", "g4155", "g8035", "g4097", "g3544", "g3096", "g810", "g5845", "g5448", "g5068", "g930", "g1714", "a2538", "g1414", "g3476", "g5934", "a2477", "g7269", "g4906", "g5588", "a5580", "g1020", "a5577", "g7917", "g3290", "g1786", "a10947", "g515", "g5670", "g856", "g1242", "g8791", "g549", "g2544", "g6199", "g6271", "g2485", "g6558", "g1787", "g5937", "g3373", "g787", "g2212", "g8397", "g4964", "g1755", "g1018", "g6312", "g825", "g1490", "g8083", "g6296", "g1446", "g3405", "g966", "g1136", "g2556", "g5754", "g3862", "g541", "g1842", "a5620", "a6522", "g6456", "g6176", "g2643", "a3006", "g1506", "g3686", "a4452", "g7912", "g586", "g4067", "g3897", "a5274", "g3610", "a5619", "a4458", "g463", "a4894", "g7462", "g3536", "g1732", "g4109", "g4093", "a5843", "g7457", "g8626", "g465", "g5801", "g1600", "g3733", "a2606", "g493", "g5463", "a2988", "g8672", "a5173", "a559", "g3355", "g3440", "a2949", "a6346", "g4160", "a5802", "g8267", "g1121", "g5464", "g6337", "g1036", "g8283", "g3253", "a6583", "g4057", "a4758", "g5337", "g1667", "g4107", "g8028", "g1245", "a2367", "g5755", "g5904", "g1601", "g1563", "g6417", "g3609", "g899", "g5746", "g6392", "g5504", "g1828", "a2971", "g2330", "g680", "a2616", "g2438", "g7464", "g4056", "a2383", "g870", "g1494", "g8092", "g1219", "g405", "g411", "g732", "g6408", "g3618", "g1366", "g5772", "g1280", "g3090", "g7570", "g5680", "g8209", "g2654", "g8110", "g5028", "g4055", "g3436", "g1522", "g7761", "g1288", "g1273", "g3809", "g1276", "g690", "g8100", "g5388", "g6374", "g8127", "g7969", "g854", "g345", "g6305", "g1794", "g3861", "g5411", "g4002", "g933", "g1823", "g3479", "g189", "g1235", "a6224", "g8147", "a7011", "g5466", "a1461", "g7262", "g2910", "g6190", "g8658", "g8279", "g3509", "g806", "a5609", "g8231", "g5458", "g573", "g823", "a2996", "g3087", "a3082", "g8331", "g4091", "g5814", "g8645", "g398", "a6044", "g1680", "g6409", "a6319", "g3700", "g1178", "g5503", "g8173", "g7278", "g2711", "a6712", "a2522", "g6085", "g6457", "a5611", "g719", "a3990", "g3358", "g7950", "a5056", "g7930", "g3843", "a3172", "g4987", "a6027", "g4982", "a2010", "g796", "g2005", "g7888", "g166", "g1385", "g3699", "g5610", "g4019", "g843", "a5724", "g429", "g1037", "g591", "a2788", "g3827", "g7675", "g5432", "g7253", "g4236", "g3508", "g3835", "g6154", "g1679", "g7988", "g5372", "g8159", "g3614", "g8686", "g3480", "g2049", "g1776", "g5671", "g1427", "g3684", "g3712", "g5190", "g6203", "g8032", "g7850", "g7867", "a3650", "a5399", "g685", "g306", "g522", "a2948", "g1677", "g5658", "g672", "g3669", "g4148", "g5601", "g7964", "g5788", "g1295", "g413", "g5795", "g7807", "g2510", "g3409", "g557", "g83", "g671", "a6344", "a4477", "g6357", "a5625", "g1663", "a2015", "g7977", "g1221", "a2566", "g5408", "g761", "g7967", "g1532", "g6697", "g5345", "a2391", "g5188", "g1603", "a3128", "a6280", "g7023", "g7903", "g4974", "g2389", "g7820", "g1649", "g909", "a3075", "g5191", "g818", "g5600", "g464", "g2874", "g1615", "g8373", "a6880", "g4106", "g8315", "g1804", "g3771", "g5520", "g815", "g3503", "g1896", "g6263", "g2023", "g932", "g4145", "g1692", "g3154", "a4751", "g3099", "g8040", "g3256", "g5427", "a5728", "g4206", "g3086", "a4950", "a5621", "g3943", "g8727", "g8629", "g8268", "a5614", "g3743", "g2609", "g490", "g3051", "a2559", "g4186", "g3524", "g5722", "g5196", "g7010", "g1077", "g3856", "a2973", "g643", "g4925", "g2764", "g1345", "g2189", "g3227", "g2922", "g8281", "g6016", "a2970", "g1986", "g652", "g370", "g3825", "g8537", "g641", "a4506", "g4053", "g51", "g1645", "g2186", "g775", "g435", "g382", "g558", "g1689", "g6338", "g2751", "g6191", "g5252", "g2999", "g4179", "g740", "a3192", "g3209", "g3709", "g5664", "g8762", "g2187", "g6207", "g2126", "g510", "g7507", "g4156", "g4893", "g3736", "a6828", "a6849", "a6966", "g5710", "g456", "g2224", "g3579", "g6422", "g6346", "g5327", "g869", "g3720", "g4197", "g734", "a3111", "a3440", "g6984", "g4176", "g726", "g2016", "g5704", "g8125", "g5752", "g3902", "g1398", "g2879", "g7670", "g1807", "g3467", "g4123", "g3822", "a5133", "g3731", "g2560", "g8187", "g3493", "g3607", "g4152", "g603", "g2226", "g3742", "g1809", "a4489", "g1577", "g8230", "g5627", "g6626", "g6193", "g1133", "g192", "g1228", "g3522", "g2159", "g8516", "g3619", "g6222", "g3873", "g1818", "g8243", "g553", "g1213", "g2557", "a4240", "g1224", "g1811", "g400", "g1460", "g8726", "g5813", "g6257", "g5211", "a5618", "g8363", "g7203", "a2611", "a4668", "g2726", "g492", "g2499", "g2009", "g2423", "g6545", "g8000", "g675", "a2304", "a2569", "g4173", "a2646", "g1225", "g1499", "g1771", "g3592", "g3072", "a4055", "g582", "a3161", "g2165", "g6304", "a6629", "g1108", "g3821", "g3078", "g3929", "g8232", "g2706", "g8844", "g847", "g1926", "g2981", "g7945", "g2869", "g203", "a6485", "a5924", "g1031", "g7136", "a2808", "g6399", "g5578", "g8823", "a6387", "g4080", "a3505", "g1207", "g2400", "g4922", "g8357", "g5875", "g2422", "g804", "g5678", "g910", "g4038", "g4887", "g646", "a2665", "g7926", "g1851", "g8784", "g7305", "g6339", "g2241", "a5185", "g1268", "g7722", "g6202", "g7934", "a5733", "g4894", "g7913", "g2740", "g50", "g3442", "a3805", "g6429", "g1359", "g4874", "g1628", "g3837", "g640", "g3071", "g7291", "g3659", "a2492", "g7808", "g418", "g3037", "a6631", "g3646", "g6289", "g6419", "g1566", "g5404", "g758", "g8676", "g1229", "g509", "a2772", "g747", "g1832", "g3838", "a4531", "g1709", "a5266", "g454", "g6308", "g2570", "g4209", "g8089", "g8190", "g6405", "g4187", "g6483", "g6471", "g2039", "g2945", "g2937", "g3674", "g3198", "g8536", "g3813", "g4195", "g6485", "g3946", "g3049", "g7883", "g1388", "g1165", "g1171", "g4024", "a2894", "g1445", "g2068", "g4978", "g4066", "a5693", "g2605", "a6276", "a4007", "g8883", "a6723", "g7272", "g4950", "g1630", "a5987", "g7852", "g8390", "g588", "g8077", "g8182", "g508", "g2705", "g8749", "g3089", "a2585", "g5526", "g8012", "a5306", "g5378", "g6740", "a6141", "g1078", "g4270", "g765", "g574", "a5666", "g1833", "g377", "g3717", "g5518", "g6394", "a5556", "g8638", "g7027", "g8261", "g3621", "g2899", "g7821", "g6601", "g7256", "a5711", "g6083", "g3844", "g1557", "g5419", "g8020", "g6402", "g1", "g2872", "g3874", "g1309", "g6210", "g8124", "g1816", "g4234", "g6687", "g4094", "g3781", "g1436", "g2880", "g6415", "g3501", "g4215", "g844", "g4885", "g3058", "a3771", "g8056", "g2714", "g7275", "g7999", "g3413", "g4246", "g2923", "g1545", "g1026", "g8200", "g6013", "g8443", "a4917", "g5821", "g2089", "g4969", "g7265", "g1554", "g511", "g660", "g6606", "g931", "g900", "g1985", "g2572", "g759", "a5918", "g2175", "g1114", "g937", "g503", "g3505", "g1991", "g5505", "g957", "g3098", "g7058", "g7257", "g7764", "g8203", "g4088", "g179", "g6285", "g6448", "g7463", "g750", "g2644", "g2103", "a2011", "g7944", "g584", "g8192", "g8656", "g741", "g505", "g8560", "g5571", "g3766", "a2705", "g1160", "g2580", "a6348", "g688", "g1631", "g3749", "g5625", "g6245", "a5958", "g1132", "g5374", "g2558", "a2987", "g6331", "a5684", "g3711", "g5790", "g1023", "g1435", "g1412", "g5536", "a2312", "g367", "g5815", "g5672", "a6229", "a6324", "g4141", "g7274", "g1002", "g3276", "g684", "a2351", "g3829", "g676", "g1536", "g5474", "g866", "a6804", "g1282", "g6259", "g5738", "g5093", "g2024", "g4095", "g5706", "g8518", "g7306", "g4249", "g1489", "g1789", "a6495", "g2464", "a6321", "g991", "g5452", "g3108", "g6354", "g3891", "g542", "g659", "g5174", "g3278", "g2749", "g373", "g332", "a2814", "g6282", "g8519", "g3818", "g6567", "g3101", "g5697", "g1669", "g2380", "g8493", "g1142", "g2901", "g2169", "g341", "a4977", "g8663", "g4164", "g6696", "g6698", "g5621", "g7949", "g714", "g1271", "g3415", "g973", "g7133", "g81", "g1821", "g6196", "a6397", "g7846", "g1484", "g1148", "g2568", "g8082", "g782", "g6700", "g3188", "g5729", "g5102", "a5533", "g7785", "g6153", "a4801", "g2548", "g6310", "g5632", "g2696", "g6427", "g1380", "g2012", "a5737", "g1217", "g3481", "g4218", "g1640", "g2382", "g1348", "g5901", "a6188", "g2010", "g2409", "a4978", "g2742", "g7786", "g7287", "g3263", "g3141", "g520", "g5633", "g590", "g2801", "g766", "g3260", "g3017", "g8048", "g1622", "a6139", "a8294", "g8442", "g6321", "g1447", "g8073", "g3872", "g613", "a2334", "g6428", "a5752", "a5594", "a6839", "g4", "g942", "g5716", "g3657", "a2834", "g4207", "g1027", "a6404", "g6598", "g3183", "g5654", "g5528", "g407", "a5322", "g8091", "a3802", "g5914", "g2141", "g1662", "g5673", "g6272", "g1834", "g6379", "g8213", "g17", "a2377", "g1104", "g5533", "g6987", "a6935", "a5411", "g1418", "a6848", "g3109", "g596", "g2776", "g8087", "g736", "g2987", "g3007", "g8116", "a3804", "g6449", "a3618", "a5267", "g5390", "g8760", "a6524", "g1593", "g5482", "g3148", "g1196", "g6570", "g496", "a2365", "g8016", "g3261", "a6323", "g7873", "a6500", "g2973", "g666", "g8627", "g6480", "a5649", "g6096", "g5885", "g5", "g3177", "g7933", "g8850", "g3166", "g2951", "g2877", "g1257", "a5981", "g3641", "g2011", "g4225", "a2445", "g978", "g4021", "g1058", "g1559", "g6478", "g4284", "g754", "g5460", "g1243", "g3151", "g1486", "g8370", "g1261", "g8115", "g5383", "g4069", "g1231", "g3542", "g4042", "g5516", "a2813", "g3850", "g2908", "g5535", "g8197", "g5067", "g5705", "g1513", "g828", "g864", "g5485", "g1511", "g2013", "g2087", "g6112", "g197", "g2050", "g2916", "g2167", "g387", "g6550", "g6066", "g7994", "g1470", "g6985", "a3185", "a5394", "g4956", "g202", "g4028", "g378", "g3520", "g2875", "g625", "g1598", "g4867", "g3393", "g8118", "g142", "g8194", "g8550", "g4210", "g6325", "g958", "g6568", "g2138", "g6509", "g6410", "a2897", "g1733", "g8246", "g566", "g8464", "g3919", "g5210", "g605", "g8350", "g7824", "g2727", "g8013", "g440", "g2132", "g5893", "a6745", "a5055", "g5531", "g1255", "g527", "g1393", "g3181", "g8635", "g1691", "g879", "a4752", "g4919", "g2579", "g8123", "g6426", "g7320", "a3181", "g5783", "g7294", "a4947", "g3734", "g8113", "g6498", "g1638", "a6889", "a6795", "g1239", "g5758", "g1431", "a6507", "g8398", "g5457", "a5655", "g3466", "g1001", "a5680", "g980", "g2744", "g1695", "g1495", "a5739", "g2611", "g1103", "g3981", "g5371", "g5379", "g5387", "g753", "g3958", "g8233", "a6277", "g8277", "g6634", "a6236", "a2686", "g4007", "g1676", "g777", "g5532", "g8735", "g5094", "a5260", "a1949", "g687", "a100", "g885", "g1029", "g4121", "g2197", "g8513", "g6989", "g8287", "g644", "g2161", "g807", "g8003", "a2309", "g1319", "g595", "g1312", "g3561", "g1417", "g1450", "g6988", "g1155", "g6194", "a2298", "g6242", "a2412", "g2042", "g2982", "a6193", "g2115", "a5484", "a4058", "g6237", "g2620", "g8014", "g7774", "g7957", "a2487", "g3160", "g1011", "a2980", "g2907", "g2766", "g669", "g8189", "g3858", "g8666", "a3263", "g5651", "g3739", "g3770"]
# means, stds, medians = [], [], []
# for i in range(1, 80, 1):
#     fh = FeatureHasher(n_features=i, input_type='string')
#     hashed_features = fh.fit_transform(categories)
#     features_as_string = list(map(lambda x: str(x), hashed_features.toarray()))
#     c = Counter(features_as_string)
#     means.append(np.mean(list(c.values())))
#     stds.append(np.std(list(c.values())))
#     medians.append(np.median(list(c.values())))
#
# print(np.min(means), means.index(np.min(means)))
# print(np.min(medians), medians.index(np.min(medians)))
#
# plt.figure(figsize=(5,4))
# plt.yscale('log')
# plt.plot(range(1, 80, 1), means, label='mean')
# plt.plot(range(1, 80, 1), medians, label='median')
# plt.xlabel('# Dummy Features for Applicant Grouping Id')
# plt.ylabel('Collisions')
# plt.legend(loc='best')
# plt.tight_layout()
# plt.savefig('hashing_applicant_ids.eps')
# plt.show()
#
#
# means, stds, medians = [], [], []
# for i in range(1, 80, 1):
#     categories = ["Speyer", "Montebelluna", "Conflans-Sainte-Honorine", "Ajax", "Groningen", "Salzgitter", "Cahors", "Allendorf", "Siegsdorf", "Lengdorf", "Dundalk", "Princeton", "České Budějovice", "Alexandria", "Emerald Hills", "Vigo", "Nuenen", "Monserrato", "Petaling Jaya", "Beer Sheva", "Assen", "Chongqing", "Funchal", "Nimes", "Osaka", "Pamplona", "Schaan", "Reinheim", "Ra'anana", "Harrisburg, Pennsylvania", "Nyíregyháza", "Logroño", "Ehningen", "Irvine", "Bad Bentheim", "Manaus", "Longueuil", "Gaithersburg", "Szczecin", "Mönchengladbach", "Solon", "Thessaloniki", "Hermsdorf", "Messolonghi", "Schleswig", "Bristol", "Kingsport", "Görlitz", "Changchun", "Schriesheim", "Schalksmühle", "Gunpo-si", "Männedorf", "Delft", "Bournemouth", "Pambio-Noranco", "Tomar", "Vestone", "Blackrock", "Podkarpackie", "Thetford", "Grafenberg", "Ditzingen", "Onex", "Anderlecht", "Verona", "Kronberg", "Fürstenau", "Spartanburg", "Öckerö", "Charlottesville", "Hatfield", "Maulburg", "Frankfurt am Main", "Kobe", "Neuried", "Namur", "Thames Ditton", "Weiden", "Novi Sad", "Aschaffenburg", "Gaggenau", "Little Rock", "Riese Pio X", "Ohlstadt", "El Segundo", "Palm Beach Gardens", "Kingston upon Hull", "Swansea", "Ås", "Ieper", "Veszprém", "Monza", "Angleton", "Braga", "Leamington", "Rovaniemi", "Byron Bay", "Bruges", "Fairlawn", "Kurume", "Albizzate", "Ebeltoft", "Imola", "Attleboro", "Wilmington", "Foster City", "Schwabach", "Billdal", "Gatineau", "Anan-shi", "Anchorage", "Neustadt", "Nerviano", "Aranjuez", "Nashua", "Le Tréport", "Victoria", "Fort Lee", "Béthune", "Cologne", "Flums", "Aschheim", "Zug", "Baarn", "Palo Alto", "Calw", "Weinfelden", "Perth", "West Palm Beach", "Rivalta di Torino", "Mielec", "Newmarket", "Graz", "Metz", "Dublin", "Saint Cloud", "Velilla de San Antonio", "Clovis", "Khartoum", "Kariya-shi", "Horgau", "Southport", "Montrouge", "Bialystok", "Tilburg", "Denver", "Rugby", "Odense", "Mendoza, Argentina", "Dorset", "Macerata", "Concord, MA", "Nacka", "Presidente Prudente", "San Lazzaro di Savena", "Nehren", "Bellinzona", "Fareham", "Swindon", "Arpajon", "Noida", "Pocatello", "Abu Dhabi", "Federal Way", "Zirndorf", "Lyon", "Sant Esteve Sesrovires", "Langewiesen", "Beychac et Caillau", "Burnley", "Engen", "Beaune", "Caudebec-en-Caux", "Les Mureaux", "Valinhos", "Sandton", "Wertingen", "Selb", "Honululu", "Den Dolder", "Iași", "Wicklow", "Chattanooga, Tennessee", "Clausthal-Zellerfeld", "Matsuyama", "Gräfenthal", "Ahmedabad", "Sankt Augustin", "Wilton", "Stadthagen", "Bundoora", "Obernburg", "Kitami", "Levallois-Perret", "Overath", "Arnsberg", "Langkampfen", "San Francisco", "Pembroke", "Königsee-Rottenbach", "Alcala de Henares", "Mytilene", "Albany", "Portland", "Koka", "Cambridge, MA", "Cornellà de Llobregat", "Bonita Springs", "Annemasse", "Herdorf", "Türkheim", "Bad Camberg", "Ramat Gan", "Weßling", "Schlierbach", "Naples", "Dyssegård", "Atlantic City", "Rzeszów", "Isleworth", "Bucharest", "Beachwood", "Cheddar", "Draper", "Emeryville", "Insheim", "Ranshofen", "Lörrach", "Riehen", "Frankenthal", "Schneeberg", "Viljandi", "Targu Mures", "Kraichtal", "Oceanside, NY", "A Coruña", "Grevenbroich", "Blindern", "Magdeburg", "Lommel", "Grafschaft-Ringen", "Rheinberg", "Rorschacherberg", "Lainate", "Esbjerg", "Fresach/Drau", "Remscheid", "Leesburg", "Sondershausen", "Antwerp", "Houghton", "Aschau im Chiemgau", "Best", "Balwyn North", "Grand Cayman", "Grenoble", "Hengelo", "Zemst", "Oakland", "Pfungstadt", "Horst", "Vechta", "Triengen", "Schwalbach am Taunus", "Genk", "San Mateo", "Krems", "Shizuishan", "Tomball", "Schörfling am Attersee", "Siegburg", "Utrecht", "Vale of Glamorgan", "Freising", "Overijse", "München", "Ylihärmä", "Langley", "Dublin, OH", "Getxo", "Einbeck", "Hennef", "Ahrensbök", "Edinburgh", "Islamabad", "West Chester", "Bidart", "Wackernheim", "Waltham", "Avignon", "Sambreville", "Nairobi", "Chester", "Houston", "St. Andrews", "Rousse, Bulgaria", "Masuda", "Andhra Pradesh", "São Paulo", "Teramo", "Mairena del Aljarafe", "New Holland", "Wülfrath", "Elbeuf", "Englewood", "Parsippany", "Höchst", "Kavala", "Laufach", "Bording", "De Lier", "Grasbrunn", "Frankfurt (Oder)", "Kolbingen", "Röhrmoos", "Nottinghamshire", "Tarbes", "Leidschendam", "La Baule", "Palazzo Pignano", "Hertfordshire", "Eiterfeld", "Neuenhaus", "Uberlândia", "Richmond", "Vénissieux", "Sargans", "Nunhem", "Ferrara", "Lansdale", "Stord", "Chungju-si", "Zwevegem", "Ruppichteroth", "Sint-Truiden", "Geroldswil", "Craiova", "Berwyn", "Bulle", "Athens", "Wilson", "Osijek", "Paris", "Sorocaba", "Marshall, TX", "Lakewood", "Győr", "Logan", "virtual", "Nokia", "Simmerath", "Fuldabrück", "Hallbergmoos", "Piešťany", "Raisio", "Oostkamp", "Giesen", "Doylestown", "Tortona", "Mullsjö", "Santa Rosa", "Hamburg", "Czosnów", "Kitchener", "Eschenbach", "Brasov", "Gradignan", "Fort Lauderdale", "Purchase", "Simmern", "Matsue", "Hortolandia", "Guasticce", "Lambertville", "Großostheim", "Ribeirao Preto", "Banská Bystrica", "Elgoibar", "Fermo", "Maple Grove", "Klaus", "Senago", "Colmar-Berg", "Runcorn", "Vancouver, WA", "Marinha Grande", "Ulm", "Zettling", "Saccolongo", "Bad Heilbrunn", "Rijeka", "Kajaani", "Augsburg", "Langenfeld", "San Jose", "Arrasate-Mondragón", "Oetwil am See", "Risskov", "Farnborough", "Borgo Maggiore", "Willich", "Koskenkorva", "Adelaide", "Qurum", "Pringy", "Seneffe", "Hall in Tirol", "Akita", "North Logan", "Harahan", "Carrollton", "Deerfield", "Bad Krozingen", "Eltville", "Coimbra", "Oxfordshire", "Jeonju-si", "Reinach", "Paterna", "Alderley Edge", "Bangalore", "Tessenderlo", "Perrysburg", "Lancaster", "Hebden Bridge", "Korneuburg", "Hoofddorp", "Faro, Portugal", "Bellingham", "Koblenz", "Brugelette", "Burbank", "Kielce", "Manisa", "Iwata", "Raynham", "Wrexham", "Gorzów Wielkopolski", "Westerlo", "Andover, MA", "Stendal", "Ringe", "Hilchenbach", "Flawil", "Sialkot", "Valletta", "Hsinchu City", "Porvoo", "Harsewinkel", "Dalmine", "Gwadar", "Gräfelfing", "Hoffman Estates", "Cheongju", "Köthen", "Schwelm", "Wichita", "Balingen", "Ottobrunn", "Rock Hill", "Frechen", "Hägersten", "Tegucigalpa", "Winnenden", "Briey", "Lohmar", "West Sussex", "Manhattan Beach", "Ristiina", "Merelbeke", "Pratteln", "Szarvas", "Sophia-Antipolis", "Westport", "Komaki", "Nozay", "Lincoln, UK", "Volketswil", "Miami", "Siegen", "Heppenheim", "Erie", "Racine", "Buggenhout", "Cantanhede", "Werndorf", "Veenendaal", "Northeim", "Burgdorf", "Coppell", "Cavezzo", "6th of October City", "Schocherswil", "Cebu", "Burgbernheim", "Seongnam", "Abingdon", "Pietarsaari", "Leamington Spa", "Conshohocken", "Orbassano", "Rimbach", "Drancy", "Danbury", "Nishitokyo", "Panama City", "Edingen-Neckarhausen", "West Lafayette", "Landau", "Le Neubourg", "Maribor", "Seon", "Pfinztal", "San Biagio di Callalta", "Argostoli", "Crawley", "Wien", "Sherbrooke", "Morigny-Champigny", "Noáin", "Galway", "Lancaster, UK", "Ortenburg", "Grand-Saconnex", "Amsterdam", "Sèvres", "Blagnac", "Uithoorn", "Morley", "Savitaipale", "Vällingby", "Neu-Ulm", "Sarriguren", "Orem", "Salzhausen", "Nittenau", "Korbach", "Baja", "Saint-Germain-en-Laye", "Wesseling", "Murten", "Chicago", "Leinfelden-Echterdingen", "Rotherham", "Hameln", "Sao Carlos", "Greenwich", "Dummerstorf", "Montreuil", "Orpington", "Tucson", "St. Leon-Rot", "St. Florent", "Clarence", "Brignais", "Yangon", "Laupheim", "Leipzig", "Visakhapatnam", "Petten", "Bensheim", "Malmö", "Stade", "Nurmijärvi", "Stadtbergen", "Soltau", "Middlesbrough", "College Park", "Konz", "Lonsdale", "Wangen im Allgäu", "Ladenburg", "Varkaus", "Burnaby", "Bobigny", "Villanueva de Gállego", "West Newbury", "Paju-si", "Stevenage", "Slagelse", "Hinnerup", "Merseburg", "Mirandola", "Lomma", "Steinhausen", "Oberstenfeld", "Brookings", "Weil im Schönbuch", "Sinzing", "Castleblayney", "Herdecke", "Nieuwegein", "Werdohl", "Sansepolcro", "Aberystwyth", "Genoa", "Motala", "Rielasingen-Worblingen", "Ochten", "Kawasaki", "Samarate", "Oberdischingen", "Nieuw-Vennep", "New Britain", "Billingstad", "Badalona", "Fourneville", "Moosthenning", "Glasgow", "Tres Cantos", "Dwingeloo", "Dehli", "Duncan", "Urbino", "Oulunsalo", "Brentford", "Veitshöchheim", "Weilheim", "Marl", "Croydon", "Tuzla", "Cheltenham", "Fellbach", "Lomazzo", "Lubsko", "Beek-Ubbergen", "Jennersdorf", "Tübingen", "Villeneuve-d’Ascq", "Fridingen", "Belém", "Oakville", "Osan-si", "Grugliasco", "Clearwater", "Tägerwilen", "Loos", "Großefehn", "Lausanne", "Ibiza", "Lindau", "Thuwal", "Hyvinkää", "Crolles", "Garching", "Salo", "Chiyoda-ku", "Kristianstad", "Gränichen", "Engomi", "Urbana", "Trenčín", "Carlow", "Leimuiden", "Białystok", "Baar", "Kremsmünster", "Rousset", "Vista", "Hausach", "Buford", "Dudelange", "Dedemsvaart", "Harsum", "Kelvin Grove", "Alexandra, ZA", "Warren", "Borne", "Siegen-Wilnsdorf", "Castellanza", "Würzburg", "Bologna", "Warrenville", "Coesfeld", "Berkeley", "Lisbon", "Rauma", "Harderwijk", "Laudenbach", "Casablanca", "Winterthur", "Wallisellen", "Piracicaba", "Mont-Saint-Guibert", "Louvain-la-Neuve", "Selzach", "Harbin", "Capua", "Neumarkt", "Ávila", "Windischgarsten", "Niederbuchsiten", "Haar", "Hellerup", "Triesenberg", "Meilen", "Ghaziabad", "Faridabad", "Zielona Gora", "Tourcoing", "Clermont-Ferrand", "Toyota-shi", "Larkspur, CA", "Ilmenau", "Aindling", "Jelgava", "Malahide", "Kladno", "Caguas", "Braunschweig", "Den Haag", "Fairport", "Lastrup", "Bodman-Ludwigshafen", "Bozeman", "Ludwigshafen am Rhein", "Gremsdorf", "Sandviken", "Lincoln", "Rochester Hills", "Quebec", "Heilbronn", "Leiden", "Carpi", "Staphorst", "Taguig", "Livermore", "Mount Waverley", "Cestas", "Midland, MI", "Palmerston North", "Renton", "Hattiesburg", "Busan", "Tsukuba", "Mihara", "Mulhouse", "Haarlem", "Nagold", "Aichtal", "Timaru", "Thionville", "Rouen", "Wotton-under-Edge", "Sprundel", "Hitachinaka", "Costa Mesa", "Gillingham", "Rosenheim", "Sao Bernardo do Campo", "Athlone", "Alfdorf", "Lynge", "Helsingborg", "West Haven", "Northwood", "Pontcharra", "São Luís", "Obernai", "Thüngen", "Zagnansk", "Solaize", "Conyers", "Tinley Park", "Guarulhos", "Bendern", "Kaposvár", "Kohler", "Casella", "Bucu", "Bingen", "Zama", "Perroy", "Lima", "Haldenwang", "Wallingford, UK", "Fabriano", "Newport, England", "Santa Maria", "Neutraubling", "Cary", "Wedemark", "Sharjah", "Balmain, NSW", "Brézins", "Southborough", "Penzberg", "Stirling", "Wendlingen", "Budapest", "Siuro", "San Rafael", "Henderson", "Scorzè", "Minnetonka", "Tunis", "Diepenbeek", "Danvers", "Macquarie Park", "Curno", "Maceió", "Nagakute", "Ventura", "Newbury Park", "Flores da Cunha", "Weinstadt", "Frankton", "Annecy", "Pforzheim", "Frascati", "Zwingenberg", "Ede", "Amersfoort", "Montigny-le-Bretonneux", "Orléans", "Waurn Ponds", "Road Town", "Corfu", "Lower Hutt", "Fredensborg", "Dübendorf", "Schlieren", "Pliezhausen", "Kelsterbach", "Gemeses", "Wickliffe", "Wolvega", "Graal-Müritz", "Worms", "Rudolstadt", "Hyllinge", "Zürich", "Svishtov", "Herrenberg", "Aurora", "Vilnius", "Chalfont St Peter", "Aviano", "Sapporo", "Ringaudai", "Prato Sesia", "Leeuwarden", "Paderborn", "Baghdad", "Kurashiki", "Rezekne", "Bad Lauchstädt", "Bremen", "Aßlar", "Frümsen", "Linz", "Nova Gorica", "Allentown", "Boca Raton", "Tarrytown", "Englewood Cliffs", "Brentwood", "Jeddah", "Matran", "Montemarciano", "Senningerberg", "Horley", "Mandeure", "Saint Petersburg", "Le Locle", "Bytom", "Maastricht-Airport", "Juarez", "Koszalin", "Valbonne", "Longview", "Maintal", "Gemünden", "Itzehoe", "Vöcklabruck", "Hillsborough, CA", "Newton", "Bad Wurzach", "St. Egidien", "Basel", "Kirchlengern", "Kenthurst", "Ingelheim", "Uedem", "Fairfield, CT", "Thun", "Winscombe", "Erding", "Khanewal", "Decatur", "Zossen", "Istanbul", "Alloa", "Chevigny-Saint-Sauveur", "Dayton", "Zeulenroda-Triebes", "Menen", "Alnwick", "Huelva", "Kirkland", "Carlton", "Savona", "Herborn", "Mouans-Sartoux", "Ankara", "Bagneux", "Balzers", "Villach", "Huizhou", "Eggenstein-Leopoldshafen", "Pirmasens", "Culver City", "Roth", "Arvada", "Manukau", "Villingendorf", "Ibbenbüren", "Maassluis", "Guaranésia", "Halle", "Springfield", "Kelheim", "Faribault", "Broomfield", "Rolle", "Assens", "Kirksville", "Memmingen", "Ticehurst", "Trondheim", "Barrington", "Durban", "Agno", "Osterberg", "Luton", "Kiryu", "Florence", "Grassau", "Arras", "Adana", "Mohali", "Unna", "Neuherberg", "Icheon-si", "Northbrook", "Versailles", "Abbott Park", "Dubnica", "Niestetal", "Vejle", "Ellicott City", "Aulnoye-Aymeries", "Madrid", "Cagliari", "Orsay", "Richmond, VA", "Hilversum", "Borås", "Willebroek", "Walnut Creek", "Birsfelden", "Seefeld, Germany", "Sestao", "Guttenberg", "Boulogne-Billancourt", "Hirschaid", "Laramie", "Gothenburg", "Esch-Sur-Alzette", "Telgte", "Visp", "Reichenbach", "Västra Frölunda", "Melsungen", "Leoben", "Tralee", "Stamford", "Dearborn", "Orlando", "Saarbrücken", "Bièvres", "Tostedt", "Wuelfrath", "Main Beach", "Trittau", "Lorient", "Northumberland", "Mahwah", "Neftenbach", "Falkenstein", "Sogndal", "Burgdorf, Schweiz", "La Ravoire", "Unterhaching", "Urmitz", "Debrecen", "Notre Dame", "Kempele", "Kirchzarten", "Osterholz-Scharmbeck", "Yachiyo", "Zittau", "Woodcliff Lake", "Weißenbrunn", "Eindhoven", "Basingstoke", "Enschede", "Ennepetal", "New Milton", "Wettstetten", "Bentwisch", "Vitoria-Gasteiz", "Hofkirchen", "Groß-Zimmern", "La-Roche-sur-Yon", "Hermaringen", "Burghausen", "Stuvsta", "Kokkola", "Cartagena", "Wuxi", "Amherst, NY", "Meudon", "Herzogenrath", "Herisau", "Mikkeli", "Apeldoorn", "Antdorf", "Frome", "Niederrieden", "Prestbury", "Suita", "Carson City", "Taufkirchen", "Thornbury", "Grünberg", "Nordhausen", "Großwallstadt", "Gilching", "Medina", "Alkmaar", "Köln", "Berchtesgaden", "Messina", "Broadview", "Venaria", "Momo", "Huntsville", "Medellin", "Coursan", "Natal", "Eppelheim", "Ingelheim am Rhein", "Carugo", "Rozzano", "La Chaux-de-Fonds", "Valley", "Michalowice", "Tomaszów Mazowiecki", "Crespellano", "Bergheim", "Redwitz an der Rodach", "Norfolk, VA", "Greenville", "Montecchio Maggiore", "Ikoma", "Toronto", "Plattling", "Bargau", "St. Helena, CA", "Praha", "Massing", "Sarpsborg", "Liepaja", "Geseke", "Gushan District", "Glassboro", "Lechbruck am See", "Älta", "Saddle Brook", "Sant Cugat del Vallès", "St Albans", "Bornem", "Köping", "Sfantu Gheorghe", "Epping", "Lebanon", "Alzenau", "Chiusa di Ginestreto", "Prague", "Msida", "Elmshorn", "Wootton Wawen", "Ferney-Voltaire", "Sehmatal", "Tczew", "Orimattila", "Easingwold", "Dar es Salaam", "Mariehamn", "Colonia Polanco", "Richland", "Qinhuangdao", "Vienna", "Cagnes sur Mer", "Hanock", "Wetzikon", "Ciudad Real", "Hergiswil", "Wangs", "Sudbury", "Schloß Holte-Stukenbrock", "Mindelheim", "Guimareães", "Southampton", "Wunstorf", "Saint Paul", "Heiden", "Hattingen", "Markt Schwaben", "Heiligenhaus", "Prien am Chiemsee", "Monastir", "Seymour", "Glens Falls", "London, Canada", "Stillwater", "Sana’a", "Cham", "Lorette", "Phnom Penh", "La Madeleine", "Rueil Malmaison", "Fort Wayne", "Lisboa", "Santiago", "Kulmbach", "Farum", "Lippstadt", "Turku", "Spring", "Angevillers", "Markt Wald", "Cannes", "São José dos Campos", "Castres", "Rapid City", "Egaleo", "Worcester", "Daejeon", "Wilbraham", "Gjøvik", "Bad Homburg", "Volkach", "Herning", "Hallstadt", "Sint-Martens-Latem", "Duderstadt", "Ölbronn-Dürrn", "Herne", "Freiberg (Sachsen)", "Boronia", "Reggio Emilia", "Werder", "Puchheim", "Albstadt", "Dunedin", "Vreeland", "Neidenfels", "Sierksdorf", "Gallneukirchen", "Konstanz", "Rivoli", "St. Helier, Jersey", "Marly", "Wyandotte", "Oranienburg", "Westbury", "Ascoli Piceno", "Villeurbanne", "Vari", "Itapetininga", "Brighton", "St Andrews", "Saskatoon", "Yao", "Zerbst", "Vaasa", "Stadtlohn", "Segrate", "Efeler", "Royston", "Bitterfeld-Wolfen", "City of Industry", "Hong Kong", "Angers", "Florianopolis", "Kecskemét", "Eschborn", "Burscheid", "Addison", "Everett", "South Bend", "Watford", "Springfield, VA", "Corsico", "Kernen", "Metz-Tessy", "Marburg", "Grosbliederstroff", "Taplow", "Böblingen", "Le Port", "Austin", "Lugano", "Verden", "Gwangju", "Kiev", "Montevideu", "Rockland", "Daegu", "Freiburg", "Waghäusel", "Parma", "Hartlepool", "Cairo", "Aurich", "Oppenweiler", "Windisch", "Saverne", "Oeiras", "Bordes", "Backnang", "Pyongyang", "Tampere", "Cluj-Napoca", "Coinsins", "Histon", "Roslin-Midlothian", "Yamagata", "Schrems", "Lenzing", "Stein", "Tåstrup", "Vancouver", "Schwinde", "Limburg", "Raubling", "Plovdiv", "Esplugues de Llobregat", "Vedbæk", "Birkenwerder", "Adliswil", "Thebarton", "Liberty Lake", "Stockport", "Bergen op Zoom", "Beloit", "Belmont", "New Orleans, Louisiana", "Molsheim", "Pritzwalk", "New Town Abbey", "North Yorkshire", "Metzingen", "Laatzen", "Viby", "Amerang", "Anjo", "Bellshill", "Üsküdar", "Bochum", "Almería", "Tagelswangen", "October", "Carouge", "Kibworth", "Fragnes", "Tananger", "Tbilisi", "Reims", "Bad Salzuflen", "Nishio", "Navasota", "Girona", "Poynton", "Joinville", "Zumikon", "Asbach-Bäumenheim", "Milford", "Salzburg", "Suzhou", "Quetta", "Hörbranz", "Nürtingen", "Nanjing", "Sandwich", "Ivanka pri Dunaji", "Saronno", "Maaseik", "Mortsel", "Kolkata", "Potenza", "Horsens", "Sundsvall", "Jhelum", "Warszawa", "Cambridgeshire", "Gardanne", "Feistritz an der Drau", "Zeltweg", "Nether Poppleton", "Eberbach", "Wetter", "Varna", "Jungingen", "Wittenberg", "Port Washington", "Milton Keynes", "Kingston, Jamaica", "St. Blasien-Menzenschwand", "Biella", "Bratislava", "Campbell", "Kautokeino", "Saint-Maurice-de-Beynost", "Tours", "Bala Cynwyd", "Lublin", "Nassau", "Stittsville", "Uxbridge", "Otawara", "Crewe", "Wildau", "St. Georgen", "Xiamen", "Suwon", "Biebertal", "Heerlen", "Penang", "Marlborough", "Santena", "Stockach", "Mitaka", "Raleigh", "Postojna", "Foetz", "Troubsko", "Guyancourt", "Kasur", "Lelystad", "Buxtehude", "Wayne", "Villetaneuse", "Ludwigshafen", "Luccu", "Diegem", "Tromsø", "Straubing", "Vandoeuvre-Les-Nancy", "Nicosia, Northern Cyprus", "Harrison", "Tittmoning", "La Garenne-Colombes", "Gabrovo", "Zollikerberg", "Waltrop", "Gründau", "Kings Langley", "Leicester", "Veliko Tarnovo", "Bozenkowo. gm. Osielsko", "St. Louis, MO", "Split", "Hollenbach", "Hof", "Clydebank", "Södertälje", "Beaverton", "Weesp", "Wilhelmsdorf", "Veliky Novgorod", "Nogent-sur-Marne", "Ludwigsburg", "Môtiers", "Missoula", "Plymouth", "Puli Township", "L'Isle-sur-la-Sorgue", "Morristown", "Saint-Martin-d'Hères", "Pori", "Pianezza", "Neve Ilan", "Amman", "Treviso", "Deinze", "Berndorf", "Belfast", "Pinellas Park", "Lille", "Grand Rapids", "Triptis", "Aalborg", "Macon, Georgia", "Cuenca", "Milton Hill", "Mundolsheim", "Moers", "Bad Neustadt", "Oradea", "Hörsching", "Bethlehem", "Nijmegen", "Thousand Oaks", "Poitiers", "Port Moresby", "Oberschaeffolsheim", "Kyoto", "Diemen", "Traunreut", "Oak Ridge", "High Wycombe", "Worcestershire", "Sigtuna", "Sarasota", "Woburn", "Majadahonda", "Campobasso", "Waldbronn", "Kortrijk", "Irving", "Dehradun", "Sittard-Geelen", "Geel", "Rho", "Brussels", "Baldham", "Troyes", "Ypres", "Gelsenkirchen", "Münsingen", "Rimini", "Samara", "Coburg", "Móstoles", "Venice", "Shenyang", "Herrsching", "Bakel", "Fukui", "Arlon", "Elkenroth", "Wohlen", "Gaza City", "Alba Iulia", "Slough", "Sulzberg", "Ikeja", "Barbing", "Steyning", "Northcote", "Le Havre", "Almere", "Gentofte", "Tavistock", "Thiva", "Ruhland", "Weingarten", "Hillerød", "Algiers", "Derio", "Rijen", "North Somerset", "Peltre", "Kapfenberg", "North Wollongong", "Newport", "Huizen", "Colmenar Viejo", "Trnava", "Hillerstorp", "Hørsholm", "Twyford", "Santander", "Mötzingen", "Diadema", "Fällanden", "Montevideo", "Saint-Fons", "Ahaus", "Rehau", "Málaga", "Dubai", "Chapell Hill", "Erlangen", "Wauwatosa", "Reading", "Bacau", "Jesi", "Indianapolis", "Chennai", "Landsberg", "Irigny", "Veldhoven", "Hickory", "Sandnes", "Vila Real", "Roggwil", "Ickenham", "Mulfingen", "Tama-shi", "Huntington Beach", "Gütersloh", "Prešove", "Manama", "Reichstett", "Stralsund", "Millersville", "Novosibirsk", "Providenciales", "Hart-im-Zillertal", "Burghaun", "Kent", "Eschen", "Mechelen", "Roswell", "Temecula", "Bad Nauheim", "Bergamo", "Goiania", "Gyöngyös", "Bradford", "London", "Lahr", "Bad Säckingen", "Montgomery, Alabama", "Fort Washington", "Suwon-si", "Glen Allen", "Manlius", "Detroit", "Grubbenvorst", "Montvale", "Zornheim", "Newark", "Louisville, KY", "Manchester, NH", "Halden", "Beckenried", "Berkel en Rodenrijs", "Limhamn", "Corning", "Scottsdale", "Schwaigern", "Reutlingen", "Cromwell", "Bietigheim-Bissingen", "Asaka", "Ås, Norway", "Guadalajara", "Northampton", "Shibuya", "Stoke Gifford", "Préverenges", "Schwandorf", "Aberdeen", "Malaga", "Emden", "Christchurch", "Sopron", "Tulsa", "Teddington", "Ashburn", "Wittenbach", "Ziegelbrücke-Niederurnen", "Port Elizabeth", "Pittsburgh", "Eilenburg", "Harrogate", "Lupfig", "Yokkaichi-shi", "Saint Louis", "Constanta", "Ravenna", "Prague 10", "Schwäbisch Hall", "Kornwestheim", "Lawrence", "Izola", "Nathan", "Hirosaki", "Rayne", "Olathe", "Langenau", "Pruhonice", "Jonköping", "Bad Münder", "Liberec", "Suceava", "Borlänge", "Cuxhaven", "Bad Ragaz", "Pitesti", "Eger", "Sendai", "Faro", "Oberägeri", "Bad Liebenwerda", "St. Gallen", "Zimmerbach", "Regensdorf", "Blomberg", "Recife", "Bramsche", "Tampa", "Washington, DC", "Albufeira", "Hamm", "Toruń", "Genay", "Norman", "Tijuana", "Meulebeke", "Saint-Louis", "Krasnodar", "Rouffach", "Enfield", "Gif-sur-Yvette", "Meggen", "Löffingen", "Eppingen", "Red Deer", "Pero", "Loanhead", "Vergennes", "Schwaig", "Amstelveen", "Weißenhorn", "Sofia", "København", "Ballerup", "San Vito", "Caen", "Amberg", "Burnsville", "Cirencester", "Maumee", "Karlsruhe", "Gland", "Krylbo", "Lienz", "Porto", "Schiphol", "Mestrino", "Higashiosaka", "Lod", "Beerse", "Dulles", "Montpellier", "Manno", "Saint-Maurice", "Putte", "Falls Church", "Turvey", "York", "Harstad", "Bocholt", "Bielefeld", "Dhaka", "Saitama", "Sassnitz", "Sassenheim", "Gröbenzell", "Casamassima", "Haddonfield", "Kufstein", "Lafayette", "Barueri", "Konya", "Bergneustadt", "Gardabaer", "Ponta Grossa", "Mirpur", "Halifax", "Rio de Janeiro", "Exeter", "East Lansing", "Wahlstedt", "Hanover", "Muscatine", "Arkel", "S-chanf", "Seinäjoki", "Kowloon", "Reno", "Bedford", "Saint-Ouen", "Markneukirchen", "Aventura, Florida", "Xanthi", "Letchworth Garden City", "Atlanta", "Torino", "Vitória", "Neponset", "Holmdel", "Tuscaloosa", "Kamenz", "Târgoviste", "Holland", "Camberley", "Merseyside", "Laguna Hills", "Neckarsulm", "Resita", "Meckenbeuren", "Taastrup", "Dinslaken", "Pachuca", "Cedarhurst", "Parkville", "Casale Monferrato", "Warwick", "Tallinn", "Hausham", "Seongnam-si", "White Plains", "Eatontown", "Chobham", "Guaynabo", "South Yarra", "Jaén", "Podgorica", "Ingolstadt", "Bielsko-Biała", "Neunkirchen", "Frederiksberg", "Skanderborg", "Golden", "Livonia", "Smørum", "Burgas", "Mülheim an der Ruhr", "Asan-si", "Bad Laasphe", "Plan-les-Ouates", "Gevrey-Chambertin", "Sabadell", "Unterlüß", "Hechingen", "Münster", "Kuwait City", "Dogana", "Bayswater", "Trübbach", "Pomona", "Altenberge", "León", "Vaduz", "Ann Arbor", "Krailling", "Eysins", "Bad Schwalbach", "Morrisville", "Bentley", "South San Francisco", "Campo Limpo Paulista", "Lusaka", "Sendenhorst", "Ramat-Gan", "Örebro", "Schwarzenbek", "Mainz", "Aschau am Inn", "Montbéliard", "Mölnlycke", "Astana", "Isehara", "Kraków", "Kongsberg", "Viborg", "Pune", "Lymington", "Remagen", "Stansstad", "Wayzata", "Achim", "Esenler", "Sambuceto", "Aberdeenshire", "Noordwijk", "Penryn", "Archamps", "Hanoi", "Oelsnitz/Erzgebirge", "Unterföhring", "Brecksville", "Olen", "Morioka-shi", "Monroe Township", "King of Prussia", "San Donato Milanese", "Poole", "Maple Valley", "Galveston", "Warsaw", "Ho Chi Minh City", "Wehrheim", "Pomezia", "Ettlingen", "Colombo", "Bobingen", "Dury", "Bloomfield Hills", "Frisco", "Filton", "Murfreesboro", "Montone", "Goleta", "Le Vésinet", "Birkenhead", "Lübbecke", "Armonk", "Uppsala", "Elven", "Kelkheim", "La Laguna", "Karleby", "Arnstorf", "Sant'Agata Bolognese", "Mesa", "Bad Rodach", "Saint-Priest", "Füssen", "Greifswald", "Péruwelz", "Hudiksvall", "Wilsdruff", "Putzbrunn", "Bad Ems", "Agrate Brianza", "Frickenhausen", "Lleida", "Long Branch, NJ", "Hisingsbacka", "Schneisingen", "Baden", "Beckum", "Cali", "Greer", "Meißen", "Pau", "Maastricht", "Delta", "Charbonnières-les-Bains", "Belley", "West Yorkshire", "Kawaguchi", "Mol", "Naperville", "Taichung", "Baia-Mare", "Cambridge", "Glostrup", "Sarreguemines", "Voreppe", "Renchen", "Olean", "Santo Domingo", "Vantaa", "Camerino", "Riga", "Lagos", "Santa Cruz de Tenerife", "Heidelberg", "Coswig", "Guelph", "Düren", "Bad Bellingen", "Nevele", "Forli", "Espoo", "Ainring", "Boise", "Schwerin", "Langenthal", "Minato", "Torrance", "Curbridge", "Doha", "Strasbourg", "Monmouthshire", "Pilsen", "Rüschlikon", "Chania", "Zuchwil", "Kansas City", "Skelleftea", "Toulouse", "Luanda", "Magog", "Wesel", "Alpharetta", "Arbon", "Franklinton", "Olsztyn", "Espergærde", "Ibaraki-shi", "Bamberg", "Eislingen", "Ecublens", "Perm", "Ansfelden", "Grosse Pointe Farms", "Monnaz", "Sydney", "Compiègne", "Danderyd", "Langweid am Lech", "Wellington", "Bad Oldesloe", "Woodland Hills", "Waco", "Billerica", "Waterloo", "Saga", "Sligo", "Flowood", "Sundbyberg", "Melbourne, FL", "Avon Lake", "Katowice", "Arcavacata", "Dundee", "Abbotabad", "Biberach", "Norfolk", "Indianola", "Porsgrunn", "Fronhausen", "Cachan", "Pressig", "Rosh HaAyin", "Letterkenny", "Skövde", "Edmond", "Hawthorn", "Erbil", "Katrineholm", "Nuremberg", "Alicante", "Gaimersheim", "Daun", "Bingham Farms", "Neustadt an der Aisch", "Chislehurst", "Lisle", "Totnes", "Pullman", "Earth City", "Sidney", "North Wales", "San Giovanni In Persiceto", "Toledo, ES", "Regina", "Larnaca", "Sissach", "Jona", "Rancho Santa Fe", "Ekaterinburg", "Hagen", "Greifensee", "Zgierz", "Vlotho", "Andechs", "Curitiba", "Stratford-upon-Avon", "Narvik", "Hongcheon-gun", "Pretoria", "Hedehusene", "Little Chalfont", "Rickmansworth", "Claremont, ZA", "Tyne and Wear", "San Antonio", "Nesna", "Heraklion", "Brisbane", "Adligenswil", "Roosendaal", "Issoudun", "Arezzo", "Ylistaro", "Mödling", "Klagenfurt", "Duisburg", "Fårevejle", "Erkrath", "Le Mans", "Osnabrück", "Buchs", "Minneapolis", "Kuopio", "Vreden", "Wismar", "Rheinfelden", "Biel", "Oberbipp", "Saint-Laurent-les-Tours", "Dierikon", "Cremona", "Sandy Bay", "Mountain View", "Cergy", "Burladingen", "Hilzingen", "Radebeul", "Schorndorf", "Bottrop", "Canberra", "Filderstadt", "Grieskirchen", "Oostzaan", "Helferskirchen", "Patras", "Fluorn-Winzeln", "Sao Jose dos Campos", "Gliwice", "East Hanover", "Adendorf", "Zvolen", "Seregno", "Short Hills", "Koper", "Grenchen", "Lucca", "Lüneburg", "Buc", "Hercules", "Thornton", "Colorado Springs", "Santa Cruz", "Norrköping", "Wald", "Silver Spring", "Staffordshire", "Sobral", "Coventry", "Brühl", "Lappeenranta", "Ottawa", "Edenkoben", "Steinen", "Neu-Isenburg", "Sheffield", "Machecoul", "Pflugerville", "Stroud", "Mumbai", "Neusäß", "Sachseln", "Chevy Chase", "Erechim", "Deggendorf", "Forth Worth", "Karlstad", "Devon", "Egelsbach", "Chatsworth", "Gwacheon-si", "Weisendorf", "Wolhusen", "La Jolla", "Remchingen", "Concepción", "Kristinehamn", "Sturefors", "Vélizy-Villacoublay", "Leioa", "Bletchley", "Torun", "Gävle", "Auckland", "Tyger Valley", "Huddersfield", "Sarstedt", "Goch", "Hemer", "Toongabbie", "Jacksonville", "Bazancourt", "Aalto", "Szeged", "Hwaseong-si", "Castelnau-le-Lez", "Yverdon-les-Bains", "Netanya", "Inverurie", "Leicestershire", "Leon", "Naruto", "Nice", "Kellinghusen", "Grünhain-Beierfeld", "Soest", "Bruchsal", "Bergen", "La Roche-sur-Foron", "Indirapurum", "Nieuw Vennep", "Pleven, BG", "Schenectady", "Komárno", "Incheon", "Damascus", "Granada", "Maienfeld", "San Cristóbal de La Laguna", "Willoughby", "Berkhamsted", "Waldachtal", "Fürth", "Gaziantep", "Ruderatshofen", "Bucks", "Morhange", "Colchester", "Herlev", "Stäfa", "Heeze", "Nördlingen", "Erdweg", "Rio", "Galați", "Kigali", "Shrewsbury", "Mount Prospect", "Stewartville", "Chagrin Falls", "Buochs", "Endicott", "Taunusstein", "Heuchelheim", "Seitenstetten", "Eurasburg", "Sesvete", "Shaoxing", "Lyngby", "Willemstad", "Thonon-les-Bains", "Meersburg", "Lancashire", "Oberhausen", "Ormskirk", "Saint-Grégoire", "Edison", "Elkhart", "Manhattan, KS", "Poirino", "Starnberg", "Marly-le-Roi", "Milton", "Telford", "Getafe", "Weybridge", "Kiryat Gat", "Gerenzano", "Pavia", "Nicosia", "Sailauf", "Prüm", "Santa Ana", "Hamilton, New Zealand", "Nürnberg", "Southbank", "Mitchell's Plain", "Fisciano", "Lawrenceville", "North Chicago", "Sorrento", "Newtown", "Cupertino", "Chandler", "Ellwangen", "Steinfurt", "Bloomington", "Brockville", "Longuich", "Linden", "Lystrup", "Kenilworth", "Bad Zurzach", "Bronx", "Hachioji", "Geesthacht", "Muscat", "Giza", "Lugo", "Vilafranca del Penedés", "Neuilly-sur-Seine", "Ljubljana", "West Des Moines", "Tarcento", "Kesteren", "Bellaterra", "Bern", "Seoul", "Umeå", "Conwy", "Breda", "Southam", "Kempten", "Strovolos", "Glenview", "Culemborg", "Adelberg", "Portsmouth", "Malmesbury", "Moerkapelle", "Somerville", "Cypress", "Rheda-Wiedenbrück", "Loosdrecht", "Rybnik", "Kuala Lumpur", "Potsdam", "Mittweida", "Eberswalde", "Boudry", "Isle of Anglesey", "Varese", "Larissa", "Las Rozas", "Koblach", "Carquefou", "Frimley", "Wexford", "Montanaso Lombardo", "Bangkok", "Lahti", "Wollerau", "Varennes", "Mijdrecht", "Saint-Prex", "Grafing", "Framingham", "Almaty", "Buchdorf", "Carnoustie", "Vaihingen/Enz", "Tirschenreuth", "Holtsville", "Skopje", "Žilina", "Charenton-le-Pont", "Collegno", "Bahawalnagar", "Aix-en-Provence", "Hoboken", "Sceaux", "North Olmsted", "Panchkula", "Le Noirmont", "The Hague", "Preston", "Holzwickede", "Kronach", "Oftringen", "Bioggio", "Zagreb", "Oberdorf", "Auburn", "Moosach", "Cranberry Township", "Chalon-sur-Saône", "Ormond Beach", "Tokyo", "Koropi", "Uzwil", "Newport Beach", "Thermalbad Wiesenbad", "Caesarea", "Brough", "Eskilstuna", "Stollberg", "Waldenburg", "Boppard", "Egå", "Tychy", "Sliedrecht", "Arnhem", "Mons", "Wielsbeke", "Denton", "Wettenberg", "Fürstenfeldbruck", "Fridolfing", "Oberding", "Gurgaon", "Targu-Jiu", "Lexington", "Madhya Pradesh", "Wörth an der Donau", "Villeneuve-d'Ascq", "Senftenberg", "Gelnhausen", "Epe", "Santa Fe Springs", "Kaunas", "Minato-ku", "Haag", "Nesslau", "Luxembourg", "Koganei", "Menden", "Stoke-on-Trent", "Bentonville", "Addlestone", "San Clemente", "Oklahoma City", "Olpe", "Oberhaching", "Teresina", "Kiryat Shmona", "Chenôve", "Siauliai", "Morris Plains", "Sint-Niklaas", "Ketzin", "Sarajevo", "Friedberg", "Angelbachtal", "Les Ulis", "Thatcham", "Am Salzhaff", "Bannockburn", "Seekirchen am Wallersee", "Illerkirchberg", "Salerno", "Budrio", "Loma Linda", "Gütenbach", "Bubendorf", "Herzogenaurach", "Antalya", "Lane Cove", "Mougins", "Rehna", "Baton Rouge, Louisiana", "Ansbach", "Clichy", "Baltimore", "Pasadena", "Houghton Regis", "Dessel", "Frederick, Maryland", "Lahore", "'s-Hertogenbosch", "Leawood", "Petaluma", "La Seyne sur Mer", "Matera", "Helsinki", "Massy", "Sinabelkirchen", "Goyang-si", "Baesweiler", "Nancy", "Grefrath", "Bühl", "Jouy-en-Josas", "Czernichów", "South Gate", "Boxmeer", "Salaunes", "Foggia", "Santa Barbara", "Ústí nad Orlicí", "Chippenham", "Bürmoos", "Napoli", "Stanford", "Reykjavik", "Chanhassen", "Genève", "Valenciennes", "Saint Vith", "Vilsbiburg", "Sonneberg", "Ashdod", "Calderara di Reno", "Wonju-si", "Sittard", "Planegg-Martinsried", "Charny", "Weert", "Foshan", "Kürnach", "Düzce", "Chakwal", "Albacete", "Shanghai", "Stara Zagora", "Manchester", "Issy-les-Moulineaux", "South Yorkshire", "Tarpoley", "Rijnsburg", "Padua", "Selmsdorf", "Meyreuil", "Starogard Gdański", "Witten", "Hazelwood", "Hämeenlinna", "Liesberg", "Aliso Viejo", "Rose Park", "Gembloux", "Neuruppin", "Kharkov", "Gloucestershire", "Deuil La Barre", "Elmsford", "Scholes", "Taunton", "Częstochowa", "Hundested", "Aretxabaleta", "Calenzano", "Jundiaí", "Vlaardingen", "Buttrio", "Wolfertschwenden", "Sulzbach", "Pescara", "Tielt-Winge", "Kirchberg", "Savigneux", "Itasca", "Yokohama", "Halmstad", "Solingen", "Johnston", "Serdang", "Tangiers", "Uster", "Santiago de Compostela", "Ribeirão Preto", "Onna, JP", "Catanzaro", "Asnières-sur-Seine", "East Sussex", "Vermillion", "Kesariani", "Comerio", "Pirna", "Bodø", "Heusenstamm", "Beverly Hills", "Parramatta", "Karkkila", "Krems an der Donau", "Schramberg", "Saint-Etienne-de-Saint-Geoirs", "Carlisle", "Bonaduz", "Edgware", "Kabul", "Ichenhausen", "Eden Prairie", "Grand Island", "Peynier", "Heerbrugg", "Olomouc", "Jenbach", "Zhuhai", "Audubon", "Thoiry", "Gerlingen", "Maputo", "Flintbek", "La Ciotat", "Burgwedel", "Kaarst", "Isle of Wight", "Bauru", "Stockholm", "Joensuu", "Seraing", "West Conshohocken", "Le Chesnay", "Höhenkirchen", "Zeven", "Lidköping", "Niederrohrdorf", "Regensburg", "Gonfreville-l'Orcher", "Mont-Saint Guibert", "Chambéry", "Pianoro", "North Kingstown", "Pordenone", "Rajamäki", "Neuhausen am Rheinfall", "Florham Park", "Marshalltown", "Shumen", "Sesto San Giovanni", "Biel-Benken", "Takashima", "Medford", "Allendale", "Troy", "Tempe", "Chichibu", "Højbjerg", "Mannheim", "São Carlos", "Besigheim", "Delbrück", "Edirne", "New Castle", "Cajamar, Brazil", "Jakarta", "Alpnach Dorf", "Gumpoldskirchen", "Isny", "Feldkirch", "Baytown", "Savannah", "Kankaanpää", "Goiânia", "Forssa", "Kemi", "Tremont", "Eschelbronn", "Monte Colombo", "Cardiff", "Ruston", "Köniz", "São José dos Pinhais", "João Pessoa", "Zürich-Flughafen", "Lincolnshire", "Multan", "Hersbruck", "Bollate", "Maule", "Corte", "Burgos", "Evergem", "Davis", "Morgantown", "Spånga", "Brenat", "New Cairo", "Buckingham", "Le Landeron", "Dedham", "Blewbury", "Neukirchen-Vluyn", "Fontainebleau", "Wetzlar", "Wolverhampton", "Stuttgart", "Weiz", "Klosterneuburg", "Schkeuditz", "Pointe-Claire", "Itami-shi", "Longbenton", "Edegem", "Golden Valley", "Bangor, Wales", "Schiltigheim", "Fukushima", "Canal San Bovo", "Northfield", "Princess Anne", "Limassol", "Pfäffikon", "Buenos Aires", "Lidingö", "Weimar", "Yamaguchi", "Visby", "Beauvais Cedex", "Campo Bom", "Offenbach", "Ipswich", "Paris La Défense", "Pohang-si", "Wolfratshausen", "Waterbury", "Paphos", "San Ramon", "Natick", "Kanazawa", "Velbert", "Lodi", "Or Yehuda", "Fuzhou", "Gdansk", "Libourne", "Jalandhar", "St. Etienne", "Bremerhaven", "Ponta Delgada", "Härnösand", "Beit Shemesh", "Macau", "Setúbal", "Guntramsdorf", "Kragujevac", "Drogenbos", "Fontanafredda", "Charleston", "Pindorama", "Rheinstetten", "Liverpool", "Zwickau", "Berg", "Herzliya", "Clayton", "Lünen", "Schaanwald", "Cranford, New Jersey", "Levanger", "Maisons-Alfort", "Annonay", "Bourg-la-Reine", "Claines", "Växjö", "Aubange", "Moline", "Piaseczno", "Godmanchester", "Jülich", "Landquart", "Toft", "Richterswil", "Solrød Strand", "Tullinge", "Brasília", "Alger", "Weil am Rhein", "Bad Schallerbach", "Starkville", "Wuhan", "Löchgau", "Hyderabad", "Gersthofen", "Maebashi-shi", "Little Island", "Wijchen", "Vientiane", "Jingzhou", "Bursa", "Bryan", "Oberkochen", "Guildford", "Clamart", "Mississauga", "Moscow", "Franklin Lakes", "Yongin-si", "Stadland-Seefeld", "Sevenoaks", "Bessenbach", "Linköping", "Campinas", "Middlebury", "Schaumburg", "Calabasas", "Octeville-sur-Mer", "Rehovot", "Stony Brook", "Copenhagen", "Hornsyld", "Dielsdorf", "Rethymno", "Wairarapa", "Eichenau", "Saint-Ignat", "Kreuzlingen", "Durham", "Miskolc", "Bydgoszcz", "Zedelgem", "Knittlingen", "Los Gatos", "Villejuif", "La Habana", "Schenefeld", "Hull", "Biel/Bienne", "Mold", "DeKalb", "Winchester", "Páty", "Greensboro", "Milano", "Massillon", "Pontoise", "Pessac", "Echt", "Le Mesnil-Saint-Denis", "Beaumaris", "Kühlungsborn", "Dardilly", "Monument", "Biot", "Crystal Lake", "Breidenbach", "Veauche", "Fremont", "Pisa", "Usingen", "Delémont", "Celle Ligure", "Olbernhau", "Albuquerque", "Sunshine", "Esch-Belval", "Waldsassen", "Markdorf", "Jerusalem", "Gießen", "Georgsmarienhütte", "Weifang", "Maidenhead", "Stavanger", "Nucleo Bandeirante", "Toyama", "Bilbao", "Mundelein", "Clapiers", "Bad Wünnenberg", "Berlin", "Erlanger", "Fredericton", "Ahrensfelde", "Norwood", "Botevgrad", "Saint-Etienne", "Überlingen", "Fredrikstad", "Giessen", "Siena", "Wuppertal", "Bnei Brak", "Allen", "Kiel", "Reggio Calabria", "Sollentuna", "Tromso", "Buckinghamshire", "Newcastle", "Barboursville", "Germantown", "Norcross", "Hinterberg", "Gateshead", "Tallaght", "Opfikon", "Nisterau", "Düsseldorf", "Kennewick", "Djursholm", "Mauren", "Waalre", "Ebenhausen", "Pettenbach", "Oberkirch", "Redmond", "Roseville", "Izegem", "Wolmirstedt", "Asker", "Hasbergen", "Freiberg", "Baar-Ebenhausen", "Erfurt", "Wirral", "New York", "Freiburg im Breisgau", "Milwaukee", "Pleidelsheim", "Aubonne", "Andernach", "Kempenich", "Kamp-Lintfort", "Buffalo Grove", "Papenburg", "Meissen", "Salaspils", "Fargo", "Milan", "Bad Gandersheim", "Elk River", "Rancho Santa Margarita", "Lüdenscheid", "Loughborough", "Landsberg am Lech", "Bad Kreuznach", "Bad Reichenhall", "Neuenhagen", "La Plaine Saint Denis", "Udenhout", "Hempstead", "Colombelles", "Manassas", "Loja", "Montréal", "Manitowoc", "Sutton", "Chapel Hill", "Nashville", "Niš", "Bresso", "Siedlce", "Bermatingen", "Machelen", "Saaldorf-Surheim", "Pará de Minas", "Walsall", "Siegenburg", "Isselburg", "Hiroshima", "Heidenheim", "Germering", "Humlebæk", "Uhingen", "Kiliney", "Duluth", "Passau", "Chiba", "Salamanca", "Northamptonshire", "Reston, VA", "Sunrise", "Mansfield", "Riyadh", "Aystetten", "Darlington", "Wiesbaden", "Huddinge", "Gainesville", "Pontypridd", "Barneveld", "Thedinghausen", "Asahikawa", "Cornegliano Laudense", "Kapelle-op-den-Bos", "Zabierzów", "Rodez", "Saint Aubin", "Meyrin", "Obergriesbach", "Eede", "Gundersheim", "Hilden", "Knoxville, TN", "Campogalliano", "Mönchweiler", "Regau", "Cassinetta", "Karachi", "Lanzhou", "Alcobendas", "Porto Alegre", "Kristiansand", "Schrobenhausen", "Marchtrenk", "Le Coudray", "Saint-Denis", "Rungis", "Fuchu", "Rennes", "Altrincham", "Mudanjiang", "Newbury", "Poznań", "Kennesaw", "Mühldorf", "Rotterdam", "Kitzingen", "Kassel", "Annapolis", "Gdynia", "Sangerhausen", "Cobham", "Eching", "Dreieich", "Midlothian", "Wolfsburg", "Landshut", "Ypsilanti", "Meitingen", "Amiens", "Dessau-Roßlau", "Newcastle, AU", "Norrtälje", "Blanchardstown", "Mestre-Venezia", "Daresbury", "Oulu", "Towson", "Kotka", "Medstead", "Luzern", "Dortmund", "Windhoek", "Steyr", "Arzano", "Nagasaki", "Villepinte", "Strassen", "Rijkevorsel", "Buffalo, NY", "Lentilly", "None Torinese", "Russi", "Cornwall", "Wegberg", "Hürth", "Hinwil", "Sutton Coldfield", "Traun", "Caparica", "Hradec Kralove", "Gedera", "Niederkassel", "Ozzano dell'Emilia", "Ottendorf-Okrilla", "Raritan", "Chineham", "Möglingen", "Kundl", "Ulsan", "Dilsen-Stokkem", "Grass Valley", "Ames", "Palaiseau", "Tuttlingen", "Heudebouville", "Söderkulla", "Nagpur", "Halle (Saale)", "Nottingham", "Chiusa", "West Fargo", "Rötz", "Virsbo", "Maynooth", "Cocoa", "Hackensack", "Truro", "Fukuoka", "Blagoevgrad", "Marignane", "Novara", "Rochefort", "Morges", "Ross-On-Wye", "Porto Velho", "Victoria, BC", "Bad Liebenzell", "Neukirch/Lausitz", "Summit", "Innsbruck", "George Town", "St Andrews, Scotland", "Las Palmas de Gran Canaria", "Medellín", "Rochester", "Kagoshima-shi", "Espelkamp", "Princeton, NJ", "Penicuik", "Peshawar", "Malgesso", "Upland", "Prem", "Bad Münstereifel", "Bordeaux", "Bolzano", "Würselen", "Bodelshausen", "Ronkonkoma", "Oita", "Stadtallendorf", "Memphis, Tennessee", "Hingham", "Goito", "Silicon Valley", "Chandigarh", "Tsu", "Peoria", "Benevento", "Wilmette", "Cape Town", "Rockville", "Lennestadt", "Evanston", "Portorož", "Le Brassus", "Plettenberg", "Sedgefield, UK", "Gennevilliers", "Kingston", "Lenexa", "Nuneaton", "Hopkinton", "Deidesheim", "Schiplaken", "Redwood Shores", "Horsham, PA", "Barry", "Schio", "Sant Joan Despí", "Osmangazi", "Gerrards Cross", "Rockwood", "Leganés", "Monterrey", "Aubervilliers", "Unterpremstätten", "Firenze", "Urnäsch", "Hampshire", "Rahway", "Schöppingen", "Richardson", "Vaucresson", "Rheineck", "Laval", "Rolla", "Oldenburg", "Hoddesdon", "Grünwald", "Cassino", "Wilsonville", "Göteborg", "Altenstadt", "Plauen", "Großpostwitz", "Westwood", "Altheim", "Oss", "Delaware", "Fairfield", "Frohnleiten", "Zwijnaarde", "Ibaraki", "St. Petersburg, FL", "Castellón de la Plana", "Meinerzhagen", "Hockenheim", "Frauenfeld", "Karlskoga", "Salford", "San Pedro Sula", "Zell an der Pram", "Southlake", "Nacogdoches", "Ratingen", "Cleveleys", "Marseille", "Madurai", "Söderhamn", "Dornbirn", "Halberstadt", "Mequon", "Antrim", "Nagoya", "Providence, RI", "Hoorn", "San Salvo", "Gujranwala", "Carlsbad", "Aurangabad", "Domzale", "Egmating", "Fukaya", "Bella Vista", "College Station", "Granges-Paccot", "Neuwied", "Woodridge", "Sunnyvale", "Wokingham", "Gent", "Contagem", "Dossenheim", "Tienen", "Pozuelo de Alarcón", "Orono", "Los Altos", "Stühlingen", "Como", "Viterbo", "Puteaux", "Gotha", "Katwijk", "Fortaleza", "Kaiseraugst", "Cypress, TX", "Cleveland", "Orange Beach", "Bethesda", "Kolkwitz", "Lund", "Dallas", "Plano", "Seeshaupt", "Tysons Corner", "Bülach", "Großbottwar", "Bad Mergentheim", "Offenburg", "Al Ain", "Cádiz", "Soverato", "Wiltshire", "Hafnarfjordur", "Wieselburg", "Carlow, Ireland", "Sugar Land", "Covilhã", "Overpelt", "Värnamo", "Beverly", "Kjeller", "Derbyshire", "San Marcos", "Niederhasli", "Bad Langensalza", "Lomm", "Hirakata", "Fort Mill", "Menlo Park", "Herndon", "Perugia", "Faisalabad", "Suresnes", "Broadway", "Shreveport", "Douala", "Quakenbrück", "Flagstaff", "Boulder", "Bayreuth", "Tallahassee", "Raufoss", "Prilly", "Great Bookham", "Hod Hasharon", "Timișoara", "Caracas", "La Hulpe", "Värmdö", "Southfield", "Oslo", "Shillingford", "Dordrecht", "Boadilla del Monte", "Sycamore", "Gumi", "Dornach", "Marilia", "Stockdorf", "Folsom", "Quakertown", "Rijswijk", "Chengdu", "Donaueschingen", "Schiltach", "Marktheidenfeld", "Harlow", "Trento", "Waltershausen", "Bettembourg", "Wenzhou", "Gotteszell", "Ithaca", "Rancho Cordova", "Moda", "Porta Westfalica", "Norwich", "Whitby", "Bischofszell", "Winnipeg", "Roncadelle", "Tutzing", "Klaipeda", "Neubrandenburg", "Évora", "Anyang-si", "Kami", "Tab", "Gechingen", "Bjerringbro", "Alfter", "Chula Vista", "Angouleme", "Heslington", "Ultimo", "Treis-Karden", "Schmalkalden", "Piraeus", "Ergersheim", "Grosse Pointe", "Eastbourne", "Redhill", "La Tour-de-Peilz", "Feldkirchen", "Matsumoto", "Reinbek", "Beijing", "Blankenfelde-Mahlow", "Pully", "Valencia", "Untergruppenbach", "Manosque", "La Norville", "Gronau", "Sundebru", "Camarillo", "Gebze-Kocaeli", "Korntal-Münchingen", "Pontedera", "San Giovanni Teatino", "Bolton", "Overveen", "Raanana", "Trévoux", "Zephyr Cove", "Sant Boi de Llobregat", "Gauting", "Molina de Segura", "Bromley", "Bari", "Watertown", "Leioa-Erandio", "Chiyoda", "Rajasthan", "Raunheim", "Attenschwiller", "Nagaoka", "Täby", "Dorchester", "Göttingen", "Toledo", "Olten", "Hamar", "Mandeville, Louisiana", "Simbach am Inn", "Heimdal, NO", "Bath", "Augusta", "Torrelodones", "Coslada", "Simpson", "Aarau", "Les Paccots", "Meerbusch", "Finsterwalde", "Darmstadt", "Plaisir", "Erkelenz", "Son", "Altach", "Créteil", "La Verrière", "Singapore", "Centennial", "Tunbridge Wells", "Neuendettelsau", "Ivrea", "Szolnok", "Macclesfield", "Woluwe-Saint-Lambert", "Oberentfelden", "North Canton", "Somerset", "Ohrdruf", "Brackenheim", "Essen", "Küssnacht", "Langenhagen", "Trichy", "Lemont", "Madison", "Waldbüttelbrunn", "Airlington Heights", "Cheonan-si", "Longmont", "Marcy-l'Étoile", "Woodstock", "Waldkirch", "Kostinbrod", "Älvsjö", "Ramallah", "Markranstädt", "Farmington Hills", "Brea", "Tuam", "Uusikaupunki", "Wilhelmshaven", "Novo Hamburgo", "Hangzhou", "Abbotsford", "Essex", "Correggio", "Opole", "Tomi", "Zweibrücken", "Silkeborg", "Helmond", "Wallingford", "Aschau", "Issoire", "Des Plaines", "Bellevue, WA", "Harrison, NY", "Tiverton", "Rocky Hill", "Grimsby", "Bucaramanga", "Melbourne", "Oldenzaal", "Hefei", "Del Mar", "Andwil", "Wiehl", "Lage", "Pfullingen", "North Ryde", "Suffern", "Schaffhausen", "Kolding", "Zlín", "Taipei", "Schwanenstadt", "Norwalk", "San Fior", "Nagano", "Sha Tin", "Ochsenhausen", "Stadskanaal", "Pardubice", "Hohenstein-Ernstthal", "Schopfloch", "Roßdorf", "Ronse", "Wuhu", "Recklinghausen", "South Jordan", "Provo", "Lampeter", "Hartenstein", "Bangor", "Bahawalpur", "Rome", "Furtwangen", "Amriswil", "Pullach", "La Garde", "Tel Aviv", "Golf, IL", "San Jose, Costa Rica", "Kouloura", "Hünenberg", "Hausen am Albis", "Twickenham", "Glashütte", "Owen", "Diamond Bar", "Rogers", "Vänge", "Geelong", "Örebrö", "Riedt bei Neerach", "Vitoria", "Fribourg", "Fujiyoshida", "Brederis", "Les Clayes-sous-Bois", "Santa Fe", "Cheyenne", "New Brunswick", "Winksele", "Müllheim", "Oegstgeest", "Minsk", "Syracuse", "Redditch", "Eaton", "Châtenay-Malabry", "Palermo", "Enniscorthy", "Midland", "Kalamata", "Södertörn", "Leuna", "Melville", "Altrip", "Sevilla", "Llandeilo", "Tremblay-en-France", "Spokane", "Schwäbisch Gmünd", "Kaisersesch", "Castelnovo", "Spelle", "Latsia", "St. Petersburg", "Hagfors", "Brooklyn Heights", "Rüdlingen", "Günzburg", "Ottignies-Louvain-la-Neuve", "IJsselstein", "Sassari", "Enna", "Jena", "Philadelphia", "Lich", "Holzminden", "Brooklyn", "Idstein", "Abuja", "Bielsko-Biala", "Saalfeld", "Toon", "Cheshire", "Barchfeld", "Centurion", "Winston-Salem", "Ängelholm", "Douglas", "Martos", "Rostock", "Gråsten", "Arisdorf", "Stipshausen", "Baku", "Kloten", "Kozani", "Stans", "Bahrain", "Elche", "Jackson, Mississippi", "Hanau", "Santa Clara", "Zoetermeer", "Davidson", "Takamatsu", "Voorburg", "Dublin, CA", "Himeji-shi", "Nivelles", "Elmwood Park", "Loßburg", "Guatemala City", "Rheine", "Avon", "Oxnard", "Barberton", "Alsfeld", "Alsip", "Donostia - San Sebastián", "Bruce", "New Rochelle", "Jaipur", "Poissy", "Serra di Ferro", "Zell", "Lemgo", "Markkleeberg", "Tokushima", "Geneva", "Libertyville", "Hainichen", "Boll", "Ghent", "Künzelsau", "Corbetta", "Košice", "Ismaning", "Weinheim", "Mwanza", "Lubbock", "Westborough", "Chieti", "West Jordan", "Needham", "Caceres", "Korschenbroich", "Birmingham", "Batesville", "Lokeren", "Gladbeck", "Windhagen", "Trostberg", "Omaha", "Haugesund", "St-Etienne du Rouvray", "Xi'an", "Viernheim", "Izmir", "Marlow", "Auburn Hills", "Riihimäki", "Wels", "Bryanston", "Barcelona", "West Drayton", "Herstal", "Vildbjerg", "Grüsch", "Shunde", "Warwickshire", "Burnfoot", "Fairview", "Caluire-et-Cuire", "Bebington", "Commerce City", "Randers", "La Celle-St-Cloud", "Nyon", "Roubaix", "Sennwald", "Gijón", "Ogden", "Bad Soden", "Ober-Mörlen", "Aichhalden", "Hennigsdorf", "Reutte", "Lynchburg", "Lannach", "Monaco", "Baranzate", "Lindlar", "Vlissingen", "Beaconsfield", "Rixensart", "Bönen", "Blieskastel", "Drachten", "L'Hospitalet de Llobregat", "Belcamp, Maryland", "Neuenbürg", "Columbia", "Watermael-Boitsfort", "Johnson City, Tennessee", "Miyazaki", "Lenzkirch", "Oberhaid", "Wilmerding", "Murdoch", "Fort Collins", "Krauchenwies", "Thônex", "Montreal", "Yonkers", "Kochel am See", "Damme", "Adrian", "Manasquan", "Birmingham, Alabama", "Garmisch-Partenkirchen", "Heddesheim", "Hutchinson", "Renningen", "Capurso", "Westlake", "Billund", "Saint-Marcel", "Vendenheim", "Lübeck", "New Port, South Wales", "Fulpmes", "Viçosa", "Brno", "Ostrava", "San Carlos", "Molde", "Geislingen", "Venray", "Monheim am Rhein", "Frazer", "Dania Beach", "Larchmont", "Neubeuern", "Canterbury", "Selm", "Boston", "Ústí nad Labem", "Rhode-Saint-Genèse", "Dachau", "Neubiberg", "Mladá Boleslav", "Maltepe", "Bergisch Gladbach", "Gulf Breeze", "Louisville, CO", "North Shore City", "Serres", "Nilüfer", "New Delhi", "Largo", "Tarragona", "Fountain Valley", "Malé", "Bothell", "Florianópolis", "Dimitrovgrad", "Essingen", "Glen Ellyn", "Sarnia", "Obersontheim", "Trier", "Salt Lake City", "Waterford", "Romeo", "Baden-Baden", "Karlskrona", "Horb", "Reichelsheim", "Salem", "Ålesund", "Bodenkirchen", "Coimbatore", "Callaghan", "Dietzenbach", "Peabody", "Au", "Corvallis", "Muttenz", "Gmunden", "Surrey, UK", "Wageningen", "Dormagen", "Zielona Góra", "Kallithea", "Findel", "Churchover", "Berg am Starnberger See", "Moncalieri", "Colombes", "Brandenburg an der Havel", "Schweinfurt", "Zofingen", "Secunderabad", "Sevelen", "Niederteufen", "Stratford, CT", "Beograd", "Turnhout", "Apex", "Jüchen", "Płock", "Allschwil", "Ravensburg", "Kópavogur", "Aas", "Tottori", "Nordborg", "Bancroft", "Cranfield", "Lecce", "Changsha", "Gatersleben", "Airasca", "Hannover", "Warlingham", "Windsor", "Lake Zurich", "Tettnang", "Pluvigner", "Shenzhen", "Ossy", "Alberta", "Liège", "La Gaude", "Brøndby", "Erpel", "Staines-upon-Thames", "Armentières", "Rottenburg", "Lewisville", "Cameri", "Rawalpindi", "Löddeköpinge", "Gorinchem", "Saint-Chamond", "Kalamazoo", "Subicao, AU", "Bloemfontein", "Milpitas", "Asheville", "Anyang", "Suhl", "Modena", "Swanage", "Arco", "Acton", "Goldbach", "Mexico City", "New Haven", "Mawson Lakes", "Gangwon", "Gifu", "Nysa", "Neuchâtel", "Pleasanton", "Zaventem", "Schiedam", "Erlenbach", "Tosu", "Farnham", "Shoham", "Marktredwitz", "Wissembourg", "Vevey", "Aalen", "Greenwood Village", "Notting Hill", "Dresden", "San Diego", "Chichester", "Shelbyville", "Petersfield", "Walldorf", "Jaraguá do Sul", "Lachelle", "Leeds", "Pyeongtaek-si", "Weggis", "West Midlands", "Vercelli", "Limerick", "Friedrichshafen", "La Rochelle", "Clinton", "Rödermark", "Steenwijk", "Wolfenbüttel", "Cincinnati", "Danville", "Charlotte", "Wood Dale", "Ulaanbaatar", "Ploiesti", "Qingdao", "Hulshout", "Königsee", "Mobile", "Alnarp", "Rastatt", "Clemson", "Venlo", "Castellbisbal", "Opava", "Syke", "Kempen", "Sanford", "Hettenleidelheim", "Freudenberg", "Attendorn", "Talence", "Greenwich, CT", "Gyeongsan", "Afula", "Konyaaltı", "Megiddo", "Kontich", "Sant Vicenç dels Horts", "Weisenheim am Berg", "Bernburg", "Merignac", "Neukirchen bei Altmünster", "Surbiton", "Skawina", "Saarlouis", "Dollard des Ormeaux", "Wigton", "Leatherhead", "Solna", "Farmington", "Lauffen am Neckar", "Glendale, WI", "Kaiserslautern", "Roskilde", "Saginaw", "Hamilton", "Burlington", "Büdelsdorf", "Honolulu", "Garbsen", "Arlington", "Gera", "Calgary", "Harpenden", "Easton", "Alcalá de Henares", "Verberie", "Skaneateles Falls", "İncek Gölbaşı", "Argenteuil", "Trieste", "Bad Oeynhausen", "Wevelgem", "Branchburg", "Struer", "Aveiro", "Kariya", "Lentzweiler", "Dorsheim", "Dijon", "Uddeholm", "Birkenau", "Mont-Saint-Aignan", "Valladolid", "Blumenau", "Servon", "Randburg, Johannesburg", "Arad", "Leer", "Bogota", "Lake Forest", "Accra", "Hünfelden", "Para de Minas", "Almelo", "Jersey City", "Detmold", "Toyohashi", "Martinsried", "Hagenbach", "Udine", "Jarinu", "Columbus", "Dalian", "Aarhus", "Camaçari", "Bechhofen", "Bonn", "Iraklion", "Östersund", "Greater London", "Jönköping", "Pierre", "Kumamoto", "Therwil", "Petrosani", "Howald", "Kreuztal", "Neuss", "Bridgewater", "Nove Mesto nad Metuji", "Eugene", "Melikgazi", "Westford", "Schoten", "Glendale", "Petah Tikva", "Hasselt", "Derby", "Möhrendorf", "Denham", "Holzkirchen", "Ahrensburg", "Serdivan", "Krefeld", "Renens", "Hadsten", "Icking", "Verl", "Goslar", "Rankweil", "Wädenswil", "Oxford", "Haifa", "Viance", "Windlesham", "Vanderbijlpark", "Tolentini", "Zelzate", "McLean", "St. Kilda", "Jupiter", "Boecillo", "Flitwick", "McKinney", "Sinsheim", "Porcia", "Padova", "Koekange", "Oberbüren", "Feltham", "Dorval", "Hardheim", "Rüsselsheim", "Weidenbach", "Pearl River", "Sakura", "Sölna", "Farroupilha", "Toowoomba", "Port Louis", "Las Vegas", "Mildenau", "Lengerich", "Daytona Beach", "Nantes", "Villingen-Schwenningen", "St. Jean", "Bornheim", "Radolfzell", "Peterborough", "Łódź", "Bodenwöhr", "North Haven", "Iserlohn", "Ruse", "Lysaker", "Edmonton", "Villa Verucchio", "Diepholz", "Managua", "Stratford", "Ansan-si", "Caterham", "Sunderland", "Akishima", "Gibraltar", "Nanterre", "Sterling Heights", "Oelde", "Middletown", "Jyväskylä", "Miesbach", "Sacramento", "Okazaki-shi", "Bad Aibling", "Seattle", "Gosheim", "Beirut", "Hüfingen", "Paisley", "Fulda", "Ariel", "Newtownabbey", "Epirus", "Goole", "Cuiabá", "Zollikofen", "Boortmeerbeek", "Lerum", "Lanark", "Pasching", "Glatten", "Besançon", "Rye Brook", "Oviedo", "Steinbach", "Albi", "Catania", "Villafranca", "Sandy, Utah", "Internet", "Camas, WA", "Arequipa", "Cork", "Antony", "Biatorbágy", "Wroclaw", "Didcot", "Greven", "Bunnik", "Zurich", "Västerås", "Thale", "Norderstedt", "Wako", "Jinan", "Wil", "Daugavpils", "Oberursel", "St. Pölten", "Eisenstadt", "Kiyosu-shi", "Wolfach", "Castello di Godego", "Kosterneuburg", "Vernier", "Uden", "Houthalen-Helchteren", "Guangzhou", "Bagsværd", "Vellmar", "Inverness", "Volda", "Bois-Colombes", "Middlesex", "Chemnitz", "Belo Horizonte", "Bakewell", "Plochingen", "Pfronten", "Donauwörth", "Williston, VT", "Newcastle upon Tyne", "Singen", "Roma", "Orange County", "Covina", "Schönefeld", "Johannesburg", "Bujumbura", "Betim", "Ketsch", "Stellenbosch", "Welwyn Garden City", "Meyzieu", "Le Haillan", "Falkenberg", "Aachen", "Seligenstadt", "Hildesheim", "Chelmsford", "Littleton", "Zella-Mehlis", "Liptovský Mikuláš", "Saint-Apollinaire", "Vejstrup", "Benton Harbor", "Bautzen", "Tirana", "Dhahran", "Kaufbeuren", "East Lothian", "Brest", "Bilkent", "Höchstadt", "Be'er Sheva", "Vosselaar", "Malvern", "Oakbrook Terrace", "Ramberg", "Sunbury-on-Thames", "Chalandri", "Bad Überkingen", "Riverside", "Stourbridge", "Suwanee", "Potchefstroom", "Leuven", "Biasca", "Codogno", "Bertrange", "Monterey", "Limoges", "Maasland", "Chaska", "Cordoba", "Des Moines", "Caserta", "Civitanova Marche", "Littlehampton", "Greifenstein", "Santa Cruz de Mieres", "Schiphol-Rijk", "Aesch", "Göppingen", "Aracajú", "San Sebastian", "Pantin", "Palma de Mallorca", "Los Angeles", "Cluses", "Visé", "Fort Worth", "Tartu", "Wipperfürth", "Raaba", "Euskirchen", "Labège", "Redwood City", "Troisdorf", "Lisse", "Villigen", "Eagle Mountain", "Novi", "Sao Jose do Rio Preto", "Murcia", "Salvador", "Västervik", "Cottbus", "Hradec Králové", "Waiblingen", "Sausalito", "Hombrechtikon", "Oberderdingen", "Marnate", "Darien", "Duxford", "Suffolk", "Keele", "Mount Kisco", "Pécs", "Coriano", "Gujrat", "Ancona", "Horw", "Volos", "New Milford", "Grabels", "Belgrade", "L'Aquila", "Illkirch", "Leverkusen", "Malibu", "Dainville", "Eggenfelden", "Mollet del Vallès", "Kleinblittersdorf", "Munich", "Alameda", "Brescia", "Biedenkopf", "Cheseaux-sur-Lausanne", "Karlsbad", "Hayward", "Königs Wusterhausen", "Oak Creek", "Heverlee", "Walferdange", "Vierkirchen", "Kasugai", "Vazia", "Bloomfield", "Bernin", "Bedford Park", "Tianjin", "Saratoga", "Lovech", "Akron", "Jundiai", "Maringa", "Kampala", "Hafizabad", "Ewing", "Alkhobar", "Arcueil", "The Woodlands", "Bad Buchau", "Alton", "Sindelfingen", "Santa Monica", "Minden", "Bengaluru", "Westchester", "Phoenix", "Ostfildern", "Ottenhofen", "Rende", "Iphofen", "Sao Paulo", "Tyler", "Bad Urach", "Esslingen", "Caxias do Sul", "Trois-Rivières", "IJmuiden", "Guanajuato", "Iowa City", "Utsunomiya", "Suwa", "Oberndorf", "Wheeling", "Eskisehir", "Ajalvir", "Kilmaine", "Corgémont", "Surat", "Heide", "Unterschleißheim", "Courbevoie", "Radom", "Sinzheim", "Selangor", "Geleen", "Niederönz", "Morsbach", "Hartford", "Medolla", "Nitra", "La Riche", "Luleå", "Muncy", "Leighton Buzzard", "Mettmann", "Flensburg", "Leonberg", "Jacareí", "Olching", "Potters Bar", "Al Qurum", "Hillsborough, NJ", "Dongguan", "Übach-Palenberg", "North Andover", "Blanquefort", "Nesselwang", "Zaragoza", "Rueil-Malmaison", "Palatine", "Vicenza", "Herford", "Åkersberga", "Sibiu", "Warstein"]
#     fh = FeatureHasher(n_features=i, input_type='string')
#     hashed_features = fh.fit_transform(categories)
#     features_as_string = list(map(lambda x: str(x), hashed_features.toarray()))
#     c = Counter(features_as_string)
#     means.append(np.mean(list(c.values())))
#     stds.append(np.std(list(c.values())))
#     medians.append(np.median(list(c.values())))
#
# print(np.min(means), means.index(np.min(means)))
# print(np.min(medians), medians.index(np.min(medians)))
#
# plt.figure(figsize=(5,4))
# plt.yscale('log')
# plt.plot(range(1, 80, 1), means, label='mean')
# plt.plot(range(1, 80, 1), medians, label='median')
# plt.xlabel('# Dummy Features for Cities')
# plt.ylabel('Collisions')
# plt.legend(loc='best')
# plt.tight_layout()
# plt.savefig('hashing_cities.eps')
# plt.show()
#
#
# #############################
#
colors = [sns.diverging_palette(220, 20, n=6)[i] for i in [2, 1, 0, -2]]

# # accuracy, 1 applicant
# s = StringIO(""" Baseline,  DT,  LR, DNN
# Procter & Gamble,      0.756,  0.779,   0.965, 0.96511
# IDF Innov,    0.135,  0.124,   0.348, 0.24719
# Fraunhofer,    0.480,  0.440,  0.680, 0.68 """)
# # accuracy
# s = StringIO(""" Baseline,  DT,  LR, DNN
# United States,      0.126,  0.359,   0.686, 0.41420
# Germany,            0.162,  0.294,   0.518, 0.53266
# United Kingdom,     0.288,  0.327,   0.654, 0.59574""")
# # accuracy, 1-country binary
# s = StringIO(""" Baseline,  DT,  LR
# United States,      0.989,  0.976,   0.8774
# Germany,            0.946,  0.559,   0.6518
# United Kingdom,     0.981,  0.962,   0.7308""")
# # accuracy
# s_mean = StringIO(""" Baseline,  DT,  LR
# Arithmetic,      0.598,  0.647,   0.647
# Median,            0.824,  0.706,   1.000""")
# # accuracy
# s = StringIO(""" Baseline,  DT, LR, DNN
# Complete,        0.436,     0.473,   0.0,    0.7006
# Train=2016/17,   0.461,     0.471,   0.5410, 0.73450""")
# # accuracy
# s = StringIO(""" Baseline,  DT, LR, DNN
# Complete,        0.976,     0.816,   0.0,    0.9453
# Train=2016/17,   0.968,     0.879,   0.8111, 0.8608""")
#
# df = pd.read_csv(s, index_col=0, delimiter=', ', skipinitialspace=True)
# ax = df.plot(kind='bar', rot=0, width=0.75, figsize=(5,4), color=colors)
# for p in ax.patches:
#     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.01 ))
# plt.ylim(ymax=1.1)
# plt.yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
# plt.legend(loc='upper right', ncol=1)
# plt.ylabel('Accuracy')
# plt.tight_layout()
# plt.savefig('result_multi_accuracy_1_applicant.eps')
# plt.show()
#
# # precision, 1 applicant
# s = StringIO(""" Baseline,  DT,  LR, DNN
# Procter & Gamble,  0.756,  0.887,   0.887, 0.7037
# IDF Innov,         0.135,  0.124,   0.1955, 0.041667
# Fraunhofer,        0.480,  0.462,   0.4412, 0.3305""")
# # precision, 1 country multi
# s = StringIO(""" Baseline,  DT,  LR, DNN
# United States,  0.197,  0.340,   0.2841, 0.27483
# Germany,        0.35714,  0.317,   0.3389, 0.30155
# United Kingdom, 0.339,  0.313,   0.3766, 0.21296""")
# # precision_new
# s_1_country_binary = StringIO(""" Baseline,  DT,  LR
# United States,  0.0,  0.1363,   0.06122
# Germany,        0.283,  0.08536,   0.09793
# United Kingdom, 0.0,  0.0,   0.066666""")
# # precision
# s_mean = StringIO(""" Baseline,  DT,  LR
# Arithmetic,      0.578,  0.588,   0.5295
# Median,            0.909,  0.667,   0.9091""")
# # precision
# s = StringIO(""" Baseline, DT, LR, DNN
# Complete,        0.557,     0.515,   0.0,    0.6108
# Train=2016/17,   0.593,     0.498,   0.5192, 0.61746""")
# # precision, new
# s = StringIO(""" Baseline, DT, LR, DNN
# Complete,        0.4356,     0.0749,   0.0,    0.1924
# Train=2016/17,   0.2971,     0.09409,   0.07071, 0.105""")
#
# df = pd.read_csv(s, index_col=0, delimiter=', ', skipinitialspace=True)
# ax = df.plot(kind='bar', rot=0, width=0.75, figsize=(5,4), color=colors)
# for p in ax.patches:
#     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.01 ))
# plt.ylim(ymax=1.1)
# plt.ylabel('Precision')
# plt.tight_layout()
# plt.savefig('result_multi_precision_1_applicant.eps')
# plt.show()
#
# # surprisal
# s_1_applicant = StringIO(""" Baseline,  DT,  LR
# Procter & Gamble,   0.000,  0.023,   0.023
# IDF Innov,          0.135,  0.124,  0.157
# Fraunhofer,         0.000,  0.100,  0.080""")
# # surprisal
# s_1_country = StringIO(""" Baseline,  DT,  LR
# United States,   0.000,  0.063,   0.040
# Germany,         0.000,  0.060,   0.044
# United Kingdom,  0.000,  0.058,   0.038""")
# # surprisal
# s_mean = StringIO(""" Baseline,  DT,  LR
# Arithmetic,      0.0,  0.025,   0.000
# Median,            0.0,  0.0,   0.0""")
# # surprisal
# s = StringIO(""" Baseline, DT, LR, DNN
# Complete,        0.0,       0.035,   0.0,    0.0
# Train=2016/17,   0.000,     0.028,   0.007,  0""")
#
# df = pd.read_csv(s, index_col=0, delimiter=', ', skipinitialspace=True)
# ax = df.plot(kind='bar', rot=0, width=0.75, figsize=(5,4), color=colors)
# # for p in ax.patches:
# #     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.01 ))
# plt.ylim(ymax=0.31)
# plt.ylabel('Surprisal, $θ = 4.5$')
# plt.yticks([0, 0.1, 0.2, 0.3])
# plt.tight_layout()
# plt.savefig('result_multi_surprisal_1_country.eps')
# plt.show()
#
# # recall, new
# s_1_country_binary = StringIO(""" Baseline,  DT,  LR
# United States,   0.000,  0.2727,   0.8181818
# Germany,         0.2083,  0.875,   0.79166
# United Kingdom,  0.000,  0.00,   1.0""")
# # recall, new
# s = StringIO(""" Baseline, DT, LR, DNN
# Complete,        0.14617,   0.6312,   0.0,    0.4516
# Train=2016/17,   0.3089,    0.5083,   0.60797,  0.6903""")
# df = pd.read_csv(s, index_col=0, delimiter=', ', skipinitialspace=True)
# ax = df.plot(kind='bar', rot=0, width=0.75, figsize=(5,4), color=colors)
# # for p in ax.patches:
# #     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.01 ))
# plt.ylim(ymax=1.1, ymin=0.0)
# plt.ylabel('Recall, new')
# plt.yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
# plt.tight_layout()
# plt.legend(loc='upper left')
# plt.savefig('result_binary_recall_1_country.eps')
# plt.show()
#
# # probs
# s_1_applicant = StringIO(""" correct, false
# Procter & Gamble,   0.7255,  0.6882
# IDF Innov,          0.0380,  0.0355
# Fraunhofer,         0.4250,  0.27328""")
# s_error_1_applicant = StringIO(""" correct, false
# Procter & Gamble,   0.13639,  0.0598
# IDF Innov,          0.00685, 0.0061
# Fraunhofer,         0.14327,  0.1278""")
# # probs
# s_1_country = StringIO(""" correct, false
# United States,   0.2561,  0.209603
# Germany,         0.32904,  0.2352
# United Kingdom,         0.4250,  0.2352""")
# s_error_1_country = StringIO(""" correct, false
# United States,   0.078,  0.10056
# Germany,         0.20479,  0.12879
# United Kingdom,  0.1624,  0.1036""")
# # probs
# s_1_country_binary = StringIO(""" correct, false
# United States,   0.83525,  0.7120
# Germany,         0.8634,  0.6807
# United Kingdom,         0.6375,  0.5759""")
# s_error_1_country_binary = StringIO(""" correct, false
# United States,   0.06130,  0.1117
# Germany,         0.137,  0.1060
# United Kingdom,  0.0816,  0.0546""")
# # probs
# s_mean = StringIO(""" correct, false
# Arithmetic,   0.1499,  0.0709
# Median,         0.83525,  0.71206""")
# s_error_mean = StringIO(""" correct, false
# Arithmetic,         0.119,  0.07324
# Median,  0.0613,  0.111""")
# # probs
# s = StringIO(""" correct, false
# Complete,        0.6852,  0.4200
# Train=2016/17,   0.7182,  0.4125""")
# s_error = StringIO(""" correct, false
# Complete,              0.200,  0.1520
# Train=2016/17,         0.235,  0.2033""")
# # probs
# s = StringIO(""" correct, false
# Complete,        0.9109,  0.6793
# Train=2016/17,   0.8380,  0.7106""")
# s_error = StringIO(""" correct, false
# Complete,              0.1005,  0.1398
# Train=2016/17,         0.1268,  0.1288""")
#
# df = pd.read_csv(s, index_col=0, delimiter=', ', skipinitialspace=True)
# errors = pd.read_csv(s_error, index_col=0, delimiter=', ', skipinitialspace=True)
# ax = df.plot(kind='bar', rot=0, width=0.75, figsize=(5,4), color=[default_colors[0], default_colors[1]], yerr=errors)
# # for p in ax.patches:
# #     ax.annotate(str(round(p.get_height(),2)), (p.get_x(), p.get_height() * 1.01 ))
# plt.ylim(ymax=1.1)
# plt.yticks([0, 0.2, 0.4, 0.6, 0.8, 1])
# plt.ylabel("Average Posterior Probability")
# plt.tight_layout()
# plt.savefig('result_probs_median.eps')
# plt.show()
#
# ##################
#
# gap_sizes = [1, 10, 100, 1000, 10000]
# baseline = [0.294, 0.303, 0.262, 0.252, 0.228]
# log_regression = [ 0.1718, 0.2485, 0.2978, 0.3828, 0.3625]
# log_regression_k3 = [0.337, 0.435, 0.526, 0.658,  0.644]
# examples = [163, 499, 1289, 2806, 3332]
#
# fig, ax1 = plt.subplots(figsize=(5,4))
# ax1.plot(gap_sizes, baseline, label='Baseline', linestyle='--', c=sns.color_palette()[0])
# ax1.plot(gap_sizes, log_regression, label='LR', c=sns.color_palette()[0])
# ax1.plot(gap_sizes, log_regression_k3, label='LR, $\kappa = 3$', linestyle=':', c=sns.color_palette()[0])
# ax1.legend(loc='upper left')
# ax1.set_ylabel('Accuracy')
# ax1.set_ylim(ymax=1.2, ymin=0.0)
# ax1.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
# ax1.set_xlabel('Maximum Gap Size')
# ax1.set_xticks(gap_sizes)
# ax1.set_xscale('log')
# ax2 = ax1.twinx()
# ax2.plot(gap_sizes, examples, label='# Examples', c=sns.color_palette()[-2])
# ax2.legend(loc='upper right')
# ax2.set_yscale('log')
# ax2.set_ylabel('# Examples')
# ax2.set_ylim(ymax=20000, ymin=10)
# # ax2.set_yticks([0, 1, 2, 3, 4, 5])
# fig.tight_layout()
# plt.savefig('gap_sizes_ger.eps')
# plt.show()
#
# gap_sizes = [1, 10, 100, 1000, 10000]
# baseline = [0.241, 0.245, 0.203, 0.156, 0.149]
# log_regression = [0.1899, 0.1818, 0.2711, 0.3258, 0.3179]
# log_regression_k3 = [0.329, 0.355, 0.533, 0.678, 0.662]
# examples = [79, 363, 1527,  6206, 7371]
#
# fig, ax1 = plt.subplots(figsize=(5,4))
# ax1.plot(gap_sizes, baseline, label='Baseline', linestyle='--', c=sns.color_palette()[0])
# ax1.plot(gap_sizes, log_regression, label='LR', c=sns.color_palette()[0])
# ax1.plot(gap_sizes, log_regression_k3, label='LR, $\kappa = 3$', linestyle=':', c=sns.color_palette()[0])
# ax1.legend(loc='upper left')
# ax1.set_ylabel('Accuracy')
# ax1.set_ylim(ymax=1.2, ymin=0.0)
# ax1.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
# ax1.set_xlabel('Maximum Gap Size')
# ax1.set_xticks(gap_sizes)
# ax1.set_xlim(xmin=0.1, xmax=10000)
# ax1.set_xscale('log')
# ax2 = ax1.twinx()
# ax2.plot(gap_sizes, examples, label='# Examples', c=sns.color_palette()[-2])
# ax2.legend(loc='upper right')
# ax2.set_yscale('log')
# ax2.set_ylabel('# Examples')
# ax2.set_ylim(ymax=20000, ymin=10)
# # ax2.set_yticks([0, 1, 2, 3, 4, 5])
# fig.tight_layout()
# plt.savefig('gap_sizes_us.eps')
# plt.show()
#
# #####################################
#
# gap_sizes = [1, 10, 100, 1000, 10000]
# accuracy = [0.4479, 0.4749, 0.5949, 0.7897, 0.8187]
# precision_new = [0.312, 0.261127, 0.2003, 0.197080, 0.1951]
# recall_new = [0.907, 0.8712, 0.82666, 0.771428, 0.75977]
# examples = [163, 499, 1286,  2806, 3332]
#
# fig, ax1 = plt.subplots(figsize=(5,4))
# ax1.plot(gap_sizes, accuracy, label='Accuracy', linestyle='--', c=sns.color_palette()[0])
# ax1.plot(gap_sizes, precision_new, label='Precision, new', c=sns.color_palette()[0])
# ax1.plot(gap_sizes, recall_new, label='Recall, new', linestyle=':', c=sns.color_palette()[0])
# ax1.legend(loc='upper left')
# ax1.set_ylabel('Accuracy, Precision, Recall')
# ax1.set_ylim(ymax=1.2, ymin=0.0)
# ax1.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
# ax1.set_xlabel('Maximum Gap Size')
# ax1.set_xticks(gap_sizes)
# ax1.set_xlim(xmin=0.1, xmax=10000)
# ax1.set_xscale('log')
# ax2 = ax1.twinx()
# ax2.plot(gap_sizes, examples, label='# Examples', c=sns.color_palette()[-2])
# ax2.legend(loc='upper right')
# ax2.set_yscale('log')
# ax2.set_ylabel('# Examples')
# ax2.set_ylim(ymax=20000, ymin=10)
# # ax2.set_yticks([0, 1, 2, 3, 4, 5])
# fig.tight_layout()
# plt.savefig('gap_sizes_ger_lr.eps')
# plt.show()
#
# gap_sizes = [1, 10, 100, 1000, 10000]
# accuracy = [0.675, 0.768, 0.872, 0.932, 0.942]
# precision_new = [0.386, 0.377, 0.377, 0.377, 0.377]
# recall_new = [0.395, 0.228, 0.153, 0.131, 0.128]
# examples = [163, 499, 1286,  2806, 3332]
#
# fig, ax1 = plt.subplots(figsize=(5,4))
# ax1.plot(gap_sizes, accuracy, label='Accuracy', linestyle='--', c=sns.color_palette()[0])
# ax1.plot(gap_sizes, precision_new, label='Precision, new', c=sns.color_palette()[0])
# ax1.plot(gap_sizes, recall_new, label='Recall, new', linestyle=':', c=sns.color_palette()[0])
# ax1.legend(loc='upper left')
# ax1.set_ylabel('Accuracy, Precision, Recall')
# ax1.set_ylim(ymax=1.2, ymin=0.0)
# ax1.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
# ax1.set_xlabel('Maximum Gap Size')
# ax1.set_xticks(gap_sizes)
# ax1.set_xlim(xmin=0.1, xmax=10000)
# ax1.set_xscale('log')
# ax2 = ax1.twinx()
# ax2.plot(gap_sizes, examples, label='# Examples', c=sns.color_palette()[-2])
# ax2.legend(loc='upper right')
# ax2.set_yscale('log')
# ax2.set_ylabel('# Examples')
# ax2.set_ylim(ymax=20000, ymin=10)
# # ax2.set_yticks([0, 1, 2, 3, 4, 5])
# fig.tight_layout()
# plt.savefig('gap_sizes_ger_baseline.eps')
# plt.show()

# #####################################
#
# gap_sizes = [1, 10, 100, 1000]
# baseline = [0.515, 0.560, 0.602, 0.602]
# log_regression = [ 0.4279, 0.4797, 0.5319, 0.5319]
# log_regression_k3 = [0.611, 0.658, 0.699, 0.699]
# examples = [229, 1009, 1598, 1598]
#
# fig, ax1 = plt.subplots(figsize=(5,4))
# ax1.plot(gap_sizes, baseline, label='Baseline', linestyle='--', c=sns.color_palette()[0])
# ax1.plot(gap_sizes, log_regression, label='LR', c=sns.color_palette()[0])
# ax1.plot(gap_sizes, log_regression_k3, label='LR, $\kappa = 3$', linestyle=':', c=sns.color_palette()[0])
# ax1.legend(loc='upper left')
# ax1.set_ylabel('Accuracy')
# ax1.set_ylim(ymax=1.2, ymin=0.0)
# ax1.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
# ax1.set_xlabel('Maximum Gap Size')
# ax1.set_xticks(gap_sizes)
# ax1.set_xscale('log')
# ax2 = ax1.twinx()
# ax2.plot(gap_sizes, examples, label='# Examples', c=sns.color_palette()[-2])
# ax2.legend(loc='upper right')
# ax2.set_yscale('log')
# ax2.set_ylabel('# Examples')
# ax2.set_ylim(ymax=20000, ymin=10)
# # ax2.set_yticks([0, 1, 2, 3, 4, 5])
# fig.tight_layout()
# plt.savefig('gap_sizes_arithmetic.eps')
# plt.show()

##################################

# gap_sizes = [1, 10, 100, 1000, 10000]
# baseline = [0.476, 0.504, 0.506, 0.458, 0.442]
# dnn = [ 0.1528, 0.1901, 0.4684, 0.4607, 0.4526]
# dnn_k3 = [0.2550, 0.3088, 0.62250, 0.6680,  0.66874]
# examples = [5348, 18577, 49812, 88662, 96823]
#
# fig, ax1 = plt.subplots(figsize=(5,4))
# ax1.plot(gap_sizes, baseline, label='Baseline', linestyle='--', c=sns.color_palette()[0])
# ax1.plot(gap_sizes, dnn, label='DNN', c=sns.color_palette()[0])
# ax1.plot(gap_sizes, dnn_k3, label='DNN, $\kappa = 3$', linestyle=':', c=sns.color_palette()[0])
# ax1.legend(loc='upper left')
# ax1.set_ylabel('Accuracy')
# ax1.set_ylim(ymax=1.2, ymin=0.0)
# ax1.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
# ax1.set_xlabel('Maximum Gap Size')
# ax1.set_xticks(gap_sizes)
# ax1.set_xscale('log')
# ax2 = ax1.twinx()
# ax2.plot(gap_sizes, examples, label='# Examples', c=sns.color_palette()[-2])
# ax2.legend(loc='upper right')
# ax2.set_yscale('log')
# ax2.set_ylabel('# Examples')
# ax2.set_ylim(ymax=300000, ymin=100)
# # ax2.set_yticks([0, 1, 2, 3, 4, 5])
# fig.tight_layout()
# plt.savefig('gap_sizes_complete2016.eps')
# plt.show()

###############################

# accuracy = [0.4788, 0.6226, 0.7010, 0.7481, 0.7840, 0.8051, 0.8216, 0.8334, 0.8422, 0.8493]
# kappas = range(1, 11)
# plt.figure(figsize=(5,4))
# plt.plot(kappas, accuracy, marker='o', mec='white', mew=2, ms=8)
# plt.xlabel('$\kappa$')
# plt.xticks(range(1,11))
# plt.ylabel('Accuracy')
# plt.yticks([0, 0.2, 0.4, 0.6, 0.8, 1])
# plt.ylim(ymax=1)
# plt.tight_layout()
# plt.savefig('accuracy_per_kappa.eps')
# plt.show()
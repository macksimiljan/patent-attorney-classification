from datetime import datetime

import pandas as pd

from pac.utils.database.connection_point import instantiate_engine

from pac.utils.database.connection_point import fetch_multiple_rows
from pac.utils.database.connection_point import fetch_single_row
from pac.utils.plotter.histograms import map_to_histogram_data
from pac.utils.plotter.histograms import show_histogram
from pac.preanalysis.queries.patents_queries import *


def run_patents_preanalysis(connection, text_file, figures_path):

    with open(text_file, 'a') as f:
        f.write('===== Patents {} =====\n'.format(datetime.now()))
        f.write('#EpoPatents: {}\n'.format(number_patents(connection)))
        f.write('#Patents without applicant: {}\n'.format(patents_without_applicant_count(connection)))
        f.write('#Patents without agent: {}\n'.format(patents_without_agent_count(connection)))

    result_set = frequency_status_category(connection)
    x_values, y_values = map_to_histogram_data(result_set, 'status_category', 'rel_freq')
    path = '{}/{}'.format(figures_path, 'patent_status_category_freq.eps')
    show_histogram(x_values, y_values, 'Status Category', '%Patents', figure_path=path)

    result_set = frequency_ep_application_date(connection)
    x_values, y_values = map_to_histogram_data(result_set, 'ep_application_date', 'rel_freq')
    path = '{}/{}'.format(figures_path, 'patent_status_ep_application_date_freq.eps')
    show_histogram(x_values, y_values, 'EP Application Date', '%Patents', y_logarithmic=True, figure_path=path)

    engine = instantiate_engine()
    result = pd.read_sql_query(query_patents_per_month(), engine)
    path = '{}/{}'.format(figures_path, 'patents_per_month.eps')
    show_histogram(result.iloc[0:-1]['ep_application_date_month'], result.iloc[0:-1]['no'],
                   'Month', '#Patent Applications', figure_path=path, date_on_x_axis=True)


def number_patents(connection):
    result = fetch_single_row(connection, query_count_all())
    return result[0]


def frequency_status_category(connection):
    result = fetch_multiple_rows(connection, query_frequency_status_category())
    return result


def frequency_ep_application_date(connection):
    result = fetch_multiple_rows(connection, query_frequency_ep_application_date())
    return result


def patents_without_applicant_count(connection):
    result = fetch_single_row(connection, query_patents_without_applicants_count())
    return result[0]


def patents_without_agent_count(connection):
    result = fetch_single_row(connection, query_patents_without_agents_count())
    return result[0]

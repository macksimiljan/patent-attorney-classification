import matplotlib.pyplot as plt

import seaborn as sns
from numpy import log2

from pac.utils.plotter import COLORS


def show_scatter_plot(x_values, y_values, x_label, y_label,
                      dot_labels=None, dot_label_position=None, dot_size=None,
                      y_logarithmic=False, x_logarithmic=False,
                      group_points=False, group_color=False,
                      figure_path=None):
    if group_points:
        x_values, y_values, frequencies = group_and_add_frequency_of_points(x_values, y_values)
        if group_color:
            cmap = sns.cubehelix_palette(as_cmap=True)
            fig, ax = plt.subplots()
            points = ax.scatter(x_values, y_values, c=frequencies, s=dot_size, cmap=cmap)
            fig.colorbar(points)
        else:
            area = list(map(lambda freq: (20 * log2(freq+1)), frequencies))
            fig, ax = plt.subplots()
            ax.scatter(x_values, y_values, s=area, c=COLORS['basalt'])
    else:
        fig, ax = plt.subplots()
        ax.scatter(x_values, y_values, c=COLORS['basalt'], s=dot_size)

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    if dot_labels is not None:
        delta_x = 0.001 if dot_label_position is None else dot_label_position[0]
        delta_y = 0.005 if dot_label_position is None else dot_label_position[1]
        for i, label in enumerate(dot_labels):
            ax.annotate(label, (x_values[i], y_values[i]), xytext=(x_values[i]+delta_x, y_values[i]+delta_y))

    if y_logarithmic:
        plt.yscale('log', nonposy='clip')
        plt.ylim(ymin=0.7)

    if x_logarithmic:
        plt.xscale('log', nonposy='clip')
        plt.xlim(xmin=0.7)

    if figure_path:
        plt.savefig(figure_path, bbox_inches='tight')
    plt.show()


def group_and_add_frequency_of_points(x_values, y_values):
    point_map = {}
    for x, y in zip(x_values, y_values):
        if x in point_map:
            ys = point_map[x]
            if y in ys:
                ys[y] += 1
            else:
                ys[y] = 1
        else:
            point_map[x] = {y: 1}

    grouped_x_values, grouped_y_values, point_frequencies = [], [], []
    for x in point_map:
        for y, freq in point_map[x].items():
            grouped_x_values.append(x)
            grouped_y_values.append(y)
            point_frequencies.append(freq)

    return grouped_x_values, grouped_y_values, point_frequencies

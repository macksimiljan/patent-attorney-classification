from functools import reduce

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from numpy import arange
from numpy import int64

from pac.utils.plotter import COLORS
from pac.preanalysis.helpers import map_null_value


def show_histogram(x_values, y_values, x_label, y_label, y_logarithmic=False, figure_path=None, maximum=None, x_ticks=None, date_on_x_axis=False):
    first_x_element = x_values[0]
    if date_on_x_axis:
        show_date_based_histogram(x_values, y_values, x_label, y_label, y_logarithmic, figure_path, x_ticks)
    elif type(first_x_element) is int or type(first_x_element) is int64:
        show_integer_based_histogram(x_values, y_values, x_label, y_label, y_logarithmic, figure_path, maximum, x_ticks)
    elif type(first_x_element) is str:
        show_nominal_based_histogram(x_values, y_values, x_label, y_label, y_logarithmic, figure_path)
    else:
        raise RuntimeError


def show_date_based_histogram(x_values, y_values, x_label, y_label, y_logarithmic=False, figure_path=None, maximum=None, x_ticks=None):
    # plot data
    fig, ax = plt.subplots()
    ax.plot(x_values, y_values, 'k')

    years = mdates.YearLocator()  # every year
    months = mdates.MonthLocator()  # every month
    yearsFmt = mdates.DateFormatter('%Y')
    ax.xaxis.set_major_locator(years)
    ax.xaxis.set_major_formatter(yearsFmt)
    ax.xaxis.set_minor_locator(months)

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    if figure_path:
        plt.savefig(figure_path, bbox_inches='tight')
    plt.show()


def show_integer_based_histogram(x_values, y_values, x_label, y_label, y_logarithmic=False, figure_path=None, maximum=None, x_ticks=None):
    x = arange(len(x_values))
    plt.bar(x, y_values, color=COLORS['basalt'])
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    if maximum is not None:
        plt.xlim(xmax=maximum['x'])
        plt.ylim(ymax=maximum['y'])
    if y_logarithmic:
        plt.yscale('log', nonposy='clip')
        plt.ylim(ymin=0.7)
    if x_ticks is not None:
        plt.xticks(x_ticks)
    # plt.axhline(y=mean, zorder=0, color='red')
    if figure_path:
        plt.savefig(figure_path, bbox_inches='tight')
    plt.show()


def show_nominal_based_histogram(x_values, y_values, x_label, y_label, y_logarithmic=False, figure_path=None):
    N = len(x_values)
    men_means = y_values

    ind = arange(N)  # the x locations for the groups
    width = 0.6 # the width of the bars

    fig, ax = plt.subplots()
    ax.bar(ind, men_means, width, color=COLORS['basalt'])

    # add some text for labels, title and axes ticks
    ax.set_ylabel(y_label)
    ax.set_xlabel(x_label)
    ax.set_xticks(ind)
    ax.set_xticklabels(x_values)

    if y_logarithmic:
        plt.yscale('log', nonposy='clip')

    fig.tight_layout()
    character_count_on_x = reduce(lambda x, y: x + y, map(lambda x: len(x), x_values))
    if character_count_on_x > 60:
        fig.autofmt_xdate()

    if figure_path:
        plt.savefig(figure_path, bbox_inches='tight')
    plt.show()


def show_multiple_histograms(x_values, y_values_array, x_label, y_label, labels, y_logarithmic=True, maximum=None, figure_path=None, show_x_values=False):
    if len(y_values_array) != 2:
        raise RuntimeError

    x_values = x_values if maximum is None else x_values[0:maximum]
    n = len(x_values) if maximum is None else maximum
    indices = arange(n)  # the x locations for the groups
    series0 = y_values_array[0] if maximum is None else y_values_array[0][0:maximum]
    series1 = y_values_array[1] if maximum is None else y_values_array[1][0:maximum]

    width = 0.35  # the width of the bars

    fig, ax = plt.subplots()
    bars1 = ax.bar(indices, series0, width, color=COLORS['granat'], label=labels[0])
    bars2 = ax.bar(indices + width, series1, width, color=COLORS['aquamarin'], label=labels[1])

    # add some text for labels, title and axes ticks
    ax.set_ylabel(y_label)
    ax.set_xlabel(x_label)

    if show_x_values:
        character_count_on_x = reduce(lambda x, y: x + y, map(lambda x: len(x), x_values))
        if character_count_on_x > 60:
            fig.autofmt_xdate()
        ax.set_xticks(indices + width / 2)
        ax.set_xticklabels(x_values)

    ax.legend()

    if y_logarithmic:
        plt.yscale('log', nonposy='clip')

    if figure_path:
        plt.savefig(figure_path, bbox_inches='tight')
    fig.tight_layout()
    plt.show()


def show_rank_histogram(x_values, y_values, x_label, y_label, x_logarithm=True, y_logarithm=True, figure_path=None):
    plt.scatter(x_values, y_values, c=COLORS['basalt'], s=20)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    if y_logarithm:
        plt.yscale('log', nonposy='clip')
    if x_logarithm:
        plt.xscale('log', nonposy='clip')
        plt.xlim(xmin=0.7)

    if figure_path:
        plt.savefig(figure_path, bbox_inches='tight')
    plt.show()


def map_to_histogram_data(result_set, x_name, y_name, null_value=None, ignore_null_value=False):
    first_x_element = result_set[0][x_name]
    if type(first_x_element) is int:
        return map_to_integer_based_histogram_data(result_set, x_name, y_name, null_value, ignore_null_value)
    elif type(first_x_element) is str:
        return map_to_nominal_based_histogram_data(result_set, x_name, y_name)
    else:
        raise RuntimeError


def map_to_integer_based_histogram_data(result_set, x_name, y_name, null_value=None, ignore_null_value=False):
    if null_value is None:
        x_values = []
        y_values = []
    else:
        x_values = [0]
        y_values = [null_value]

    next_expected_x = 0 if len(x_values) == 0 else x_values[0] + 1
    if ignore_null_value and next_expected_x == 0:
        next_expected_x = 1

    for row in result_set:
        next_actual_x = row[x_name]
        while next_expected_x < next_actual_x:
            x_values.append(next_expected_x)
            y_values.append(0)
            next_expected_x += 1
        x_values.append(row[x_name])
        y_values.append(row[y_name])
        next_expected_x += 1

    return x_values, y_values


def map_to_nominal_based_histogram_data(result_set, x_name, y_name):
    x_values = []
    y_values = []

    for row in result_set:
        x_values.append(map_null_value(row[x_name]))
        y_values.append(row[y_name])

    return x_values, y_values


def map_to_multiple_histogram_data(result_sets, x_name, y_name, ignore_null_value=False):
    first_x_element = result_sets[0][0][x_name]
    if type(first_x_element) is int:
        return map_to_integer_based_multiple_histogram_data(result_sets, x_name, y_name, ignore_null_value)
    elif type(first_x_element) is str:
        return map_to_nominal_based_multiple_histogram_data(result_sets, x_name, y_name)
    else:
        raise RuntimeError


def map_to_integer_based_multiple_histogram_data(result_sets, x_name, y_name, ignore_null_value=False):
    next_expected_x = 1 if ignore_null_value else 0
    maximum_x = max(map(lambda rs: max(map(lambda row: row[x_name], rs)), result_sets))
    x_values = list(range(next_expected_x, maximum_x + 1))
    y_values_array = []

    for result_set in result_sets:
        y_values = []
        next_expected_x = 1 if ignore_null_value else 0
        for row in result_set:
            next_actual_x = row[x_name]
            while next_expected_x < next_actual_x:
                y_values.append(0)
                next_expected_x += 1
            y_values.append(row[y_name])
            next_expected_x += 1

        while next_expected_x <= maximum_x:
            y_values.append(0)
            next_expected_x += 1
        y_values_array.append(y_values)

    return x_values, y_values_array


def map_to_nominal_based_multiple_histogram_data(result_sets, x_name, y_name):
    if len(result_sets) != 2:
        raise RuntimeError

    y_values_array = []
    x_values0 = list(map(lambda row: row[x_name], result_sets[0]))
    x_values1 = list(map(lambda row: row[x_name], result_sets[1]))
    expected_x_values = combine_nominal_values(x_values0, x_values1)

    for result_set in result_sets:
        next_actual_x = None
        i = 0
        y_values = []
        for row in result_set:
            next_expected_x = expected_x_values[i]
            next_actual_x = row[x_name]
            while next_actual_x != next_expected_x:
                y_values.append(0)
                i += 1
                next_expected_x = expected_x_values[i]

            if next_actual_x == next_expected_x:
                y_values.append(row[y_name])
                i += 1
            else:
                raise RuntimeError

        while i != len(expected_x_values):
            y_values.append(0)
            i += 1

        y_values_array.append(y_values)

    return expected_x_values, y_values_array


def combine_nominal_values(values0, values1):
    return sorted(list(set(values0 + values1)))

COLORS = {
    'granat': '#B02F2C',
    'karneol': '#D64242',
    'aquamarin': '#8AC2D1',
    'basalt': '#261831',
    'blue': '#33B8CA',
    'dark_blue':  '#1F82C0',
    'green': '#6DBFA9',
    'kiwi': '#B1C800',
    'fraunhofer': '#009374'
}
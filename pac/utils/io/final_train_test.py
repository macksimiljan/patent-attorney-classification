import os

from numpy import load
from scipy.sparse import load_npz

from pac.utils.io import DATA_PATH


def read_features_agent_encoding_applicants_agent_probabilities(training_interval, test_year):
    path = os.path.join(DATA_PATH,
                        '{}_{}#{}'.format(
                            training_interval[0],
                            training_interval[1],
                            test_year
                        ))
    ys = load(os.path.join(path, 'ys_features.npz'))

    return ys['features'], ys['agent_encoding'], ys['applicants_of_test'], dict(ys['agent_probabilities'])


def read_gap_sizes(training_interval, test_year):
    path = os.path.join(DATA_PATH,
                        '{}_{}#{}'.format(
                            training_interval[0],
                            training_interval[1],
                            test_year
                        ))
    ys = load(os.path.join(path, 'ys_features.npz'))

    return ys['gap_sizes']


def read_X_train_y_train_X_test_y_test(training_interval, test_year, test_class='multi'):
    path = os.path.join(DATA_PATH,
                        '{}_{}#{}'.format(
                            training_interval[0],
                            training_interval[1],
                            test_year
                        ))
    ys = load(os.path.join(path, 'ys_features.npz'))
    X_train = load_npz(os.path.join(path, 'X_train.npz'))
    X_test = load_npz(os.path.join(path, 'X_test.npz'))
    y_train = ys['y_train_{}'.format(test_class)]
    y_test = ys['y_test_{}'.format(test_class)]

    return X_train, y_train, X_test, y_test

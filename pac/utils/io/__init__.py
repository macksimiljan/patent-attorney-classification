import os

PROJECT_PATH = os.path.join(os.path.dirname(os.path.dirname(__file__)), '..', '..')
DATA_PATH = os.path.join(PROJECT_PATH, 'data')
FREQUENCIES_PATH = os.path.join(DATA_PATH, 'agent_frequencies')

DATABASE_CONFIG_FILE = os.path.join(PROJECT_PATH, 'database_config.yml')

APPLICANT_CATEGORIES_FILE = 'applicant_categories.csv'

TRAINING_FILE_NAME = 'training_dataset.json'
SPLIT_TRAINING_FILE_NAME = 'split_training_dataset.json'
TEST_FILE_NAME = 'test_dataset.json'

PREPROCESSED_TRAINING_FEATURES_FILE_NAME = 'X_train.csv'
PREPROCESSED_TRAINING_CLASSES_FILE_NAME = 'y_train.csv'
X_TRAIN_SPARSE = 'X_train_sparse.npz'
PREPROCESSED_TEST_FEATURES_FILE_NAME = 'X_test.csv'
PREPROCESSED_TEST_CLASSES_FILE_NAME = 'y_test.csv'
X_TEST_SPARSE = 'X_test_sparse.npz'

PATH_TO_TEST_FILE_TRAINING = os.path.join(PROJECT_PATH, 'tests', 'data', '2013', 'training_dataset.json')
PATH_TO_TEST_FILE_SPLIT_TRAINING = os.path.join(PROJECT_PATH, 'tests', 'data', '2013', 'split_training_dataset.json')
PATH_TO_TEST_FILE_TEST = os.path.join(PROJECT_PATH, 'tests', 'data', '2013', 'test_dataset.json')
PATH_TO_TEST_FILE_PREPROCESSED_TRAINING = os.path.join(PROJECT_PATH, 'tests', 'data', '2013', 'X_train.csv')
PATH_TO_TEST_FILE_PREPROCESSED_TEST = os.path.join(PROJECT_PATH, 'tests', 'data', '2013', 'X_test.csv')

PANDAS_SETTINGS = {'to_json': {'lines': True, 'orient': 'records'}}

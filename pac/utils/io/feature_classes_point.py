import pandas as pd
from scipy.sparse import load_npz
from scipy.sparse import save_npz

from pac import Constants
from pac.utils.io import *


def read_dataset(path):
    df = pd.read_json(path_or_buf=path,
                      lines=PANDAS_SETTINGS['to_json']['lines'],
                      orient=PANDAS_SETTINGS['to_json']['orient'])
    return df


def read_training_dataset(year_start, year_end=None):
    df = pd.read_json(path_or_buf=training_file_for(year_start, year_end),
                      lines=PANDAS_SETTINGS['to_json']['lines'],
                      orient=PANDAS_SETTINGS['to_json']['orient'])
    return df


def read_split_training_dataset(year_start, year_end=None):
    df = pd.read_json(path_or_buf=split_training_file_for(year_start, year_end),
                      lines=PANDAS_SETTINGS['to_json']['lines'],
                      orient=PANDAS_SETTINGS['to_json']['orient'])
    return df


def iterator_split_training_dataset(year_start, year_end=None, path=None, chunksize=None):
    path = split_training_file_for(year_start, year_end) if path is None else path
    chunksize = 10**5 if chunksize is None else chunksize
    iterator = pd.read_json(path_or_buf=path,
                            lines=PANDAS_SETTINGS['to_json']['lines'],
                            orient=PANDAS_SETTINGS['to_json']['orient'],
                            chunksize=chunksize)
    return iterator


def write_as_training_dataset(dataset, year_start, year_end=None):
    dataset.to_json(path_or_buf=training_file_for(year_start, year_end),
                    orient=PANDAS_SETTINGS['to_json']['orient'],
                    lines=PANDAS_SETTINGS['to_json']['lines'])


def write_as_split_training_dataset(split_dataset, year_start, year_end=None):
    split_dataset.to_json(path_or_buf=split_training_file_for(year_start, year_end),
                          orient=PANDAS_SETTINGS['to_json']['orient'],
                          lines=PANDAS_SETTINGS['to_json']['lines'])


def read_x_for(train_test, year_start, year_end=None):
    if train_test == 'train':
        return pd.read_csv(x_train_file_for(year_start, year_end))
    elif train_test == 'test':
        return pd.read_csv(x_test_file_for(year_start))
    else:
        raise RuntimeError


def read_x_sparse_for(train_test, year_start, year_end=None):
    if train_test == 'train':
        return load_npz(x_sparse_train_file_for(year_start, year_end))
    elif train_test == 'test':
        return load_npz(x_sparse_test_file_for(year_start))
    else:
        raise RuntimeError


def read_y_for(train_test, year_start, year_end=None):
    if train_test == 'train':
        return pd.read_csv(y_train_file_for(year_start, year_end), header=None, names=['class'])
    elif train_test == 'test':
        return pd.read_csv(y_test_file_for(year_start), header=None, names=['class'])
    else:
        raise RuntimeError


def write_preprocessed_feature_matrix(matrix, train_test, year_start, year_end=None):
    if train_test == 'train':
        path = x_train_file_for(year_start, year_end)
    elif train_test == 'test':
        path = x_test_file_for(year_start)
    else:
        raise RuntimeError
    matrix.to_csv(path, index=False)


def write_X_sparse(train_test, X, year_start, year_end=None):
    if train_test == 'train':
        path = x_sparse_train_file_for(year_start, year_end)
    elif train_test == 'test':
        path = x_sparse_test_file_for(year_start)
    else:
        raise RuntimeError
    save_npz(path, X)


def write_preprocessed_classes_vector(vector, train_test, year_start, year_end=None):
    if train_test == 'train':
        path = y_train_file_for(year_start, year_end)
    elif train_test == 'test':
        path = y_test_file_for(year_start)
    else:
        raise RuntimeError
    vector.to_csv(path, index=False)


def read_test_dataset(year, gap_size=None):
    df = pd.read_json(path_or_buf=test_file_for(year),
                      lines=PANDAS_SETTINGS['to_json']['lines'],
                      orient=PANDAS_SETTINGS['to_json']['orient'])
    if gap_size is not None:
        df = df[df[Constants.Test.Features.gap_size] == gap_size]

    return df


def iterator_test_dataset(year, path=None, chunksize=None):
    path = test_file_for(year) if path is None else path
    chunksize = 10**5 if chunksize is None else chunksize
    iterator = pd.read_json(path_or_buf=path,
                            lines=PANDAS_SETTINGS['to_json']['lines'],
                            orient=PANDAS_SETTINGS['to_json']['orient'],
                            chunksize=chunksize)
    return iterator


def write_as_test_dataset(dataset, year):
    dataset.to_json(
        path_or_buf=test_file_for(year),
        orient=PANDAS_SETTINGS['to_json']['orient'],
        lines=PANDAS_SETTINGS['to_json']['lines'])


def x_train_file_for(year_start, year_end=None):
    return os.path.join(
        data_folder(year_start, year_end),
        PREPROCESSED_TRAINING_FEATURES_FILE_NAME
    )


def x_sparse_train_file_for(year_start, year_end=None):
    return os.path.join(
        data_folder(year_start, year_end),
        X_TRAIN_SPARSE
    )


def x_sparse_test_file_for(year):
    return os.path.join(
        data_folder(year),
        X_TEST_SPARSE
    )


def y_train_file_for(year_start, year_end=None):
    return os.path.join(
        data_folder(year_start, year_end),
        PREPROCESSED_TRAINING_CLASSES_FILE_NAME
    )


def x_test_file_for(year):
    return os.path.join(
        data_folder(year),
        PREPROCESSED_TEST_FEATURES_FILE_NAME
    )


def y_test_file_for(year):
    return os.path.join(
        data_folder(year),
        PREPROCESSED_TEST_CLASSES_FILE_NAME
    )


def delete_x_test(year, path=None):
    path = x_test_file_for(year) if path is None else path
    os.remove(path)


def delete_x_train(year_start, year_end, path=None):
    path = x_train_file_for(year_start, year_end) if path is None else path
    os.remove(path)


def training_file_for(year_start, year_end=None):
    return os.path.join(
        data_folder(year_start, year_end),
        TRAINING_FILE_NAME)


def split_training_file_for(year_start, year_end=None):
    return os.path.join(
        data_folder(year_start, year_end),
        SPLIT_TRAINING_FILE_NAME)


def test_file_for(year):
    return os.path.join(
        data_folder(year),
        TEST_FILE_NAME
    )


def read_split_training_dataset_for_tests():
    return read_dataset(PATH_TO_TEST_FILE_SPLIT_TRAINING)


def read_test_dataset_for_tests():
    return read_dataset(PATH_TO_TEST_FILE_TEST)


def data_folder(year_start, year_end=None):
    folder_name = str(year_start) if year_end is None else '{}_{}'.format(year_start, year_end)
    return os.path.join(DATA_PATH, folder_name)
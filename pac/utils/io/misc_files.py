import pandas as pd

from pac.utils.io import *


def read_applicant_categories():
    return pd.read_csv(os.path.join(DATA_PATH, APPLICANT_CATEGORIES_FILE))


def write_applicant_categories(data):
    data.to_csv(os.path.join(DATA_PATH, APPLICANT_CATEGORIES_FILE),
                index=False)


def read_feature_dict(year_start, year_end=None):
    path_suffix = '{}_{}'.format(year_start, year_end) if year_end is not None else '{}'.format(year_start)
    file = os.path.join(DATA_PATH, path_suffix, 'feature_columns.txt')
    with open(file) as f:
        columns = f.readlines()
    col_dict = {}
    for index, col in enumerate(columns):
        col_dict[col.rstrip()] = index
    return col_dict


def write_frequencies(applicants, agent_probability_functions, year_start, year_end=None):
    file_name = '{}_{}.tsv'.format(year_start, year_end) if year_end is not None else '{}.tsv'.format(year_start)
    file = os.path.join(FREQUENCIES_PATH, file_name)
    with open(file, 'w') as f:
        for applicant, prob_function in zip(applicants, agent_probability_functions):
            f.write("{}\t{}\n".format(applicant, prob_function))


def read_frequencies(year_start, year_end= None):
    file_name = '{}_{}.tsv'.format(year_start, year_end) if year_end is not None else '{}.tsv'.format(year_start)
    file = os.path.join(FREQUENCIES_PATH, file_name)
    with open(file) as f:
        lines = f.readlines()
    probs_dict = {}
    for line in lines:
        data = line.split("\t")
        applicant = data[0]
        prob_function = eval(data[1].rstrip())
        probs_dict[applicant] = prob_function
    return probs_dict

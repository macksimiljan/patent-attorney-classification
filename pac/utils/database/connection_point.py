import psycopg2
import yaml
from psycopg2.extras import DictCursor
from sqlalchemy import create_engine

from pac.utils.io import DATABASE_CONFIG_FILE


def open_connection():
    with open(DATABASE_CONFIG_FILE) as f:
        db_config = yaml.load(f)

    return psycopg2.connect(
        database = db_config['postgres']['database'],
        user = db_config['postgres']['user'],
        host = db_config['postgres']['host'],
        password = db_config['postgres']['password'])


def fetch_single_row(connection, query):
    cursor = connection.cursor(cursor_factory=DictCursor)
    cursor.execute(query)
    row = cursor.fetchone()
    return row


def fetch_multiple_rows(connection, query):
    cursor = connection.cursor(cursor_factory=DictCursor)
    cursor.execute(query)
    rows = cursor.fetchall()
    return rows


def instantiate_engine():
    with open(DATABASE_CONFIG_FILE) as f:
        db_config = yaml.load(f)
    database = db_config['postgres']['database'],
    user = db_config['postgres']['user'],
    host = db_config['postgres']['host'],
    password = db_config['postgres']['password']

    url = 'postgresql://{}:{}@{}/{}'.format(user[0], password, host[0], database[0])
    engine = create_engine(url)
    return engine

from collections import Counter


class BaselineClassifier:

    def __init__(self, settings=None, agent_encoding=None):
        self._settings = Settings() if settings is None else settings

        self.class_probabilities = None
        self._agent_encoding = list(agent_encoding)

    def fit(self, class_probabilities_train):
        self.class_probabilities = {}
        for applicant, classes in class_probabilities_train.items():
            classes_bar = {}
            for c, prob in classes.items():
                if self._settings.task == 'binary':
                    if c == 0:
                        classes_bar[0] = prob
                    if c == 1:
                        classes_bar[1] = prob
                else:
                    classes_bar = classes.copy()
                    classes_bar.pop(0, None)
                    classes_bar.pop(1, None)
            self.class_probabilities[applicant] = classes_bar

    def predict(self, applicants_of_test):
        y_pred = []
        for applicant in applicants_of_test:
            if applicant not in self.class_probabilities:
                if self._settings.task == 'multi':
                    predicted = self._agent_encoding.index(self._settings.default_class)
                else:
                    predicted = self._settings.default_class
                y_pred.append(predicted)
            else:
                probabilities = Counter(self.class_probabilities[applicant])
                if len(probabilities) > 1:
                    a = 42
                most_common = probabilities.most_common(1)[0][0]
                if self._settings.task == 'multi':
                    most_common = self._agent_encoding.index(most_common)
                y_pred.append(most_common)
        return y_pred


class Settings:

    def __init__(self, default_class='None', task='multi'):
        self.default_class = default_class
        self.task = task

    def __str__(self):
        return 'default: {}, task: {}'.format(self.default_class, self.task)

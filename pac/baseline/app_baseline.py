from collections import Counter
from time import time

import numpy as np
from sklearn.metrics import accuracy_score

from pac.baseline.baseline_classifier import BaselineClassifier
from pac.baseline.baseline_classifier import Settings as BaselineSettings
from pac.evaluation.classification_metrics import high_surprisal_counts
from pac.evaluation.classification_metrics import precision_recall_support_labels
from pac.evaluation.classification_metrics import surprisal
from pac.utils.io.final_train_test import read_X_train_y_train_X_test_y_test
from pac.utils.io.final_train_test import read_features_agent_encoding_applicants_agent_probabilities
from pac.utils.io.misc_files import read_applicant_categories
from pac.utils.io.final_train_test import read_gap_sizes

gap_sizes = [0, 1, 10, 100, 1000, 10000]
test_divided_by_applicant_categories = False
training_interval = (2016, None)
test_year = 2017
default_class = 'g1980'
task = 'multi'

print('|--- AppBaseline: training: {}, test: {}, gap sizes: {}, default class: {}, task: {}'.format(
    training_interval,
    test_year,
    gap_sizes,
    default_class,
    task
))

_, y_train, _, y_true = read_X_train_y_train_X_test_y_test(training_interval, test_year, test_class=task)
_, agent_encoding, applicants, agent_probabilities = read_features_agent_encoding_applicants_agent_probabilities(training_interval, test_year)
print('|--- AppBaseline: training size: {:8}'.format(y_train.shape[0]))
print('|--- AppBaseline: test size:     {:8}'.format(y_true.shape[0]))
print('|--- AppBaseline: frequent class in multi-training:', agent_encoding[Counter(y_train).most_common(1)[0][0]])

start_time = time()
settings = BaselineSettings(default_class=default_class,
                            task=task)
baseline = BaselineClassifier(settings, agent_encoding=agent_encoding)

applicants_categories = read_applicant_categories() if test_divided_by_applicant_categories else None


def run_simple_test():
    if len(gap_sizes) > 1 or gap_sizes[0] is not None:
        gap_sizes_of_test_data = read_gap_sizes(training_interval, test_year)
        baseline.fit(agent_probabilities)
        for gap_size in gap_sizes:
            mask = np.isin(gap_sizes_of_test_data, list(range(gap_size + 1)))
            y_pred = baseline.predict(applicants[mask])
            y_true_mod = y_true[mask]
            print('|--- AppBaseline: gap size:  {:8.0f}'.format(gap_size))
            print('|--- AppBaseline: size:      {:8.0f}'.format(len(y_pred)))
            print('|--- AppBaseline: accuracy:  {:8.3f}'.format(accuracy_score(y_true_mod, y_pred)))
            precision, recall, support, predicted_labels, _ = precision_recall_support_labels(y_true_mod, y_pred)
            print('|--- AppBaseline: precision:  {:8.3f}'.format(np.mean(precision)))
            # print('|--- AppBaseline: precision, new:  {:8.3f}'.format(precision[1]))
            # print('|--- AppBaseline: recall, new:     {:8.3f}'.format(recall[1]))
    else:
        baseline.fit(agent_probabilities)
        y_pred = baseline.predict(applicants)
        print('|--- AppBaseline: accuracy:  {:8.3f}'.format(accuracy_score(y_true, y_pred)))
        precision, recall, support, predicted_labels, _ = precision_recall_support_labels(y_true, y_pred)
        print('|--- AppBaseline: precision: {:8.3f}\t\tdev.: {:8.4f}'.format(np.mean(precision), np.std(precision)))
        print('|--- AppBaseline: ', precision, recall, predicted_labels)
        print('|--- AppBaseline: recall:   {:8.3f}'.format(np.mean(recall)))
        surprisal_ = surprisal(y_true, y_pred, applicants, agent_probabilities, agent_encoding=agent_encoding, task=task)
        print('|--- AppBaseline: surprisal: {:8.2f}'.format(surprisal_))
        high_surprisal_ = high_surprisal_counts(y_true, y_pred, applicants, agent_probabilities, agent_encoding=agent_encoding, task=task)
        print('|--- AppBaseline: high surprisal: {:8.3f}'.format(high_surprisal_))


def run_test_divided_by_applicant_categories():
    raise RuntimeError

    agent_frequencies_per_applicant = read_frequencies(training_interval[0], training_interval[1])
    for gap_size in gap_sizes:
        test = read_test_dataset(test_year) if gap_size is None else read_test_dataset(test_year, gap_size=gap_size)
        unique_categories = np.unique(applicants_categories['categories'].values)
        for category in unique_categories:
            applicants = applicants_categories.loc[applicants_categories['categories'] == category]
            applicants = applicants['applicant_grouping'].values
            test_subset = test.loc[test['applicant_grouping'].isin(applicants)]
            start_time = time()
            job.test_classifier(test_subset, agent_frequencies_per_applicant)
            print('  |--- done test for category {} ({} items) in {}s'.format(category,
                                                                              test_subset.shape[0],
                                                                              round(time() - start_time, 4)))
            precisions = job.current_precisions()
            precision = np.average(precisions)
            std = np.std(precisions)
            print('  |--- precision: {}, std: {}, surprisal: {}'.format(round(precision, 4),
                                                                        round(std, 4),
                                                                        round(job.surprisal_, 4)))


if test_divided_by_applicant_categories:
    run_test_divided_by_applicant_categories()
else:
    run_simple_test()

print('|--- AppPreprocessing: done in {}s'.format(round(time() - start_time, 2)))

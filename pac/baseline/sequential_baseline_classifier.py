from collections import Counter

import pandas as pd

from pac import Constants


class SequentialBaselineClassifier:

    def __init__(self, strategy, default_class=None, class_column='agent_grouping_list', ngram=2):
        if strategy not in ['most_common', 'last', 'first', 'ngram', 'combined']:
            raise ValueError

        self._strategy = strategy
        self._default_class= default_class
        self._class_column = class_column
        self._ngram = ngram
        self._ngram_tree = {}
        self._applicant_class_map = pd.DataFrame()

    def fit(self, training_sequences):
        if self._strategy == 'most_common':
            self._fit_most_common(training_sequences)
            return
        elif self._strategy == 'last':
            self._fit_last(training_sequences)
            return
        elif self._strategy == 'first':
            self._fit_first(training_sequences)
            return
        elif self._strategy in ['ngram', 'combined']:
            self._fit_ngram(training_sequences)
            return

    def predict(self, applicants):
        predicted_agents = []
        if self._strategy in ['most_common', 'last', 'first']:
            for applicant in applicants:
                predicted_agents.append(self._predict_for(applicant))
        elif self._strategy in ['ngram', 'combined']:
            raise RuntimeError('Not yet implemented!')
        return predicted_agents

    def _fit_most_common(self, training_sequences):
        def map_function(classes): return Counter(classes).most_common(1)[0][0]
        self._fit(training_sequences, map_function)

    def _fit_last(self, training_sequences):
        def map_function(classes): return classes[-1]
        self._fit(training_sequences, map_function)

    def _fit_first(self, training_sequences):
        def map_function(classes): return classes[0]
        self._fit(training_sequences, map_function)

    def _fit(self, training_sequences, map_function):
        classes_column = training_sequences[self._class_column]
        if self._class_column == 'agent_countries_list':
            classes_column = classes_column.map(lambda classes: self._make_hashable(classes))
        last_column = classes_column.map(map_function)
        self._applicant_class_map = pd.DataFrame({
            Constants.Training.Features.grouping: training_sequences[Constants.Training.Features.grouping],
            'class': last_column
        })
        self._applicant_class_map = self._applicant_class_map.set_index(Constants.Training.Features.grouping)

    def _fit_ngram(self, training_sequences):
        for index, row in training_sequences.iterrows():
            agent_sequence = row[self._class_column]
            ngrams = []
            if len(agent_sequence) >= self._ngram:
                for i in range(len(agent_sequence) - self._ngram + 1):
                    ngrams.append(agent_sequence[i:i+self._ngram])
                self._save_ngrams(ngrams)

    def ngram_tree(self):
        return self._ngram_tree

    def _save_ngrams(self, ngrams):
        for ngram in ngrams:
            subtree = self._ngram_tree
            for i in range(len(ngram)):
                item = ngram[i]
                if item in subtree:
                    subsubtree = subtree[item]
                    if type(subsubtree) is int:
                        subtree[item] += 1
                    else:
                        subtree = subsubtree
                else:
                    if i == len(ngram) - 1:
                        subtree[item] = 1
                    else:
                        subtree[item] = {}
                        subtree = subtree[item]

    def _predict_for(self, applicant):
        if applicant in self._applicant_class_map.index:
            return self._applicant_class_map.loc[applicant]['class']
        else:
            return self._default_class

    def _predict_ngram(self, agent_sequence):
        if len(agent_sequence) < self._ngram:
            if self._strategy == 'combined':
                return self._predict_for(agent_sequence)
            else:
                return None
        context = agent_sequence[-self._ngram:-1]
        subtree = self._ngram_tree
        for previous_agent in context:
            if previous_agent in subtree:
                subtree = subtree[previous_agent]
            else:
                if self._strategy == 'combined':
                    return self._predict_for(agent_sequence)
                else:
                    return None
        best_agent_with_freq = max(subtree, key=lambda key: subtree[key])
        return best_agent_with_freq

    def _make_hashable(self, item):
        return list(map(lambda v: '{}'.format(v), item))


import matplotlib.pyplot as plt
from sklearn.decomposition import TruncatedSVD
import seaborn as sns
from collections import Counter

from pac.utils.io.final_train_test import read_X_train_y_train_X_test_y_test

# This transformer performs linear dimensionality reduction by means of truncated singular value decomposition (SVD), aka LSA.
# Contrary to PCA, this estimator does not center the data before computing the singular value decomposition.
# This means it can work with scipy.sparse matrices efficiently.
X_train, y_train, X_test, y_test = read_X_train_y_train_X_test_y_test([2016, None], 2017, test_class='binary')

print('SVD')
svd = TruncatedSVD(n_components=2, random_state=42)
X_train= svd.fit_transform(X_train)

X_test = svd.transform(X_test)
print(Counter(y_train.tolist()).most_common(5))
print(Counter(y_test.tolist()).most_common(5))

colors = list(map(lambda x: {0: 'red', 1: 'blue'}.get(x, 'gray'), y_train))
# colors = list(map(lambda x: {11: 'red', 24: 'magenta', 9: 'blue', 18: 'cyan', 17: 'yellow'}.get(x, 'gray'), y_train))
# colors_complete = list(map(lambda x: {2277: 'red', 1565: 'magenta', 2882: 'blue', 1313: 'cyan', 2898: 'yellow'}.get(x, 'gray'), y_train))
# colors_4_agents = list(map(lambda x: {1: 'red', 3: 'magenta', 2: 'blue', 0: 'cyan'}.get(x, 'gray'), y_train))
# colors_uk = list(map(lambda x: {85: 'red', 114: 'magenta', 187: 'blue', 108: 'cyan', 16: 'yellow'}.get(x, 'gray'), y_train))
# colors_germany = list(map(lambda x: {306: 'red', 259: 'magenta', 541: 'blue', 430: 'cyan', 534: 'yellow'}.get(x, 'gray'), y_train))
plt.scatter(X_train[:, 0], X_train[:, 1], c=colors, marker='o', s=25, alpha=0.3)
plt.xlabel('SV1')
plt.ylabel('SV2')
plt.title('2012-2016, Filter: 1 Applicant')

# colors = list(map(lambda x: {0: 'red', 1: 'blue', 2: 'cyan', 3: 'magenta'}[x], y_test))
# plt.scatter(X_test[:, 0], X_test[:, 1], c=colors, marker='v')
# #plt.show()
#
# colors = list(map(lambda x: {0: 'red', 1: 'blue', 2: 'cyan', 3: 'magenta'}[x], y_train))
# plt.scatter(X_train[:, 0], X_train[:, 1], c=colors, marker='o')
plt.show()


from time import time

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score

from collections import Counter
from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import RandomOverSampler
from imblearn.over_sampling import SMOTE

from pac.evaluation.classification_metrics import precision_recall_support_labels
from pac.evaluation.classification_metrics import surprisal
from pac.evaluation.classification_metrics import high_surprisal_counts
from pac.utils.io.final_train_test import read_X_train_y_train_X_test_y_test
from pac.utils.io.final_train_test import read_features_agent_encoding_applicants_agent_probabilities
from pac.utils.io.final_train_test import read_gap_sizes

sns.set_palette(sns.diverging_palette(220, 20, n=5))
sns.set_style("ticks")

training_interval = (2016, 2017)
test_year = 2018
task = 'binary'

print('|--- LogisticRegression: training: {}, test: {}, task: {}'.format(
    training_interval,
    test_year,
    task
))

X_train, y_train, X_test, y_test = read_X_train_y_train_X_test_y_test(training_interval, test_year, test_class=task)

# over_sampler = SMOTE(random_state=0)#, ratio={0: 3*1689, 1: 1689})
# X_train, y_train = over_sampler.fit_sample(X_train, y_train)

# X_test = X_train
# y_test = y_train

features, agent_encoding, applicants_of_test, agent_probabilities = read_features_agent_encoding_applicants_agent_probabilities(training_interval, test_year)

print('|--- LogisticRegression: training size: {:8}'.format(y_train.shape[0]))
print('|--- LogisticRegression: test size:     {:8}'.format(y_test.shape[0]))
print('|--- LogisticRegression: train class distribution, top 5: {}'.format(Counter(y_train).most_common(5)))
print('|--- LogisticRegression: test class distribution, top 5:  {}'.format(Counter(y_test).most_common(5)))


def correct_and_wrong_applicants(y_pred, y_true):
    y_true_mod = np.array(y_true).ravel()
    y_pred_mod = np.array(y_pred).ravel()
    applicants = np.array(applicants_of_test).ravel()
    correct_indices = np.where((y_true_mod == y_pred_mod))[0]
    applicants_correct = np.take(applicants, correct_indices)
    wrong_indices = np.where((y_true_mod != y_pred_mod))[0]
    applicants_wrong = np.take(applicants, wrong_indices)
    both_applicants = np.intersect1d(applicants_correct, applicants_wrong)
    applicants_correct = np.setdiff1d(applicants_correct, both_applicants)
    applicants_wrong = np.setdiff1d(applicants_wrong, both_applicants)
    return applicants_correct, applicants_wrong, both_applicants


def most_likely_agents(classifier, X_test, threshold=3):
    probs_matrix = classifier.predict_proba(X_test)
    result = []
    stds = []
    for probs in probs_matrix:
        probs = list(probs)
        highest_probs = list(sorted(list(enumerate(probs)), key=lambda p: -p[1]))[0:threshold]
        stds.append(np.std(list(map(lambda p: p[1], highest_probs))))
        classes = list(map(
            lambda p: classifier.classes_[p[0]],
            highest_probs))
        result.append(classes)
    print('|--- MostLikelyAgents: average standard deviation: {:4.3f}'.format(np.mean(stds)))
    return result


def threshold_eval(top_agents_matrix, y_true):
    correct = 0
    for top_agents, y in zip(top_agents_matrix, y_true):
        if y in top_agents:
            correct += 1
    return correct / y_true.shape[0]


def probs_of_correct_and_false_classes(classifier, X_test, y_true, y_pred):
    probs_matrix = classifier.predict_proba(X_test)
    max_probs = probs_matrix.max(axis=1)
    true_probs = []
    false_probs = []
    for y1, y2, prob in zip(y_true, y_pred, max_probs):
        true_probs.append(prob) if y1 == y2 else false_probs.append(prob)
    return [np.mean(true_probs), np.std(true_probs), np.median(true_probs)], \
           [np.mean(false_probs), np.std(false_probs), np.median(false_probs)]


gap_sizes = read_gap_sizes(training_interval, test_year)
precisions = []
accuracies = []
surprisals = []
threshold_accuracies_3 = []
threshold_accuracies_5 = []
train_accuracies = []
precision_values = []
recall_values = []
training_sizes = []
test_sizes = []
cs = [0.00001, 0.00005, 0.0001, 0.0005, 0.001, 0.01, 0.1]
max_iters = [1, 5, 10, 100]
learning_rate = cs
allowed_number_agents = [1, 2, 5, 10, 20, 50, 100]
maximum_gap_sizes = [0, 1, 10, 100, 1000, 10000]
x_label = 'Inverse Regularization Strength (L2)' # '# Epochs' # 'Learning Rate'  'Class Evidence' 'Gap Sizes' #
variables = [0.0005]
# y_test_orig = y_test.copy()
# X_test_orig = X_test.copy()
# y_train_orig = y_train.copy()
# X_train_orig = X_train.copy()
for v in variables:
    # allowed_agents = list(map(
    #     lambda x: x[0],
    #     list(filter(
    #         lambda x: x[1] >= v,
    #         list(Counter(y_train_orig).items())))))
    # mask = np.isin(y_train_orig, allowed_agents)
    # y_train = y_train_orig[mask]
    # X_train = X_train_orig[mask]
    # mask = np.isin(y_test_orig, allowed_agents)
    # y_test = y_test_orig[mask]
    # X_test = X_test_orig[mask]

    # mask = np.isin(gap_sizes, list(range(v + 1)))
    # y_test = y_test_orig[mask]
    # X_test = X_test_orig[mask]

    training_sizes.append(y_train.shape[0])
    test_sizes.append(y_test.shape[0])



    print('\n|--- LogisticRegression: v = {}'.format(v))
    # classifier = LogisticRegression(penalty='l2',
    #                                 C=v,
    #                                 solver='sag',
    #                                 n_jobs=4,
    #                                 max_iter=1)
    classifier = SGDClassifier(loss='log',
                               penalty='l2',
                               max_iter=5,
                               alpha=v,
                               # learning_rate='constant',
                               # eta0=v,
                               n_jobs=4,
                               class_weight='balanced',
                               random_state=42)

    start_time = time()
    classifier.fit(X_train, y_train)

    # print('|--- LogisticRegression: predicting ... ')
    y_pred = classifier.predict(X_test)

    y_pred_train = classifier.predict(X_train)
    accuracy_train = accuracy_score(y_train, y_pred_train)
    train_accuracies.append(accuracy_train)
    print(y_train.shape[0], y_test.shape[0])
    accuracy = accuracy_score(y_test, y_pred)
    print('|--- LogisticRegression: accuracy:  {:8.4f}'.format(accuracy))
    precision, recall, support, predicted_labels, pres_orig = precision_recall_support_labels(y_test, y_pred)
    print('|--- LogisticRegression: precision: {:8.4f}\t\tdev.: {:8.4f}'.format(np.mean(precision), np.std(precision)))
    print('|--- LogisticRegression: (agent, precision)',
          list(sorted(zip(predicted_labels, precision), key=lambda p: p[0])))
    print('|--- LogisticRegression: (agent, recall)',
          list(sorted(zip(predicted_labels, recall), key=lambda p: p[0])))
    print('|--- LogisticRegression: recall: {:8.4f}\t\tdev.: {:8.4f}'.format(np.mean(recall), np.std(recall)))
    surprisal_ = surprisal(y_test, y_pred, applicants_of_test, agent_probabilities, agent_encoding=agent_encoding, task=task)
    print('|--- LogisticRegression: surprisal: {:8.2f}'.format(surprisal_))
    high_surprisal_ = high_surprisal_counts(y_test, y_pred, applicants_of_test, agent_probabilities, agent_encoding=agent_encoding, task=task)
    print('|--- LogisticRegression: high surprisal: {:8.3f}'.format(high_surprisal_))

    precisions.append(np.mean(precision))
    accuracies.append(accuracy)
    surprisals.append(surprisal_)

    # top_agents = most_likely_agents(classifier, X_test, threshold=3)
    # threshold_accuracy = threshold_eval(top_agents, y_test)
    # threshold_accuracies_3.append(threshold_accuracy)
    # print('|--- LogisticRegression: threshold accuracy: {:8.3f}'.format(threshold_accuracy))
    # top_agents = most_likely_agents(classifier, X_test, threshold=5)
    # threshold_accuracy = threshold_eval(top_agents, y_test)
    # threshold_accuracies_5.append(threshold_accuracy)
    # print('|--- LogisticRegression: threshold accuracy: {:8.3f}'.format(threshold_accuracy))
    # print('|--- LogisticRegression: average probs true/ false: ', probs_of_correct_and_false_classes(classifier, X_test, y_test, y_pred))

    # correct_applicants, wrong_applicants, both_applicants = correct_and_wrong_applicants(y_pred, y_test)
    # print('|--- LogisticRegression: correct applicants ({:3.0f}): {}'.format(len(correct_applicants), correct_applicants))
    # print('|--- LogisticRegression: wrong applicants   ({:3.0f}): {}'.format(len(wrong_applicants), wrong_applicants))
    # print('|--- LogisticRegression: both applicants    ({:3.0f}): {}'.format(len(both_applicants), both_applicants))

    # precision_values.append(precision)
    precision_values.append(precision[1] if len(precision) > 1 else 0)
    # recall_values.append(recall[1])



fig, ax1 = plt.subplots(figsize=(5,4))
ax1.plot(variables, accuracies, label='Accuracy', c=sns.color_palette()[-1])
ax1.plot(variables, precisions, label='Precision', linestyle='--')
# ax1.plot(variables, recall_values, label='Recall', linestyle=':')
print('|--- LogisticRegression: train accuracy:', train_accuracies)
ax1.legend(loc='upper left')
ax1.set_ylabel('Accuracy, Precision')
ax1.set_ylim(ymax=1.2, ymin=0.0)
ax1.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax1.set_xlabel(x_label)
ax1.set_xticks(variables)
ax1.set_xscale('log')
ax2 = ax1.twinx()
ax2.plot(variables, surprisals, label='Surprisal', c=sns.color_palette()[1])
ax2.legend(loc='upper right')
# ax2.set_yscale('log')
ax2.set_ylabel('Surprisal')
ax2.set_ylim(ymax=5.7, ymin=0)
ax2.set_yticks([0, 1, 2, 3, 4, 5])
fig.tight_layout()
plt.savefig('reg_strength_log_reg.eps')
plt.show()

# fig, ax1 = plt.subplots(figsize=(5,4))
# ax1.plot(variables, accuracies, label='Accuracy', c=sns.color_palette()[-1])
# ax1.plot(variables, threshold_accuracies_3, label='Accuracy, $\kappa = 3$', linestyle='--', c=sns.color_palette()[-1])
# # ax1.plot(variables, threshold_accuracies_5, label='Accuracy, $\kappa = 5$', linestyle=':', c=sns.color_palette()[-1])
# ax1.legend(loc='upper left')
# ax1.set_ylabel('Accuracy')
# ax1.set_ylim(ymax=1.2, ymin=0.0)
# ax1.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
# ax1.set_xlabel(x_label)
# ax1.set_xticks(variables)
# ax1.set_xscale('log')
# fig.tight_layout()
# plt.savefig('threshold_accuracy.eps')
# plt.show()
#
# plt.figure(figsize=(6,4))
# plt.scatter(precision_values[0], recall_values[0], alpha=0.6)
# mean_p = np.mean(precision_values[0])
# mean_r = np.mean(recall_values[0])
# plt.plot([mean_p, mean_p], [-0.1, 1.1], linestyle='--', c=sns.color_palette()[-2], label='Average')
# plt.plot([-0.1, 1.1], [mean_r, mean_r], linestyle='--', c=sns.color_palette()[-2])
# plt.xlabel('Precision')
# plt.xlim(xmin=-0.05, xmax=1.05)
# plt.ylim(ymin=-0.05, ymax=1.05)
# plt.ylabel('Recall')
# plt.legend(loc='upper left')
# plt.tight_layout()
# plt.savefig('precision_recall_ger.eps')
# plt.show()
#
# fig, ax1 = plt.subplots(figsize=(5,4))
# ax1.plot(variables, training_sizes, label='Training', c=sns.color_palette()[0])
# ax1.plot(variables, test_sizes, label='Validation', linestyle='--', c=sns.color_palette()[0])
# ax1.legend(loc='upper right')
# ax1.set_ylabel('# Examples')
# ax1.set_ylim(ymax=30000, ymin=100)
# ax1.set_yticks([100, 1000, 10000])
# ax1.set_yscale('log')
# ax1.set_xlabel(x_label)
# ax1.set_xticks(variables)
# ax1.set_xscale('log')
# fig.tight_layout()
# plt.savefig('training_test_sizes_ger.eps')
# plt.show()
from time import time

import matplotlib.pyplot as plt
from numpy import mean
from numpy import std
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from pac.evaluation.classification_metrics import precision_recall_support_labels
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz
from pydotplus import graph_from_dot_file

from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import SMOTE

import numpy as np
from collections import Counter

from pac.evaluation.classification_metrics import surprisal
from pac.evaluation.classification_metrics import high_surprisal_counts
from pac.utils.io.final_train_test import read_X_train_y_train_X_test_y_test
from pac.utils.io.final_train_test import read_features_agent_encoding_applicants_agent_probabilities


import seaborn as sns


sns.set_palette(sns.diverging_palette(220, 20, n=5))
sns.set_style("ticks")


def fit_predict_evaluate(classifier):
    classifier.fit(X_train, y_train)
    predicted_classes = classifier.predict(X_test)
    precision, recall, support, labels_predicted, _ = precision_recall_support_labels(y_test, predicted_classes)
    # _suprisal = surprisal(y_test, predicted_classes, applicants_of_test, agent_probabilities, agent_encoding=agent_encoding, task=task)
    # _surprisal_high = high_surprisal_counts(y_test, predicted_classes, applicants_of_test, agent_probabilities, agent_encoding=agent_encoding, task=task)
    accuracy = accuracy_score(y_test, predicted_classes)
    print('|--- Trees: accuracy:      {:.3f}'.format(accuracy))
    print('|--- Trees: precision avg: {:.3f}, {:.3f}'.format(mean(precision), std(precision)))
    print('|--- Trees: ', precision, recall, labels_predicted)
    print('|--- Trees: recall avg: {:.3f}, {:.3f}'.format(mean(recall), std(recall)))
    # print('|--- Trees: surprisal:     {:.2f}'.format(_suprisal))
    # print('|--- Trees: high surprisal:{:.3f}'.format(_surprisal_high))
    # precisions_test.append(mean(precision))
    # # precisions_test.append(precision[1] if len(precision) > 1 else 0)
    # recalls.append(mean(recall))
    # accuracy_test.append(accuracy)
    # surprisal_test.append(_suprisal)
    #
    # predicted_classes = classifier.predict(X_train)
    # accuracy_train.append(accuracy_score(y_train, predicted_classes))


    # export_graphviz(classifier, feature_names=features, rounded=True, out_file = 'tree.dot', max_depth=3, rotate=True)
    # graph = graph_from_dot_file('tree.dot')
    # graph.write_png('tree.png')


training_interval = (2016, 2017)
test_year = 2018
task = 'binary'
depths = [5]

print('|--- Trees: training: {}, test: {}, task: {}'.format(
    training_interval,
    test_year,
    task
))

X_train, y_train, X_test, y_test = read_X_train_y_train_X_test_y_test(training_interval, test_year, test_class=task)
features, agent_encoding, applicants_of_test, agent_probabilities = read_features_agent_encoding_applicants_agent_probabilities(training_interval, test_year)

# allowed_agents = list(map(
#     lambda x: x[0],
#     list(filter(
#         lambda x: x[1] > 5,
#         list(Counter(y_train).items())))))
# mask = np.isin(y_train, allowed_agents)
# y_train = y_train[mask]
# X_train = X_train[mask]
# mask = np.isin(y_test, allowed_agents)
# y_test = y_test[mask]
# X_test = X_test[mask]
#
over_sampler = RandomUnderSampler(random_state=0)
X_train, y_train = over_sampler.fit_sample(X_train, y_train)

# X_test = X_train
# y_test = y_train

print('|--- Trees: training size: {:8}'.format(y_train.shape[0]))
print('|--- Trees: test size:     {:8}'.format(y_test.shape[0]))
print('|--- Trees: train class distribution, top5:  {}'.format(Counter(y_train).most_common(5)))
print('|--- Trees: test class distribution, top 5:  {}'.format(Counter(y_test).most_common(5)))

# Decision Tree
print('Decision Tree')
precisions_train = []
precisions_test = []
accuracy_test = []
surprisal_test = []
accuracy_train = []
recalls = []
for d in depths:
    print('|--- Trees: depth: {:8}'.format(d))
    tree = DecisionTreeClassifier(criterion='gini',
                                  max_depth=d,
                                  random_state=1)
    fit_predict_evaluate(tree)


fig, ax1 = plt.subplots(figsize=(5,4))
fig.figsize = (5,4)
ax1.plot(depths, accuracy_test, label='Accuracy', c=sns.color_palette()[-1])
ax1.plot(depths, accuracy_train, label='Accuracy, Training', c=sns.color_palette()[-2], linestyle='--')
# ax1.plot(depths, precisions_test, label='Precision', linestyle='--')
# ax1.plot(depths, recalls, label='Recall', linestyle=':', c=sns.color_palette()[0])
ax1.legend(loc='upper left')
ax1.set_ylabel('Accuracy, Precision')
ax1.set_ylim(ymax=1.2, ymin=0.0)
ax1.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax1.set_xlabel('Depth of Decision Tree')
ax1.set_xticks(depths)
ax2 = ax1.twinx()
ax2.plot(depths, surprisal_test, label='Surprisal',  c=sns.color_palette()[1])
ax2.legend(loc='upper right')
# ax2.set_yscale('log')
ax2.set_ylabel('Surprisal')
ax2.set_ylim(ymax=5.7, ymin=0)
ax2.set_yticks([0, 1, 2, 3, 4, 5])
fig.tight_layout()
plt.savefig('depth_dec_tree.eps')
plt.show()




from pac.evaluation.classification_metrics import surprisal

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.metrics import accuracy_score
from pac.evaluation.classification_metrics import precision_recall_support_labels

from pac.classification.deep.wide_and_deep import dataset
from pac.utils.io.final_train_test import read_features_agent_encoding_applicants_agent_probabilities


_, feature_columns = dataset.build_model_columns()

my_optimizer = tf.train.ProximalAdagradOptimizer(
    learning_rate=0.1,
    l1_regularization_strength=0.01
)
my_optimizer = tf.contrib.estimator.clip_gradients_by_norm(my_optimizer, 5.0)

classifier = tf.estimator.DNNClassifier(
    feature_columns=feature_columns,
    hidden_units=[200, 100],
    n_classes=2,
    optimizer=my_optimizer
)

train_file = '../../../data/tf_data_train.csv'
test_file = '../../../data/tf_data_test.csv'

################
# Parameters
################
_, agent_encoding, applicants_of_test, agent_probabilities = read_features_agent_encoding_applicants_agent_probabilities((2012, 2016), 2017)
task = 'binary'
################

print('|--- DNN: precision: training ... ')
classifier.train(
    input_fn=lambda: dataset.input_fn(train_file, 100, False, 500),
    steps=None)

# evaluation_metrics = classifier.evaluate(
#     input_fn=lambda: dataset.input_fn(train_file, None, False, 100),
#     steps=1000)
# print("Training set metrics:")
# for m in evaluation_metrics:
#     print(m, evaluation_metrics[m])
# print("---")

print('|--- DNN: precision: predicting ... ')

y_true = pd.read_csv(train_file).values[:, -1].astype(int)
y_pred = []

predictions = classifier.predict(input_fn=lambda: dataset.input_fn(train_file, None, False, y_true.shape[0]))
for p, label_true in zip(predictions, y_true):
    label_pred = eval(p['classes'][0])
    y_pred.append(label_pred)


print('TRAINING')
precisions, _, _, _, _ = precision_recall_support_labels(y_true, y_pred)
print('|--- DNN: precision: {:8.3f}\t\tdev.: {:8.3f}'.format(np.mean(precisions), np.std(precisions)))
print('|--- DNN: precisions: {}'.format(precisions[0:5]))
print('|--- DNN: accuracy:  {:8.3f}'.format(accuracy_score(y_true, y_pred)))
print()


evaluation_metrics = classifier.evaluate(
    input_fn=lambda: dataset.input_fn(test_file, None, False, 100),
    steps=100)

y_true = pd.read_csv(test_file).values[:, -1].astype(int)
y_pred = []

predictions = classifier.predict(input_fn=lambda: dataset.input_fn(test_file, None, False, y_true.shape[0]))
for p, label_true in zip(predictions, y_true):
    label_pred = eval(p['classes'][0])
    y_pred.append(label_pred)

print("Test set metrics:")
for m in evaluation_metrics:
    print(m, evaluation_metrics[m])
print("---")



print('TEST')
precisions, _, _, support, _ = precision_recall_support_labels(y_true, y_pred)
print('|--- DNN: precision: {:8.3f}\t\tdev.: {:8.3f}'.format(np.mean(precisions), np.std(precisions)))
print('|--- DNN: precisions: {}'.format(precisions[0:5]))
print('|--- DNN: accuracy:  {:8.3f}'.format(accuracy_score(y_true, y_pred)))
surprisal_ = surprisal(y_true, y_pred, applicants_of_test, agent_probabilities, agent_encoding=agent_encoding, task=task)
print('|--- DNN: surprisal: {:8.2f}'.format(surprisal_))

for p, s in zip(precisions, support):
    print('precision:', round(p, 4), ', support:', s)


# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Download and clean the Census Income Dataset."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# pylint: disable=wrong-import-order
import tensorflow as tf

# pylint: enable=wrong-import-order


_CSV_COLUMNS = [
    'application_date', 'ipc_class0', 'ipc_class1', 'ipc_class2', 'ipc_class3',
    'city0', 'city1', 'city2', 'city3', 'country', 'status', 'applicant_type', 'applicant',
    'label'
]

_CSV_COLUMN_DEFAULTS = [[0.0], [''], [''], [''], [''], [''], [''], [''], [''], [''], [''], [''], [''], [0]]

_HASH_BUCKET_SIZE = 1000

NUM_EXAMPLES = {
        'train': None,
        'validation': None,
}


def build_model_columns():
    """Builds a set of wide and deep feature columns."""
    # Continuous variable columns
    # vocabularies
    vocabulary_ipc = ['ipc_f42', 'ipc_f02', 'ipc_e04', 'ipc_a01', 'ipc_b81', 'ipc_b22', 'ipc_f24', 'ipc_f22', 'ipc_a24',
                      'ipc_g02', 'ipc_c14', 'ipc_a21', 'ipc_c10', 'ipc_b26', 'ipc_g09', 'ipc_c23', 'ipc_a43', 'ipc_f01',
                      'ipc_c12', 'ipc_f41', 'ipc_g06', 'ipc_b29', 'ipc_b30', 'ipc_b65', 'ipc_g03', 'ipc_c05', 'ipc_b06',
                      'ipc_f17', 'ipc_c02', 'ipc_g05', 'ipc_e05', 'ipc_a22', 'ipc_b24', 'ipc_c07', 'ipc_c01', 'ipc_h01',
                      'ipc_f21', 'ipc_b64', 'ipc_a45', 'ipc_b27', 'ipc_a61', 'ipc_b25', 'ipc_b33', 'ipc_d07', 'ipc_f25',
                      'ipc_e01', 'ipc_a63', 'ipc_e21', 'ipc_d01', 'ipc_b41', 'ipc_f15', 'ipc_c25', 'ipc_h03', 'ipc_b09',
                      'ipc_c11', 'ipc_a62', 'ipc_b02', 'ipc_b28', 'ipc_b67', 'ipc_b63', 'ipc_b66', 'ipc_h02', 'ipc_b05',
                      'ipc_f16', 'ipc_c13', 'ipc_d04', 'ipc_g21', 'ipc_c22', 'ipc_b82', 'ipc_h05', 'ipc_b08', 'ipc_b31',
                      'ipc_g04', 'ipc_b07', 'ipc_g01', 'ipc_c03', 'ipc_f23', 'ipc_a41', 'ipc_g11', 'ipc_g12', 'ipc_h04',
                      'ipc_g07', 'ipc_a47', 'ipc_b01', 'ipc_c04', 'ipc_c08', 'ipc_b62', 'ipc_g08', 'ipc_a46', 'ipc_b32',
                      'ipc_c21', 'ipc_b44', 'ipc_c40', 'ipc_c09', 'ipc_f28', 'ipc_d06', 'ipc_d21', 'ipc_b60', 'ipc_b03',
                      'ipc_c30', 'ipc_g10', 'ipc_d03', 'ipc_b23', 'ipc_b61', 'ipc_b21', 'ipc_a23', 'ipc_f04', 'ipc_f03',
                      'ipc_c06', 'ipc_b04', 'ipc_e02', 'ipc_f26', 'ipc_e06', 'ipc_b42', '']
    vocabulary_cities = ['Karlsruhe', 'Freising', 'München', 'Lonsdale', 'Mönchengladbach', 'Weimar', 'Potsdam',
                         'Ingolstadt', 'Mülheim_an_der_Ruhr', 'Palo_Alto', 'Nuremberg', 'Landshut', 'Santa_Clara',
                         'Iserlohn', 'Neubeuern', 'Wolfsburg', 'Richmond', "Sant'Agata_Bolognese", 'Munich', 'Garching',
                         'Reichenbach', 'Ilmenau', 'Duisburg', 'Bochum', 'Frankfurt_(Oder)', 'Jülich', 'Freiburg',
                         'Köthen', 'Markneukirchen', 'Schneeberg', 'Bernburg', 'Cologne', 'Amberg', 'Bielefeld',
                         'Soest', 'Zwickau', 'Reutlingen', 'Wetzlar', 'Lübeck', 'Kassel', 'Mannheim', 'Cambridge',
                         'Varese', 'Kiel', 'Köln', 'Hildesheim', 'Plymouth', 'Siegen', 'Menlo_Park', 'Koblenz',
                         'Zweibrücken', 'Schmalkalden', 'Berlin', 'Leipzig', 'Mainz', 'Steyr', 'Pleasanton', 'Marburg',
                         'Bonn', 'Freiberg_(Sachsen)', 'Lüneburg', 'Pullach', 'Darmstadt', 'Neckarsulm',
                         'Dessau-Roßlau', 'Wexford', 'Ulm', 'Dresden', 'Oberkirch', 'Göttingen', 'Crewe', 'Schweinfurt',
                         'Wuppertal', 'Rosenheim', 'San_Diego', 'Gaimersheim', 'Santiago', 'Kempten', 'Krefeld',
                         'Oldenburg', 'Newark', 'Würzburg', 'Aalen', 'Paderborn', 'Bayreuth', 'Kaiserslautern',
                         'Tübingen', 'Neubiberg', 'Mladá_Boleslav', 'Weiden', 'Oberkochen', 'Jena', 'Steinfurt',
                         'Osnabrück', 'Cottbus', 'Frankfurt_am_Main', 'Regensburg', 'Hennigsdorf', 'Erlangen',
                         'Södertälje', 'Saarbrücken', 'Bremen', 'Deggendorf', 'Rostock', 'Hannover', 'Münster',
                         'Chemnitz', 'Düsseldorf', 'Siegen-Wilnsdorf', 'Essingen', 'Magdeburg', 'Coburg', 'Essen',
                         'Greifswald', 'Porto', 'Sunnyvale', 'Geesthacht', 'Senftenberg', 'Gießen', 'Bremerhaven',
                         'Augsburg', 'Gelsenkirchen', 'Giessen', 'Braunschweig', 'Aachen', 'La_Rochelle', 'Halle',
                         'Dortmund', 'Biberach', 'Heidelberg', 'Stuttgart', 'Mittweida', 'Konstanz', 'Stralsund',
                         'Hamburg', 'Mountain_View', 'Kühlungsborn', '']
    vocabulary_countries = ['Austria', 'Australia', 'Chile', 'Canada', 'United_States', 'Czech_Republic', 'Portugal',
                            'Italy', 'United_Kingdom', 'Switzerland', 'Sweden', 'Ireland', 'France', 'Germany', '']
    vocabulary_types = ['service_provider', 'tto', 'technology_company', 'research_organisation', '']
    vocabulary_status = ['successful', 'in_progress', 'failed', '']
    vocabulary_applicants = ['', 'g197', 'g1992', 'g8737', 'g174', 'a5273', 'g4034', 'g81', 'g45', 'g4906', 'g54', 'g7820', 'g702', 'g4069', 'g324', 'g4883', 'g83', 'g142', 'g203', 'g5364', 'g5365', 'g201', 'g2521', 'g2511', 'g4887', 'g4051', 'g4938', 'g313', 'g189', 'g202', 'g4926', 'g4957', 'g50', 'g392', 'g6567', 'g578', 'g60', 'g56', 'g4899', 'g43', 'g26', 'g182', 'g1071', 'g51', 'g1686', 'g198', 'g6242', 'g4896', 'g194', 'g6438', 'g1993', 'g580', 'g4907', 'g3256', 'g78', 'g196', 'g2957', 'g3413', 'g4965', 'g4939', 'g4892', 'g4881', 'g4935', 'g3516', 'g25', 'g4052', 'g200', 'g463', 'g4929', 'g4927', 'g5361', 'g6435', 'g40', 'g4889', 'g4912', 'g4885', 'g1688', 'g342', 'g5366', 'g4950', 'g1994', 'g4956', 'g4920', 'g2513', 'g4888', 'g5363', 'g5369', 'g4894', 'g3035', 'g191', 'g2487', 'g5370', 'g8317', 'g569', 'g85', 'g4036', 'g4893', 'g5371', 'g502', 'a447', 'a28', 'g4928', 'g5367', 'g59', 'g4091', 'g188', 'g525', 'g41', 'a100', 'g8387', 'g178', 'g1170', 'g71', 'g53', 'g4953', 'g4946', 'g192', 'g4902', 'g176', 'g4919', 'g179', 'g55', 'g44', 'g42', 'g5373', 'g1687', 'g52', 'g180', 'g5372', 'g2937', 'g2999', 'g6436', 'g2523', 'g5362']

    # Continuous columns
    application_date = tf.feature_column.numeric_column('application_date')
    ipc_class0 = tf.feature_column.categorical_column_with_vocabulary_list('ipc_class0', vocabulary_ipc)
    ipc_class1 = tf.feature_column.categorical_column_with_vocabulary_list('ipc_class1', vocabulary_ipc)
    ipc_class2 = tf.feature_column.categorical_column_with_vocabulary_list('ipc_class2', vocabulary_ipc)
    ipc_class3 = tf.feature_column.categorical_column_with_vocabulary_list('ipc_class3', vocabulary_ipc)
    city0 = tf.feature_column.categorical_column_with_vocabulary_list('city0', vocabulary_cities)
    city1 = tf.feature_column.categorical_column_with_vocabulary_list('city1', vocabulary_cities)
    city2 = tf.feature_column.categorical_column_with_vocabulary_list('city2', vocabulary_cities)
    city3 = tf.feature_column.categorical_column_with_vocabulary_list('city3', vocabulary_cities)
    country = tf.feature_column.categorical_column_with_vocabulary_list('country', vocabulary_countries)
    status = tf.feature_column.categorical_column_with_vocabulary_list('status', vocabulary_status)
    applicant_type = tf.feature_column.categorical_column_with_vocabulary_list('applicant_type', vocabulary_types)
    applicant = tf.feature_column.categorical_column_with_vocabulary_list('applicant', vocabulary_applicants)

    deep_columns = [
        application_date,
        tf.feature_column.embedding_column(ipc_class0, dimension=4),
        tf.feature_column.embedding_column(ipc_class1, dimension=4),
        tf.feature_column.embedding_column(ipc_class2, dimension=4),
        tf.feature_column.embedding_column(ipc_class3, dimension=4),
        tf.feature_column.embedding_column(city0, dimension=4),
        tf.feature_column.embedding_column(city1, dimension=4),
        tf.feature_column.embedding_column(city2, dimension=4),
        tf.feature_column.embedding_column(city3, dimension=4),
        tf.feature_column.embedding_column(country, dimension=2),
        tf.feature_column.indicator_column(status),
        tf.feature_column.indicator_column(applicant_type),
        tf.feature_column.embedding_column(applicant, dimension=4)
    ]

    deep_columns_without_embedding = [
        application_date,
        tf.feature_column.indicator_column(ipc_class0),
        tf.feature_column.indicator_column(ipc_class1),
        tf.feature_column.indicator_column(ipc_class2),
        tf.feature_column.indicator_column(city0),
        tf.feature_column.indicator_column(city1),
        tf.feature_column.indicator_column(city2),
        tf.feature_column.indicator_column(country),
        tf.feature_column.indicator_column(status),
        tf.feature_column.indicator_column(applicant_type),
        tf.feature_column.indicator_column(applicant)
    ]

    return [], deep_columns


def input_fn(data_file, num_epochs, shuffle, batch_size):
    """Generate an input function for the Estimator."""
    assert tf.gfile.Exists(data_file), ('%s not found.' % data_file)

    def parse_csv(value):
        tf.logging.info('Parsing {}'.format(data_file))
        columns = tf.decode_csv(value, record_defaults=_CSV_COLUMN_DEFAULTS)
        features = dict(zip(_CSV_COLUMNS, columns))
        labels = features.pop('label')
        return features, labels

    # Extract lines from input files using the Dataset API.
    dataset = tf.data.TextLineDataset(data_file)

    if shuffle:
        dataset = dataset.shuffle(buffer_size=NUM_EXAMPLES['train'])

    dataset = dataset.map(parse_csv, num_parallel_calls=3)

    # We call repeat after shuffling, rather than before, to prevent separate
    # epochs from blending together.
    dataset = dataset.repeat(num_epochs)
    dataset = dataset.batch(batch_size)
    return dataset

class Constants:

    class Training:

        class Features:

            grouping = 'applicant_grouping'
            is_too = 'applicant_is_tto_list'
            is_research_organisation = 'applicant_is_research_organisation_list'
            is_service_provider = 'applicant_is_service_provider_list'
            is_technology_company = 'applicant_is_technology_company_list'
            cities = 'applicant_cities_list'
            countries = 'applicant_countries_list'
            lat_lon_pairs = 'applicant_lat_lon_pairs_list'

            date = 'patent_ep_application_date_list'
            status_category = 'patent_status_category_list'
            second_level_classes = 'patent_second_level_classes_list'
            first_level_classes = 'patent_first_level_classes_list'

        class Classes:

            targets = 'encoded_targets'
            groupings = 'agent_grouping_list'
            old_vs_new = 'agent_old_vs_new'
            countries = 'agent_countries_list'

    class Test:

        class Features:

            gap_size = 'missing_applicants_from_sequence_count'
            previous_agents = 'previous_agents'
            countries = 'applicant_countries'
            cities = 'applicant_cities'
            first_level_classes = 'patent_first_level_classes'
            second_level_classes = 'patent_second_level_classes'
            date = 'patent_ep_application_date'
            status_category = 'patent_status_category'
            grouping = 'applicant_grouping'
            is_research_organisation = 'applicant_is_research_organisation'
            is_tto = 'applicant_is_tto'
            is_service_provider = 'applicant_is_service_provider'
            is_technology_company = 'applicant_is_technology_company'

        class Classes:

            target = 'encoded_target'
            grouping = 'agent_grouping'
            old_vs_new = 'agent_old_vs_new'

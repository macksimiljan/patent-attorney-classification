from nose.tools import assert_equal
from numpy.testing import assert_array_equal

from pac.utils.sampling.split_sequences_job import SplitSequencesJob
from pac.utils.io import PATH_TO_TEST_FILE_TRAINING
from pac.utils.io.feature_classes_point import read_dataset


def test_split_works_correctly():
    dataset = read_dataset(PATH_TO_TEST_FILE_TRAINING)
    assert_equal((3, 16), dataset.shape)

    job = SplitSequencesJob()
    split_dataset = job.split_dataset_from(dataset)
    assert_equal((6, 16), split_dataset.shape)
    assert_array_equal(['a10947', 'g1023', 'g1023', 'g1023', 'g1023', 'a1682'], split_dataset['applicant_grouping'])

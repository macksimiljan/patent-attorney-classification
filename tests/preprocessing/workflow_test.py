from nose.tools import assert_equal
from nose.tools import assert_in
from numpy.testing import assert_array_equal

from pac import Constants
from pac.preprocessing import preprocessed_features
from pac.preprocessing.mapping.countries_feature_mapping import CountriesFeatureMapping
from pac.preprocessing.mapping.ipc_classes_feature_mapping import IpcClassFeatureMapping
from pac.preprocessing.encoding.categorical_feature_encoder import NominalFeatureEncoder
from pac.preprocessing.encoding.classes_encoder import ClassesEncoder
from pac.preprocessing.mapping.date_mapping import DateMapping
from pac.utils.io import PATH_TO_TEST_FILE_SPLIT_TRAINING
from pac.utils.io import PATH_TO_TEST_FILE_TEST
from pac.utils.io.feature_classes_point import read_dataset


def test_preprocessing_workflow_is_correct():
    training = read_dataset(PATH_TO_TEST_FILE_SPLIT_TRAINING)
    test = read_dataset(PATH_TO_TEST_FILE_TEST)

    preprocessed_features.reset()
    preprocessor = ClassesEncoder()
    training, test = preprocessor.encode(training, test)

    preprocessor = DateMapping()
    training, test = preprocessor.map(training, test)

    preprocessor = NominalFeatureEncoder(Constants.Training.Features.status_category,
                                         Constants.Test.Features.status_category,
                                         prefix='patent_status')
    training, test = preprocessor.encode(training, test)

    preprocessor = NominalFeatureEncoder(Constants.Training.Features.grouping,
                                         Constants.Test.Features.grouping,
                                         prefix='applicant_grouping')
    training, test = preprocessor.encode(training, test)

    preprocessor = CountriesFeatureMapping()
    training, test = preprocessor.map(training, test)

    preprocessor = IpcClassFeatureMapping()
    training, test = preprocessor.map(training, test)

    assert_in('patent_status_failed', preprocessed_features.values())
    assert_in('applicant_grouping_a10947', preprocessed_features.values())
    assert_in('application_date', preprocessed_features.values())
    assert_in('country_Afghanistan', preprocessed_features.values())
    assert_in('ipc_a', preprocessed_features.values())
    assert_in('ipc_h99', preprocessed_features.values())

    assert_array_equal([3, 2, 1, 1, 1, 1, 1, 2, 2], training[Constants.Training.Classes.targets])
    assert_equal(6, training['patent_status_failed'].sum())
    assert_equal(1, training['applicant_grouping_a1682'].sum())
    assert_equal(1367971200, training['application_date'][0])
    assert_equal(1, training['country_Hong Kong'].sum())
    assert_equal(7, training['country_Ireland'].sum())
    assert_equal(1, training['ipc_a'].sum())
    assert_equal(4, training['ipc_b'].sum())
    assert_equal(4, training['ipc_h'].sum())
    assert_equal(3, training['ipc_b03'].sum())
    assert_equal(2, training['ipc_h01'].sum())

    assert_array_equal([3, 1, 1, 1, 1, 1, 2, 0, 0], test[Constants.Test.Classes.target])
    assert_equal(6, test['patent_status_failed'].sum())
    assert_equal(8, test['applicant_grouping_a2011'].sum())
    assert_equal(1367971200, test['application_date'][0])
    assert_equal(0, test['country_Hong Kong'].sum())
    assert_equal(8, test['country_Ireland'].sum())
    assert_equal(0, test['ipc_a'].sum())
    assert_equal(4, test['ipc_b'].sum())
    assert_equal(5, test['ipc_h'].sum())
    assert_equal(3, test['ipc_b03'].sum())
    assert_equal(2, test['ipc_h01'].sum())

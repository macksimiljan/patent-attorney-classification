from nose.tools import assert_equal
from nose.tools import assert_greater
from nose.tools import assert_is
from numpy import int64
from numpy.testing import assert_array_equal
from pandas import to_datetime

from pac import Constants
from pac.preprocessing.mapping.date_mapping import DateMapping
from pac.utils.io import PATH_TO_TEST_FILE_SPLIT_TRAINING
from pac.utils.io import PATH_TO_TEST_FILE_TEST
from pac.utils.io.feature_classes_point import read_dataset


def test_dates_are_encoded_correctly():
    training = read_dataset(PATH_TO_TEST_FILE_SPLIT_TRAINING)
    test = read_dataset(PATH_TO_TEST_FILE_TEST)

    encoder = DateMapping()
    training, test = encoder.map(training, test)

    date_column = training['application_date']
    expected_values = [1367971200,
                       1362528000,
                       1357084800,
                       1362528000,
                       1365552000,
                       1366761600,
                       1367971200,
                       1368576000,
                       1369785600]
    assert_array_equal(expected_values, date_column)

    date_column = test['application_date']
    expected_values = [1367971200,
                       1357084800,
                       1362528000,
                       1365552000,
                       1366761600,
                       1367971200,
                       1368576000,
                       1369785600,
                       1369785600]
    assert_array_equal(expected_values, date_column)


def test_date_encoder_keeps_arithmetic():
    dates = ['2018-01-01', '2018-02-01']
    encoder = DateMapping()
    encoded_dates = encoder.map_dates(dates)

    assert_equal('2018-01-16', (to_datetime((encoded_dates[0] + encoded_dates[1])/2, unit='s')).strftime('%Y-%m-%d'))
    assert_greater(encoded_dates[1], encoded_dates[0])


def test_date_encoder_returns_integer():
    dates = ['2018-01-01']
    encoder = DateMapping()
    encoded_dates = encoder.map_dates(dates)
    assert_is(type(encoded_dates[0]), int64)

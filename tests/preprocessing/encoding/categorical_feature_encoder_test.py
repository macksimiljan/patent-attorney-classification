from pac import Constants
from pac.preprocessing.encoding.categorical_feature_encoder import NominalFeatureEncoder
from pac.preprocessing.encoding.categorical_feature_encoder import OrdinalFeatureEncoding
from pac.utils.io import PATH_TO_TEST_FILE_SPLIT_TRAINING
from pac.utils.io import PATH_TO_TEST_FILE_TEST
from pac.utils.io.feature_classes_point import read_dataset

from pac.preprocessing import preprocessed_features
from numpy.testing import assert_array_equal
from nose.tools import assert_equal


def test_nominal_features_are_encoded_correctly():
    training = read_dataset(PATH_TO_TEST_FILE_SPLIT_TRAINING)
    test = read_dataset(PATH_TO_TEST_FILE_TEST)
    training_shape = training.shape
    test_shape = test.shape

    preprocessed_features.reset()
    encoder = NominalFeatureEncoder(Constants.Training.Features.status_category,
                                    Constants.Test.Features.status_category,
                                    prefix='patent_status_category')
    encoded_training, encoded_test = encoder.encode(training, test)

    assert_array_equal(['patent_status_category_failed', 'patent_status_category_in_progress', 'patent_status_category_successful'],
                       preprocessed_features.values())
    expected__shape = (training_shape[0], training_shape[1]+2)
    assert_equal(expected__shape, encoded_training.shape)
    expected__shape = (test_shape[0], test_shape[1] + 2)
    assert_equal(expected__shape, encoded_test.shape)

    assert_equal(6, encoded_training['patent_status_category_failed'].sum())
    assert_equal(1, encoded_training['patent_status_category_in_progress'].sum())
    assert_equal(2, encoded_training['patent_status_category_successful'].sum())
    assert_equal(6, encoded_test['patent_status_category_failed'].sum())
    assert_equal(1, encoded_test['patent_status_category_in_progress'].sum())
    assert_equal(2, encoded_test['patent_status_category_successful'].sum())


def test_ordinal_features_are_encoded_correctly():
    training = read_dataset(PATH_TO_TEST_FILE_SPLIT_TRAINING)
    test = read_dataset(PATH_TO_TEST_FILE_TEST)

    preprocessed_features.reset()
    encoder = OrdinalFeatureEncoding(Constants.Training.Features.grouping,
                                     Constants.Test.Features.grouping)
    encoded_training, encoded_test = encoder.encode(training, test)
    assert_array_equal([0,1,2,2,2,2,2,2,2], encoded_training[Constants.Training.Features.grouping])
    assert_array_equal([0,2,2,2,2,2,2,2,2], encoded_test[Constants.Test.Features.grouping])

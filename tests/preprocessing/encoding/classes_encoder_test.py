from pac import Constants
from pac.preprocessing.encoding.classes_encoder import ClassesEncoder
from pac.utils.io import PATH_TO_TEST_FILE_SPLIT_TRAINING
from pac.utils.io import PATH_TO_TEST_FILE_TEST
from pac.utils.io.feature_classes_point import read_dataset


def test_classes_are_encoded_correctly():
    training = read_dataset(PATH_TO_TEST_FILE_SPLIT_TRAINING)
    test = read_dataset(PATH_TO_TEST_FILE_TEST)

    encoder = ClassesEncoder(Constants.Training.Classes.groupings, Constants.Test.Classes.grouping)
    encoded_training, encoded_test = encoder.encode(training, test)

    assert [3, 2, 1, 1, 1, 1, 1, 2, 2] == encoded_training[Constants.Training.Classes.targets].tolist()
    assert [3, 1, 1, 1, 1, 1, 2, 0, 0] == encoded_test[Constants.Test.Classes.target].tolist()

    encoded_classes = encoder.internal_encoder().classes_.tolist()
    assert ['g1127', 'g2355', 'g6791', 'g6875'] == encoded_classes

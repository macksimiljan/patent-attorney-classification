from nose.tools import assert_true, assert_equal
from scipy.sparse import isspmatrix

from pac.preprocessing.encoding.categorical_feature_encoder import CityEncoder


def test_city_encoding_works_correctly():
    applicant_cities = [None, ['Aachen'], ['Aachen', 'Aalborg']]
    encoded_cities = CityEncoder().encode(applicant_cities)

    assert_true(isspmatrix(encoded_cities))
    assert_equal((3, 4232), encoded_cities.shape)
    assert_equal(encoded_cities[:, 1].sum(), 2)

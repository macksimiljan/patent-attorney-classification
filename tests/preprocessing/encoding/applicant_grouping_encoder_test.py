import pandas as pd
from nose.tools import assert_equal, assert_true
from scipy.sparse import isspmatrix, hstack, csr_matrix

from pac.preprocessing.encoding.categorical_feature_encoder import ApplicantGroupingEncoder

import numpy as np


def test_encoding_works_correctly():
    X = pd.DataFrame(data={'applicant_grouping': ['a10947', 'a1682', 'a2011', 'a2011', np.nan],
                           'some_other_feature': [1, 2, 3, 4, 5]})
    encoded_applicants = ApplicantGroupingEncoder().encode(X['applicant_grouping'].values)
    X = X.drop('applicant_grouping', axis=1)
    X = hstack([csr_matrix(X), encoded_applicants], format='csr')

    assert_true(isspmatrix(X))
    assert_equal((5, 5828), X.shape)
    assert_equal(1, X[:, 2].sum())  # 1st col: 'some_other_feature', 2nd col: 'a100', 3rd col: 'a10947'

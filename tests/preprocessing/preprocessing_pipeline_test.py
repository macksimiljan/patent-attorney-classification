from scipy.sparse import load_npz
from nose.tools import assert_equal


def test_preprocessing_yields_the_right_sparse_matrices():
    X_train = load_npz('../../data/2012_2016/X_train_sparse.npz')
    X_test = load_npz('../../data/2017/X_test_sparse.npz')
    with open('../../data/feature_columns.txt') as f:
        columns = f.readlines()
    columns = [x.strip() for x in columns]

    assert_equal(10335, len(columns))

    assert_equal(1, X_train[0, columns.index('a100')])
    assert_equal(1, X_train[0, columns.index('applicant_is_service_provider_list')])
    assert_equal(1, X_train[0, columns.index('ipc_g06')])
    assert_equal(1, X_train[0, columns.index('country_Germany')])
    assert_equal(1, X_train[0, columns.index('Berlin')])
    assert_equal(1, X_train[0, columns.index('Bonn')])
    assert_equal(1, X_train[0, columns.index('Stuttgart')])
    assert_equal(1, X_train[0, columns.index('patent_status_in_progress')])

    assert_equal(1, X_test[0, columns.index('a10947')])
    assert_equal(1, X_test[0, columns.index('applicant_is_technology_company_list')])
    assert_equal(1513728000, X_test[0, columns.index('application_date')])
    assert_equal(1, X_test[0, columns.index('ipc_c01')])

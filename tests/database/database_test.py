from pac.utils.database.connection_point import open_connection
from pac.utils.database.connection_point import instantiate_engine


def test_connection():
    try:
        connection = open_connection()
        assert True
    except:
        assert False


def test_engine():
    try:
        engine = instantiate_engine()
        assert True
    except:
        assert False

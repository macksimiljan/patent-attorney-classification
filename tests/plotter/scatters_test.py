from numpy.testing import assert_array_equal

from pac.utils.plotter.scatters import group_and_add_frequency_of_points


def test_points_are_grouped_and_counted_correctly():
    x_values = [0.5, 0.36, 0.42, 0.42, 0.5, 0.1, 0.42]
    y_values = [10, 2, 0.7, 0.7, 10, 0.7, 0.1]

    x_values, y_values, frequencies = group_and_add_frequency_of_points(x_values, y_values)
    assert_array_equal([0.5, 0.36, 0.42, 0.42, 0.1], x_values)
    assert_array_equal([10, 2, 0.7, 0.1, 0.7], y_values)
    assert_array_equal([2, 1, 2, 1, 1], frequencies)

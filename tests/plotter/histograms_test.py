from numpy.testing import assert_array_equal

from pac.utils.plotter.histograms import combine_nominal_values
from pac.utils.plotter.histograms import map_to_histogram_data
from pac.utils.plotter.histograms import map_to_multiple_histogram_data


def test_map_to_histogram_data():
    result_set = [{'x': 0, 'y': 2}, {'x': 2, 'y': 7}, {'x': 3, 'y': 12}]

    x_values, y_values = map_to_histogram_data(result_set, 'x', 'y')
    assert_array_equal(x_values, [0, 1, 2, 3])
    assert_array_equal(y_values, [2, 0, 7, 12])


def test_map_to_multiple_histogram_data():
    result_set0 = [{'x': 0, 'y': 1}, {'x': 2, 'y': 3}, {'x': 3, 'y': 4}]
    result_set1 = [{'x': 0, 'y': 11}, {'x': 1, 'y': 12}, {'x': 3, 'y': 14}, {'x': 5, 'y': 16}]
    result_sets = [result_set0, result_set1]

    x_values, y_values_array = map_to_multiple_histogram_data(result_sets, 'x', 'y')
    assert_array_equal(x_values, [0, 1, 2, 3, 4, 5])
    assert_array_equal(y_values_array[0], [1, 0, 3, 4, 0, 0])
    assert_array_equal(y_values_array[1], [11, 12, 0, 14, 0, 16])


def test_map_to_nominal_based_multiple_histogram_data():
    result_set0 = [{'x': 'A', 'y': 1}, {'x': 'C', 'y': 3}, {'x': 'D', 'y': 4}]
    result_set1 = [{'x': 'A', 'y': 11}, {'x': 'B', 'y': 12}, {'x': 'D', 'y': 14}, {'x': 'E', 'y': 16}]
    result_sets = [result_set0, result_set1]

    x_values, y_values_array = map_to_multiple_histogram_data(result_sets, 'x', 'y')
    assert_array_equal(['A', 'B', 'C', 'D', 'E'], x_values)
    assert_array_equal([1, 0, 3, 4, 0], y_values_array[0])
    assert_array_equal([11, 12, 0, 14, 16], y_values_array[1])


def test_combine_nominal_values():
    values0 = ['A0', 'B0', 'D0']
    values1 = ['A0', 'C0']

    result = combine_nominal_values(values0, values1)
    assert_array_equal(['A0', 'B0', 'C0', 'D0'], result)
    result = combine_nominal_values(values1, values0)
    assert_array_equal(['A0', 'B0', 'C0', 'D0'], result)

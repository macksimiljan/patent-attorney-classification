import pandas as pd
from numpy.testing import assert_array_equal

from nose.tools import assert_equal

from pac.preprocessing.app_map_applicant_categories import map_applicant_data


def test_mapping_works():
    applicant_data = pd.DataFrame(data={'applicant_grouping': ['g42', 'g21', 'g17', 'g30'],
                                        'count_applications': [42, 42, 800, 800],
                                        'count_unique_agents': [10, 1, 1, 10]
                                        })

    applicant_data = map_applicant_data(applicant_data)

    assert_equal((4, 2), applicant_data.shape)
    assert_array_equal(['g42', 'g21', 'g17', 'g30'], applicant_data['applicant_grouping'].values)
    assert_array_equal([1, 0, 2, 3], applicant_data['categories'].values)

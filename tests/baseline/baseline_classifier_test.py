import pandas as pd
from numpy.testing import assert_array_equal

from pac.baseline.sequential_baseline_classifier import SequentialBaselineClassifier


def setup_training_dataset():
    return pd.DataFrame(data={
        'applicant_grouping': ['a1',
                               'a2',
                               'g10',
                               'g21',
                               'g42'],
        'agent_grouping_list': [['1', '1', '1', '2'],
                                ['3', '2'],  # elements with equal counts are ordered arbitrarily
                                ['1'],
                                ['1', '2', '2', '2'],
                                ['1', '1', '4', '1']],
        'agent_countries_list': [[['UK'], ['UK'], ['UK'], ['UK', 'GER']],
                                 [['FR'], ['UK', 'GER']],
                                 [['UK']],
                                 [['UK'], ['UK', 'GER'], ['UK', 'GER'], ['UK', 'GER']],
                                 [['UK'], ['UK'], None, ['UK']]]
    })


def setup_test_dataset():
    return pd.DataFrame(data={
        'applicant_grouping': ['a1',
                               'a1',
                               'a2',
                               'g10',
                               'g21',
                               'g42',
                               'g333'],
        'missing_applicants_from_sequence_count': [0, 1, 0, 0, 0, 0, 0],
        'agent_grouping': ['1', '2', '3', '1', '2', '1', '3'],
        'agent_countries': [['UK'], ['UK', 'GER'], ['FR'], ['UK'], ['UK', 'GER'], ['UK'], ['FR']]
    })


def test_sequential_baseline_classifier_works_for_most_common():
    classifier = SequentialBaselineClassifier(strategy='most_common')
    training = setup_training_dataset()
    test = setup_test_dataset()

    classifier.fit(training)
    predicted_agents = classifier.predict(test['applicant_grouping'])
    assert_array_equal(['1', '1'], predicted_agents[0:2])
    assert('2' == predicted_agents[2] or '3' == predicted_agents[2])
    assert_array_equal(['1', '2', '1', None], predicted_agents[3:])


def test_sequential_baseline_classifier_works_for_last():
    classifier = SequentialBaselineClassifier(strategy='last')
    training = setup_training_dataset()
    test = setup_test_dataset()

    classifier.fit(training)
    predicted_agents = classifier.predict(test['applicant_grouping'])
    assert_array_equal(['2', '2', '2', '1', '2', '1', None], predicted_agents)


def test_sequential_baseline_classifier_works_for_most_common_on_countries():
    classifier = SequentialBaselineClassifier(strategy='last', class_column='agent_countries_list')
    training = setup_training_dataset()
    test = setup_test_dataset()

    classifier.fit(training)
    predicted_classes = classifier.predict(test['applicant_grouping'])
    assert_array_equal(["['UK', 'GER']", "['UK', 'GER']", "['UK', 'GER']", "['UK']", "['UK', 'GER']", "['UK']", None],
                       predicted_classes)
